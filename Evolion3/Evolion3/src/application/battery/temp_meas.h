#ifndef TEMP_MEAS_H_
#define TEMP_MEAS_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: temp_meas.h                                                    */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for temp_meas.c, which provides prototypes for the            */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
#include <temp_sensor_ic.h>
#include <iface_rtos_start.h>

#define NB_TEMP_SAMPLES_TO_AVG_MAX   10
#define TEMP_SENSE_ERROR  29999

/*********************************************************************
* PUBLIC vReadTemperatures()
*---------------------------------------------------------------------
* Measure sensor temperatures
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vReadTemperatures(stBmsData_t* stMyBMSData);

#endif /* TEMP_MEAS_H_ */