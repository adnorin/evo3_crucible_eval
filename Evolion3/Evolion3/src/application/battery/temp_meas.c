/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: temp_meas.c                                                    */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <temp_meas.h>
#include <conf_battery_settings.h>

/* Sensor I2C addresses */
/* NOTE: During debugging ONLY using eval board use sensor 0 as 1st sensor, otherwise replace with sensor 1 */
#ifndef I2C_TEST_ADR_SETUP
const uint8_t uchTempSensorAddress[NUM_TEMP_SENSORS] = {CELL_TEMP_SENSOR_0_ADDR,
                                                        CELL_TEMP_SENSOR_1_ADDR,
                                                        CELL_TEMP_SENSOR_2_ADDR,
                                                        CELL_TEMP_SENSOR_3_ADDR,
                                                        ELEC_TEMP_SENSOR_0_ADDR,
                                                        ELEC_TEMP_SENSOR_1_ADDR,
                                                        ELEC_TEMP_SENSOR_2_ADDR,
                                                        ELEC_TEMP_SENSOR_3_ADDR};
#else
#if (I2C_TEST_ADR_SETUP == 1)
const uint8_t uchTempSensorAddress[NUM_TEMP_SENSORS] = {ELEC_TEMP_SENSOR_0_ADDR,
                                                        ELEC_TEMP_SENSOR_1_ADDR,
                                                        ELEC_TEMP_SENSOR_2_ADDR,
                                                        ELEC_TEMP_SENSOR_3_ADDR,
                                                        ELEC_TEMP_SENSOR_0_ADDR,
                                                        ELEC_TEMP_SENSOR_1_ADDR,
                                                        ELEC_TEMP_SENSOR_2_ADDR,
                                                        ELEC_TEMP_SENSOR_3_ADDR};
#else
#if (I2C_TEST_ADR_SETUP == 2)
const uint8_t uchTempSensorAddress[NUM_TEMP_SENSORS] = {CELL_B_SAMPLE_TEMP_SENSOR_0_ADDR,
                                                        CELL_B_SAMPLE_TEMP_SENSOR_1_ADDR,
                                                        CELL_B_SAMPLE_TEMP_SENSOR_2_ADDR,
                                                        CELL_B_SAMPLE_TEMP_SENSOR_3_ADDR,
                                                        CELL_B_SAMPLE_TEMP_SENSOR_4_ADDR,
                                                        CELL_B_SAMPLE_TEMP_SENSOR_5_ADDR,
                                                        ELEC_B_SAMPLE_TEMP_SENSOR_0_ADDR,
                                                        ELEC_B_SAMPLE_TEMP_SENSOR_1_ADDR,
                                                        ELEC_B_SAMPLE_TEMP_SENSOR_2_ADDR,
                                                        ELEC_B_SAMPLE_TEMP_SENSOR_3_ADDR};
#else
const uint8_t uchTempSensorAddress[NUM_TEMP_SENSORS] = {CELL_TEMP_SENSOR_2_ADDR,
                                                        CELL_TEMP_SENSOR_3_ADDR,
                                                        CELL_TEMP_SENSOR_2_ADDR,
                                                        CELL_TEMP_SENSOR_3_ADDR,
                                                        CELL_TEMP_SENSOR_2_ADDR,
                                                        CELL_TEMP_SENSOR_3_ADDR,
                                                        CELL_TEMP_SENSOR_2_ADDR,
                                                        CELL_TEMP_SENSOR_3_ADDR};
#endif
#endif
#endif

/*****************************/
/* Private Functions         */
/*****************************/
/*********************************************************************
* PRIVATE svFindMinMaxCellTemperatures()
*---------------------------------------------------------------------
* Finds cells with minimum and maximum average temperatures, for cell
* temperatures.
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
static void svFindMinMaxCellTemperatures(stBmsData_t* stMyBMSData)
{
    uint8_t ucMinIdx = 0;
    uint8_t ucMaxIdx = 0;
    int16_t shMin = 0;
    int16_t shMax = 0;
    uint8_t ucTempSensorIdx;

    for(ucTempSensorIdx = CELL_TEMP_SENSOR_START_INDEX; ucTempSensorIdx < ELEC_TEMP_SENSOR_START_INDEX + NUM_CELL_TEMP_SENSORS; ucTempSensorIdx++)
    {
        /* Find Min Cell Temperature */
        if(stMyBMSData->shTemperatureSensor[ucTempSensorIdx] <= stMyBMSData->shTemperatureSensor[ucMinIdx])
        {
            ucMinIdx = ucTempSensorIdx;
            shMin = stMyBMSData->shTemperatureSensor[ucTempSensorIdx];
        }

        /* Find Max Cell Temperature */
        if(stMyBMSData->shTemperatureSensor[ucTempSensorIdx] >= stMyBMSData->shTemperatureSensor[ucMaxIdx])
        {
            ucMaxIdx = ucTempSensorIdx;
            shMax = stMyBMSData->shTemperatureSensor[ucTempSensorIdx];
        }
    }
    /* Return Min and Max cell temperature values */
    stMyBMSData->ucCellTemperatureMinIdx = ucMinIdx;
    stMyBMSData->ucCellTemperatureMaxIdx = ucMaxIdx;
    stMyBMSData->shCellTemperatureMin = shMin;
    stMyBMSData->shCellTemperatureMax = shMax;
}


/*********************************************************************
* PRIVATE svFindMinMaxElectronicsTemperatures()
*---------------------------------------------------------------------
* Finds cells with minimum and maximum average temperatures, for 
* electronics temperatures.
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
static void svFindMinMaxElectronicsTemperatures(stBmsData_t* stMyBMSData)
{
    uint8_t ucMinIdx = 0;
    uint8_t ucMaxIdx = 0;
    int16_t shMin = 0;
    int16_t shMax = 0;
    uint8_t ucTempSensorIdx;

    for(ucTempSensorIdx = ELEC_TEMP_SENSOR_START_INDEX; ucTempSensorIdx < ELEC_TEMP_SENSOR_START_INDEX + NUM_ELEC_TEMP_SENSORS; ucTempSensorIdx++)
    {
        /* Find Min Electronics Temperature */
        if(stMyBMSData->shTemperatureSensor[ucTempSensorIdx] <= stMyBMSData->shTemperatureSensor[ucMinIdx])
        {
            ucMinIdx = ucTempSensorIdx;
            shMin = stMyBMSData->shTemperatureSensor[ucTempSensorIdx];
        }

        /* Find Max Electronics Temperature */
        if(stMyBMSData->shTemperatureSensor[ucTempSensorIdx] >= stMyBMSData->shTemperatureSensor[ucMaxIdx])
        {
            ucMaxIdx = ucTempSensorIdx;
            shMax = stMyBMSData->shTemperatureSensor[ucTempSensorIdx];
        }
    }
    /* Return Min and Max Electronics temperature values */
    stMyBMSData->ucElectronicsTemperatureMinIdx = ucMinIdx;
    stMyBMSData->ucElectronicsTemperatureMaxIdx = ucMaxIdx;
    stMyBMSData->shElectronicsTemperatureMin = shMin;
    stMyBMSData->shElectronicsTemperatureMax = shMax;
}


/*****************************/
/* Public Functions          */
/*****************************/
/*********************************************************************
* PUBLIC vReadTemperatures()
*---------------------------------------------------------------------
* Measure sensor temperatures
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vReadTemperatures(stBmsData_t* stMyBMSData)
{
    uint8_t ucTempSensorIdx;
    uint8_t sucTempSensorAveragingIdx;
    int16_t sshAvgTempValue;
    int16_t shRawTempValue;
    static uint8_t sucTempSensorSampleIdx = 0;
    static int16_t sshRawTempValue[NUM_TEMP_SENSORS][NB_TEMP_SAMPLES_TO_AVG_MAX];
    static bool sbMinNumberOfTempSamplesAcquired = false;


    /* Get a temperature reading from each sensor */
    for(ucTempSensorIdx=0; ucTempSensorIdx<NUM_TEMP_SENSORS; ucTempSensorIdx++)
    {
        /* Get raw data value */
        shRawTempValue = (int16_t)ushReadRawTempValue(uchTempSensorAddress[ucTempSensorIdx]);
        if(shRawTempValue != TEMP_SENSE_ERROR)
        {
            sshRawTempValue[ucTempSensorIdx][sucTempSensorSampleIdx] = shRawTempValue;
        
            /* Scale Value */
            sshRawTempValue[ucTempSensorIdx][sucTempSensorSampleIdx] = (sshRawTempValue[ucTempSensorIdx][sucTempSensorSampleIdx] >> 7)*5;

            /* Until we get at least const_ucCfgNbTemperatureSamplesToAvg of samples, it is not safe to average them, so skip this until we do */
            if (sbMinNumberOfTempSamplesAcquired == true)
            {
                /* Clear intermediate average temp values as we're about to add all of the temperature samples to it and then get the average */
                sshAvgTempValue = 0;

                /* Add each temperature sample for the given sensor to the intermediate temperature value */
                for (sucTempSensorAveragingIdx=0; sucTempSensorAveragingIdx<const_ucCfgNbTemperatureSamplesToAvg; sucTempSensorAveragingIdx++)
                {
                    sshAvgTempValue+= sshRawTempValue[ucTempSensorIdx][sucTempSensorAveragingIdx];
                }
                /* Divide by the total number of samples to get the average */
                sshAvgTempValue = sshAvgTempValue / const_ucCfgNbTemperatureSamplesToAvg;

                /* Save temperature value to temperature buffer */
                stMyBMSData->shTemperatureSensor[ucTempSensorIdx] = sshAvgTempValue; /* Increment every call to cycle through all 8 sensors one at a time */
            }
            /* Until we have enough samples to safely average them, just update the temperature sensor values with the most recent sample */
            else
            {
                stMyBMSData->shTemperatureSensor[ucTempSensorIdx] = sshRawTempValue[ucTempSensorIdx][sucTempSensorSampleIdx];
            }
        }        
    }

    /* Increment sample array index */
    if (++sucTempSensorSampleIdx >= const_ucCfgNbTemperatureSamplesToAvg)
    {
        /* Reset sample index once we have acquired the configured amount */
        sucTempSensorSampleIdx = 0;

        /* Set flag once we have enough samples to safely start averaging */
        sbMinNumberOfTempSamplesAcquired = true;
    }
    
    
    
    /* Calculate Min/Max cell and electronics temperatures */
    svFindMinMaxCellTemperatures(stMyBMSData);
    svFindMinMaxElectronicsTemperatures(stMyBMSData);
}

/******************************** End of File ********************************/
