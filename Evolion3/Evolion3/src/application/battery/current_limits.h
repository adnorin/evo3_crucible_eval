#ifndef CURRENT_LIMITS_H_
#define CURRENT_LIMITS_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: current_limits.c                                               */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for current_limits.c, which provides prototypes for the       */
/* public interface functions. Also includes types and enumeration           */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define NB_IMX_SAMPLES_TO_AVG_MAX           10
#define TEMP_TO_INDEX_FACTOR                5

#define MAX_CURRENT_COEFF                   1000
#define MIN_CURRENT_COEFF                   0

#define I2T_ARRAY_LENGTH                    10
#define TCELL_TO_CURRENT_LIMIT_ARRAY_LENGTH 211

typedef enum
{
    IMX_MAX = 0,
    IMX_MIN,
    IMX_DECREASING,
}eIMxVcellState_t;

/*********************************************************************
* PUBLIC vCalculateIMDIMR()
*---------------------------------------------------------------------
* IMD/IMR calculation routine
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vCalculateIMDIMR(stBmsData_t* stMyBMSData);

/*********************************************************************
* PUBLIC vDetermineIfI2tExceeded()
*---------------------------------------------------------------------
* IMD/IMR calculation routine
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vDetermineIfI2tExceeded(stBms_FastData_t* stMyBMSData);

#endif /* CURRENT_LIMITS_H_ */
