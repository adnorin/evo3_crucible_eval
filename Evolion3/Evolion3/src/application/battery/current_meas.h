#ifndef CURRENT_MEAS_H_
#define CURRENT_MEAS_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: current_meas.h                                                 */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for current_meas.c, which provides prototypes for the         */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define NB_CURRENT_SAMPLES_TO_AVG_MAX 100 /* 10s at 100ms per sample */

/*********************************************************************
* PUBLIC uiMeasureCurrent()
*---------------------------------------------------------------------
* Measure battery current
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void uiMeasureCurrent(void);

/*********************************************************************
* PUBLIC vAverageCurrent()
*---------------------------------------------------------------------
* Routine to average current over a configurable amount of samples
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vAverageCurrent(stBmsData_t* stMyBMSData);

#endif /* CURRENT_MEAS_H_ */