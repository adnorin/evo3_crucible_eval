/****************************************************************************/
/* (C) Copyright 2017 by SAFT                                               */
/* All rights reserved                                                      */
/*                                                                          */
/* This program is the property of Saft America, Inc. and can only be       */
/* used and copied with the prior written authorization of SAFT.            */
/*                                                                          */
/* Any whole or partial copy of this program in either its original form or */
/* in a modified form must mention this copyright and its owner.            */
/****************************************************************************/
/* PROJECT: Evolion 3 Software                                              */
/****************************************************************************/
/* FILE NAME: heater_control.c                                              */
/****************************************************************************/
/* AUTHOR : Greg Cordell                                                    */
/* LAST MODIFIER: Gerald Percherancier                                      */
/****************************************************************************/
/* FILE DESCRIPTION:                                                        */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/
#include <asf.h>
#include <conf_gpio.h>
#include <iface_rtos_start.h>
#include <heater_control.h>

static const int16_t const_shCfgHeaterControlCellTMax = 750;
static const int16_t const_shCfgHeaterControlOnThreshold = 100;
static const int16_t const_shCfgHeaterControlOffThreshold = 150;
static const TickType_t const_uiCfgHeaterDelay = 10000/portTICK_RATE_MS;


/*********************************************************************
* PUBLIC vControlHeater()
*---------------------------------------------------------------------
* Heater control routine
*
* Inputs:
* stMyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vControlHeater(stBmsData_t* stMyBMSData)
{
    static int16_t sshMinCellTemperature = 0;
    static int16_t sshMaxCellTemperature = 0;
    static TickType_t suiHeaterTickCountStart = 0;
    static TickType_t suiHeaterTickCount = 0;
    static bool sbHeaterCounterElapsed = true;

    /* Get min cell temperature */
    sshMinCellTemperature = stMyBMSData->shCellTemperatureMin;

    /* Get max cell temperature */
    sshMaxCellTemperature = stMyBMSData->shCellTemperatureMax;

    /* Check to see that the minimum time between heater transitions has elapsed and that the hottest cell is not too hot */
    if ( sbHeaterCounterElapsed && sshMaxCellTemperature <= const_shCfgHeaterControlCellTMax)
    {
        if ( stMyBMSData->bHeaterEnable == false )
        {
            if ( sshMinCellTemperature < const_shCfgHeaterControlOnThreshold )
            {
                /* If the heater is currently disabled and the coldest cell is below the Heater On threshold, turn the heater on */
                port_pin_set_output_level(HEATER_ENABLE_PIN,HEATER_ENABLE_ACTIVE);
                stMyBMSData->bHeaterEnable = true;
                sbHeaterCounterElapsed = false;
                suiHeaterTickCountStart = xTaskGetTickCount();
            }
        }
        else
        {
            if ( sshMinCellTemperature > const_shCfgHeaterControlOffThreshold )
            {
                /* If the heater is currently enabled and the coldest cell is above the Heater Off threshold, turn the heater off */
                port_pin_set_output_level(HEATER_ENABLE_PIN,HEATER_ENABLE_INACTIVE);
                stMyBMSData->bHeaterEnable = false;
                sbHeaterCounterElapsed = false;
                suiHeaterTickCountStart = xTaskGetTickCount();
            }
        }
    }
    else if (sshMaxCellTemperature > const_shCfgHeaterControlCellTMax)
    {
        /* If the hottest cell goes above the max cell temperature threshold, ensure that the heater is off */
        if ( stMyBMSData->bHeaterEnable == true )
        {
            port_pin_set_output_level(HEATER_ENABLE_PIN,HEATER_ENABLE_INACTIVE);
            stMyBMSData->bHeaterEnable = false;
        }
    }

    if ( sbHeaterCounterElapsed == false )
    {
        suiHeaterTickCount = xTaskGetTickCount();
        if ( suiHeaterTickCount >= ( suiHeaterTickCountStart + const_uiCfgHeaterDelay ) )
        {
            sbHeaterCounterElapsed = true;
        }
    }
}

/******************************** End of File ********************************/
