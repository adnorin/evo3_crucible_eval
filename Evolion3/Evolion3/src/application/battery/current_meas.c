/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: current_meas.c                                                 */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <current_meas.h>
#include <conf_battery_settings.h>

/*********************************************************************
* PUBLIC uiMeasureCurrent()
*---------------------------------------------------------------------
* Measure battery current
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void uiMeasureCurrent(void)
{
    ;
}

/*********************************************************************
* PUBLIC vAverageCurrent()
*---------------------------------------------------------------------
* Routine to average current over a configurable amount of samples
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vAverageCurrent(stBmsData_t* stMyBMSData)
{
    static int16_t sshMeasuredCurrentSamples[NB_CURRENT_SAMPLES_TO_AVG_MAX];  /* Array to store the measured current samples to average */
    static uint8_t sucMeasuredCurrentAveragingIdx = 0;  /* Index to use to store samples in the array */
    uint8_t sucAveragingLoopIdx;    /* Variable to use to loop through array */
    int32_t siMeasuredCurrentAverage = 0;     /* Average measured current */

    /* Update the array with the latest current sample */
    sshMeasuredCurrentSamples[sucMeasuredCurrentAveragingIdx] = stMyBMSData->shMeasuredCurrent;

    /* Reset the Averaging index if we reach the end of the number of samples we're supposed to store */
    if (++sucMeasuredCurrentAveragingIdx >= const_ucCfgNbCurrentSamplesToAvg)
    {
        sucMeasuredCurrentAveragingIdx = 0;
    }

    /* Iterate through stored current samples, adding each value to the Average value */
    for (sucAveragingLoopIdx = 0; sucAveragingLoopIdx < const_ucCfgNbCurrentSamplesToAvg; sucAveragingLoopIdx++)
    {
        siMeasuredCurrentAverage += sshMeasuredCurrentSamples[sucAveragingLoopIdx];
    }
    /* Divide the summed individual current measurement by the count of measurements to get the average */
    siMeasuredCurrentAverage = siMeasuredCurrentAverage / const_ucCfgNbCurrentSamplesToAvg;

    /* Store the averaged value in the BMS data buffer */
    stMyBMSData->shMeasuredCurrentAvg = (int16_t) siMeasuredCurrentAverage;
}


/******************************** End of File ********************************/
