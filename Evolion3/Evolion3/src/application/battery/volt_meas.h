#ifndef VOLT_MEAS_H_
#define VOLT_MEAS_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: volt_meas.h                                                    */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for volt_meas.c, which provides prototypes for the            */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

/*********************************************************************
* PUBLIC uiVoltMeasure()
*---------------------------------------------------------------------
* Measure voltage
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void uiVoltMeasure(void);


#endif /* VOLT_MEAS_H_ */