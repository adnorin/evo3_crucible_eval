/****************************************************************************/
/* (C) Copyright 2017 by SAFT                                               */
/* All rights reserved                                                      */
/*                                                                          */
/* This program is the property of Saft America, Inc. and can only be       */
/* used and copied with the prior written authorization of SAFT.            */
/*                                                                          */
/* Any whole or partial copy of this program in either its original form or */
/* in a modified form must mention this copyright and its owner.            */
/****************************************************************************/
/* PROJECT: Evolion 3 Software                                              */
/****************************************************************************/
/* FILE NAME: SOC_calculation.c                                             */
/****************************************************************************/
/* AUTHOR : Greg Cordell                                                    */
/* LAST MODIFIER: None                                                      */
/****************************************************************************/
/* FILE DESCRIPTION:                                                        */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <bat_cell_controller_ic.h>
#include <conf_battery_settings.h>
#include <SOC_calculation.h>

static uint16_t ushGetCellSOC(uint16_t ushCellVoltage);

/*********************************************************************
* PUBLIC vCalculateSOC()
*---------------------------------------------------------------------
* SOC calculation routine
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vCalculateSOC(stBmsData_t *pstMyBMSData)
{
    uint8_t ucSOCMinimumIdx = 0;    /* Index of cell with lowest SOC */
    uint8_t ucSOCMaximumIdx = 0;    /* Index of cell with highest SOC */
    uint16_t ushSOCAverage = 0;     /* Average cell SOC */
    uint16_t ushTempSOC = 0;        /* Overall battery SOC */
    uint16_t ushCellSOC[NUM_OF_CELLS] = {[0 ... (NUM_OF_CELLS-1)] = 0};    /* Array of individual cell SOCs */
    static uint16_t sushSOCSamples[NB_SOC_SAMPLES_TO_AVG_MAX] = {[0 ... (NB_SOC_SAMPLES_TO_AVG_MAX-1)] = 0};
    static uint16_t sushSOCPrevious = 0;  /* SOC from previous run of SOC routine */
    static uint8_t sucSOCAveragingIdx = 0;
    static bool sbFirstRun = true; /* If this is the first run through the algorithm, bypass the ramp rate restriction so we can get the SOC close to the real value quickly.*/

    /* Get the SOC of each cell and also find the minimum and maximum cell SOC */
    for (uint8_t ucIdx = 0; ucIdx < NUM_OF_CELLS; ucIdx++)
    {
        ushCellSOC[ucIdx] = ushGetCellSOC(pstMyBMSData->ushCellVoltage[ucIdx]);
        ushSOCAverage += ushCellSOC[ucIdx];

        if (ushCellSOC[ucIdx] < ushCellSOC[ucSOCMinimumIdx])
        {
            ucSOCMinimumIdx = ucIdx;
        }
        if (ushCellSOC[ucIdx] > ushCellSOC[ucSOCMaximumIdx])
        {
            ucSOCMaximumIdx = ucIdx;
        }
    }
    /* Calculate the average SOC of all the cells */
    ushSOCAverage = ushSOCAverage / NUM_OF_CELLS;

    /* Calculate the SOC proportional to the previous SOC in order for the SOC to be more intuitive to the customer */
    ushTempSOC = (uint16_t) ( ( ( ( (uint32_t) ushCellSOC[ucSOCMinimumIdx] ) * ( const_ushCfgSOCMaximum - sushSOCPrevious ) ) / const_ushCfgSOCMaximum ) + ( ( ( (uint32_t) ushCellSOC[ucSOCMaximumIdx] ) * sushSOCPrevious ) / const_ushCfgSOCMaximum ) );

    /* The proportional calculation makes it impossible to reach 0 or 100% SOC, so add in some small tolerances at the end to get to these extremes */
    if (ushTempSOC > const_ushCfgSOCFullyCharged)
    {
        ushTempSOC = const_ushCfgSOCMaximum;
    }
    else if (ushTempSOC < const_ushCfgSOCFullyDischarged)
    {
        ushTempSOC = const_ushCfgSOCMinimum;
    }

    /* Store the SOC value just calculated into the array of samples to be averaged */
    sushSOCSamples[sucSOCAveragingIdx] = ushTempSOC;

    /* Once we have stored each sample in the array of samples to be averaged, find the average value and limit the rate of change to within a certain range from the previous SOC value */
    if (++sucSOCAveragingIdx >= const_ucCfgNbSOCSamplesToAvg)
    {
        /* Clear the temporary SOC value as we're going to use it to add all of the SOC samples */
        ushTempSOC = 0;
        for (sucSOCAveragingIdx = 0;sucSOCAveragingIdx < const_ucCfgNbSOCSamplesToAvg; sucSOCAveragingIdx++)
        {
            ushTempSOC += sushSOCSamples[sucSOCAveragingIdx];
        }
        /* Calculate average SOC from all samples */
        ushTempSOC = ushTempSOC / const_ucCfgNbSOCSamplesToAvg;

        /* Reset Averaging index so that it has the correct value on the next call to this function */
        sucSOCAveragingIdx = 0;

        /* Limit the rate of change of the current SOC value to within a certain delta of the previous value */
        if (ushTempSOC > (sushSOCPrevious + const_ushCfgSOCRateOfChangeMax))
        {
            ushTempSOC = min(sushSOCPrevious + const_ushCfgSOCRateOfChangeMax,const_ushCfgSOCMaximum);
        }
        else if (ushTempSOC < (sushSOCPrevious - const_ushCfgSOCRateOfChangeMax))
        {
            ushTempSOC = max(sushSOCPrevious - const_ushCfgSOCRateOfChangeMax,const_ushCfgSOCMinimum);
        }

        /* If this is the first run through here, ignore the previous averaging and rate of change limitations and just set the SOC to the last sample value */
        /* This allows us to get close to the correct SOC very quickly after power up instead of waiting for a long time per the rate of change constriction */
        if (sbFirstRun == true)
        {
            ushTempSOC = sushSOCSamples[const_ucCfgNbSOCSamplesToAvg-1];
            sbFirstRun = false;
        }

        /* Update the previous SOC value for the proportional calculation on the next pass through */
        sushSOCPrevious = ushTempSOC;
    }

    /* Update values in the shared BMS data structure */
    pstMyBMSData->ushSOC = sushSOCPrevious;
    pstMyBMSData->ushSOCMax = ushCellSOC[ucSOCMaximumIdx];
    pstMyBMSData->ushSOCMin = ushCellSOC[ucSOCMinimumIdx];
    pstMyBMSData->ushSOCAvg = ushSOCAverage;
}

/*********************************************************************
* PRIVATE ushGetCellSOC()
*---------------------------------------------------------------------
* SOC calculation routine
*
* Inputs:
*   ushCellVoltage - Cell Voltage on which to calculate SOC
* Returns:
*   SOC of cell voltage passed to function
*
**********************************************************************/
static uint16_t ushGetCellSOC(uint16_t ushCellVoltage)
{
    uint16_t ushSOCValue = 0;
    if ( ushCellVoltage > const_ushCfgCellVOneHundredSOC )
    {
        ushSOCValue = 1000;
    }
    else if ( ushCellVoltage < const_ushCfgCellVZeroSOC )
    {
        ushSOCValue = 0;
    }
    else
    {
        ushSOCValue = const_ushCfgSOCLUT[ushCellVoltage - const_ushCfgCellVZeroSOC];
    }
    return ushSOCValue;
}