/****************************************************************************/
/* (C) Copyright 2017 by SAFT                                               */
/* All rights reserved                                                      */
/*                                                                          */
/* This program is the property of Saft America, Inc. and can only be       */
/* used and copied with the prior written authorization of SAFT.            */
/*                                                                          */
/* Any whole or partial copy of this program in either its original form or */
/* in a modified form must mention this copyright and its owner.            */
/****************************************************************************/
/* PROJECT: Evolion 3 Software                                              */
/****************************************************************************/
/* FILE NAME: current_limits.c                                              */
/****************************************************************************/
/* AUTHOR : Greg Cordell                                                    */
/* LAST MODIFIER: Gerald Percherancier                                      */
/****************************************************************************/
/* FILE DESCRIPTION:                                                        */
/*  This file includes the logic to create battery current limits as a      */
/*   function of cell temperature and cell voltage. Also includes logic to  */
/*   determine if the battery is currently exceeding predefined I2T limits  */
/*   so as to prevent the non-replaceable fuse from blowing.                */
/****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <bat_cell_controller_ic.h>
#include <current_limits.h>
#include <temp_sensor_ic.h>
#include <conf_battery_settings.h>

/*********************************************************************
* PUBLIC vCalculateIMDIMR()
*---------------------------------------------------------------------
* IMD/IMR calculation routine
*
* Inputs:
*   stMyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vCalculateIMDIMR(stBmsData_t* stMyBMSData)
{
    int32_t iIMDMinTcell;   /* IMD based on temperature of coldest cell */
    int32_t iIMDMaxTcell;   /* IMD based on temperature of warmest cell */
    int32_t iIMDTcell;      /* IMD based on temperature of cells */
    static int32_t siIMDTcellPrevious = 0;  /* Previous value of IMD based on temperature of cells */

    int32_t iIMRMinTcell;   /* IMR based on temperature of coldest cell */
    int32_t iIMRMaxTcell;   /* IMR based on temperature of coldest cell */
    int32_t iIMRTcell;      /* IMR based on temperature of cells */
    static int32_t siIMRTcellPrevious = 0;  /* Previous value of IMR based on temperature of cells */

    uint16_t ushMinVcell;   /* Minimum cell voltage */
    uint16_t ushMaxVcell;   /* Maximum cell voltage */

    static eIMxVcellState_t sucIMRVcellState = IMX_MAX;
    static eIMxVcellState_t sucIMDVcellState = IMX_MAX;
    uint16_t ushIMDMinVcellCoeff;   /* Coefficient to reduce the IMD based on temperature by cell voltage */
    static uint16_t sushIMDMinVcellCoeffPrevious = MAX_CURRENT_COEFF;   /* Previous value of the coefficient */
    uint16_t ushIMRMaxVcellCoeff;   /* Coefficient to reduce the IMD based on temperature by cell voltage */
    static uint16_t sushIMRMaxVcellCoeffPrevious = MAX_CURRENT_COEFF;   /* Previous value of the coefficient */

    static uint8_t sucIMxSampleIdx = 0;
    uint8_t ucIMxFilteringIdx;
    static int32_t siIMD[NB_IMX_SAMPLES_TO_AVG_MAX] = {[0 ... (NB_IMX_SAMPLES_TO_AVG_MAX-1)] = 0};  /* IMD based on cell temperature and min cell voltage */
    static int32_t siIMR[NB_IMX_SAMPLES_TO_AVG_MAX] = {[0 ... (NB_IMX_SAMPLES_TO_AVG_MAX-1)] = 0};  /* IMR based on cell temperature and max cell voltage */

    int32_t siIMRFiltered = 0;
    int32_t siIMDFiltered = 0;

    int16_t sshMinTcell;         /* Minimum cell temperature */
    uint8_t scMinTcelltoIndex;   /* Variable for converting the minimum cell temperature to a LUT index */
    int16_t sshMaxTcell;         /* Maximum cell temperature */
    uint8_t scMaxTcelltoIndex;   /* Variable for converting the maximum cell temperature to a LUT index */

    /* Get min and max cell temperature values */
    sshMinTcell = stMyBMSData->shCellTemperatureMin;
    sshMaxTcell = stMyBMSData->shCellTemperatureMax;

    /* If the cell temperature is within the normal operating range, get the temperature-derived current limits */
    if (sshMinTcell >= const_shCfgMinTcellToCalculateCurrentLimits && sshMinTcell <= const_shCfgMaxTcellToCalculateCurrentLimits)
    {
        scMinTcelltoIndex = (uint8_t) ( (sshMinTcell - const_shCfgMinTcellToCalculateCurrentLimits) / TEMP_TO_INDEX_FACTOR );
        iIMDMinTcell = const_iCfgIMDLUT[scMinTcelltoIndex];
        iIMRMinTcell = const_iCfgIMRLUT[scMinTcelltoIndex];

        scMaxTcelltoIndex = (uint8_t) ( (sshMaxTcell - const_shCfgMinTcellToCalculateCurrentLimits) / TEMP_TO_INDEX_FACTOR );
        iIMDMaxTcell = const_iCfgIMDLUT[scMaxTcelltoIndex];
        iIMRMaxTcell = const_iCfgIMRLUT[scMaxTcelltoIndex];
    }
    /* If the cell temperature(s) are outside the valid range, set the temperature-derived current limits to 0 */
    else
    {
        iIMDMinTcell = 0;
        iIMRMinTcell = 0;
        iIMDMaxTcell = 0;
        iIMRMaxTcell = 0;
    }

    /* Take lowest magnitude allowable current value for hottest and coldest cell for discharge and charge, respectively */
    iIMDTcell = max(iIMDMinTcell,iIMDMaxTcell);
    iIMRTcell = min(iIMRMinTcell,iIMRMaxTcell);

    /* Get min and max cell voltage values */
    ushMinVcell = stMyBMSData->ushMinCellV;
    ushMaxVcell = stMyBMSData->ushMaxCellV;

    /* if the min cell voltage is above the threshold to start decreasing IMD, then set the IMD coefficient to its max value */
    if (ushMinVcell >= const_ushCfgVcellToStartDecreasingIMD)
    {
        sucIMDVcellState = IMX_MAX;
    }
    /* if the min cell voltage is below the threshold to inhibit discharge, then set the IMD coefficient to its min value */
    else if (ushMinVcell <= const_ushCfgVcellToInhibitDischarge)
    {
        sucIMDVcellState = IMX_MIN;
    }
    /* if the min cell voltage is somewhere in between, then decrease IMD coefficient proportionally to the cell voltage */
    else
    {
        sucIMDVcellState = IMX_DECREASING;
    }

    switch (sucIMDVcellState)
    {
        case IMX_MAX:
        ushIMDMinVcellCoeff = MAX_CURRENT_COEFF;
        sushIMDMinVcellCoeffPrevious = ushIMDMinVcellCoeff;
        siIMDTcellPrevious = iIMDTcell;
        break;

        case IMX_MIN:
        ushIMDMinVcellCoeff = MIN_CURRENT_COEFF;
        sushIMDMinVcellCoeffPrevious = ushIMDMinVcellCoeff;
        siIMDTcellPrevious = iIMDTcell;
        break;

        case IMX_DECREASING:
        ushIMDMinVcellCoeff = MAX_CURRENT_COEFF - ( ( ( const_ushCfgVcellToStartDecreasingIMD - ushMinVcell ) * ( MAX_CURRENT_COEFF - MIN_CURRENT_COEFF ) ) / ( const_ushCfgVcellToStartDecreasingIMD - const_ushCfgVcellToInhibitDischarge ) );

        /* We must only decrease in this region, the current limit coefficient will return to its max value once we charge past the voltage limit to decrease */
        if (ushIMDMinVcellCoeff < sushIMDMinVcellCoeffPrevious)
        {
            sushIMDMinVcellCoeffPrevious = ushIMDMinVcellCoeff;
        }
        else
        {
            ushIMDMinVcellCoeff = sushIMDMinVcellCoeffPrevious;
        }

        if (iIMDTcell > siIMDTcellPrevious)
        {
            siIMDTcellPrevious = iIMDTcell;
        }
        else
        {
            iIMDTcell = siIMDTcellPrevious;
        }
        break;
    }

    siIMD[sucIMxSampleIdx] = (iIMDTcell * ushIMDMinVcellCoeff) / MAX_CURRENT_COEFF;

    /* if the max cell voltage is below the threshold to start decreasing IMR, then set the IMR coefficient to its max value */
    if (ushMaxVcell <= const_ushCfgVcellToStartDecreasingIMR)
    {
        sucIMRVcellState = IMX_MAX;

    }
    /* if the max cell voltage is above the threshold to inhibit charge, then set the IMR coefficient to its min value */
    else if (ushMaxVcell >= const_ushCfgVcellToInhibitCharge)
    {
        sucIMRVcellState = IMX_MIN;
    }
    /* if the max cell voltage is somewhere in between, then decrease IMR coefficient proportionally to the cell voltage */
    else
    {
        sucIMRVcellState = IMX_DECREASING;
    }

    switch (sucIMRVcellState)
    {
        case IMX_MAX:
        ushIMRMaxVcellCoeff = MAX_CURRENT_COEFF;
        sushIMRMaxVcellCoeffPrevious = ushIMRMaxVcellCoeff;
        siIMRTcellPrevious = iIMRTcell;
        break;

        case IMX_MIN:
        ushIMRMaxVcellCoeff = MIN_CURRENT_COEFF;
        sushIMRMaxVcellCoeffPrevious = ushIMRMaxVcellCoeff;
        siIMRTcellPrevious = iIMRTcell;
        break;

        case IMX_DECREASING:
        ushIMRMaxVcellCoeff = MAX_CURRENT_COEFF - ( ( ( ushMaxVcell - const_ushCfgVcellToStartDecreasingIMR ) * ( MAX_CURRENT_COEFF - MIN_CURRENT_COEFF ) ) / ( const_ushCfgVcellToInhibitCharge - const_ushCfgVcellToStartDecreasingIMR ) );

        /* We must only decrease in this region, the current limit coefficient will return to its max value once we discharge past the voltage limit to decrease */
        if (ushIMRMaxVcellCoeff < sushIMRMaxVcellCoeffPrevious)
        {
            sushIMRMaxVcellCoeffPrevious = ushIMRMaxVcellCoeff;
        }
        else
        {
            ushIMRMaxVcellCoeff = sushIMRMaxVcellCoeffPrevious;
        }

        if (iIMRTcell < siIMRTcellPrevious)
        {
            siIMRTcellPrevious = iIMRTcell;
        }
        else
        {
            iIMRTcell = siIMRTcellPrevious;
        }
        break;
    }

    siIMR[sucIMxSampleIdx] = (iIMRTcell * ushIMRMaxVcellCoeff) / MAX_CURRENT_COEFF;

    /* Iterate through stored current limit samples, adding each value to the value to be averaged */
    for (ucIMxFilteringIdx = 0; ucIMxFilteringIdx < const_ucCfgNbCurrentLimitSamplesToAvg; ucIMxFilteringIdx++)
    {
        siIMDFiltered += siIMD[ucIMxFilteringIdx];
        siIMRFiltered += siIMR[ucIMxFilteringIdx];
    }

    /* Divide the summed individual current limits by the count to get the average */
    siIMDFiltered = siIMDFiltered/const_ucCfgNbCurrentLimitSamplesToAvg;
    siIMRFiltered = siIMRFiltered/const_ucCfgNbCurrentLimitSamplesToAvg;


    /* Reset the Averaging index if we reach the end of the number of samples we're supposed to store */
    if (++sucIMxSampleIdx >= const_ucCfgNbCurrentLimitSamplesToAvg)
    {
        sucIMxSampleIdx = 0;
    }

    /* Put the outputs in the shared BMS data structure */
    stMyBMSData->shDischargeCurrentLimit = (int16_t) (siIMDFiltered/10);
    stMyBMSData->shChargeCurrentLimit = (int16_t) (siIMRFiltered/10);
}

/*********************************************************************
* PUBLIC vDetermineIfI2tExceeded()
*---------------------------------------------------------------------
*   Logic to determine if I2t has been exceeded within the last
*    10-100ms based on comparison criteria for each 10ms sample
*
* Inputs:
*   stMyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vDetermineIfI2tExceeded(stBms_FastData_t* stMyBMSData)
{
    static uint32_t suiI2tValues[I2T_ARRAY_LENGTH] = {[0 ... (I2T_ARRAY_LENGTH-1)] = 0};    /* Array of I2T samples for the last 100ms (1 sample per 10ms) */
    static bool sbI2tExceeded[I2T_ARRAY_LENGTH] = {[0 ... (I2T_ARRAY_LENGTH-1)] = false};   /* Flags of if I2T threshold has been exceeded for the last 100ms (1 sample per 10ms) */
    static uint8_t sucI2tIncrementIdx = 0;  /* Variable used to increment through the 10ms samples up to 100ms */
    bool sbI2tExceededLogicalOR = false;    /* We want to know if the I2t has been exceeded for any of the samples taken, so this variable is used to logically OR each of the sbI2tExceeded[] samples */
    uint8_t ucI2tLoopIdx;                   /* Variable used to loop through array indices */
    uint8_t ucI2t010msCnt = 0;              /* Count of how many times the 10ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t020msCnt = 0;              /* Count of how many times the 20ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t030msCnt = 0;              /* Count of how many times the 30ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t040msCnt = 0;              /* Count of how many times the 40ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t050msCnt = 0;              /* Count of how many times the 50ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t060msCnt = 0;              /* Count of how many times the 60ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t070msCnt = 0;              /* Count of how many times the 70ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t080msCnt = 0;              /* Count of how many times the 80ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t090msCnt = 0;              /* Count of how many times the 90ms I2 threshold has been exceeded over the last 100ms */
    uint8_t ucI2t100msCnt = 0;              /* Count of how many times the 100ms I2 threshold has been exceeded over the last 100ms */

    /* Store current I2 value into array at the current increment index */
    suiI2tValues[sucI2tIncrementIdx] = (uint32_t)( (int32_t)stMyBMSData->shMeasuredCurrent * (int32_t)stMyBMSData->shMeasuredCurrent );

    /* Loop through I2 value array to get counts of how many times I2t has been met or exceeded */
    for (ucI2tLoopIdx = 0;ucI2tLoopIdx < I2T_ARRAY_LENGTH; ucI2tLoopIdx++)
    {
        /* If the I2 value exceeds the 10ms threshold, increment that and all the subsequent counts */
        if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[0])
        {
            ucI2t010msCnt++;
            ucI2t020msCnt++;
            ucI2t030msCnt++;
            ucI2t040msCnt++;
            ucI2t050msCnt++;
            ucI2t060msCnt++;
            ucI2t070msCnt++;
            ucI2t080msCnt++;
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 20ms threshold, increment that and all the subsequent counts */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[1])
        {
            ucI2t020msCnt++;
            ucI2t030msCnt++;
            ucI2t040msCnt++;
            ucI2t050msCnt++;
            ucI2t060msCnt++;
            ucI2t070msCnt++;
            ucI2t080msCnt++;
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 30ms threshold, increment that and all the subsequent counts */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[2])
        {
            ucI2t030msCnt++;
            ucI2t040msCnt++;
            ucI2t050msCnt++;
            ucI2t060msCnt++;
            ucI2t070msCnt++;
            ucI2t080msCnt++;
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 40ms threshold, increment that and all the subsequent counts */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[3])
        {
            ucI2t040msCnt++;
            ucI2t050msCnt++;
            ucI2t060msCnt++;
            ucI2t070msCnt++;
            ucI2t080msCnt++;
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 50ms threshold, increment that and all the subsequent counts */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[4])
        {
            ucI2t050msCnt++;
            ucI2t060msCnt++;
            ucI2t070msCnt++;
            ucI2t080msCnt++;
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 60ms threshold, increment that and all the subsequent counts */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[5])
        {
            ucI2t060msCnt++;
            ucI2t070msCnt++;
            ucI2t080msCnt++;
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 70ms threshold, increment that and all the subsequent counts */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[6])
        {
            ucI2t070msCnt++;
            ucI2t080msCnt++;
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 80ms threshold, increment that and all the subsequent counts */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[7])
        {
            ucI2t080msCnt++;
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 90ms threshold, increment that and all the subsequent counts */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[8])
        {
            ucI2t090msCnt++;
            ucI2t100msCnt++;
        }
        /* If the I2 value exceeds the 100ms threshold, increment that count */
        else if (suiI2tValues[ucI2tLoopIdx] >= const_uiCfgFastOverCurrentI2tValues[9])
        {
            ucI2t100msCnt++;
        }
    }

    /* Determine if i2t has been exceeded for the appropriate number of times to set the flag */
    if (ucI2t010msCnt >= 1)
    {
        /* If the 10ms threshold has been exceeded at least 1 time, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t020msCnt >= 2)
    {
        /* If the 20ms threshold has been exceeded at least 2 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t030msCnt >= 3)
    {
        /* If the 30ms threshold has been exceeded at least 3 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t040msCnt >= 4)
    {
        /* If the 40ms threshold has been exceeded at least 4 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t050msCnt >= 5)
    {
        /* If the 50ms threshold has been exceeded at least 5 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t060msCnt >= 6)
    {
        /* If the 60ms threshold has been exceeded at least 6 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t070msCnt >= 7)
    {
        /* If the 70ms threshold has been exceeded at least 7 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t080msCnt >= 8)
    {
        /* If the 80ms threshold has been exceeded at least 8 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t090msCnt >= 9)
    {
        /* If the 90ms threshold has been exceeded at least 9 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else if (ucI2t100msCnt >= 10)
    {
        /* If the 100ms threshold has been exceeded at least 10 times, activate the I2T exceeded flag */
        sbI2tExceeded[sucI2tIncrementIdx] = true;
    }
    else
    {
        /* If none of the previous applies, deactivate the flag */
        sbI2tExceeded[sucI2tIncrementIdx] = false;
    }

    /* Ensure that the flag is set for at least 100ms (10*10ms) so that the 100ms task can see it */
    for (ucI2tLoopIdx = 0;ucI2tLoopIdx < I2T_ARRAY_LENGTH; ucI2tLoopIdx++)
    {
        sbI2tExceededLogicalOR |= sbI2tExceeded[ucI2tLoopIdx];
    }

    /* Update value in BMS data structure */
    stMyBMSData->bI2tExceededFlag = sbI2tExceededLogicalOR;

    /* Increment array index */
    if (++sucI2tIncrementIdx >= I2T_ARRAY_LENGTH)
    {
        sucI2tIncrementIdx = 0;
    }
}

/******************************** End of File ********************************/
