/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: volt_meas.c                                                    */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <volt_meas.h>

/*********************************************************************
* PUBLIC uiVoltMeasure()
*---------------------------------------------------------------------
* Measure voltage
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void uiVoltMeasure(void)
{
    
}

/******************************** End of File ********************************/
