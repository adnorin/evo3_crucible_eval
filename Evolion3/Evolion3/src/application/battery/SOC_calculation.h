#ifndef SOC_CALCULATION_H_
#define SOC_CALCULATION_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: SOC_calculation.h                                              */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for SOC_calculation.c, which provides prototypes for the      */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define NB_SOC_SAMPLES_TO_AVG_MAX   50
#define VCELL_TO_SOC_ARRAY_LENGTH   680

/*********************************************************************
* PUBLIC vCalculateSOC()
*---------------------------------------------------------------------
* SOC calculation routine
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vCalculateSOC(stBmsData_t *pstMyBMSData);

#endif /* SOC_CALCULATION_H_ */
