/****************************************************************************/
/* (C) Copyright 2017 by SAFT                                               */
/* All rights reserved                                                      */
/*                                                                          */
/* This program is the property of Saft America, Inc. and can only be       */
/* used and copied with the prior written authorization of SAFT.            */
/*                                                                          */
/* Any whole or partial copy of this program in either its original form or */
/* in a modified form must mention this copyright and its owner.            */
/****************************************************************************/
/* PROJECT: Evolion 3 Software                                              */
/****************************************************************************/
/* FILE NAME: heater_control.h                                              */
/****************************************************************************/
/* AUTHOR : Greg Cordell                                                    */
/* LAST MODIFIER: None                                                      */
/****************************************************************************/
/* FILE DESCRIPTION:                                                        */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/
#ifndef HEATER_CONTROL_H_
#define HEATER_CONTROL_H_

/*********************************************************************
* PUBLIC vControlHeater()
*---------------------------------------------------------------------
* Heater control routine
*
* Inputs:
* stMyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vControlHeater(stBmsData_t* stMyBMSData);

#endif /* HEATER_CONTROL_H_ */
