#ifndef FET_MANAGEMENT_H_
#define FET_MANAGEMENT_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: fet_management.h                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for fet_management.c, which provides prototypes for the       */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
#include <iface_rtos_start.h>

/*********************************************************************
* PUBLIC vFetStateManager()
*---------------------------------------------------------------------
* FET state machine management routine
*
* Inputs:
*     stMyBMSData - Current data for BMS
*     stMyFaultAlarms - Active Faults
* Returns: None
*
**********************************************************************/
void vFetStateManager(stBms_FastData_t* stMyBMSData, stFaultsandLimits_t* stMyFaultsandLimits);

#endif /* FET_MANAGEMENT_H_ */