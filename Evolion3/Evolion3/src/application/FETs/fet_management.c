/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: fet_management.c                                               */
/*****************************************************************************/
/* AUTHOR : Mayank Raj                                                       */
/* LAST MODIFIER: Mayank Raj                                                 */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

#include <asf.h>
#include <fet_management.h>
#include <fet_controller.h>
#include <iface_rtos_start.h>
#include <faults.h>
#include <bat_cell_controller_ic.h>
#include <fets.h>
#include <conf_battery_settings.h>

/*********************************************************************
* private vSetFetsOperationalState()
*---------------------------------------------------------------------
* Sets the operational state of each FET based on the FET State Machine
* "state".
*
* Inputs:
*    eMyStateMachineState - Fet State Machine state
* Returns: None
*
**********************************************************************/

static void vSetFetsOperationalState(eStates_machine_t eMyStateMachineState)
{
    uint16_t ushFetBitArray;

    switch(eMyStateMachineState)
    {

        /**************************
        Forbidden Charge
        Regulated Discharge
        ***************************/
        case FORBIDDEN_CHARGE_REG_DISCHARGE_LV0:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK;
        break;

        case FORBIDDEN_CHARGE_REG_DISCHARGE_LV1:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK;
        break;

        case FORBIDDEN_CHARGE_REG_DISCHARGE_LV2:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_2_MASK;
        break;

        case FORBIDDEN_CHARGE_REG_DISCHARGE_LV3:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK;
        break;

        case FORBIDDEN_CHARGE_REG_DISCHARGE_LV4:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_3_MASK;
        break;

        case FORBIDDEN_CHARGE_REG_DISCHARGE_LV5:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        case FORBIDDEN_CHARGE_REG_DISCHARGE_LV6:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        case FORBIDDEN_CHARGE_REG_DISCHARGE_LV7:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;


        /**************************
        Forbidden Charge
        Full Discharge
        ***************************/
        case FORBIDDEN_CHARGE_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK;
        break;


        /**************************
        Regulated Charge
        Regulated Discharge
        ***************************/
        case REG_CHARGE_REG_DISCHARGE:
        ushFetBitArray = REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK;
        break;


        /**************************
        Regulated Charge
        Full Discharge
        ***************************/
        case REG_CHARGE_LVL0_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        break;

        case REG_CHARGE_LVL1_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK;
        break;

        case REG_CHARGE_LVL2_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_2_MASK;
        break;

        case REG_CHARGE_LVL3_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK;
        break;

        case REG_CHARGE_LVL4_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_3_MASK;
        break;

        case REG_CHARGE_LVL5_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        case REG_CHARGE_LVL6_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        case REG_CHARGE_LVL7_FULL_DISCHARGE:
        ushFetBitArray = FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;


        /**************************
        Full Charge
        Regulated Discharge
        ***************************/
        case FULL_CHARGE_LVL0_REG_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        break;

        case FULL_CHARGE_LVL1_REG_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK;
        break;

        case FULL_CHARGE_LVL2_REG_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_2_MASK;
        break;

        case FULL_CHARGE_LVL3_REG_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK;
        break;

        case FULL_CHARGE_LVL4_REG_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_3_MASK;
        break;

        case FULL_CHARGE_LVL5_REG_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        case FULL_CHARGE_LVL6_REG_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        case FULL_CHARGE_LVL7_REG_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;


        /**************************
        Full Charge
        Full Discharge
        ***************************/
        case FULL_CHARGE_FULL_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | FULL_DISCHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK | REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;


        /**************************
        Regulated Charge
        Forbidden Discharge
        ***************************/
        case REG_CHARGE_LV0_FORBIDDEN_DISCHARGE:
        ushFetBitArray = SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        break;

        case REG_CHARGE_LV1_FORBIDDEN_DISCHARGE:
        ushFetBitArray = SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK;
        break;

        case REG_CHARGE_LV2_FORBIDDEN_DISCHARGE:
        ushFetBitArray = SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_2_MASK;
        break;

        case REG_CHARGE_LV3_FORBIDDEN_DISCHARGE:
        ushFetBitArray = SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK;
        break;

        case REG_CHARGE_LV4_FORBIDDEN_DISCHARGE:
        ushFetBitArray = SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_3_MASK;
        break;

        case REG_CHARGE_LV5_FORBIDDEN_DISCHARGE:
        ushFetBitArray = SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        case REG_CHARGE_LV6_FORBIDDEN_DISCHARGE:
        ushFetBitArray = SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        case REG_CHARGE_LV7_FORBIDDEN_DISCHARGE:
        ushFetBitArray = SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        ushFetBitArray |= REGULATED_CURRENT_LVL_1_MASK | REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
        break;

        /**************************
        Full Charge
        Forbidden Discharge
        ***************************/
        case FULL_CHARGE_FORBIDDEN_DISCHARGE:
        ushFetBitArray = FULL_CHARGE_FET_ENABLE_MASK | SAFETY_CHARGE_FET_ENABLE_MASK | REG_CHARGE_FET_ENABLE_MASK;
        break;

        /**************************
        Forbidden Charge
        Forbidden Discharge
        ***************************/
        case FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE:
        ushFetBitArray = 0;
        break;

        default:
        break;
    }
    vSetFetsONOFF(ushFetBitArray);
}

/*********************************************************************
* PUBLIC vFetStateManager()
*---------------------------------------------------------------------
* FET state machine management routine
*
* Inputs:
*    stMyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vFetStateManager(stBms_FastData_t* stMyBMSData, stFaultsandLimits_t* stMyFaultsandLimits)
{
    static eStates_machine_t seState_machine = FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
    static bool sbState_machine_count=true;
    static uint8_t ucInitialWaitCnt = 9;
    int16_t shBattVoltage = 0;
    int16_t shBattNetworkVoltage = 0;
    int16_t const_shCfgVsafConThreshld = 25; // TODO: Move to configuration parameters
    bool bOverVoltageConditions;
    bool bUnderVoltageConditions;
    
    /* Establish the Over and Under Voltage Conditions */
    bOverVoltageConditions  = stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_CELL_OVER_VOLTAGE_ALARM) || 
                              stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_BATTERY_OVER_VOLTAGE_ALARM) || 
                              stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_BATTERY_OVER_SOC_ALARM);
    bUnderVoltageConditions = stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_CELL_UNDER_VOLTAGE_ALARM) ||  
                              stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_BATTERY_UNDER_VOLTAGE_ALARM) || 
                              stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_BATTERY_UNDER_SOC_ALARM);

    /* Get the battery network voltage */
    shBattNetworkVoltage = stMyBMSData->ushAnalogVoltage[ANALOG_IN_6];

    /* Get the battery voltage*/
    shBattVoltage=stMyBMSData->ushSumOfCellsVoltage;
    
    if(ucInitialWaitCnt == 0)
    {                
        /* Check for Over/Under temperatures in cells and Over temperature in electronics */
        if((stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CELL_OVER_TEMP_ALARM)) ||(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CELL_UNDER_TEMP_ALARM))
        ||(stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_ELECTRONICS_OVER_TEMP_ALARM)))
        {
            seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
        }
        else
        {
            /* The check for Voltages only happens the first time through the state machine or when in
            Forbidden Charge Forbidden Discharge state */
            if((sbState_machine_count==true)||(seState_machine==FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE))
            {
                seState_machine = CHECK_FOR_OVER_UNDER_VOLTAGES;
                sbState_machine_count=false;
            }
        }

        /* Main FET State Machine */
        switch(seState_machine)
        {
            /*********************
            Check Voltages
            **********************/
            case CHECK_FOR_OVER_UNDER_VOLTAGES:
            if((bOverVoltageConditions) && (bUnderVoltageConditions))
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV0;
            }
            else if(bUnderVoltageConditions)
            {
                seState_machine=REG_CHARGE_LV0_FORBIDDEN_DISCHARGE;
            }
            else
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            break;

            /**************************
            Forbidden Charge
            Regulated Discharge level0
            ***************************/
            case FORBIDDEN_CHARGE_REG_DISCHARGE_LV0:
            if(!bOverVoltageConditions)
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level1 */
            else if(!(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM)) && !(stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM)) && !(stMyBMSData->bI2tExceededFlag))
            {
                if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit1) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit1))
                {
                    seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV1;
                }
            }
            break;

            /***************************
            Forbidden Charge
            Regulated Discharge level1
            ***************************/
            case FORBIDDEN_CHARGE_REG_DISCHARGE_LV1:
            if(!bOverVoltageConditions)
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level2 */
            else if( (abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit2) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit2_3))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV2;
            }
            /* check for regulated level0 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit1) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit1))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV0;
            }
            break;

            /***************************
            Forbidden Charge
            Regulated Discharge level2
            ***************************/
            case FORBIDDEN_CHARGE_REG_DISCHARGE_LV2:
            if(!(bOverVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level3 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit3) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit2_3))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV3;
            }
            /* check for regulated level1 */
            else if( (abs(stMyFaultsandLimits->shDischargeCurrentLimit)< const_shCfgCurrentLimit2) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit2_3))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV1;
            }
            break;

            /***************************
            Forbidden Charge
            Regulated Discharge level3
            ***************************/
            case FORBIDDEN_CHARGE_REG_DISCHARGE_LV3:
            if(!(bOverVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level4 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit4) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV4;
            }
            /* check for regulated level2 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit3) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit2_3))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV2;
            }
            break;

            /***************************
            Forbidden Charge
            Regulated Discharge level4
            ***************************/
            case FORBIDDEN_CHARGE_REG_DISCHARGE_LV4:
            if(!(bOverVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level5 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit5) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV5;
            }
            /* check for regulated level3 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit4) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV3;
            }
            break;

            /***************************
            Forbidden Charge
            Regulated Discharge level5
            ***************************/
            case FORBIDDEN_CHARGE_REG_DISCHARGE_LV5:
            if(!(bOverVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level6 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit6) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV6;
            }
            /* check for regulated level4 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit5) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV4;
            }
            break;

            /***************************
            Forbidden Charge
            Regulated Discharge level6
            ***************************/
            case FORBIDDEN_CHARGE_REG_DISCHARGE_LV6:
            if(!(bOverVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level7 */
            if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit7) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV7;
            }
            /* check for regulated level5 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit6) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV5;
            }
            break;

            /***************************
            Forbidden Charge
            Regulated Discharge level7
            ***************************/
            case FORBIDDEN_CHARGE_REG_DISCHARGE_LV7:
            if(!(bOverVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* Check for the battery and network voltage difference, if it is within safe range go into full discharge */
            else if ((abs(shBattNetworkVoltage-shBattVoltage))<const_shCfgVsafConThreshld)// TBD: need to know real threshold
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level6 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit7) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FORBIDDEN_CHARGE_REG_DISCHARGE_LV6;
            }
            break;

            /***************************
            Forbidden Charge
            Full Discharge
            ***************************/
            case FORBIDDEN_CHARGE_FULL_DISCHARGE:
            /* If an over current Alarm or the I2T exceeded flag is set go to regulated discharge */
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= FORBIDDEN_CHARGE_REG_DISCHARGE_LV0;
            }
            else if (!(bOverVoltageConditions))
            {
                seState_machine=REG_CHARGE_LVL0_FULL_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge
            Regulated Discharge
            ***************************/
            case REG_CHARGE_REG_DISCHARGE:
            if(!(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM)) && !(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM)) && !(stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM)) && !(stMyBMSData->bI2tExceededFlag))
            {
                if (stMyBMSData->shMeasuredCurrent<0)
                {
                    seState_machine=FULL_CHARGE_LVL0_REG_DISCHARGE;
                }
                else //stMyBMSData->shMeasuredCurrent>=0
                {
                    seState_machine=REG_CHARGE_LVL0_FULL_DISCHARGE;
                }
            }
            break;

            /***************************
            Regulated Charge Level0
            Full Discharge
            ***************************/
            case  REG_CHARGE_LVL0_FULL_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level1 */
            else if(!(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM)))
            {
                if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit1) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit1))
                {
                    seState_machine=REG_CHARGE_LVL1_FULL_DISCHARGE;
                }
            }
            break;

            /***************************
            Regulated Charge Level1
            Full Discharge
            ***************************/
            case  REG_CHARGE_LVL1_FULL_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level2 */
            else if( (abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit2) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit2_3))
            {
                seState_machine=REG_CHARGE_LVL2_FULL_DISCHARGE;
            }
            /* check for regulated level0 */
            else if ((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit1) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit1))
            {
                seState_machine=REG_CHARGE_LVL0_FULL_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge Level2
            Full Discharge
            ***************************/
            case  REG_CHARGE_LVL2_FULL_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level3 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit3) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit2_3))
            {
                seState_machine=REG_CHARGE_LVL3_FULL_DISCHARGE;
            }
            /* check for regulated level1 */
            else if( (abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit2) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit2_3))
            {
                seState_machine=REG_CHARGE_LVL1_FULL_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge Level3
            Full Discharge
            ***************************/
            case  REG_CHARGE_LVL3_FULL_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level4 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit4) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LVL4_FULL_DISCHARGE;
            }
            /* check for regulated level2 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit3) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit2_3))
            {
                seState_machine=REG_CHARGE_LVL2_FULL_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge Level4
            Full Discharge
            ***************************/
            case  REG_CHARGE_LVL4_FULL_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level5 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit5) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LVL5_FULL_DISCHARGE;

            }
            /* check for regulated level3 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit4) ||   (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LVL3_FULL_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge Level5
            Full Discharge
            ***************************/
            case  REG_CHARGE_LVL5_FULL_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level6 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit6) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LVL6_FULL_DISCHARGE;
            }
            /* check for regulated level4 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit5) || (abs(shBattVoltage-shBattNetworkVoltage)<const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LVL4_FULL_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge Level6
            Full Discharge
            ***************************/
            case  REG_CHARGE_LVL6_FULL_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level7 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit7) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LVL7_FULL_DISCHARGE;
            }
            /* check for regulated level5 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit6) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LVL5_FULL_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge Level7
            Full Discharge
            ***************************/
            case  REG_CHARGE_LVL7_FULL_DISCHARGE:
            /* Check for the battery and network voltage difference, if it is within safe range go into full charge */
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if ((abs(shBattNetworkVoltage-shBattVoltage))<const_shCfgVsafConThreshld) // TBD: need to know real threshold
            {
                seState_machine=FULL_CHARGE_FULL_DISCHARGE;
            }
            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;
            }
            /* check for regulated level6 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit7) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LVL6_FULL_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Regulated Discharge Level0
            ***************************/
            case FULL_CHARGE_LVL0_REG_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level1 */
            else if(!(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM)))
            {
                if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit1) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit1))
                {
                    seState_machine=FULL_CHARGE_LVL1_REG_DISCHARGE;
                }
            }
            break;

            /***************************
            Full Charge
            Regulated Discharge Level1
            ***************************/
            case FULL_CHARGE_LVL1_REG_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level2 */
            else if( (abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit2) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit2_3))
            {
                seState_machine=FULL_CHARGE_LVL2_REG_DISCHARGE;
            }
            /* check for regulated level0 */
            else if ((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit1) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit1))
            {
                seState_machine=FULL_CHARGE_LVL0_REG_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Regulated Discharge Level2
            ***************************/
            case FULL_CHARGE_LVL2_REG_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level3 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit3) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit2_3))
            {
                seState_machine=FULL_CHARGE_LVL3_REG_DISCHARGE;
            }
            /* check for regulated level1 */
            else if( (abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit2) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit2_3))
            {
                seState_machine=FULL_CHARGE_LVL1_REG_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Regulated Discharge Level3
            ***************************/
            case FULL_CHARGE_LVL3_REG_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level4 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit4) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FULL_CHARGE_LVL4_REG_DISCHARGE;
            }
            /* check for regulated level2 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit3) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit2_3))
            {
                seState_machine=FULL_CHARGE_LVL2_REG_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Regulated Discharge Level4
            ***************************/
            case FULL_CHARGE_LVL4_REG_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level5 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit5) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FULL_CHARGE_LVL5_REG_DISCHARGE;
            }
            /* check for regulated level3 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit4) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FULL_CHARGE_LVL3_REG_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Regulated Discharge Level5
            ***************************/
            case FULL_CHARGE_LVL5_REG_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level6 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit6) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FULL_CHARGE_LVL6_REG_DISCHARGE;
            }
            /* check for regulated level4 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit5) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FULL_CHARGE_LVL4_REG_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Regulated Discharge Level6
            ***************************/
            case FULL_CHARGE_LVL6_REG_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level7 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)>=const_shCfgCurrentLimit7) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FULL_CHARGE_LVL7_REG_DISCHARGE;
            }
            /* check for regulated level5 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit6) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FULL_CHARGE_LVL5_REG_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Regulated Discharge Level7
            ***************************/
            case FULL_CHARGE_LVL7_REG_DISCHARGE:
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine= REG_CHARGE_REG_DISCHARGE;
            }
            else if ((abs(shBattNetworkVoltage-shBattVoltage))<const_shCfgVsafConThreshld) // TBD: need to know real threshold
            {
                seState_machine=FULL_CHARGE_FULL_DISCHARGE;
            }
            else if (bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level6 */
            else if((abs(stMyFaultsandLimits->shDischargeCurrentLimit)<const_shCfgCurrentLimit7) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=FULL_CHARGE_LVL6_REG_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Full Discharge
            ***************************/
            case FULL_CHARGE_FULL_DISCHARGE:
            /* If I2T limit is exceeded determine whether were are charging or discharging */
            if(stMyBMSData->bI2tExceededFlag || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM))
            {
                /* If discharging go to regulated discharge */
                if (stMyBMSData->shMeasuredCurrent<0)
                {
                    seState_machine=FULL_CHARGE_LVL0_REG_DISCHARGE;
                }
                /* If charging go to regulated charge */
                else
                {
                    seState_machine=REG_CHARGE_LVL0_FULL_DISCHARGE;
                }
            }

            /* Check for over current during discharge alarm and go to regulated discharge if set */
            else if (stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_DISCHARGE_OVER_CURRENT_ALARM))
            {
                seState_machine=FULL_CHARGE_LVL0_REG_DISCHARGE;
            }

            /* Check for over current during charge alarm and go to regulated charge if set */
            else if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM))
            {
                seState_machine=REG_CHARGE_LVL0_FULL_DISCHARGE;
            }

            else if (bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FULL_DISCHARGE;

            }
            else if(bUnderVoltageConditions)
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge level0
            Forbidden Discharge
            ***************************/
            case REG_CHARGE_LV0_FORBIDDEN_DISCHARGE:
            if (!(bUnderVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level1 */
            else if(!(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM)) && !(stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM)) && !(stMyBMSData->bI2tExceededFlag))
            {
                if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit1) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit1))
                {
                    seState_machine=REG_CHARGE_LV1_FORBIDDEN_DISCHARGE;
                }
            }
            break;

            /***************************
            Regulated Charge level1
            Forbidden Discharge
            ***************************/
            case REG_CHARGE_LV1_FORBIDDEN_DISCHARGE:
            if (!(bUnderVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }

            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level2 */
            else if( (abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit2) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit2_3))
            {
                seState_machine=REG_CHARGE_LV2_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level0 */
            else if ((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit1) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit1))
            {
                seState_machine=REG_CHARGE_LV0_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge level2
            Forbidden Discharge
            ***************************/
            case REG_CHARGE_LV2_FORBIDDEN_DISCHARGE:
            if (!(bUnderVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level3 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit3) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit2_3))
            {
                seState_machine=REG_CHARGE_LV3_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level1 */
            else if( (abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit2) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit2_3))
            {
                seState_machine=REG_CHARGE_LV1_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge level3
            Forbidden Discharge
            ***************************/
            case REG_CHARGE_LV3_FORBIDDEN_DISCHARGE:
            if (!(bUnderVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level4 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit4) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LV4_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level2 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit3) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit2_3))
            {
                seState_machine=REG_CHARGE_LV2_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge level4
            Forbidden Discharge
            ***************************/
            case REG_CHARGE_LV4_FORBIDDEN_DISCHARGE:
            if (!(bUnderVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level5 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit5) &&(abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LV5_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level3 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit4) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LV3_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge level5
            Forbidden Discharge
            ***************************/
            case REG_CHARGE_LV5_FORBIDDEN_DISCHARGE:
            if (!(bUnderVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level6 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit6) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LV6_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level4 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit5) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LV4_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge level6
            Forbidden Discharge
            ***************************/
            case REG_CHARGE_LV6_FORBIDDEN_DISCHARGE:
            if (!(bUnderVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level7 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)>=const_shCfgCurrentLimit7) && (abs(shBattVoltage-shBattNetworkVoltage)<=const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LV7_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level5 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit6) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LV5_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Regulated Charge level7
            Forbidden Discharge
            ***************************/
            case REG_CHARGE_LV7_FORBIDDEN_DISCHARGE:
            if (!(bUnderVoltageConditions))
            {
                seState_machine=REG_CHARGE_REG_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* Check for the battery and network voltage difference, if it is within safe range go into full charge */
            else if((abs(shBattNetworkVoltage-shBattVoltage))<const_shCfgVsafConThreshld)// TBD: need to know real threshold
            {
                seState_machine=FULL_CHARGE_FORBIDDEN_DISCHARGE;
            }
            /* check for regulated level6 */
            else if((abs(stMyFaultsandLimits->shChargeCurrentLimit)<const_shCfgCurrentLimit7) || (abs(shBattVoltage-shBattNetworkVoltage)>const_ushCfgVoltageLimit4_7))
            {
                seState_machine=REG_CHARGE_LV6_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Full Charge
            Forbidden Discharge
            ***************************/
            case FULL_CHARGE_FORBIDDEN_DISCHARGE:
            /* If an over current Alarm or the I2T exceeded flag is set go to regulated charge */
            if(stMyFaultsandLimits->uiMesaDefFaults&(1<<FLT_CHARGE_OVER_CURRENT_ALARM) || stMyFaultsandLimits->uiVendDefFaults&(1<<FLT_EXTREME_OVER_CURRENT_ALARM) || stMyBMSData->bI2tExceededFlag)
            {
                seState_machine=REG_CHARGE_LV0_FORBIDDEN_DISCHARGE;
            }
            else if (!(bUnderVoltageConditions))
            {
                seState_machine=FULL_CHARGE_LVL0_REG_DISCHARGE;
            }
            else if(bOverVoltageConditions)
            {
                seState_machine=FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE;
            }
            break;

            /***************************
            Forbidden Charge
            Forbidden Discharge
            ***************************/
            case FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE:
            break;

            default:
            break;
        }
    }
    else
    {
        ucInitialWaitCnt--;
    }
    
    /* Copy the FET state machine state */
    stMyBMSData->eFetManagementState = seState_machine;
    
    /* Build the appropriate bit mask corresponding to the Fet states */
    vSetFetsOperationalState(seState_machine);

}

/******************************** End of File ********************************/
