/****************************************************************************/
/* (C) Copyright 2017 by SAFT                                               */
/* All rights reserved                                                      */
/*                                                                          */
/* This program is the property of Saft America, Inc. and can only be       */
/* used and copied with the prior written authorization of SAFT.            */
/*                                                                          */
/* Any whole or partial copy of this program in either its original form or */
/* in a modified form must mention this copyright and its owner.            */
/****************************************************************************/
/* PROJECT: Evolion 3 Software                                              */
/****************************************************************************/
/* FILE NAME: cell_balancing.c                                              */
/****************************************************************************/
/* AUTHOR : Greg Cordell                                                    */
/* LAST MODIFIER: Gerald Percherancier                                      */
/****************************************************************************/
/* FILE DESCRIPTION:                                                        */
/*                                                                          */
/*                                                                          */
/*                                                                          */
/****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <bat_cell_controller_ic.h>
#include <conf_battery_settings.h>
#include <cell_balancing.h>


/*********************************************************************
* PUBLIC vBalanceCells()
*---------------------------------------------------------------------
* Cell balancing routine
*
* Inputs:
* stMyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vBalanceCells(stBmsData_t *pstMyBMSData)
{
    static uint16_t sushCellsToBalanceLocal;    /* Bit array of cells that we want to balance on this pass through */
    static uint16_t sushCellsToBalanceLocalPrevious;    /* Bit array of cells that we balanced the last time through */
    static uint16_t sshSequenceCount;   /* Counter used for duration between balancing logic executions */
    uint8_t ucCellIdx;                  /* Used to iterated through balancing logic for each cell */
    int16_t shMaxTcell;                 /* Maximum cell temperature */
    uint16_t ushMinCellVLocal;          /* Minimum cell voltage */

    /* Need to limit minimum cell voltage to at least the hysteresis values, otherwise the deactivation criteria math can overflow back around with a very low cell voltage */
    ushMinCellVLocal = max(pstMyBMSData->ushMinCellV,const_ushCfgCellBalancingHysteresis);

    shMaxTcell = pstMyBMSData->shCellTemperatureMax;

    /* check that the temp sensor closest to the balancing resistors is <= the electronics over-temp threshold */
    if ( pstMyBMSData->shTemperatureSensor[BMS_IC_TEMP_SENSOR_INDEX] <= const_shCfgCellBalancingElecTMax )
    {
        /* check that that all cell temp sensors are <= the cell over-temp threshold */
        if ( shMaxTcell <= const_shCfgCellBalancingCellTMax )
        {
            for ( ucCellIdx = 0; ucCellIdx < NUM_OF_CELLS; ucCellIdx++ )
            {
                /* check that the cell is above the minimum balancing voltage */
                if ( pstMyBMSData->ushCellVoltage[ucCellIdx] > const_ushCfgCellBalancingVoltageMin )
                {
                    /* Only enable/disable balancing of cells based on their voltage at beginning of sequence */
                    if ( sshSequenceCount == 0 )
                    {
                        /* check that the cell is more than the balancing delta threshold above the lowest cell to enable balancing */
                        if ( ( pstMyBMSData->ushCellVoltage[ucCellIdx] > ( ushMinCellVLocal + const_ushCfgCellBalancingDelta ) ) )
                        {
                            sushCellsToBalanceLocal |= ( 1 << ucCellIdx );
                        }
                        /* check that the cell is balanced within the (delta threshold - hysteresis) above the lowest cell to disable balancing */
                        else if ( ( ( sushCellsToBalanceLocalPrevious >> ucCellIdx ) & 0x1) && ( pstMyBMSData->ushCellVoltage[ucCellIdx] <= ( ushMinCellVLocal + const_ushCfgCellBalancingDelta - const_ushCfgCellBalancingHysteresis) ) )
                        {
                            sushCellsToBalanceLocal &= ~(1 << ucCellIdx);
                        }
                        else
                        {
                            sushCellsToBalanceLocal &= ~(1 << ucCellIdx);
                        }
                        sushCellsToBalanceLocalPrevious = sushCellsToBalanceLocal;
                    }
                }
                else
                {
                    sushCellsToBalanceLocal &= ~(1 << ucCellIdx);
                }
            }
        }
        else
        {
            sushCellsToBalanceLocal = 0;
        }
    }
    else
    {
        sushCellsToBalanceLocal = 0;
    }

    /* Two 56Ohm balancing resistors in parallel per cell gives 28Ohm, 3.56V (average voltage per cell) / 28Ohm gives ~127mA */
    /* In order to balance 1mAh out of a cell, we must balance for 28.35s */
    /* Just use 30s for easy understanding */
    if ( ++sshSequenceCount >= const_ushCfgCellBalancingDuration  )
    {
        sshSequenceCount = 0;
    }

    pstMyBMSData->ushCellsToBalance = sushCellsToBalanceLocal;
    vCellBalancingRequest(sushCellsToBalanceLocal);
}

/******************************** End of File ********************************/
