#ifndef CELL_BALANCING_H_
#define CELL_BALANCING_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: cell_balancing.h                                               */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for cell_balancing.c, which provides prototypes for the       */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

/*********************************************************************
* PUBLIC vBalanceCells()
*---------------------------------------------------------------------
* Cell balancing routine
*
* Inputs:
*       MyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void vBalanceCells(stBmsData_t* pstMyBMSData);

#endif /* CELL_BALANCING_H_ */
