/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: main.c                                                         */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Greg Cordell                                               */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Main function for the Evolion 3 Battery Software                          */
/*                                                                           */
/*****************************************************************************/

#include <asf.h>
#include <main.h>
#include <iface_i2c_dma.h>
#include <iface_spi_dma.h>
#include <iface_rtos_start.h>
#include <iface_wdt.h>
#include <iface_rtc_timer.h>
#include <test_ram.h>
#include <test_flash.h>
#include <iface_gpio.h>
#include <conf_gpio.h>
#include <iface_extint.h>
#include <comm_external.h>
#include <comm_internal.h>
#include <iface_usb_cdc.h>
#include <temp_sensor_ic.h>
#include <bat_cell_controller_ic.h>
#include <led_driver_ic.h>
#include <self_tests.h>
#include <eeprom_controller.h>
#include <led_driver_ic.h>
#include <RamDataStorage.h>
#include <RomDataStorage.h>

/* Memory Test Results */
uint32_t uiRamTestResults;
bool bFlashTestResults;

/* Public variables */
static stGlobalStatus_t sstGlobalStatus;

/* Prototypes */
static void svInitializeGlobalStatusFlags(stGlobalStatus_t * spMyGlobalStatus);

#define STACK_ADD 0x20005708

/******************************************************************************
* main()
*------------------------------------------------------------------------------
* Main Function
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
int main (void)
{
    /**************************************************/
    /* Initialization                                 */
    /**************************************************/
    svInitializeGlobalStatusFlags(&sstGlobalStatus);
    system_init();

    //memset(.stack, 0x55, 0x2000);

    /**************************************************/
    /* Memory Tests                                   */
    /**************************************************/
    uiRamTestResults = uiGetRamTestResults();
    bFlashTestResults = bVerifyFlashCRC();

    /**************************************************/
    /* Start RTOS                                     */
    /**************************************************/
#if configUSE_TRACE_FACILITY == 1
    /* Init and start tracing */
    vTraceEnable(TRC_START);
#endif

    /* Set RTOS Started flag */
    sstGlobalStatus.bRtosStarted = true;

    /* Start Operating System */
    vStartRtosTasks();

    /* Should never reach here */
    while(1);

    return(0);
}


/******************************************************************************
* PRIVATE vInitializePeripherals1()
*------------------------------------------------------------------------------
* vInitializePeripherals before putting the system to sleep
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
void vInitializePeripherals1(void)
{
    /**************************************************/
    /* GPIOs                                          */
    /**************************************************/
    vConfigureGPIO();

    /**************************************************/
    /* Reset over-current latch IC via GPIO           */
    /**************************************************/
    vResetOverCurrentLatch();

    /**************************************************/
    /* Reset NXP MC33771 BMS IC                       */
    /**************************************************/
    vResetBmsIc();

    /**************************************************/
    /* External Interrupts                            */
    /**************************************************/
    vConfigureExternalInterruptChannels();
    vConfigureButtonExternalInterruptCallbacks();

    /**************************************************/
    /* Start I2C Configure Port                       */
    /**************************************************/
    bConfigureI2cPort();

    /**************************************************/
    /* Initialize Temperature Sensors - I2C           */
    /**************************************************/
    vInitializeTempSensors();

    /**************************************************/
    /* Initialize HMI LED Driver - I2C                */
    /**************************************************/
    vInitializeHMILedDriver();

    /**************************************************/
    /* Start SPI Port 1 BMS IC                        */
    /**************************************************/
    //vInitializeBatteryCellCtl();

    /**************************************************/
    /* Init Sleep Mode Routines                       */
    /**************************************************/
    sleepmgr_init();
    sleepmgr_lock_mode(SLEEPMGR_STANDBY);
}


/******************************************************************************
* PUBLIC vInitializePeripherals2()
*------------------------------------------------------------------------------
* vInitializePeripherals after waking up from initial sleep
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
void vInitializePeripherals2(void)
{
    /**************************************************/
    /* Start USART Port 1                             */
    /**************************************************/
    vUsartExtCommInitialize();

    /**************************************************/
    /* Start USART Port 2                             */
    /**************************************************/
    vUsartIntCommInitialize();

    /**************************************************/
    /* Start USB CDC Port                             */
    /**************************************************/
    vStartUSBCDC();

    /**************************************************/
    /* Start SPI Port 1 BMS IC                        */
    /**************************************************/
    vInitializeBatteryCellCtl();

    /**************************************************/
    /* Start SPI Port 2 EEPROM                        */
    /**************************************************/
    vEEPROM_Init();

    /**************************************************/
    /* Configure temp sensor thresholds               */
    /**************************************************/
    vTempSensorsSetOverTempAndHysteresis();
}


/******************************************************************************
* PRIVATE vInitializeGlobalStatusFlags()
*------------------------------------------------------------------------------
* Initializes the structure that contains the global status flags
*
* Inputs:
*   spMyGlobalStatus - Pointer to sGlobalStatus structure
* Returns: Nont
*
*******************************************************************************/
static void svInitializeGlobalStatusFlags(stGlobalStatus_t * spMyGlobalStatus)
{
    spMyGlobalStatus->bRtosStarted = false;
    spMyGlobalStatus->eBatteryOperatingMode = NORMAL_MODE;
    spMyGlobalStatus->bBatterySleeping = false;
}


/******************************************************************************
* PUBLIC vSetBatteryOperatingMode()
*------------------------------------------------------------------------------
* Sets the battery operating mode
*
* Inputs:
*   eNewBatOperatingMode - Operating Mode Enum
* Returns: None
*
*******************************************************************************/
void vSetBatteryOperatingMode(eBatteryOperatingMode_t eNewBatOperatingMode)
{
    sstGlobalStatus.eBatteryOperatingMode = eNewBatOperatingMode;
}


/******************************************************************************
* PUBLIC dGetBatteryOperatingMode()
*------------------------------------------------------------------------------
* Sets the battery operating mode
*
* Inputs:
*   eNewBatOperatingMode - Operating Mode Enum
* Returns: None
*
*******************************************************************************/
eBatteryOperatingMode_t eGetBatteryOperatingMode(void)
{
    return(sstGlobalStatus.eBatteryOperatingMode);
}


/******************************************************************************
* PUBLIC vGetRefToGlobalStatusFlags()
*------------------------------------------------------------------------------
* Returns a reference (pointer) to the structure that contains the global status
* flags.
*
* Inputs: None
* Returns:
*   Pointer to sGlobalStatus structure
*
*******************************************************************************/
stGlobalStatus_t * stGetRefToGlobalStatusFlags(void)
{
    return(&sstGlobalStatus);
}


/******************************************************************************
* PUBLIC vBatteryGoToSleep()
*------------------------------------------------------------------------------
* Turns off peripherals (temp sensors, led controller, etc.) and switches the
* microprocessor to sleep mode.
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
void vBatteryGoToSleep(void)
{
    /* Make sure this routing is not called more than once */
    if(sstGlobalStatus.bBatterySleeping == false)
    {
        /* Set sleeping flag */
        sstGlobalStatus.bBatterySleeping = true;

        /* Check if the RTOS is running, suspend all tasks before going to sleep */
        if(sstGlobalStatus.bRtosStarted == true)
        {
            vTaskSuspendAll();
        }

        /* Disable interrupts, unwanted wake up sources */
        system_interrupt_disable(USB_IRQn);
        system_interrupt_disable(SERCOM3_IRQn);
        system_interrupt_disable(SERCOM4_IRQn);

        /* Command LED driver to go to sleep */
        vLEDDriverSleep();
        delay_ms(10);

        /* Command Temperature Sensors to go to sleep */
        vTempSensorsShutdown();
        delay_ms(10);

        /* Disable the switched 12V rail */
        OUTPUT_SET(SLEEP_PWDN_12V_COMP_PIN,SLEEP_PWDN_12V_COMP_ACTIVE);

        /* Set processor in deep sleep (STANDBY) */
        sleepmgr_enter_sleep();
    }
}


/******************************************************************************
* PUBLIC vBatteryWakeUp()
*------------------------------------------------------------------------------
* Turns ON peripherals (temp sensors, led controller, etc.) and switches the
* microprocessor from sleep mode to normal operating mode.
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
void vBatteryWakeUp(void)
{
    /* Make sure this routine is not called more than once */
    if(sstGlobalStatus.bBatterySleeping == true)
    {
        /* Clear sleeping flag */
        sstGlobalStatus.bBatterySleeping = false;

        /* Short delay after waking up to allow clocks to stabilize */
        delay_ms(100);

        /* Wake up LED Driver */
        vLEDDriverWakeup();
        delay_ms(10);

        /* Wake up Temperature Sensors */
        vTempSensorsWakeUp();
        delay_ms(10);

        /* Enable the switched 12V rail */
        OUTPUT_SET(SLEEP_PWDN_12V_COMP_PIN,SLEEP_PWDN_12V_COMP_INACTIVE);

        /* Command network voltage monitor to go to sleep */
        /* ushIna220_WakeUp(INA220_ADDRESS); */
        /* delay_ms(10); */

        /* Enable previously disabled interrupts */
        system_interrupt_enable(USB_IRQn);
        system_interrupt_enable(SERCOM3_IRQn);
        system_interrupt_enable(SERCOM4_IRQn);

        /* If RTOS was running before sleeping, resume all tasks */
        if(sstGlobalStatus.bRtosStarted == true)
        {
            xTaskResumeAll();
        }
    }
}
