#ifndef MAIN_H_
#define MAIN_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc and can only be         */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: main.h                                                         */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for main.c, which provides prototypes for the                 */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#include <sleepmgr.h>

/* Custom types */
typedef enum
{
    UNDEFINED_MODE = 0,
    NORMAL_MODE,
    TEST_MODE,  
}eBatteryOperatingMode_t;

typedef struct
{
    bool bRtosStarted;
    bool bBatterySleeping;
    eBatteryOperatingMode_t eBatteryOperatingMode;  
}stGlobalStatus_t;


/******************************************************************************
* PUBLIC vSetBatteryOperatingMode()
*------------------------------------------------------------------------------
* Sets the battery operating mode
*
* Inputs:
*   eNewBatOperatingMode - Operating Mode Enum
* Returns: None
*
*******************************************************************************/
void vSetBatteryOperatingMode(eBatteryOperatingMode_t eNewBatOperatingMode);


/******************************************************************************
* PUBLIC dGetBatteryOperatingMode()
*------------------------------------------------------------------------------
* Sets the battery operating mode
*
* Inputs:
*   eNewBatOperatingMode - Operating Mode Enum
* Returns: None
*
*******************************************************************************/
eBatteryOperatingMode_t eGetBatteryOperatingMode(void);


/******************************************************************************
* PUBLIC stGetRefToGlobalStatusFlags()
*------------------------------------------------------------------------------
* Returns a reference (pointer) to the structure that contains the global status
* flags.
*
* Inputs: None
* Returns:
*   Pointer to sGlobalStatus structure
*
*******************************************************************************/
stGlobalStatus_t * stGetRefToGlobalStatusFlags(void);


/******************************************************************************
* PUBLIC vBatteryGoToSleep()
*------------------------------------------------------------------------------
* Turns off peripherals (temp sensors, led controller, etc.) and switches the
* microprocessor to sleep mode.
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
void vBatteryGoToSleep(void);


/******************************************************************************
* PUBLIC vBatteryWakeUp()
*------------------------------------------------------------------------------
* Turns ON peripherals (temp sensors, led controller, etc.) and switches the
* microprocessor from sleep mode to normal operating mode.
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
void vBatteryWakeUp(void);


/******************************************************************************
* PUBLIC vInitializePeripherals1()
*------------------------------------------------------------------------------
* Initialize peripherals before putting the system to sleep
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
void vInitializePeripherals1(void);


/******************************************************************************
* PUBLIC vInitializePeripherals2()
*------------------------------------------------------------------------------
* Initialize peripherals after waking up from initial sleep
*
* Inputs: None
* Returns: None
*
*******************************************************************************/
void vInitializePeripherals2(void);


#endif /* MAIN_H_ */