/**
 * \file
 *
 * \brief USB configuration file for CDC application
 *
 * Copyright (c) 2009-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#ifndef _CONF_USB_H_
#define _CONF_USB_H_

#include "compiler.h"

/* Device definition */
#define  USB_DEVICE_VENDOR_ID           USB_VID_ATMEL
#define  USB_DEVICE_PRODUCT_ID          USB_PID_ATMEL_ASF_CDC
#define  USB_DEVICE_MAJOR_VERSION       1
#define  USB_DEVICE_MINOR_VERSION       0
#define  USB_DEVICE_POWER               100 // Consumption on Vbus line (mA)
#define  USB_DEVICE_ATTR                (USB_CONFIG_ATTR_BUS_POWERED)
/* (USB_CONFIG_ATTR_SELF_POWERED) */
/* (USB_CONFIG_ATTR_BUS_POWERED) */
/* (USB_CONFIG_ATTR_REMOTE_WAKEUP|USB_CONFIG_ATTR_SELF_POWERED) */
/* (USB_CONFIG_ATTR_REMOTE_WAKEUP|USB_CONFIG_ATTR_BUS_POWERED) */

/* USB Device string definitions*/
#define  USB_DEVICE_MANUFACTURE_NAME      "Saft saftbatteries.com powered by Atmel"
#define  USB_DEVICE_PRODUCT_NAME          "Evolion"
#define  USB_DEVICE_SERIAL_NAME           "Rackmount Evolion" /* should never use dynamic serial name, will lead to individual COM ports per module */

/* USB Device Callback definitions */
#define  UDC_VBUS_EVENT(bVBusHigh)        vCDCVBusCallback(bVBusHigh)
#define  UDC_SOF_EVENT()                  vCDCSOFCallback()
#define  UDC_SUSPEND_EVENT()              vCDCSuspendCallback()
#define  UDC_RESUME_EVENT()               vCDCResumeCallback()

/* Mandatory when USB_DEVICE_ATTR authorizes remote wakeup feature
#define  UDC_REMOTEWAKEUP_ENABLE()
#define  UDC_REMOTEWAKEUP_DISABLE() */

#define  UDC_SUSPEND_LPM_EVENT()            vCDCSuspendLPMCallback()
#define  UDC_REMOTEWAKEUP_LPM_ENABLE()      vCDCRemoteWakeupLPMEnableCallback()
#define  UDC_REMOTEWAKEUP_LPM_DISABLE()     vCDCRemoteWakeupLPMDisableCallback()

/* Configuration of CDC interface */

/* Number of communication port used (1 to 3) */
#define  UDI_CDC_PORT_NB 1

/* Interface callback definition */
#define  UDI_CDC_ENABLE_EXT(cPort)          bCDCEnableCallback(cPort)
#define  UDI_CDC_DISABLE_EXT(cPort)         vCDCDisableCallback(cPort)
#define  UDI_CDC_RX_NOTIFY(cPort)           vCDCRxNotifyCallback(cPort)
#define  UDI_CDC_TX_EMPTY_NOTIFY(cPort)     vCDCTxEmptyNotifyCallback(cPort)
#define  UDI_CDC_SET_CODING_EXT(cPort, stConfig)    vCDCConfigCallback(cPort, stConfig)
#define  UDI_CDC_SET_DTR_EXT(cPort, bEnable)        vCDCSetDTRCallback(cPort, bEnable)
#define  UDI_CDC_SET_RTS_EXT(cPort,bEnable)         vCDCSetRTSCallback(cPort, bEnable)

/* Define this when the transfer CDC Device to Host is a low rate (<512000 bauds) to reduce CDC buffers size */
#define  UDI_CDC_LOW_RATE

/* Default configuration of communication port */
#define  UDI_CDC_DEFAULT_RATE             115200
#define  UDI_CDC_DEFAULT_STOPBITS         CDC_STOP_BITS_1
#define  UDI_CDC_DEFAULT_PARITY           CDC_PAR_EVEN
#define  UDI_CDC_DEFAULT_DATABITS         8

//! The includes of classes and other headers must be done at the end of this file to avoid compilation errors
#include "udi_cdc_conf.h"
#include "iface_usb_cdc.h"

#endif // _CONF_USB_H_
