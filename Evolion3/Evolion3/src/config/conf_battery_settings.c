/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: conf_battery_settings.c                                        */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Declarations for all configuration and parameter variables used throughout*/
/* the program. All variables are stored in static positions within FLASH    */
/*                                                                           */
/*****************************************************************************/
#include <conf_battery_settings.h>


/* Configuration variables */
/* I^2t values to determine if we are at risk of exceeding fuse capabilities */
__attribute__((section(".cfg.const_uiCfgFastOverCurrentI2tValues")))    const uint32_t const_uiCfgFastOverCurrentI2tValues[I2T_ARRAY_LENGTH] = {1650000000, 825000000, 550000000, 412500000, 330000000, 275000000, 235714286, 206250000, 183333333, 165000000};


/* Current limit settings */
__attribute__((section(".cfg.const_shCfgMinTcellToCalculateCurrentLimits")))    const int16_t const_shCfgMinTcellToCalculateCurrentLimits = -300;   /* Minimum valid cell temperature to allow for current limits to be established */
__attribute__((section(".cfg.const_shCfgMaxTcellToCalculateCurrentLimits")))    const int16_t const_shCfgMaxTcellToCalculateCurrentLimits = 750;    /* Maximum valid cell temperature to allow for current limits to be established */
__attribute__((section(".cfg.const_ushCfgVcellToStartDecreasingIMD")))          const uint16_t const_ushCfgVcellToStartDecreasingIMD =      2600;   /* Cell voltage to start decreasing IMD */
__attribute__((section(".cfg.const_ushCfgVcellToInhibitDischarge")))            const uint16_t const_ushCfgVcellToInhibitDischarge =        2500;   /* Cell voltage to set IMD to 0 */
__attribute__((section(".cfg.const_ushCfgVcellToStartDecreasingIMR")))          const uint16_t const_ushCfgVcellToStartDecreasingIMR =      3950;   /* Cell voltage to start decreasing IMR */
__attribute__((section(".cfg.const_ushCfgVcellToInhibitCharge")))               const uint16_t const_ushCfgVcellToInhibitCharge =           4050;   /* Cell voltage to set IMR to 0 */

/* IMD lookup table, one value for each 0.5 degrees C from -30 to + 75 */
/* -30 through -20.5 */
/* -20 through -10.5 */
/* -10 through -0.5 */
/* 0 through 9.5 */
/* 10 through 19.5 */
/* 20 through 29.5 */
/* 30 through 39.5 */
/* 40 through 49.5 */
/* 50 through 54.5 */
/* 55 through 69.5 */
/* 70 through 75 */
__attribute__((section(".cfg.const_iCfgIMDLUT"))) const int32_t const_iCfgIMDLUT[TCELL_TO_CURRENT_LIMIT_ARRAY_LENGTH] = {
    0, -2444, -4888, -7333, -9777, -12222, -14666, -17111, -19555, -22000, -24444, -26888, -29333, -31777, -34222, -36666, -39111, -41555, -44000, -44000, \
    -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, \
    -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, \
    -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, \
    -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, \
    -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, \
    -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, \
    -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, \
    -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, -44000, \
    -44000, -42700, -41400, -40100, -38800, -37500, -36200, -34900, -33600, -32300, -31000, -29700, -28400, -27100, -25800, -24500, -23200, -21900, -20600, -19300, -18000, -16700, -15400, -14100, -12800, -11500, -10200, -8900, -7600, -6300, \
    -5000, -4500, -4000, -3500, -3000, -2500, -2000, -1500, -1000, -500, 0
};

/* IMR lookup table, one value for each 0.5 degrees C from -30 to + 75 */
/* -30 through -20.5 */
/* -20 through -10.5 */
/* -10 through -0.5 */
/* 0 through 9.5 */
/* 10 through 19.5 */
/* 20 through 29.5 */
/* 30 through 44.5 */
/* 45 through 54.5 */
/* 55 through 75 */
__attribute__((section(".cfg.const_iCfgIMRLUT"))) const int32_t const_iCfgIMRLUT[TCELL_TO_CURRENT_LIMIT_ARRAY_LENGTH] = {
    0, 400, 800, 1200, 1600, 2000, 2400, 2800, 3200, 3600, 4000, 4400, 4800, 5200, 5600, 6000, 6400, 6800, 7200, 7600, \
    8000, 8200, 8400, 8600, 8800, 9000, 9200, 9400, 9600, 9800, 10000, 10200, 10400, 10600, 10800, 11000, 11200, 11400, 11600, 11800, \
    12000, 12250, 12500, 12750, 13000, 13250, 13500, 13750, 14000, 14250, 14500, 14750, 15000, 15250, 15500, 15750, 16000, 16250, 16500, 16750, \
    17000, 17350, 17700, 18050, 18400, 18750, 19100, 19450, 19800, 20150, 20500, 20850, 21200, 21550, 21900, 22250, 22600, 22950, 23300, 23650, \
    24000, 24400, 24800, 25200, 25600, 26000, 26400, 26800, 27200, 27600, 28000, 28400, 28800, 29200, 29600, 30000, 30400, 30800, 31200, 31600, \
    32000, 32500, 33000, 33500, 34000, 34500, 35000, 35500, 36000, 36500, 37000, 37500, 38000, 38500, 39000, 39500, 40000, 40500, 41000, 41500, \
    42000, 42066, 42133, 42200, 42266, 42333, 42400, 42466, 42533, 42600, 42666, 42733, 42800, 42866, 42933, 43000, 43066, 43133, 43200, 43266, 43333, 43400, 43466, 43533, 43600, 43666, 43733, 43800, 43866, 43933, \
    44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, 44000, \
    44000, 42900, 41800, 40700, 39600, 38500, 37400, 36300, 35200, 34100, 33000, 31900, 30800, 29700, 28600, 27500, 26400, 25300, 24200, 23100, 22000, 20900, 19800, 18700, 17600, 16500, 15400, 14300, 13200, 12100, 11000, 9900, 8800, 7700, 6600, 5500, 4400, 3300, 2200, 1100, 0
};


/* Current averaging setting */
__attribute__((section(".cfg.const_ucCfgNbCurrentLimitSamplesToAvg")))  const uint8_t const_ucCfgNbCurrentLimitSamplesToAvg = 10;


/* Current averaging setting */
__attribute__((section(".cfg.const_ucCfgNbCurrentSamplesToAvg")))   const uint8_t const_ucCfgNbCurrentSamplesToAvg = 10;


/* Temperature averaging setting */
__attribute__((section(".cfg.const_ucCfgNbTemperatureSamplesToAvg")))   const uint8_t const_ucCfgNbTemperatureSamplesToAvg = 10;


/* SOC settings */
__attribute__((section(".cfg.const_ushCfgCellVOneHundredSOC"))) const uint16_t const_ushCfgCellVOneHundredSOC = 4000;   /* Cell OCV corresponding to 100% SOC */
__attribute__((section(".cfg.const_ushCfgCellVZeroSOC")))       const uint16_t const_ushCfgCellVZeroSOC =       3321;   /* Cell OCV corresponding to 0% SOC */
__attribute__((section(".cfg.const_ushCfgSOCMaximum")))         const uint16_t const_ushCfgSOCMaximum =         1000;   /* Maximum possible SOC */
__attribute__((section(".cfg.const_ushCfgSOCMinimum")))         const uint16_t const_ushCfgSOCMinimum =         0;      /* Minimum possible SOC */
__attribute__((section(".cfg.const_ushCfgSOCFullyCharged")))    const uint16_t const_ushCfgSOCFullyCharged =    998;    /* SOC to consider the battery as fully charged so that we don't sit forever at 99.9% */
__attribute__((section(".cfg.const_ushCfgSOCFullyDischarged"))) const uint16_t const_ushCfgSOCFullyDischarged = 2;      /* SOC to consider the battery as fully discharged */
__attribute__((section(".cfg.const_ushCfgSOCRateOfChangeMax"))) const uint16_t const_ushCfgSOCRateOfChangeMax = 1;      /* Maximum rate of change of SOC between two updates (2 seconds between updates as set by const_ucCfgNbSOCSamplesToAvg =  20) 2 = 0.2% */
__attribute__((section(".cfg.const_ucCfgNbSOCSamplesToAvg")))   const uint8_t const_ucCfgNbSOCSamplesToAvg =    20;     /* Number of SOC samples to accumulate and then average */

/* SOC lookup table, each line is a range of 2% SOC */
__attribute__((section(".cfg.const_ushCfgSOCLUT"))) const uint16_t const_ushCfgSOCLUT[VCELL_TO_SOC_ARRAY_LENGTH] = {  0, 3, 5, 8, 10, 12, 14, 16, 18, \
    20, 22, 24, 26, 28, 30, 32, 34, 36, 38, \
    40, 42, 44, 46, 48, 50, 51, 53, 54, 55, 56, 58, 59, \
    60, 61, 63, 64, 65, 66, 68, 69, 70, 71, 73, 74, 75, 76, 78, 79, \
    80, 81, 83, 84, 85, 86, 88, 89, 90, 91, 92, 93, 94, 96, 97, 98, 99, \
    100, 101, 102, 103, 104, 105, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 115, 116, 117, 118, 119, \
    120, 121, 122, 123, 123, 124, 125, 126, 127, 128, 128, 129, 130, 131, 132, 133, 134, 135, 135, 136, 137, 138, 139, \
    140, 141, 142, 143, 143, 144, 145, 146, 147, 148, 148, 149, 150, 151, 152, 153, 154, 156, 157, 158, 159, \
    160, 161, 162, 163, 164, 166, 167, 168, 169, 170, 171, 172, 173, 174, 176, 177, 178, 179, \
    180, 181, 182, 183, 184, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, \
    200, 201, 203, 204, 206, 207, 209, 210, 211, 213, 214, 216, 217, 219, \
    220, 221, 223, 224, 226, 227, 229, 230, 231, 233, 234, 236, 237, 239, \
    240, 241, 243, 244, 246, 247, 249, 250, 252, 254, 256, 258, \
    260, 262, 264, 266, 268, 270, 272, 274, 276, 278, \
    280, 282, 284, 286, 288, 290, 292, 293, 295, 297, 298, \
    300, 303, 307, 310, 313, 315, 318, \
    320, 323, 325, 328, 330, 333, 335, 338, \
    340, 343, 345, 348, 350, 353, 357, \
    360, 363, 365, 368, 370, 373, 375, 378, \
    380, 383, 385, 388, 390, 393, 395, 398, \
    400, 403, 407, 410, 413, 415, 418, \
    420, 423, 427, 430, 433, 435, 438, \
    440, 443, 445, 448, 450, 453, 455, 458, \
    460, 463, 465, 468, 470, 473, 475, 478, \
    480, 483, 485, 488, 490, 493, 495, 498, \
    500, 503, 505, 508, 510, 512, 514, 516, 518, \
    520, 522, 524, 526, 528, 530, 532, 534, 536, 538, \
    540, 542, 544, 546, 548, 550, 552, 553, 555, 557, 558, \
    560, 562, 563, 565, 567, 568, 570, 571, 573, 574, 576, 577, 579, \
    580, 582, 583, 585, 587, 588, 590, 591, 593, 594, 596, 597, 599, \
    600, 601, 602, 603, 604, 606, 607, 608, 609, 610, 611, 612, 613, 614, 616, 617, 618, 619, \
    620, 621, 622, 623, 624, 626, 627, 628, 629, 630, 631, 632, 633, 634, 636, 637, 638, 639, \
    640, 641, 642, 643, 644, 645, 646, 647, 648, 649, 650, 651, 652, 653, 654, 656, 657, 658, 659, \
    660, 661, 662, 663, 664, 666, 667, 668, 669, 670, 671, 672, 673, 674, 675, 676, 677, 678, 679, \
    680, 681, 682, 683, 684, 686, 687, 688, 689, 690, 691, 692, 693, 694, 695, 696, 697, 698, 699, \
    700, 701, 703, 704, 706, 707, 709, 710, 711, 713, 714, 716, 717, 719, \
    720, 721, 723, 724, 725, 726, 728, 729, 730, 731, 733, 734, 736, 737, 739, \
    740, 741, 743, 744, 745, 746, 748, 749, 750, 752, 753, 755, 757, 758, \
    760, 762, 763, 765, 767, 768, 770, 772, 773, 775, 777, 778, \
    780, 782, 783, 785, 787, 788, 790, 791, 793, 794, 796, 797, 799, \
    800, 802, 803, 805, 807, 808, 810, 811, 813, 814, 816, 817, 819, \
    820, 821, 823, 824, 826, 827, 829, 830, 831, 833, 834, 836, 837, 839, \
    840, 841, 843, 844, 846, 847, 849, 850, 852, 853, 855, 857, 858, \
    860, 862, 863, 865, 867, 868, 870, 871, 873, 874, 876, 877, 879, \
    880, 882, 883, 885, 887, 888, 890, 891, 893, 894, 896, 897, 899, \
    900, 901, 903, 904, 906, 907, 909, 910, 911, 913, 914, 915, 916, 918, 919, \
    920, 921, 923, 924, 926, 927, 929, 930, 931, 933, 934, 935, 936, 938, 939, \
    940, 941, 943, 944, 945, 946, 948, 949, 950, 951, 952, 953, 954, 955, 956, 957, 958, 959, \
    960, 961, 962, 963, 964, 965, 965, 966, 967, 968, 969, 970, 971, 972, 973, 974, 975, 976, 977, 978, 979, \
    980, 981, 982, 983, 984, 985, 985, 986, 987, 988, 989, 990, 991, 992, 993, 994, 995, 995, 996, 997, 998, 999, 1000 };


/* Cell balancing configuration variables */
__attribute__((section(".cfg.const_ushCfgCellBalancingDuration")))      const uint16_t const_ushCfgCellBalancingDuration =      100;    /* 100 = 10s, set to 50 = 5s for testing */
__attribute__((section(".cfg.const_ushCfgCellBalancingDelta")))         const uint16_t const_ushCfgCellBalancingDelta =         20;     /* 20mV */
__attribute__((section(".cfg.const_ushCfgCellBalancingVoltageMin")))    const uint16_t const_ushCfgCellBalancingVoltageMin =    3500;   /* 3500 mV */
__attribute__((section(".cfg.const_ushCfgCellBalancingHysteresis")))    const uint16_t const_ushCfgCellBalancingHysteresis =    10;     /* 10mV */
__attribute__((section(".cfg.const_shCfgCellBalancingCellTMax")))       const int16_t const_shCfgCellBalancingCellTMax =        750;    /* 75 degC */
__attribute__((section(".cfg.const_shCfgCellBalancingElecTMax")))       const int16_t const_shCfgCellBalancingElecTMax =        850;    /* 85 degC */


/* Electronics Over-temp Fault Settings */
__attribute__((section(".cfg.const_shCfgElectronicsOverTempAlarmThreshold")))   const int16_t const_shCfgElectronicsOverTempAlarmThreshold =    800;    /* 80 degrees C */
__attribute__((section(".cfg.const_shCfgElectronicsOverTempWarningThreshold"))) const int16_t const_shCfgElectronicsOverTempWarningThreshold =  750;    /* 75 degrees C */
__attribute__((section(".cfg.const_shCfgElectronicsTempFaultHysteresis")))      const int16_t const_shCfgElectronicsTempFaultHysteresis =       100;    /* 10 degrees C */
__attribute__((section(".cfg.const_ushCfgElectronicsTempFaultDelay")))          const uint16_t const_ushCfgElectronicsTempFaultDelay =          10;     /* 1 second */

/* Battery SOC Fault Settings */
__attribute__((section(".cfg.const_ushCfgBatteryUnderSOCAlarmThreshold")))      const uint16_t const_ushCfgBatteryUnderSOCAlarmThreshold =      0;      /* 0 = 0% SOC */
__attribute__((section(".cfg.const_ushCfgBatteryUnderSOCWarningThreshold")))    const uint16_t const_ushCfgBatteryUnderSOCWarningThreshold =    100;    /* 100 = 10% SOC */
__attribute__((section(".cfg.const_ushCfgBatteryOverSOCAlarmThreshold")))       const uint16_t const_ushCfgBatteryOverSOCAlarmThreshold =       1000;   /* 1000 = 100% SOC */
__attribute__((section(".cfg.const_ushCfgBatteryOverSOCWarningThreshold")))     const uint16_t const_ushCfgBatteryOverSOCWarningThreshold =     1000;   /* 1000 = 100% SOC */
__attribute__((section(".cfg.const_ushCfgBatterySOCFaultHysteresis")))          const uint16_t const_ushCfgBatterySOCFaultHysteresis =          10;     /* 10 = 1% SOC */
__attribute__((section(".cfg.const_ushCfgBatterySOCFaultDelay")))               const uint16_t const_ushCfgBatterySOCFaultDelay =               10;     /* 10 = 1 second */

/* Over Current Fault Settings */
__attribute__((section(".cfg.const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage")))     const uint16_t const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage =     120;    /*  120 % of IMD */
__attribute__((section(".cfg.const_ushCfgOverCurrentInDischargeWarningThresholdPercentage")))   const uint16_t const_ushCfgOverCurrentInDischargeWarningThresholdPercentage =   110;    /*  110% of IMD */
__attribute__((section(".cfg.const_shCfgOverCurrentInDischargeMinimumCurrent")))                const int16_t const_shCfgOverCurrentInDischargeMinimumCurrent =                 -200;   /* -2 A */
__attribute__((section(".cfg.const_ushCfgOverCurrentInChargeAlarmThresholdPercentage")))        const uint16_t const_ushCfgOverCurrentInChargeAlarmThresholdPercentage =        120;    /*  120 % of IMR */
__attribute__((section(".cfg.const_ushCfgOverCurrentInChargeWarningThresholdPercentage")))      const uint16_t const_ushCfgOverCurrentInChargeWarningThresholdPercentage =      110;    /*  110% of IMR */
__attribute__((section(".cfg.const_shCfgOverCurrentInChargeMinimumCurrent")))                   const int16_t const_shCfgOverCurrentInChargeMinimumCurrent =                    200;    /*  2 A */
__attribute__((section(".cfg.const_ushCfgOverCurrentFaultHysteresis")))                         const uint16_t const_ushCfgOverCurrentFaultHysteresis =                         5;      /*  5% */
__attribute__((section(".cfg.const_ushCfgOverCurrentFaultDelay")))                              const uint16_t const_ushCfgOverCurrentFaultDelay =                              300;    /*  30 seconds */
__attribute__((section(".cfg.const_ushCfgOverCurrentThresholdPercentageBase")))                 const uint16_t const_ushCfgOverCurrentThresholdPercentageBase =                 100;    /*  100% */

/* Over/Under Temp Fault Parameters */
__attribute__((section(".cfg.const_shCfgCellOverTempAlarmThreshold")))      const int16_t const_shCfgCellOverTempAlarmThreshold =       750;    /*  75 degrees C */
__attribute__((section(".cfg.const_shCfgCellOverTempWarningThreshold")))    const int16_t const_shCfgCellOverTempWarningThreshold =     500;    /*  50 degrees C */
__attribute__((section(".cfg.const_shCfgCellUnderTempAlarmThreshold")))     const int16_t const_shCfgCellUnderTempAlarmThreshold =      -300;   /* -30 degrees C */
__attribute__((section(".cfg.const_shCfgCellUnderTempWarningThreshold")))   const int16_t const_shCfgCellUnderTempWarningThreshold =    -100;   /* -10 degrees C */
__attribute__((section(".cfg.const_shCfgCellTempFaultHysteresis")))         const int16_t const_shCfgCellTempFaultHysteresis =          50;     /*  5  degrees C */
__attribute__((section(".cfg.const_ushCfgCellTempFaultDelay")))             const uint16_t const_ushCfgCellTempFaultDelay =             100;    /*  10 seconds */

/* Cell Over/Under Voltage Fault Parameters */
__attribute__((section(".cfg.const_ushCfgCellOverVoltageAlarmThreshold")))      const uint16_t const_ushCfgCellOverVoltageAlarmThreshold =      4150;   /*  4.15 volts */
__attribute__((section(".cfg.const_ushCfgCellOverVoltageWarningThreshold")))    const uint16_t const_ushCfgCellOverVoltageWarningThreshold =    4050;   /*  4.05 volts */
__attribute__((section(".cfg.const_ushCfgCellUnderVoltageAlarmThreshold")))     const uint16_t const_ushCfgCellUnderVoltageAlarmThreshold =     2200;   /*  2.2  volts */
__attribute__((section(".cfg.const_ushCfgCellUnderVoltageWarningThreshold")))   const uint16_t const_ushCfgCellUnderVoltageWarningThreshold =   2500;   /*  2.5  volts */
__attribute__((section(".cfg.const_ushCfgCellVoltageFaultHysteresis")))         const uint16_t const_ushCfgCellVoltageFaultHysteresis =         50;     /*  0.05 volts */
__attribute__((section(".cfg.const_ushCfgCellVoltageFaultDelay")))              const uint16_t const_ushCfgCellVoltageFaultDelay =              20;     /*  2 seconds */

/* Battery Over/Under Voltage Fault Parameters */
__attribute__((section(".cfg.const_ushCfgBatteryOverVoltageAlarmThreshold")))       const uint16_t const_ushCfgBatteryOverVoltageAlarmThreshold =       5740;   /*  57.40 V */
__attribute__((section(".cfg.const_ushCfgBatteryOverVoltageWarningThreshold")))     const uint16_t const_ushCfgBatteryOverVoltageWarningThreshold =     5670;   /*  56.70 V */
__attribute__((section(".cfg.const_ushCfgBatteryUnderVoltageAlarmThreshold")))      const uint16_t const_ushCfgBatteryUnderVoltageAlarmThreshold =      3780;   /*  37.80 V */
__attribute__((section(".cfg.const_ushCfgBatteryUnderVoltageWarningThreshold")))    const uint16_t const_ushCfgBatteryUnderVoltageWarningThreshold =    4200;   /*  42.00 V */
__attribute__((section(".cfg.const_ushCfgBatteryVoltageFaultHysteresis")))          const uint16_t const_ushCfgBatteryVoltageFaultHysteresis =          50;     /*  0.5 V */
__attribute__((section(".cfg.const_ushCfgBatteryVoltageFaultDelay")))               const uint16_t const_ushCfgBatteryVoltageFaultDelay =               50;     /*  5 s */

/* Cell Temp Difference Fault Parameters */
__attribute__((section(".cfg.const_shCfgCellTempDifferenceWarningThreshold")))  const int16_t const_shCfgCellTempDifferenceWarningThreshold =   100;    /*  10 degrees C */
__attribute__((section(".cfg.const_shCfgCellTempDifferenceFaultHysteresis")))   const int16_t const_shCfgCellTempDifferenceFaultHysteresis =    20;     /*  2  degrees C */
__attribute__((section(".cfg.const_ushCfgCellTempDifferenceFaultDelay")))       const uint16_t const_ushCfgCellTempDifferenceFaultDelay =       300;    /*  30 seconds */

/* Cell Voltage Imbalance Fault Parameters */
__attribute__((section(".cfg.const_ushCfgCellVoltageImbalanceWarningThreshold")))   const uint16_t const_ushCfgCellVoltageImbalanceWarningThreshold =   100;    /*  100 mV */
__attribute__((section(".cfg.const_ushCfgCellVoltageImbalanceFaultHysteresis")))    const uint16_t const_ushCfgCellVoltageImbalanceFaultHysteresis =    50;     /*  50 mV */
__attribute__((section(".cfg.const_ushCfgCellVoltageImbalanceFaultDelay")))         const uint16_t const_ushCfgCellVoltageImbalanceFaultDelay =         20;     /*  2 seconds */

/* Regulated Current Path Parameters */
__attribute__((section(".cfg.const_ushCfgVoltageLimit0")))      const uint16_t const_ushCfgVoltageLimit0=3873;  /* 38.73V */
__attribute__((section(".cfg.const_ushCfgVoltageLimit1")))      const uint16_t const_ushCfgVoltageLimit1=1260;  /* 12.60V */
__attribute__((section(".cfg.const_ushCfgVoltageLimit2_3")))    const uint16_t const_ushCfgVoltageLimit2_3=775; /* 7.75V */
__attribute__((section(".cfg.const_ushCfgVoltageLimit4_7")))    const uint16_t const_ushCfgVoltageLimit4_7=448; /* 4.48 */
__attribute__((section(".cfg.const_shCfgCurrentLimit0")))       const int16_t const_shCfgCurrentLimit0=52;      /* 0.52A */
__attribute__((section(".cfg.const_shCfgCurrentLimit1")))       const int16_t const_shCfgCurrentLimit1=175;     /* 1.75A */
__attribute__((section(".cfg.const_shCfgCurrentLimit2")))       const int16_t const_shCfgCurrentLimit2=269;     /* 2.69A */
__attribute__((section(".cfg.const_shCfgCurrentLimit3")))       const int16_t const_shCfgCurrentLimit3=366;     /* 3.66A */
__attribute__((section(".cfg.const_shCfgCurrentLimit4")))       const int16_t const_shCfgCurrentLimit4=454;     /* 4.54A */
__attribute__((section(".cfg.const_shCfgCurrentLimit5")))       const int16_t const_shCfgCurrentLimit5=510;     /* 5.10A */
__attribute__((section(".cfg.const_shCfgCurrentLimit6")))       const int16_t const_shCfgCurrentLimit6=604;     /* 6.04A */
__attribute__((section(".cfg.const_shCfgCurrentLimit7")))       const int16_t const_shCfgCurrentLimit7=660;     /* 6.60A */

/* Redundant Over Temp Parameters */
__attribute__((section(".cfg.const_cCfgRedundantOverTempElectronicsThreshold")))    const int8_t const_cCfgRedundantOverTempElectronicsThreshold =      85; /* 85 degrees C */
__attribute__((section(".cfg.const_cCfgRedundantOverTempElectronicsHysteresis")))   const int8_t const_cCfgRedundantOverTempElectronicsHysteresis =     80; /* 80 degrees C */
__attribute__((section(".cfg.const_cCfgRedundantOverTempCellThreshold")))           const int8_t const_cCfgRedundantOverTempCellThreshold =             85; /* 85 degrees C */
__attribute__((section(".cfg.const_cCfgRedundantOverTempCellHysteresis")))          const int8_t const_cCfgRedundantOverTempCellHysteresis =            80; /* 80 degrees C */

/* Self Test Parameters */
__attribute__((section(".cfg.const_fCfgCellVoltageChannelErrorMin")))                           const float const_fCfgCellVoltageChannelErrorMin =                              -32.59999847412109375;      /* -32.6 mV */
__attribute__((section(".cfg.const_fCfgCellVoltageChannelErrorMax")))                           const float const_fCfgCellVoltageChannelErrorMax =                              12.6000003814697265625;     /* 12.6 mV */
__attribute__((section(".cfg.const_ushCfgCellTerminalOpenDetectionErrorMax")))                  const uint16_t const_ushCfgCellTerminalOpenDetectionErrorMax =                  150;    /* 150 mV */
__attribute__((section(".cfg.const_ushCfgCellTerminalOpenDetectionBaselineDiscrepancyMax")))    const uint16_t const_ushCfgCellTerminalOpenDetectionBaselineDiscrepancyMax =    500;    /* 500 mV */
__attribute__((section(".cfg.const_shCfgCellTerminalLeakageErrorMax")))                         const int16_t const_shCfgCellTerminalLeakageErrorMax =                          30;     /* 30 mV */
__attribute__((section(".cfg.const_shCfgCurrentSenseAmplifierShortedErrorMax")))                const int16_t const_shCfgCurrentSenseAmplifierShortedErrorMax =                 372;    /* 37.2 uV */
__attribute__((section(".cfg.const_ushCfgVRefDiagMin")))                                        const uint16_t const_ushCfgVRefDiagMin =                                        1230;   /* 123.0 mV */
__attribute__((section(".cfg.const_ushCfgVRefDiagMax")))                                        const uint16_t const_ushCfgVRefDiagMax =                                        1290;   /* 129.0 mV */
__attribute__((section(".cfg.const_iCfgStackToSumOfCellsCongruenceVoltageMin")))                const int32_t const_iCfgStackToSumOfCellsCongruenceVoltageMin =                 0;      /* 0 mV */
__attribute__((section(".cfg.const_iCfgStackToSumOfCellsCongruenceVoltageMax")))                const int32_t const_iCfgStackToSumOfCellsCongruenceVoltageMax =                 3000;   /* 3000 mV */
__attribute__((section(".cfg.const_fCfgSupplyVoltage3V3Min")))                                  const float const_fCfgSupplyVoltage3V3Min =                                     2.9700000286102294921875;   /* 3.3V - 10% = 2.97V */
__attribute__((section(".cfg.const_fCfgSupplyVoltage3V3Max")))                                  const float const_fCfgSupplyVoltage3V3Max =                                     3.63000011444091796875;     /* 3.3V + 10% = 3.63V */
__attribute__((section(".cfg.const_fCfgSupplyVoltage5VMin")))                                   const float const_fCfgSupplyVoltage5VMin =                                      4.5;                        /* 5V - 10% = 4.5V */
__attribute__((section(".cfg.const_fCfgSupplyVoltage5VMax")))                                   const float const_fCfgSupplyVoltage5VMax =                                      5.5;                        /* 5V + 10% = 5.5V */
__attribute__((section(".cfg.const_fCfgSupplyVoltage12VMin")))                                  const float const_fCfgSupplyVoltage12VMin =                                     10.80000019073486328125;    /* 12V - 10% = 10.8V */
__attribute__((section(".cfg.const_fCfgSupplyVoltage12VMax")))                                  const float const_fCfgSupplyVoltage12VMax =                                     13.19999980926513671875;    /* 12V + 10% = 13.2V */

/* External Communication Parameters */
__attribute__((section(".param.const_ucParExternalCommSlaveId")))     const uint8_t  const_ucParExternalCommSlaveId =     (uint8_t)1;                          /* use 1-247 value */
__attribute__((section(".param.const_ucParExternalCommBaudRate")))    const uint8_t  const_ucParExternalCommBaudRate =    (uint8_t)EXTCOMM_BAUD_RATE_115200;   /* use eExtCommsBaudRate values */
__attribute__((section(".param.const_ucParExternalCommParity")))      const uint8_t  const_ucParExternalCommParity =      (uint8_t)EXTCOMM_PARITY_NONE;        /* use eExtCommsParity values */     /* normal default is EXTCOMM_PARITY_EVEN, NONE is used for now in order to not break compatibility with supplier board checkout tests */
__attribute__((section(".param.const_ucParExternalCommStopBits")))    const uint8_t  const_ucParExternalCommStopBits =    (uint8_t)EXTCOMM_STOP_BITS_1;        /* use eExtCommsStopBits values */
__attribute__((section(".param.const_ucParExternalCommType")))        const uint8_t  const_ucParExternalCommType =        (uint8_t)EXTCOMM_TYPE_LEGACY;        /* use eExtCommsType values */
