#ifndef CONF_BATTERY_SETTINGS_H_
#define CONF_BATTERY_SETTINGS_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: conf_battery_settings.h                                        */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: Nicolas Aumetre                                            */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for all configuration and parameter variables used throughout */
/* the program. All declarations are stored in static positions within FLASH */
/*                                                                           */
/*****************************************************************************/
#include <iface_rtos_start.h>
#include <current_limits.h>
#include <SOC_calculation.h>


/* Configuration variables */
/* I^2t values to determine if we are at risk of exceeding fuse capabilities */
extern const uint32_t const_uiCfgFastOverCurrentI2tValues[I2T_ARRAY_LENGTH];


/* Current limit settings */
extern const int16_t const_shCfgMinTcellToCalculateCurrentLimits;   /* Minimum valid cell temperature to allow for current limits to be established */
extern const int16_t const_shCfgMaxTcellToCalculateCurrentLimits;   /* Maximum valid cell temperature to allow for current limits to be established */
extern const uint16_t const_ushCfgVcellToStartDecreasingIMD;        /* Cell voltage to start decreasing IMD */
extern const uint16_t const_ushCfgVcellToInhibitDischarge;          /* Cell voltage to set IMD to 0 */
extern const uint16_t const_ushCfgVcellToStartDecreasingIMR;        /* Cell voltage to start decreasing IMR */
extern const uint16_t const_ushCfgVcellToInhibitCharge;             /* Cell voltage to set IMR to 0 */

/* IMD lookup table, one value for each 0.5 degrees C from -30 to + 75 */
/* -30 through -20.5 */
/* -20 through -10.5 */
/* -10 through -0.5 */
/* 0 through 9.5 */
/* 10 through 19.5 */
/* 20 through 29.5 */
/* 30 through 39.5 */
/* 40 through 49.5 */
/* 50 through 54.5 */
/* 55 through 69.5 */
/* 70 through 75 */
extern const int32_t const_iCfgIMDLUT[TCELL_TO_CURRENT_LIMIT_ARRAY_LENGTH];

/* IMR lookup table, one value for each 0.5 degrees C from -30 to + 75 */
/* -30 through -20.5 */
/* -20 through -10.5 */
/* -10 through -0.5 */
/* 0 through 9.5 */
/* 10 through 19.5 */
/* 20 through 29.5 */
/* 30 through 44.5 */
/* 45 through 54.5 */
/* 55 through 75 */
extern const int32_t const_iCfgIMRLUT[TCELL_TO_CURRENT_LIMIT_ARRAY_LENGTH];


/* Current averaging setting */
extern const uint8_t const_ucCfgNbCurrentLimitSamplesToAvg;


/* Current averaging setting */
extern const uint8_t const_ucCfgNbCurrentSamplesToAvg;


/* Temperature averaging setting */
extern const uint8_t const_ucCfgNbTemperatureSamplesToAvg;


/* SOC settings */
extern const uint16_t const_ushCfgCellVOneHundredSOC;   /* Cell OCV corresponding to 100% SOC */
extern const uint16_t const_ushCfgCellVZeroSOC;         /* Cell OCV corresponding to 0% SOC */
extern const uint16_t const_ushCfgSOCMaximum;           /* Maximum possible SOC */
extern const uint16_t const_ushCfgSOCMinimum;           /* Minimum possible SOC */
extern const uint16_t const_ushCfgSOCFullyCharged;      /* SOC to consider the battery as fully charged so that we don't sit forever at 99.9% */
extern const uint16_t const_ushCfgSOCFullyDischarged;   /* SOC to consider the battery as fully discharged */
extern const uint16_t const_ushCfgSOCRateOfChangeMax;   /* Maximum rate of change of SOC between two updates (3 seconds between updates as set by NUM_OF_SOC_TO_AVERAGE 30) 2= 0.2% */
extern const uint8_t const_ucCfgNbSOCSamplesToAvg;      /* Number of SOC samples to accumulate and then average */

/* SOC lookup table, each line is a range of 2% SOC */
extern const uint16_t const_ushCfgSOCLUT[VCELL_TO_SOC_ARRAY_LENGTH];


/* Cell balancing configuration variables */
extern const uint16_t const_ushCfgCellBalancingDuration;
extern const uint16_t const_ushCfgCellBalancingDelta;
extern const uint16_t const_ushCfgCellBalancingVoltageMin;
extern const uint16_t const_ushCfgCellBalancingHysteresis;
extern const int16_t const_shCfgCellBalancingCellTMax;
extern const int16_t const_shCfgCellBalancingElecTMax;


/* Electronics Over-temp Fault Settings */
extern const int16_t const_shCfgElectronicsOverTempAlarmThreshold;
extern const int16_t const_shCfgElectronicsOverTempWarningThreshold;
extern const int16_t const_shCfgElectronicsTempFaultHysteresis;
extern const uint16_t const_ushCfgElectronicsTempFaultDelay;

/* Battery SOC Fault Settings */
extern const uint16_t const_ushCfgBatteryUnderSOCAlarmThreshold;
extern const uint16_t const_ushCfgBatteryUnderSOCWarningThreshold;
extern const uint16_t const_ushCfgBatteryOverSOCAlarmThreshold;
extern const uint16_t const_ushCfgBatteryOverSOCWarningThreshold;
extern const uint16_t const_ushCfgBatterySOCFaultHysteresis;
extern const uint16_t const_ushCfgBatterySOCFaultDelay;

/* Over Current Fault Settings */
extern const uint16_t const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage;
extern const uint16_t const_ushCfgOverCurrentInDischargeWarningThresholdPercentage;
extern const int16_t const_shCfgOverCurrentInDischargeMinimumCurrent;
extern const uint16_t const_ushCfgOverCurrentInChargeAlarmThresholdPercentage;
extern const uint16_t const_ushCfgOverCurrentInChargeWarningThresholdPercentage;
extern const int16_t const_shCfgOverCurrentInChargeMinimumCurrent;
extern const uint16_t const_ushCfgOverCurrentFaultHysteresis;
extern const uint16_t const_ushCfgOverCurrentFaultDelay;
extern const uint16_t const_ushCfgOverCurrentThresholdPercentageBase;

/* Over/Under Temp Fault Parameters */
extern const int16_t const_shCfgCellOverTempAlarmThreshold;
extern const int16_t const_shCfgCellOverTempWarningThreshold;
extern const int16_t const_shCfgCellUnderTempAlarmThreshold;
extern const int16_t const_shCfgCellUnderTempWarningThreshold;
extern const int16_t const_shCfgCellTempFaultHysteresis;
extern const uint16_t const_ushCfgCellTempFaultDelay;

/* Cell Over/Under Voltage Fault Parameters */
extern const uint16_t const_ushCfgCellOverVoltageAlarmThreshold;
extern const uint16_t const_ushCfgCellOverVoltageWarningThreshold;
extern const uint16_t const_ushCfgCellUnderVoltageAlarmThreshold;
extern const uint16_t const_ushCfgCellUnderVoltageWarningThreshold;
extern const uint16_t const_ushCfgCellVoltageFaultHysteresis;
extern const uint16_t const_ushCfgCellVoltageFaultDelay;

/* Battery Over/Under Voltage Fault Parameters */
extern const uint16_t const_ushCfgBatteryOverVoltageAlarmThreshold;
extern const uint16_t const_ushCfgBatteryOverVoltageWarningThreshold;
extern const uint16_t const_ushCfgBatteryUnderVoltageAlarmThreshold;
extern const uint16_t const_ushCfgBatteryUnderVoltageWarningThreshold;
extern const uint16_t const_ushCfgBatteryVoltageFaultHysteresis;
extern const uint16_t const_ushCfgBatteryVoltageFaultDelay;

/* Cell Temp Difference Fault Parameters */
extern const int16_t const_shCfgCellTempDifferenceWarningThreshold;
extern const int16_t const_shCfgCellTempDifferenceFaultHysteresis;
extern const uint16_t const_ushCfgCellTempDifferenceFaultDelay;

/* Cell Voltage Imbalance Fault Parameters */
extern const uint16_t const_ushCfgCellVoltageImbalanceWarningThreshold;
extern const uint16_t const_ushCfgCellVoltageImbalanceFaultHysteresis;
extern const uint16_t const_ushCfgCellVoltageImbalanceFaultDelay;

/* Regulated Current Path Parameters */
extern const uint16_t const_ushCfgVoltageLimit0;
extern const uint16_t const_ushCfgVoltageLimit1;
extern const uint16_t const_ushCfgVoltageLimit2_3;
extern const uint16_t const_ushCfgVoltageLimit4_7;
extern const int16_t const_shCfgCurrentLimit0;
extern const int16_t const_shCfgCurrentLimit1;
extern const int16_t const_shCfgCurrentLimit2;
extern const int16_t const_shCfgCurrentLimit3;
extern const int16_t const_shCfgCurrentLimit4;
extern const int16_t const_shCfgCurrentLimit5;
extern const int16_t const_shCfgCurrentLimit6;
extern const int16_t const_shCfgCurrentLimit7;

/* Redundant Over Temp Parameters */
extern const int8_t const_cCfgRedundantOverTempElectronicsThreshold;
extern const int8_t const_cCfgRedundantOverTempElectronicsHysteresis;
extern const int8_t const_cCfgRedundantOverTempCellThreshold;
extern const int8_t const_cCfgRedundantOverTempCellHysteresis;

/* Self Test Parameters */
extern const float const_fCfgCellVoltageChannelErrorMin;
extern const float const_fCfgCellVoltageChannelErrorMax;
extern const uint16_t const_ushCfgCellTerminalOpenDetectionErrorMax;
extern const uint16_t const_ushCfgCellTerminalOpenDetectionBaselineDiscrepancyMax;
extern const int16_t const_shCfgCellTerminalLeakageErrorMax;
extern const int16_t const_shCfgCurrentSenseAmplifierShortedErrorMax;
extern const uint16_t const_ushCfgVRefDiagMin;
extern const uint16_t const_ushCfgVRefDiagMax;
extern const int32_t const_iCfgStackToSumOfCellsCongruenceVoltageMin;
extern const int32_t const_iCfgStackToSumOfCellsCongruenceVoltageMax;
extern const float const_fCfgSupplyVoltage3V3Min;
extern const float const_fCfgSupplyVoltage3V3Max;
extern const float const_fCfgSupplyVoltage5VMin;
extern const float const_fCfgSupplyVoltage5VMax;
extern const float const_fCfgSupplyVoltage12VMin;
extern const float const_fCfgSupplyVoltage12VMax;

typedef enum {
    EXTCOMM_BAUD_RATE_9600 = 0,
    EXTCOMM_BAUD_RATE_19200 = 1,
    EXTCOMM_BAUD_RATE_57600 = 2,
    EXTCOMM_BAUD_RATE_115200 = 3
}eExtCommsBaudRate;

typedef enum {
    EXTCOMM_PARITY_NONE = 0,
    EXTCOMM_PARITY_ODD = 1,
    EXTCOMM_PARITY_EVEN = 2
}eExtCommsParity;

typedef enum {
    EXTCOMM_STOP_BITS_1 = 1,
    EXTCOMM_STOP_BITS_2 = 2
}eExtCommsStopBits;

typedef enum {
    EXTCOMM_TYPE_LEGACY = 1,
    EXTCOMM_TYPE_SINGLE_VOICE = 2
}eExtCommsType;

/* External Communication Parameters */
extern const uint8_t  const_ucParExternalCommSlaveId;   /* use 1-247 value */
extern const uint8_t  const_ucParExternalCommBaudRate;  /* use eExtCommsBaudRate values */
extern const uint8_t  const_ucParExternalCommParity;    /* use eExtCommsParity values */
extern const uint8_t  const_ucParExternalCommStopBits;  /* use eExtCommsStopBits values */
extern const uint8_t  const_ucParExternalCommType;      /* use eExtCommsType values */

#endif /* CONF_BATTERY_SETTINGS_H_ */
