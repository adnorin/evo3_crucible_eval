/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: init_rtos_start.c                                              */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <delay.h>
#include <asf.h>
#include <main.h>
#include <limits.h>
#include <iface_rtos_start.h>
#include <conf_usb.h>
#include <conf_gpio.h>
#include <bat_cell_controller_ic.h>
#include <iface_usart_dma.h>
#include <iface_i2c_dma.h>
#include <iface_spi_dma.h>
#include <hmi_button_read.h>
#include <hmi_controller.h>
#include <mbs.h>
#include <fet_controller.h>
#include <mbportlayer.h>
#include <mbutils.h>
#include <mbport.h>
#include <comm_external.h>
#include <comm_internal.h>
#include <current_meas.h>
#include <volt_meas.h>
#include <self_tests.h>
#include <cell_balancing.h>
#include <fets.h>
#include <heater.h>
#include <dry_contacts.h>
#include <eeprom_controller.h>
#include <temp_meas.h>
#include <heater_control.h>
#include <SOC_calculation.h>
#include <current_limits.h>
#include <mb_data_handling.h>
#include <faults.h>
#include <current_meas.h>
#include <RamDataStorage.h>
#include <RomDataStorage.h>
#include <FreeRTOS.h>
#include <fet_management.h>
#include <RomDataStorage.h>
#include <gpio_outputs.h>
#include <gpio_inputs.h>
#include <gpio_controller.h>
#include <test_flash.h>


/********************************************/
/* Private Variables                        */
/********************************************/
static TaskHandle_t      spTaskPeriodic_10ms_Hdl  = NULL;
static TaskHandle_t      spTaskPeriodic_100ms_Hdl = NULL;
static TaskHandle_t      spTaskPeriodic_1S_Hdl    = NULL;
static TaskHandle_t      spTaskEvent_USART1_Hdl   = NULL;
static TaskHandle_t      spTaskEvent_USART2_Hdl   = NULL;
static TaskHandle_t      spTaskPeriodic_Modbus_Hdl= NULL;
static TaskHandle_t      spTaskEvent_Init_Hdl     = NULL;
static TaskHandle_t      spTaskEvent_SelfTests_Hdl= NULL;
static TaskHandle_t      spTaskPeriodic_EEPROM_Hdl= NULL;

static SemaphoreHandle_t spSemBinary_USB_Hdl;
static SemaphoreHandle_t spSemBinary_USB_Rx_Hdl;
static SemaphoreHandle_t spSemBinary_USB_Tx_Empty_Hdl;
static SemaphoreHandle_t spSemBinary_USART1_Hdl;
static SemaphoreHandle_t spSemBinary_USART2_Hdl;
static SemaphoreHandle_t spSemBinary_10ms_Hdl;
static SemaphoreHandle_t spSemMutex_EEPROM_Access_Hdl;
static SemaphoreHandle_t spSemBinary_I2C_Temp_Hdl;
static SemaphoreHandle_t spSemBinary_I2C_Led_Hdl;
static SemaphoreHandle_t spSemCount_SPI_EEPROM_Fault_Hdl;
static SemaphoreHandle_t spSemCount_SPI_BMSIC_Fault_Hdl;
static SemaphoreHandle_t spSemCount_I2C_TEMPSENSE_Fault_Hdl;
static SemaphoreHandle_t spSemCount_I2C_LEDCNTL_Fault_Hdl;
static SemaphoreHandle_t spSemBinary_MB_Start_Hdl;

static QueueHandle_t     spQueue_100ms_Hdl;
static QueueHandle_t     spQueue_Diag_Data_Hdl;
static QueueHandle_t     spQueue_I2C_Data_Hdl;
static QueueHandle_t     spQueue_FetStates_Hdl;
static QueueHandle_t     spQueue_HMI_Outputs_Hdl;
static QueueHandle_t     spQueue_USART1_Data_Hdl;
static QueueHandle_t     spQueue_USART2_Data_Hdl;
static QueueHandle_t     spQueue_Dry_Contacts_Hdl;
static QueueHandle_t     spQueue_Heater_Ctl_Hdl;
static QueueHandle_t     spQueue_Cell_Balancing_Hdl;
static QueueHandle_t     spQueue_Test_Mode_Request_Hdl;
static QueueHandle_t     spQueue_RT_Data_Collection_Hdl;
static QueueHandle_t     spQueue_Fault_and_Limits_Hdl;
static QueueHandle_t     spQueue_SelfTests_Hdl;
static QueueHandle_t     spQueue_EEPROM_Access_Hdl;
static QueueHandle_t     spQueue_GPIOStates_Hdl;


static stUsartData_t    stRxUSART1Buffer;   /* USART1 Rx Buffer */
static stUsartData_t    stRxUSART2Buffer;   /* USART2 Rx Buffer */

static unsigned long    ulClocksPer10thOfAMilliSecond = 0UL; /* Used in the run time stats calculations. */

UBaseType_t uxHighWaterMark10ms;
UBaseType_t uxHighWaterMark100ms;
UBaseType_t uxHighWaterMark1s;
UBaseType_t uxHighWaterMarkMB;

/********************************************/
/* Private Function Prototypes              */
/********************************************/
static void sCheckForTestModeRequest(void);
static void sCopyFastData(stBms_FastData_t * myBms_FastDataCpy, stBmsData_t * myBmsDataCpy);

/********************************************/
/* Private Functions                        */
/********************************************/
/*********************************************************************
* PRIVATE Task svPeriodicCritical5ms()
*---------------------------------------------------------------------
* Execute high priority critical operations
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svTaskPeriodic_10ms(void *p)
{
    static stBms_FastData_t sstBMS_FastData;
    static stFaultsandLimits_t sstFaultsandLimitsSaved;
    stFaultsandLimits_t sstFaultandLimitsNew;
    TickType_t xLastWakeTime;
    BaseType_t sStatus;
    static uint8_t uiTimeControl = 0;

    /* Wait for initialization task to complete */
    if (xSemaphoreTake(spSemBinary_10ms_Hdl, portMAX_DELAY))
    {
        xLastWakeTime = xTaskGetTickCount();
        while (1)
        {
            /*  Read Battery Management IC Data */
            vBatteryCellCtlInterface(&sstBMS_FastData, uiTimeControl);

            /*  Write Battery Management IC Data */
            vBatteryCellCtlInterfaceWrite(uiTimeControl);

            /* Calculate (I^2)*t and determine if it's too large */
            vDetermineIfI2tExceeded(&sstBMS_FastData);
       
            /* Execute code based on the operating mode */
            switch(eGetBatteryOperatingMode())
            {
                case NORMAL_MODE:
                /* FET State Machine Management */
                sStatus = xQueueReceive(spQueue_Fault_and_Limits_Hdl , &sstFaultandLimitsNew, NOWAIT);
                if( sStatus == pdPASS )
                {
                    /* New fault data received every 100ms, copy new data into the static fault data buffer */
                    sstFaultsandLimitsSaved.uiMesaDefFaults = sstFaultandLimitsNew.uiMesaDefFaults;
                    sstFaultsandLimitsSaved.uiVendDefFaults = sstFaultandLimitsNew.uiVendDefFaults;
                    sstFaultsandLimitsSaved.shChargeCurrentLimit = sstFaultandLimitsNew.shChargeCurrentLimit;
                    sstFaultsandLimitsSaved.shDischargeCurrentLimit = sstFaultandLimitsNew.shDischargeCurrentLimit;
                }
            
                vFetStateManager(&sstBMS_FastData, &sstFaultsandLimitsSaved);

                break;

                case TEST_MODE:
            
                /* --------------------------------- */
                /* --- Test mode only operations --- */
                /* --------------------------------- */

                break;

                default:
                break;
            }

            /* Process FET status */
            vManageFets();

            /* Synchronize 100ms Task and Time Counter */
            /*----------------------------------------------------------------------------------*/
            if(uiTimeControl == 3)
            {
                sStatus = xQueueSendToBack(spQueue_100ms_Hdl, &sstBMS_FastData, NOWAIT);
                if( sStatus != pdPASS )
                {
                    /* The send operation could not complete because the queue was full */
                }
            }

            if (++uiTimeControl >= TIME_CTL_10MS_TASK_100MS)
            {
                uiTimeControl = 0;
            }

            //uxHighWaterMark10ms = uxTaskGetStackHighWaterMark( NULL );

            /*----------------------------------------------------------------------------------*/
            /* Execute this task every 10ms including the time it takes to perform operations within */
            vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(TIME_10MS));
        }
    }        
}


/*********************************************************************
* PRIVATE Task svTaskPeriodicMedium_100ms()
*---------------------------------------------------------------------
* Execute Med priority operations
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svTaskPeriodic_100ms(void *p)
{
    BaseType_t sStatus;
    stBms_FastData_t sstBms_FastData_Rec;
    static stFaultsandLimits_t sstFaultsandLimits;
    static stBmsData_t sstBmsData_Rec;
    static uint16_t sushRTDC_Counter = 0;
    static uint8_t sucTimeControl = 0; /* Counter used to generate 1s battery heartbeat counter */
    static uint16_t sushBatteryHeartbeatLocal = 0;

    while (1)
    {
        /* Receive data from Queue, pend here until data is received */
        sStatus = xQueueReceive(spQueue_100ms_Hdl, &sstBms_FastData_Rec, portMAX_DELAY);
        if( sStatus != pdPASS )
        {
            /* Data was not received from the queue */
        }

        /* Copy BMS data from the 10ms buffer to the 100ms buffer */
        sCopyFastData(&sstBms_FastData_Rec, &sstBmsData_Rec);

        /* Read I2C temperature data and add to queue */
        vReadTemperatures(&sstBmsData_Rec);

        /* Read Input GPIOs */
        ucReadInputGPIOStatus(&sstBmsData_Rec);

        /* Read Input Buttons */
        vReadAndDebounceButtons(&sstBmsData_Rec);
        
        /* Calculate average current */
        vAverageCurrent(&sstBmsData_Rec);

        /* Update HMI (LEDs) Outputs */
        vManageHMI();

        /* Update Heater Status */
        vManageHeater();

        /* Update Dry Contact Status */
        vManageDryContacts();
		
		/* Process GPIO status */
		bManageGPIOs();
		
        /* Calculate SOC */
        vCalculateSOC(&sstBmsData_Rec);

        /* Calculate IMD and IMR */
        vCalculateIMDIMR(&sstBmsData_Rec);

        /* Detect faults - Move to Normal Mode */
        vFault_Handler(&sstBmsData_Rec);
              

        /* Increment 1s battery heartbeat */
        if (sucTimeControl == 0)
        {
            sushBatteryHeartbeatLocal++;
        }

        sstBmsData_Rec.ushBatteryHeartbeat = sushBatteryHeartbeatLocal;

        /* Queue diagnostic data from BMS */
        xQueueOverwrite(spQueue_Diag_Data_Hdl, &sstBmsData_Rec);

        /* Check for USB Modbus command to switch Operating Mode */
        sCheckForTestModeRequest();

        /********************************************/
        /* Execute code based on the operating mode */
        /********************************************/
        switch(eGetBatteryOperatingMode())
        {
            case NORMAL_MODE:

            /* Balance cells */
            vBalanceCells(&sstBmsData_Rec);

            /* Queue faults for FET Management in the 10ms Task - Move to Normal Mode */
            sstFaultsandLimits.uiMesaDefFaults = sstBmsData_Rec.uiFaultFlagsEvt1 & FLT_MESA_ALARM_MASK;
            sstFaultsandLimits.uiVendDefFaults = sstBmsData_Rec.uiFaultFlagsEvtVnd1 & FLT_VEND_ALARM_MASK;
            sstFaultsandLimits.shChargeCurrentLimit = sstBmsData_Rec.shChargeCurrentLimit;
            sstFaultsandLimits.shDischargeCurrentLimit = sstBmsData_Rec.shDischargeCurrentLimit;

            sStatus = xQueueSendToBack(spQueue_Fault_and_Limits_Hdl, &sstFaultsandLimits, NOWAIT);
            if( sStatus != pdPASS )
            {
                /* The send operation could not complete because the queue was full */
            }

            //* Update fault counters for continuous data logging */
            vDetectFaultActivity(&sstBmsData_Rec);

            /* Detect EOC and EOD update RTDC histogram data */
            vDetectEndOfChargeAndDischarge(&sstBmsData_Rec);

            /* Accumulate current for throughput calculations */
            vAccumulateAmpHrThroughput(&sstBmsData_Rec);

            break;

            case TEST_MODE:

            /* --------------------------------- */
            /* --- Test mode only operations --- */
            /* --------------------------------- */

            break;

            default:
            break;
        }

        /* Queue snapshot of data every second */
        sushRTDC_Counter = (sushRTDC_Counter + 1) % 10;
        if (sushRTDC_Counter == 0)
        {
             /* Queue Battery data for Real Time Data Collection */
             xQueueOverwrite(spQueue_RT_Data_Collection_Hdl, &sstBmsData_Rec);
        }

        /* Reset counter used to generate 1s increment for battery heartbeat */
        if (++sucTimeControl >= TIME_CTL_100MS_TASK_1000MS)
        {
            sucTimeControl = 0;
        }

        //uxHighWaterMark100ms = uxTaskGetStackHighWaterMark( NULL );
    }
}


/*********************************************************************
* PRIVATE Task svTaskPeriodic_1S()
*---------------------------------------------------------------------
* Execute Low priority operations
*
* Inputs:  None
* Returns: None
*
**********************************************************************/

static void svTaskPeriodic_1s(void *p)
{
    static uint16_t sushFiveMinCnt = 0;
    BaseType_t sStatus;
    stBmsData_t sstBmsData_RTDC;
    st_RTDataCollection_t * pst_RTDataCollection_lcl;

    while (1)
    {
        /* Receive data from Queue, pend here until data is received */
        sStatus = xQueueReceive(spQueue_RT_Data_Collection_Hdl, &sstBmsData_RTDC, portMAX_DELAY);
        if( sStatus != pdPASS )
        {
            /* Data was not received from the queue */
        }                  
                
        /* Execute code based on the operating mode */
        switch(eGetBatteryOperatingMode())
        {
            case NORMAL_MODE:
            
            /* Perform Real Time Data Collection */
            pst_RTDataCollection_lcl = vPerformRTDataCollection(&sstBmsData_RTDC);
        
            /* Start Operational Data Save if Faults are Present */
            vDataLoggingDueToFaults(&sstBmsData_RTDC);

            /* Debug only... this goes in NORMAL_MODE only */
            /* Write Real Time Data Collected to ROM Every 5 mins */
            sushFiveMinCnt = (sushFiveMinCnt + 1) % TIME_5MIN_1SEC_TASK_CNT;
            if(sushFiveMinCnt == 0)
            {
                vStartRoutineDataSaving(pst_RTDataCollection_lcl);
            }

            break;

            case TEST_MODE:

            /* --------------------------------- */
            /* --- Test mode only operations --- */
            /* --------------------------------- */

            break;

            default:
            break;
        }
    }
}


/*********************************************************************
* PRIVATE Task svTaskEvent_USART1()
*---------------------------------------------------------------------
* Execute event driven operations from Comm Port
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svTaskEvent_USART1(void *p)
{
    while (1)
    {
        if (xSemaphoreTake(spSemBinary_USART1_Hdl, portMAX_DELAY))
        {
            /* Write Usart1 data to queue */
            xQueueOverwrite(spQueue_USART1_Data_Hdl, &stRxUSART1Buffer);

            /* Disable RS485 transceiver receive driver and enable output driver */
            OUTPUT_SET(RS485_INT_TX_ENABLE_RX_DISABLE_PIN,RS485_INT_TX_ENABLE_RX_DISABLE_ACTIVE);

            /* Tx Data (Echo) back for debug/testing */
            vUsartIntCommTx();

            /* Wait a little bit so that the DMA can shuttle data to the peripheral and the peripheral can send it out */
            vTaskDelay(TIME_1MS);

            /* Enable RS485 transceiver receive driver and disable output driver */
            OUTPUT_SET(RS485_INT_TX_ENABLE_RX_DISABLE_PIN,RS485_INT_TX_ENABLE_RX_DISABLE_INACTIVE);

            /* Re-start Usart1 data Rx */
            vUsartIntCommRx();
        }
    }
}


/*********************************************************************
* PRIVATE Task svTaskEvent_USART2()
*---------------------------------------------------------------------
* Execute event driven operations from Comm Port
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svTaskEvent_USART2(void *p)
{
    while (1)
    {
        if (xSemaphoreTake(spSemBinary_USART2_Hdl, portMAX_DELAY))
        {
            /* Write Usart1 data to queue */
            xQueueOverwrite(spQueue_USART2_Data_Hdl, &stRxUSART2Buffer);

            /* Disable RS485 transceiver receive driver and enable output driver */
            OUTPUT_SET(RS485_EXT_TX_ENABLE_RX_DISABLE_PIN,RS485_EXT_TX_ENABLE_RX_DISABLE_ACTIVE);

            /* Tx Data (Echo) back for debug/testing */
            vUsartExtCommTx();

            /* Wait a little bit so that the DMA can shuttle data to the peripheral and the peripheral can send it out */
            vTaskDelay(TIME_1MS);

            /* Enable RS485 transceiver receive driver and disable output driver */
            OUTPUT_SET(RS485_EXT_TX_ENABLE_RX_DISABLE_PIN,RS485_EXT_TX_ENABLE_RX_DISABLE_INACTIVE);

            /* Re-start Usart2 data Rx */
            vUsartExtCommRx();
        }
    }
}


/*********************************************************************
* PRIVATE Task svTaskEvent_SelfTest()
*---------------------------------------------------------------------
* Execute on-demand self-tests task
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svTaskEvent_SelfTests(void *p)
{
    static bool sbAfterInitialization = true;
    uint32_t uiSelfTestFlags;

    while (1)
    {                 
        /* Blocked until self-tests requested */
        if (xQueueReceive(spQueue_SelfTests_Hdl, &uiSelfTestFlags, portMAX_DELAY))
        {
            /* Suspend all tasks except this one */
            if(sbAfterInitialization == false)
            {
                vTaskSuspend(pstGetMBTimerTaskHandle());
                vTaskSuspend(pstGetMBSerialTaskHandle());
            }            
            vTaskSuspend(spTaskPeriodic_10ms_Hdl);
            vTaskSuspend(spTaskPeriodic_100ms_Hdl);
            vTaskSuspend(spTaskPeriodic_1S_Hdl);
            vTaskSuspend(spTaskEvent_USART1_Hdl);
            vTaskSuspend(spTaskEvent_USART2_Hdl);
            vTaskSuspend(spTaskPeriodic_Modbus_Hdl);
            vTaskSuspend(spTaskEvent_Init_Hdl);
            vTaskSuspend(spTaskPeriodic_EEPROM_Hdl);

            /* Execute self-tests */
            if (vExecuteSelfTests(uiSelfTestFlags) != 0)
            {
                /* Self-tests failed */
            }
            
            /* Resume all suspended tasks */
            if(sbAfterInitialization == false)
            {
                vTaskResume(pstGetMBTimerTaskHandle());
                vTaskResume(pstGetMBSerialTaskHandle());
            }                        
            vTaskResume(spTaskPeriodic_10ms_Hdl);
            vTaskResume(spTaskPeriodic_100ms_Hdl);
            vTaskResume(spTaskPeriodic_1S_Hdl);
            vTaskResume(spTaskEvent_USART1_Hdl);
            vTaskResume(spTaskEvent_USART2_Hdl);
            vTaskResume(spTaskPeriodic_Modbus_Hdl);
            vTaskResume(spTaskEvent_Init_Hdl);
            vTaskResume(spTaskPeriodic_EEPROM_Hdl);            
            
            if(sbAfterInitialization == true)
            {
                /* Allow main task and Modbus task to run */
                xSemaphoreGive(spSemBinary_10ms_Hdl);
                xSemaphoreGive(spSemBinary_MB_Start_Hdl);
                sbAfterInitialization = false;
            }
        }      
    }
}


/*********************************************************************
* PRIVATE Task svTaskInitialization()
*---------------------------------------------------------------------
* Execute initialization task
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svTaskInitialization(void *p)
{
    uint32_t uiSelfTestFlags = 0xFFFFFF;
 
    /**************************************************/
    /* Initialize Peripherals1                        */
    /**************************************************/
    vInitializePeripherals1();

    /**************************************************/
    /* Go to sleep                                    */
    /**************************************************/
    //vBatteryGoToSleep();

    /**************************************************/
    /* Initialize Peripherals2                        */
    /**************************************************/
    vInitializePeripherals2();

    /**************************************************/
    /* Wake Up                                        */
    /**************************************************/
    //vBatteryWakeUp();

    /**************************************************/
    /* Start self-test                                */
    /**************************************************/
    xQueueOverwrite(spQueue_SelfTests_Hdl, &uiSelfTestFlags);
        
    /**************************************************/
    /* End of task                                    */
    /**************************************************/
    vTaskDelete(NULL);
}


/*********************************************************************
* PRIVATE Task svModbusTask()
*---------------------------------------------------------------------
* Execute Modbus operations
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svModbusTask( void *pvArg )
{    
    if(xSemaphoreTake(spSemBinary_MB_Start_Hdl, portMAX_DELAY))
    {            
        while (1)
        {
            vModbusTask();
        }
    }    
}


/*********************************************************************
* PRIVATE Task svEEPROMTask()
*---------------------------------------------------------------------
* Executes EEPROM operations
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svEEPROMTask( void *pvArg )
{
    BaseType_t sStatus;
    static stEEPROMAccess_t sstEEPROMAccess;
    SemaphoreHandle_t pstMySem_SPIEEPROM_Fault_Hdl; 
    
    /* Get semaphore Handle */
    pstMySem_SPIEEPROM_Fault_Hdl = sGetSemaphoreHandle(EEPROM_SPI_FAULT);
    
    while (1)
    {
        sStatus = xQueueReceive(spQueue_EEPROM_Access_Hdl, &sstEEPROMAccess, portMAX_DELAY);
        if( sStatus != pdPASS )
        {
            /* Data was not received from the queue */
        }
        else
        {
            if( xSemaphoreTake( spSemMutex_EEPROM_Access_Hdl, pdMS_TO_TICKS(100) ) == pdTRUE )
            {
                /* We were able to obtain the semaphore and can now access the
                data buffers. */
                
                if(bEEPROM_Verify_ID() == false)
                {
                    /* Communication error with EEPROM */
                    xSemaphoreGive(pstMySem_SPIEEPROM_Fault_Hdl);
                }
                else
                {
                    /* Perform saving to EEPROM or reading from EEPROM */
                    switch(sstEEPROMAccess.ucOperationRequest) 
                    {
                        case EEPROM_ROUTINE_DATA_WRITE:            
                        vPerformRoutineDataSaving((st_RTDataCollection_t *)sstEEPROMAccess.pvDataBuffer);            
                        break;
            
                        case EEPROM_OPERATIONAL_DATA_WRITE:   
                        if(sstEEPROMAccess.uiFaultsMesa == (1<<FLT_COMMUNICATION_ERROR))
                        {
                            vPerformOperationalDataDataSaving((st_RTDataCollection_t *)sstEEPROMAccess.pvDataBuffer, RTDC_COMM_ERROR_FAULT); 
                        }            
                        else if(sstEEPROMAccess.uiFaultsMesa == (1<<FLT_CELL_OVER_TEMP_ALARM))
                        {
                            vPerformOperationalDataDataSaving((st_RTDataCollection_t *)sstEEPROMAccess.pvDataBuffer, RTDC_CELL_OVER_TEMP_FAULT);
                        }
                        else if(sstEEPROMAccess.uiFaultsVend == (1<<FLT_FUSE_BLOWN_ALARM))
                        {
                            vPerformOperationalDataDataSaving((st_RTDataCollection_t *)sstEEPROMAccess.pvDataBuffer, RTDC_FUSE_BLOWN_FAULT);
                        }
                        else if(sstEEPROMAccess.uiFaultsVend != 0 || sstEEPROMAccess.uiFaultsMesa != 0)           
                        {
                            vPerformOperationalDataDataSaving((st_RTDataCollection_t *)sstEEPROMAccess.pvDataBuffer, RTDC_ALL_OTHER_FAULTS);
                        }        
                        break;
            
                        case EEPROM_DATA_READ:     
                        vReadDataFromEEPROM();       
                        break;
            
                        default:            
                        break;
                    }                    
                }
                
                /* We have finished accessing the data buffers.  Release the
                semaphore. */
                xSemaphoreGive( spSemMutex_EEPROM_Access_Hdl );
                                    
            }
            else
            {
                /* We could not obtain the semaphore and can therefore not access
                data buffers safely. */
            }        
        }                
    }
}


/*********************************************************************
* PRIVATE sCheckForTestModeRequest()
*---------------------------------------------------------------------
* Read USB Modbus data looking for mode change sequence. Times out after
* 10 seconds. Sequence must restart after that. If sequence is complete switch
* Operating mode from NORMAL_MODE to TEST_MODE
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void sCheckForTestModeRequest(void)
{
	static uint8_t sucTestModeRequestCnt = 0;
	static uint8_t sucValue;
	static uint8_t sucLocalValue;
	static uint32_t suiTestModeRequestSequence = 0;
	static bool sucFirstTimeLoop = true;

/* Check for Modbus USB sequence to switch operating mode to Test mode */
while(xQueueReceive(spQueue_Test_Mode_Request_Hdl, &sucValue, NOWAIT) == pdPASS)
{
	if(sucFirstTimeLoop==true)
	{
		sucLocalValue=sucValue;	
		sucFirstTimeLoop = false;
	}
	
	if(sucLocalValue==SEQUENCE_FIRST)
	{
		switch(sucValue)
		{
			case SEQUENCE_FIRST:
			/* Initial value of the sequence */
			suiTestModeRequestSequence = (uint32_t)sucValue << 24;
			sucTestModeRequestCnt = 0;
			break;

			case SEQUENCE_SECOND:
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_FIRST_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue << 16;
			}
			break;

			case SEQUENCE_THIRD:
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_SECOND_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue << 8;
			}
			break;

			case SEQUENCE_FOURTH:
			sucFirstTimeLoop = true;
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_THIRD_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue;
			}

			/* Verify that the sequence has completed within the expected time period otherwise just wait for the sequence to restart */
			if(suiTestModeRequestSequence == SEQUENCE_COMPLETE && sucTestModeRequestCnt < MAX_SEQ_TIMEOUT)
			{
				/* Switch to test mode */
				vSetBatteryOperatingMode(TEST_MODE);
			}
			break;

			default:
			break;
		}
	}
	else if(sucLocalValue==SEQUENCE_RESTART_FIRST)
	{
		switch(sucValue)
		{
			case SEQUENCE_RESTART_FIRST :
			/* Initial value of the sequence */
			suiTestModeRequestSequence = (uint32_t)sucValue << 24;
			sucTestModeRequestCnt = 0;
			break;

			case SEQUENCE_RESTART_SECOND:
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_RESTART_FIRST_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue << 16;
			}
			break;

			case SEQUENCE_RESTART_THIRD:
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_RESTART_SECOND_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue << 8;
			}
			break;

			case SEQUENCE_RESTART_FOURTH:
			sucFirstTimeLoop = true;
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_RESTART_THIRD_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue;
			}

			/* Verify that the sequence has completed within the expected time period otherwise just wait for the sequence to restart */
			if(suiTestModeRequestSequence == SEQUENCE_RESTART_COMPLETE && sucTestModeRequestCnt < MAX_SEQ_TIMEOUT)
			{
				/* restart */
				system_reset();
				
			}
			break;

			default:
			break;
		}
	}
	else if(sucLocalValue==SEQUENCE_BOOTLOADER_FIRST)
	{
		switch(sucValue)
		{
			case SEQUENCE_BOOTLOADER_FIRST :
			/* Initial value of the sequence */
			suiTestModeRequestSequence = (uint32_t)sucValue << 24;
			sucTestModeRequestCnt = 0;
			break;

			case SEQUENCE_BOOTLOADER_SECOND:
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_BOOTLOADER_FIRST_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue << 16;
			}
			break;

			case SEQUENCE_BOOTLOADER_THIRD:
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_BOOTLOADER_SECOND_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue << 8;
			}
			break;

			
			case SEQUENCE_BOOTLOADER_FOURTH:
			sucFirstTimeLoop = true;
			/* Verify that the sequence is happening in the correct order */
			if(suiTestModeRequestSequence == (uint32_t)SEQUENCE_BOOTLOADER_THIRD_CHK)
			{
				suiTestModeRequestSequence |= (uint32_t)sucValue;
			}

			/* Verify that the sequence has completed within the expected time period otherwise just wait for the sequence to restart */
			if(suiTestModeRequestSequence == SEQUENCE_BOOTLOADER_COMPLETE && sucTestModeRequestCnt < MAX_SEQ_TIMEOUT)
			{
				/* set the value to stay in bootloader */
				SetBootloaderFlag();
			}
			break;

			default:
			break;
		}
		
	}
	
}

	/* Limit the counter to the maximum timeout value */
	if(++sucTestModeRequestCnt >= MAX_SEQ_TIMEOUT)
	{
	sucTestModeRequestCnt = MAX_SEQ_TIMEOUT;
	sucFirstTimeLoop = true;
	
	}
	
	

}


/*********************************************************************
* PRIVATE sCopyFastData()
*---------------------------------------------------------------------
* Copies the fast data structure into the general full BMS structure.
*
* Inputs:
     myBms_FastDataCpy -
     myBmsDataCpy -
* Returns: None
*
**********************************************************************/
static void sCopyFastData(stBms_FastData_t * myBms_FastDataCpy, stBmsData_t * myBmsDataCpy)
{
    uint8_t ucIdx;

    /* Copy fast data (minimal structure) into the general BMS structure */
    myBmsDataCpy->shMeasuredCurrent = myBms_FastDataCpy->shMeasuredCurrent;
    myBmsDataCpy->shMaxChargeCurrent = myBms_FastDataCpy->shMaxChargeCurrent;
    myBmsDataCpy->shMaxDischargeCurrent = myBms_FastDataCpy->shMaxDischargeCurrent;
    myBmsDataCpy->ushStackVoltage = myBms_FastDataCpy->ushStackVoltage;
    myBmsDataCpy->ushSumOfCellsVoltage = myBms_FastDataCpy->ushSumOfCellsVoltage;
    myBmsDataCpy->bI2tExceededFlag = myBms_FastDataCpy->bI2tExceededFlag;
    myBmsDataCpy->ushCellOverVoltageFlags = myBms_FastDataCpy->ushCellOverVoltageFlags;
    myBmsDataCpy->ushCellUnderVoltageFlags = myBms_FastDataCpy->ushCellUnderVoltageFlags;
    myBmsDataCpy->ucThermalShutdownFlag = myBms_FastDataCpy->ucThermalShutdownFlag;
    myBmsDataCpy->ushMaxCellV = myBms_FastDataCpy->ushMaxCellV;
    myBmsDataCpy->ushMinCellV = myBms_FastDataCpy->ushMinCellV;
    myBmsDataCpy->ushAvgCellV = myBms_FastDataCpy->ushAvgCellV;
    myBmsDataCpy->ucMaxCellVIdx = myBms_FastDataCpy->ucMaxCellVIdx;
    myBmsDataCpy->ucMinCellVIdx = myBms_FastDataCpy->ucMinCellVIdx;
    myBmsDataCpy->eFetManagementState = myBms_FastDataCpy->eFetManagementState;

    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        myBmsDataCpy->ushCellVoltage[ucIdx] = myBms_FastDataCpy->ushCellVoltage[ucIdx];
    }

    for(ucIdx=0;ucIdx<NUM_OF_ANA_INPUTS;ucIdx++)
    {
        myBmsDataCpy->ushAnalogVoltage[ucIdx] = myBms_FastDataCpy->ushAnalogVoltage[ucIdx];
    }
}


/********************************************/
/* Public Functions                         */
/********************************************/
/*********************************************************************
* PUBLIC Task sGetSemaphoreHandle()
*---------------------------------------------------------------------
*  Interface Function to get the Semaphore handle
*
* Inputs:
*   eSemaphoreIdx - Semaphore requested (enum)
* Returns:
*   SemaphoreHandle_t - Semaphore handle
*
**********************************************************************/
SemaphoreHandle_t sGetSemaphoreHandle(eEventDrivenSems_t eSemaphoreIdx)
{
    SemaphoreHandle_t myReturnValue = NULL;
    switch(eSemaphoreIdx)
    {
        case USB_COMM:
        myReturnValue = spSemBinary_USB_Hdl;
        break;
        case USB_COMM_RX:
        myReturnValue = spSemBinary_USB_Rx_Hdl;
        break;
        case USB_COMM_TX_EMPTY:
        myReturnValue = spSemBinary_USB_Tx_Empty_Hdl;
        break;
        case USART1_COMM:
        myReturnValue = spSemBinary_USART1_Hdl;
        break;
        case USART2_COMM:
        myReturnValue = spSemBinary_USART2_Hdl;        
        break;
        case EEPROM_ACCESS:
        myReturnValue = spSemMutex_EEPROM_Access_Hdl;
        break;
        case MAIN_TASK:
        myReturnValue = spSemBinary_10ms_Hdl;
        break;
        case I2C_TEMP_SENSOR:
        myReturnValue = spSemBinary_I2C_Temp_Hdl;
        break;
        case I2C_LED:
        myReturnValue = spSemBinary_I2C_Led_Hdl;
        break;
        case EEPROM_SPI_FAULT:
        myReturnValue = spSemCount_SPI_EEPROM_Fault_Hdl;
        break;
        case BMSIC_SPI_FAULT:
        myReturnValue = spSemCount_SPI_BMSIC_Fault_Hdl;
        break;
        case TEMPSENSE_I2C_FAULT:
        myReturnValue = spSemCount_I2C_TEMPSENSE_Fault_Hdl;
        break;
        case LEDCNTL_I2C_FAULT:
        myReturnValue = spSemCount_I2C_LEDCNTL_Fault_Hdl;
        break;
        default:
        myReturnValue = NULL;
        break;
    }
    return myReturnValue;
}


/*********************************************************************
* PUBLIC QueueHandle_t sGetQueueHandle()
*---------------------------------------------------------------------
*  Interface Function to get the Queue handle
*
* Inputs:
*   eQueueIdx - Data Queue enum
* Returns:
*   QueueHandle_t - Queue handle
*
**********************************************************************/
QueueHandle_t sGetQueueHandle(eDataQueues_t eQueueIdx)
{
    QueueHandle_t myReturnValue = NULL;
    switch(eQueueIdx)
    {
        case I2C_DATA_QUEUE:
        myReturnValue = spQueue_I2C_Data_Hdl;
        break;
        case HMI_OUTPUT_QUEUE:
        myReturnValue = spQueue_HMI_Outputs_Hdl;
        break;
        case USART1_DATA_QUEUE:
        myReturnValue = spQueue_USART1_Data_Hdl;
        break;
        case USART2_DATA_QUEUE:
        myReturnValue = spQueue_USART2_Data_Hdl;
        break;
        case DRY_CONTACTS_QUEUE:
        myReturnValue = spQueue_Dry_Contacts_Hdl;
        break;
        case HEATER_CTL_QUEUE:
        myReturnValue = spQueue_Heater_Ctl_Hdl;
        break;
        case FET_STATE_QUEUE:
        myReturnValue = spQueue_FetStates_Hdl;
        break;
        case CELL_BALANCING_QUEUE:
        myReturnValue = spQueue_Cell_Balancing_Hdl;
        break;
        case DIAG_DATA_QUEUE:
        myReturnValue = spQueue_Diag_Data_Hdl;
        break;
        case TEST_MODE_REQUEST_QUEUE:
        myReturnValue = spQueue_Test_Mode_Request_Hdl;
        break;
        case EEPROM_ACCESS_QUEUE:
        myReturnValue = spQueue_EEPROM_Access_Hdl;
        break;
        case SELF_TESTS_QUEUE:
        myReturnValue = spQueue_SelfTests_Hdl;
        break;        
        case GPIO_IN_QUEUE:
        myReturnValue = spQueue_GPIOStates_Hdl;
        break;
        default:
        myReturnValue = NULL;
        break;
    }
    return myReturnValue;
}


/*********************************************************************
* PUBLIC vStartRtosTasks()
*---------------------------------------------------------------------
* Start RTOS tasks and create Semaphores
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vStartRtosTasks(void)
{
    /**************************************************/
    /* Create Queues                                  */
    /**************************************************/
    spQueue_100ms_Hdl = xQueueCreate( 1, sizeof( stBms_FastData_t ) );
    if (spQueue_100ms_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_Diag_Data_Hdl = xQueueCreate( 1, sizeof( stBmsData_t ) );
    if (spQueue_Diag_Data_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_I2C_Data_Hdl = xQueueCreate( 1, sizeof( uint16_t ) );
    if (spQueue_I2C_Data_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_FetStates_Hdl = xQueueCreate( 1, sizeof( uint16_t ) );
    if (spQueue_FetStates_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }
    spQueue_GPIOStates_Hdl = xQueueCreate( 1, sizeof( uint16_t ) );
    if (spQueue_GPIOStates_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_HMI_Outputs_Hdl = xQueueCreate( 3, sizeof( stLEDStatus_t ) );
    if (spQueue_HMI_Outputs_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_Dry_Contacts_Hdl = xQueueCreate( 1, sizeof( uint8_t ) );
    if (spQueue_Dry_Contacts_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_Heater_Ctl_Hdl = xQueueCreate( 1, sizeof( uint8_t ) );
    if (spQueue_Heater_Ctl_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_USART1_Data_Hdl = xQueueCreate( 1, sizeof( stUsartData_t ) );
    if (spQueue_USART1_Data_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_USART2_Data_Hdl = xQueueCreate( 1, sizeof( stUsartData_t ) );
    if (spQueue_USART2_Data_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_Cell_Balancing_Hdl = xQueueCreate( 1, sizeof( uint16_t ) );
    if (spQueue_Cell_Balancing_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_Test_Mode_Request_Hdl = xQueueCreate( 4, sizeof( uint8_t ) );
    if (spQueue_Test_Mode_Request_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_RT_Data_Collection_Hdl = xQueueCreate( 1, sizeof( stBmsData_t ) );
    if (spQueue_RT_Data_Collection_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }

    spQueue_Fault_and_Limits_Hdl = xQueueCreate( 1, sizeof( stFaultsandLimits_t ) );
    if (spQueue_Fault_and_Limits_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }
    
    spQueue_EEPROM_Access_Hdl = xQueueCreate( 5, sizeof( stEEPROMAccess_t ) );
    if (spQueue_EEPROM_Access_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Queue */
        }
    }
    
    spQueue_SelfTests_Hdl = xQueueCreate( 1, sizeof( uint32_t ) );
    if (spQueue_SelfTests_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }    

    /**************************************************/
    /* Create Semaphores                              */
    /**************************************************/
    spSemBinary_USB_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_USB_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }

    spSemBinary_USB_Rx_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_USB_Rx_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }

    spSemBinary_USB_Tx_Empty_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_USB_Tx_Empty_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }

    spSemBinary_USART1_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_USART1_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }

    spSemBinary_USART2_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_USART2_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }

    spSemBinary_10ms_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_10ms_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }
    spSemMutex_EEPROM_Access_Hdl = xSemaphoreCreateMutex();
    if (spSemMutex_EEPROM_Access_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }
    
    spSemBinary_I2C_Temp_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_I2C_Temp_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }
    
    spSemBinary_I2C_Led_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_I2C_Led_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }
    
    spSemCount_SPI_EEPROM_Fault_Hdl = xSemaphoreCreateCounting(FAULT_COUNTING_SEM_MAX, 0);
    if (spSemCount_SPI_EEPROM_Fault_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }
    
    spSemCount_SPI_BMSIC_Fault_Hdl = xSemaphoreCreateCounting(FAULT_COUNTING_SEM_MAX, 0);
    if (spSemCount_SPI_BMSIC_Fault_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }
    
    spSemCount_I2C_TEMPSENSE_Fault_Hdl = xSemaphoreCreateCounting(FAULT_COUNTING_SEM_MAX, 0);
    if (spSemCount_I2C_TEMPSENSE_Fault_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }
    
    spSemCount_I2C_LEDCNTL_Fault_Hdl = xSemaphoreCreateCounting(FAULT_COUNTING_SEM_MAX, 0);
    if (spSemCount_I2C_LEDCNTL_Fault_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }

    spSemBinary_MB_Start_Hdl = xSemaphoreCreateBinary();
    if (spSemBinary_MB_Start_Hdl == NULL)
    {
        while (1)
        {
            /* Failed to Create Semaphore */
        }
    }

    /**************************************************/
    /* Create Tasks                                   */
    /**************************************************/
    if (xTaskCreate(svTaskPeriodic_10ms, "Period10ms", TASK_10MS_STACK_SIZE, NULL, TASK_HIGH_PRIORITY, &spTaskPeriodic_10ms_Hdl) != pdPASS)
    {
        while (1)
        {
            /* Failed to Create Task */
        }
    }

    if (xTaskCreate(svTaskPeriodic_100ms, "Period100ms", TASK_STACK_SIZE, NULL, TASK_MED_PRIORITY, &spTaskPeriodic_100ms_Hdl) != pdPASS)
    {
        while (1)
        {
            /* Failed to Create Task */
        }
    }

    /* The 1 second task is not needed in test mode */

    if (xTaskCreate(svTaskPeriodic_1s, "Periodic1S", TASK_STACK_SIZE, NULL, TASK_LOW_PRIORITY, &spTaskPeriodic_1S_Hdl) != pdPASS)
    {
        while (1)
        {
            ///* Failed to Create Task */
        }
    }

    if (xTaskCreate(svTaskEvent_USART1, "EventUSART1", TASK_STACK_SIZE, NULL, TASK_LOW_PRIORITY, &spTaskEvent_USART1_Hdl) != pdPASS)
    {
        while (1)
        {
            /* Failed to Create Task */
        }
    }

    if (xTaskCreate(svTaskEvent_USART2, "EventUSART2", TASK_STACK_SIZE, NULL, TASK_LOW_PRIORITY, &spTaskEvent_USART2_Hdl) != pdPASS)
    {
        while (1)
        {
            /* Failed to Create Task */
        }
    }

    if (xTaskCreate(svModbusTask, "MODBUS", MODBUS_TASK_STACKSIZE, NULL, TASK_LOW_PRIORITY, &spTaskPeriodic_Modbus_Hdl) != pdPASS) // MODBUS_TASK_PRIORITY
    {
        while (1)
        {
            /* Failed to Create Task */
        }
    }

    if (xTaskCreate(svEEPROMTask, "EEPROM", TASK_STACK_SIZE, NULL, TASK_LOW_PRIORITY, &spTaskPeriodic_EEPROM_Hdl) != pdPASS)
    {
        while (1)
        {
            /* Failed to Create Task */
        }
    }

    if (xTaskCreate(svTaskInitialization, "Initialization", TASK_10MS_STACK_SIZE, NULL, TASK_LOW_PRIORITY, &spTaskEvent_Init_Hdl) != pdPASS)
    {
        while (1)
        {
            /* Failed to Create Task */
        }
    }

    if (xTaskCreate(svTaskEvent_SelfTests, "Self-tests", TASK_10MS_STACK_SIZE, NULL, TASK_MED_PRIORITY, &spTaskEvent_SelfTests_Hdl) != pdPASS)
    {
        while (1)
        {
            /* Failed to Create Task */
        }
    }
    
    /* Initialize the USART buffers (Internal and External) Rx */
    stRxUSART1Buffer.ushSerialDataSize = 1;
    stRxUSART2Buffer.ushSerialDataSize = 1;
    vUsartExtCommTx_Parms(stRxUSART2Buffer.cSerialData, stRxUSART2Buffer.ushSerialDataSize);
    vUsartExtCommRx_Parms(stRxUSART2Buffer.cSerialData, stRxUSART2Buffer.ushSerialDataSize);
    vUsartIntCommTx_Parms(stRxUSART1Buffer.cSerialData, stRxUSART1Buffer.ushSerialDataSize);
    vUsartIntCommRx_Parms(stRxUSART1Buffer.cSerialData, stRxUSART1Buffer.ushSerialDataSize);

    /* Initialize Modbus comms */
    vMBPInit();

    /* Start RTOS Scheduler */
    vTaskStartScheduler();
}


/*******************************************************************/
/*******************************************************************/
/*******************************************************************/

void vApplicationMallocFailedHook( void )
{
    /* vApplicationMallocFailedHook() will only be called if
    configUSE_MALLOC_FAILED_HOOK is set to 1 in FreeRTOSConfig.h.  It is a hook
    function that will get called if a call to pvPortMalloc() fails.
    pvPortMalloc() is called internally by the kernel whenever a task, queue,
    timer or semaphore is created.  It is also called by various parts of the
    demo application.  If heap_1.c or heap_2.c are used, then the size of the
    heap available to pvPortMalloc() is defined by configTOTAL_HEAP_SIZE in
    FreeRTOSConfig.h, and the xPortGetFreeHeapSize() API function can be used
    to query the size of free heap space that remains (although it does not
    provide information on how the remaining heap might be fragmented). */
    taskDISABLE_INTERRUPTS();
    for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationIdleHook( void )
{
    /* vApplicationIdleHook() will only be called if configUSE_IDLE_HOOK is set
    to 1 in FreeRTOSConfig.h.  It will be called on each iteration of the idle
    task.  It is essential that code added to this hook function never attempts
    to block in any way (for example, call xQueueReceive() with a block time
    specified, or call vTaskDelay()).  If the application makes use of the
    vTaskDelete() API function (as this demo application does) then it is also
    important that vApplicationIdleHook() is permitted to return to its calling
    function, because it is the responsibility of the idle task to clean up
    memory allocated by the kernel to any task that has since been deleted. */
}
/*-----------------------------------------------------------*/

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName )
{
    ( void ) pcTaskName;
    ( void ) pxTask;

    /* Run time stack overflow checking is performed if
    configCHECK_FOR_STACK_OVERFLOW is defined to 1 or 2.  This hook
    function is called if a stack overflow is detected. */
    taskDISABLE_INTERRUPTS();
    for( ;; );
}
/*-----------------------------------------------------------*/

void vApplicationTickHook( void )
{
    /* This function will be called by each tick interrupt if
    configUSE_TICK_HOOK is set to 1 in FreeRTOSConfig.h.  User code can be
    added here, but the tick hook is called from an interrupt context, so
    code must not attempt to block, and only the interrupt safe FreeRTOS API
    functions can be used (those that end in FromISR()). */
}
/*-----------------------------------------------------------*/

void vMainConfigureTimerForRunTimeStats( void )
{
    /* Used by the optional run-time stats gathering functionality. */

    /* How many clocks are there per tenth of a millisecond? */
    ulClocksPer10thOfAMilliSecond = configCPU_CLOCK_HZ / 10000UL;
}
/*-----------------------------------------------------------*/

unsigned long ulMainGetRunTimeCounterValue( void )
{
    unsigned long ulSysTickCounts, ulTickCount, ulReturn;
    const unsigned long ulSysTickReloadValue = ( configCPU_CLOCK_HZ / configTICK_RATE_HZ ) - 1UL;
    volatile unsigned long * const pulCurrentSysTickCount = ( ( volatile unsigned long *) 0xe000e018 );
    volatile unsigned long * const pulInterruptCTRLState = ( ( volatile unsigned long *) 0xe000ed04 );
    const unsigned long ulSysTickPendingBit = 0x04000000UL;

    /* Used by the optional run-time stats gathering functionality. */


    /* NOTE: There are potentially race conditions here.  However, it is used
    anyway to keep the examples simple, and to avoid reliance on a separate
    timer peripheral. */


    /* The SysTick is a down counter.  How many clocks have passed since it was
    last reloaded? */
    ulSysTickCounts = ulSysTickReloadValue - *pulCurrentSysTickCount;

    /* How many times has it overflowed? */
    ulTickCount = xTaskGetTickCountFromISR();

    /* This is called from the context switch, so will be called from a
    critical section.  xTaskGetTickCountFromISR() contains its own critical
    section, and the ISR safe critical sections are not designed to nest,
    so reset the critical section. */
    portSET_INTERRUPT_MASK_FROM_ISR();

    /* Is there a SysTick interrupt pending? */
    if( ( *pulInterruptCTRLState & ulSysTickPendingBit ) != 0UL )
    {
        /* There is a SysTick interrupt pending, so the SysTick has overflowed
        but the tick count not yet incremented. */
        ulTickCount++;

        /* Read the SysTick again, as the overflow might have occurred since
        it was read last. */
        ulSysTickCounts = ulSysTickReloadValue - *pulCurrentSysTickCount;
    }

    /* Convert the tick count into tenths of a millisecond.  THIS ASSUMES
    configTICK_RATE_HZ is 1000! */
    ulReturn = ( ulTickCount * 10UL ) ;

    /* Add on the number of tenths of a millisecond that have passed since the
    tick count last got updated. */
    ulReturn += ( ulSysTickCounts / ulClocksPer10thOfAMilliSecond );

    return ulReturn;
}

/******************************** End of File ********************************/