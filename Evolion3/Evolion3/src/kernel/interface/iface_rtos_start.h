#ifndef INT_RTOS_START_H
#define INT_RTOS_START_H
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: iface_rtos_start.h                                             */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for iface_rtos_start.c, which provides prototypes for         */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>
#include <temp_sensor_ic.h>

#define TASK_STACK_SIZE             128
#define TASK_10MS_STACK_SIZE        160
#define MODBUS_TASK_STACKSIZE       260
#define TASK_HIGH_PRIORITY          (tskIDLE_PRIORITY + 3)
#define TASK_MED_PRIORITY           (tskIDLE_PRIORITY + 2)
#define TASK_LOW_PRIORITY           (tskIDLE_PRIORITY + 1)
#define TIME_1MS                    1
#define TIME_2MS                    2
#define TIME_3MS                    3
#define TIME_5MS                    5
#define TIME_10MS                   10
#define TIME_20MS                   20
#define TIME_100MS                  100
#define TIME_1S                     1000
#define TIME_CTL_10MS_TASK_100MS    10
#define TIME_CTL_10MS_TASK_1000MS   100
#define TIME_CTL_100MS_TASK_1000MS  10
#define TIME_5MIN_1SEC_TASK_CNT     300
#define NOWAIT                      0
#define TEST_DELAY                  10
#define NUM_OF_CELLS                14
#define NUM_OF_ANA_INPUTS           7

#define SEQUENCE_FIRST      0x0A
#define SEQUENCE_SECOND     0xFA
#define SEQUENCE_THIRD      0xCA
#define SEQUENCE_FOURTH     0xDE
#define SEQUENCE_FIRST_CHK  0x0A000000
#define SEQUENCE_SECOND_CHK 0x0AFA0000
#define SEQUENCE_THIRD_CHK  0x0AFACA00
#define SEQUENCE_COMPLETE   0x0AFACADE
#define MAX_SEQ_TIMEOUT     100

#define SEQUENCE_RESTART_FIRST      0x01
#define SEQUENCE_RESTART_SECOND     0xAB
#define SEQUENCE_RESTART_THIRD      0xEA
#define SEQUENCE_RESTART_FOURTH     0xC4
#define SEQUENCE_RESTART_FIRST_CHK  0x01000000
#define SEQUENCE_RESTART_SECOND_CHK 0x01AB0000
#define SEQUENCE_RESTART_THIRD_CHK  0x01ABEA00
#define SEQUENCE_RESTART_COMPLETE   0x01ABEAC4

#define SEQUENCE_BOOTLOADER_FIRST      0xDE
#define SEQUENCE_BOOTLOADER_SECOND     0xAD
#define SEQUENCE_BOOTLOADER_THIRD      0xC0
#define SEQUENCE_BOOTLOADER_FOURTH     0xDF
#define SEQUENCE_BOOTLOADER_FIRST_CHK  0xDE000000
#define SEQUENCE_BOOTLOADER_SECOND_CHK 0xDEAD0000
#define SEQUENCE_BOOTLOADER_THIRD_CHK  0xDEADC000
#define SEQUENCE_BOOTLOADER_COMPLETE   0xDEADC0DF

#define ALARM_BITMASK       0x1214AAAA  /* Mapping per Evt1 as enumerated in Energy-Storage-Information-Models-D4-2017-09-18b.xlsx */
#define WARNING_BITMASK     0x240B5555  /* Mapping per Evt1 as enumerated in Energy-Storage-Information-Models-D4-2017-09-18b.xlsx */

#define FAULT_COUNTING_SEM_MAX 10

/*********************************************************************/
/*  Enums                                                            */
/*********************************************************************/
typedef enum
{
    HMI_BUTTONS = 1,
    USB_COMM,
    USB_COMM_RX,
    USB_COMM_TX_EMPTY,
    USART1_COMM,
    USART2_COMM,
    EEPROM_ACCESS,
    MAIN_TASK,
    I2C_TEMP_SENSOR,
    I2C_LED,
    EEPROM_SPI_FAULT,
    BMSIC_SPI_FAULT,
    TEMPSENSE_I2C_FAULT,
    LEDCNTL_I2C_FAULT,
}eEventDrivenSems_t;

typedef enum
{
    I2C_DATA_QUEUE = 1,
    TEMP_DATA_QUEUE,
    HMI_INPUT_QUEUE,
    HMI_OUTPUT_QUEUE,
    DRY_CONTACTS_QUEUE,
    HEATER_CTL_QUEUE,
    USART1_DATA_QUEUE,
    USART2_DATA_QUEUE,
    USB_DATA_QUEUE,
    FET_STATE_QUEUE,
    GPIO_IN_QUEUE,
    CELL_BALANCING_QUEUE,
    DIAG_DATA_QUEUE,
    TEST_MODE_REQUEST_QUEUE,
    EEPROM_ACCESS_QUEUE,
    SELF_TESTS_QUEUE,
}eDataQueues_t;

typedef enum
{
    FORBIDDEN_CHARGE_FORBIDDEN_DISCHARGE,
    FORBIDDEN_CHARGE_REG_DISCHARGE_LV0,
    FORBIDDEN_CHARGE_REG_DISCHARGE_LV1,
    FORBIDDEN_CHARGE_REG_DISCHARGE_LV2,
    FORBIDDEN_CHARGE_REG_DISCHARGE_LV3,
    FORBIDDEN_CHARGE_REG_DISCHARGE_LV4,
    FORBIDDEN_CHARGE_REG_DISCHARGE_LV5,
    FORBIDDEN_CHARGE_REG_DISCHARGE_LV6,
    FORBIDDEN_CHARGE_REG_DISCHARGE_LV7,
    FORBIDDEN_CHARGE_FULL_DISCHARGE,
    REG_CHARGE_REG_DISCHARGE,
    REG_CHARGE_LVL0_FULL_DISCHARGE,
    REG_CHARGE_LVL1_FULL_DISCHARGE,
    REG_CHARGE_LVL2_FULL_DISCHARGE,
    REG_CHARGE_LVL3_FULL_DISCHARGE,
    REG_CHARGE_LVL4_FULL_DISCHARGE,
    REG_CHARGE_LVL5_FULL_DISCHARGE,
    REG_CHARGE_LVL6_FULL_DISCHARGE,
    REG_CHARGE_LVL7_FULL_DISCHARGE,
    FULL_CHARGE_LVL0_REG_DISCHARGE,
    FULL_CHARGE_LVL1_REG_DISCHARGE,
    FULL_CHARGE_LVL2_REG_DISCHARGE,
    FULL_CHARGE_LVL3_REG_DISCHARGE,
    FULL_CHARGE_LVL4_REG_DISCHARGE,
    FULL_CHARGE_LVL5_REG_DISCHARGE,
    FULL_CHARGE_LVL6_REG_DISCHARGE,
    FULL_CHARGE_LVL7_REG_DISCHARGE,
    FULL_CHARGE_FULL_DISCHARGE,
    REG_CHARGE_LV0_FORBIDDEN_DISCHARGE,
    REG_CHARGE_LV1_FORBIDDEN_DISCHARGE,
    REG_CHARGE_LV2_FORBIDDEN_DISCHARGE,
    REG_CHARGE_LV3_FORBIDDEN_DISCHARGE,
    REG_CHARGE_LV4_FORBIDDEN_DISCHARGE,
    REG_CHARGE_LV5_FORBIDDEN_DISCHARGE,
    REG_CHARGE_LV6_FORBIDDEN_DISCHARGE,
    REG_CHARGE_LV7_FORBIDDEN_DISCHARGE,
    FULL_CHARGE_FORBIDDEN_DISCHARGE,
    CHECK_FOR_OVER_UNDER_VOLTAGES,
}eStates_machine_t;


typedef enum
{
   RTDC_COMM_ERROR_FAULT,
   RTDC_CELL_OVER_TEMP_FAULT,
   RTDC_FUSE_BLOWN_FAULT,
   RTDC_ALL_OTHER_FAULTS,
}eRTDC_Faults_t;


/********************************************************************/
/*  Types and Structures                                            */
/********************************************************************/
typedef struct stBmsData
{
    int16_t shMeasuredCurrent;              /* Measured current */
    int16_t shMeasuredCurrentAvg;           /* Average measured current */
    int16_t shDischargeCurrentLimit;        /* Discharge current limit */
    int16_t shChargeCurrentLimit;           /* Charge current limit */
    uint16_t shChargeCurrentLimitDerated;   /* Charge current limit */
    int16_t shMaxChargeCurrent;             /* Maximum Charge Current */
    int16_t shMaxDischargeCurrent;          /* Maximum Discharge Current */
    uint32_t uiCurrentThroughputRawCount;   /* Accumulator for current measured at 10ms */
    uint16_t ushStackVoltage;               /* Battery stack voltage reading from BMS IC */
    uint16_t ushSumOfCellsVoltage;          /* Battery stack voltage sum of cell voltages */
    uint16_t ushCellVoltage[NUM_OF_CELLS];  /* Individual cell voltages*/
    uint16_t ushMaxCellV;                   /* Max cell voltage */
    uint16_t ushMinCellV;                   /* Min cell voltage */
    uint16_t ushAvgCellV;                   /* Avg cell voltage */
    uint8_t ucMaxCellVIdx;                  /* Index of cell with Max voltage */
    uint8_t ucMinCellVIdx;                  /* Index of cell with Min voltage */
    uint16_t ushAnalogVoltage[NUM_OF_ANA_INPUTS];   /* Misc analog I/O */
    bool bI2tExceededFlag;                  /* Flag to be set if (I^2)*t value is exceeded */
    uint16_t ushCellOverVoltageFlags;       /* Over Voltage Cell Flags */
    uint16_t ushCellUnderVoltageFlags;      /* Under Voltage Cell Flags */
    uint8_t ucThermalShutdownFlag;          /* Thermal shutdown flag */
    uint8_t ucGPIOStatus;                   /* GPIO input statuses */
    uint8_t ucButtonStatus;                 /* Push button statuses */
    uint16_t ushCellsToBalance;             /* Cells to balance */
    int16_t shTemperatureSensor[NUM_TEMP_SENSORS];  /* Temperature from sensors */
    int16_t shCellTemperatureMax;           /* Maximum temperature from cell sensors */
    int16_t shCellTemperatureMin;           /* Minimum temperature from cell sensors */
    uint8_t ucCellTemperatureMaxIdx;        /* Index range is [0,NUM_CELL_TEMP_SENSORS-1] */
    uint8_t ucCellTemperatureMinIdx;        /* Index range is [0,NUM_CELL_TEMP_SENSORS-1] */
    int16_t shElectronicsTemperatureMax;    /* Maximum temperature from electronics sensors */
    int16_t shElectronicsTemperatureMin;    /* Minimum temperature from electronics sensors */
    uint8_t ucElectronicsTemperatureMaxIdx; /* Index range is [0,NUM_ELEC_TEMP_SENSORS-1] */
    uint8_t ucElectronicsTemperatureMinIdx; /* Index range is [0,NUM_ELEC_TEMP_SENSORS-1] */
    bool bHeaterEnable;                     /* Heater enable flag */
    uint16_t ushSOC;                        /* Battery SOC value */
    uint16_t ushSOCMin;                     /* Minimum cell SOC value */
    uint16_t ushSOCMax;                     /* Maximum cell SOC value */
    uint16_t ushSOCAvg;                     /* Average cell SOC value */
    uint32_t uiFaultFlagsEvt1;              /* MESA-defined fault status bitfield */
    uint32_t uiFaultFlagsEvtVnd1;           /* Vendor-defined fault status bitfield */
    uint32_t uiFaultFlagsLegacy;            /* Legacy-compatible fault status bitfield */
    uint16_t ushBatteryHeartbeat;           /* Battery heartbeat, incremented every second */
    eStates_machine_t eFetManagementState; /* Fet state machine states */
} stBmsData_t; 


typedef struct stBms_FastData
{
   int16_t shMeasuredCurrent;              /* Measured current */
   int16_t shMaxChargeCurrent;             /* Maximum Charge Current */
   int16_t shMaxDischargeCurrent;          /* Maximum Discharge Current */
   uint16_t ushStackVoltage;               /* Battery stack voltage reading from BMS IC */
   uint16_t ushSumOfCellsVoltage;          /* Battery stack voltage sum of cell voltages */
   uint16_t ushMaxCellV;                   /* Max cell voltage */
   uint16_t ushMinCellV;                   /* Min cell voltage */
   uint16_t ushAvgCellV;                   /* Avg cell voltage */
   uint8_t ucMaxCellVIdx;                  /* Index of cell with Max voltage */
   uint8_t ucMinCellVIdx;                  /* Index of cell with Min voltage */
   uint16_t ushCellVoltage[NUM_OF_CELLS];  /* Individual cell voltages*/
   uint16_t ushAnalogVoltage[NUM_OF_ANA_INPUTS]; /* Misc analog I/O */
   bool bI2tExceededFlag;                  /* Flag to be set if (I^2)*t value is exceeded */
   uint16_t ushCellOverVoltageFlags;       /* Over Voltage Cell Flags */
   uint16_t ushCellUnderVoltageFlags;      /* Under Voltage Cell Flags */
   uint8_t ucThermalShutdownFlag;          /* Thermal shutdown flag */
   eStates_machine_t eFetManagementState; /* Fet state machine states */
} stBms_FastData_t;


typedef struct stUsartData
{
    char cSerialData[40]; /* Serial Data */
    uint16_t ushSerialDataSize; /* Number of data words in message */
} stUsartData_t;

typedef struct stUSBData
{
    uint8_t ucUSBData[320]; /* USB Serial Data */
    uint32_t uiUSBDataSize; /* Number of data bytes in message */
} stUSBData_t;

typedef struct stFaultsandLimits
{
    uint32_t uiMesaDefFaults;
    uint32_t uiVendDefFaults;
    int16_t shDischargeCurrentLimit;        /* Discharge current limit */
    int16_t shChargeCurrentLimit;           /* Charge current limit */
    
}stFaultsandLimits_t;

typedef struct stEEPROMAccess
{
    uint32_t uiFaultsMesa;
    uint32_t uiFaultsVend;
    void * pvDataBuffer;
    uint8_t ucOperationRequest;
}stEEPROMAccess_t;



/*********************************************************************
* PUBLIC Task sGetSemaphoreHandle()
*---------------------------------------------------------------------
*  Interface Function to get the Semaphore handle
*
* Inputs:
*   eSemaphoreIdx - Semaphore requested (enum)
* Returns:
*   SemaphoreHandle_t - Semaphore handle
*
**********************************************************************/
SemaphoreHandle_t sGetSemaphoreHandle(eEventDrivenSems_t eSemaphoreIdx);


/*********************************************************************
* PUBLIC QueueHandle_t sGetQueueHandle()
*---------------------------------------------------------------------
*  Interface Function to get the Queue handle
*
* Inputs:
*   eQueueIdx - Data Queue enum
* Returns:
*   QueueHandle_t - Queue handle
*
**********************************************************************/
QueueHandle_t sGetQueueHandle(eDataQueues_t eQueueIdx);


/*********************************************************************
* PUBLIC vStartRtosTasks()
*---------------------------------------------------------------------
* Start RTOS tasks and create Semaphores
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vStartRtosTasks(void);

void vApplicationMallocFailedHook( void );

void vApplicationIdleHook( void );

void vApplicationStackOverflowHook( TaskHandle_t pxTask, char *pcTaskName );

void vApplicationTickHook( void );

#endif /* INT_RTOS_START_H */
