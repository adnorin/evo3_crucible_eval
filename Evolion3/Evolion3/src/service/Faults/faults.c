/**
 * @copyright (C) Copyright 2017 by SAFT	
 *            All rights reserved	
 *             
 * @copyright This program is the property of Saft America, Inc. and can only be	
 *            used and copied with the prior written authorization of SAFT.	
 *            Any whole or partial copy of this program in either its original form or 
 *            in a modified form must mention this copyright and its owner.	
 *
 * @file faults.c	
 *
 * @author Creator: Adnorin L. Mendez	
 *
 * @author Last Modifier: Gerald Percherancier
 *
 * @date File Creation: 2018-01-22
 *
 * @date Last Modification: 2018-03-05	
 *
 * @version xx.yy.zz
 *
 * @brief Periodic fault check.	
 *	
 * Detailed description starting with 
 * Evolion 3 Software
 *	
 */ 
#include <asf.h>
#include <FreeRTOS.h>
#include <queue.h>
#include <iface_i2c_dma.h>
#include <temp_sensor_ic.h>
#include <iface_rtos_start.h>
#include <self_tests.h>
#include <faults.h>
#include <conf_battery_settings.h>
#include <RamDataStorage.h>
#include <fet_controller.h>
#include <fets.h>
#include <bat_cell_controller_ic.h>
#include <conf_gpio.h>

/* Prototypes */
static uint32_t suiCellOverTempFaultDetect(int16_t shMyMaxTemperature);
static uint32_t suiCellUnderTempFaultDetect(int16_t shMyMinTemperature);
static uint32_t suiElectronicsOverTempFaultDetect(int16_t shMyMaxTemperature);
static uint32_t suiCellOverVoltageFaultDetect(uint16_t ushMaxCellV);
static uint32_t suiCellUnderVoltageFaultDetect(uint16_t ushMinCellV);
static uint32_t suiBatteryOverVoltageFaultDetect(uint16_t ushBattVoltage);
static uint32_t suiBatteryUnderVoltageFaultDetect(uint16_t ushBattVoltage);
static uint32_t suiOverSOCFaultDetect(uint16_t ushSOC);
static uint32_t suiUnderSOCFaultDetect(uint16_t ushSOC);
static uint32_t suiOverCurrentChargeFaultDetect(int16_t shMeasuredCurrent, int16_t shChargeCurrentLimit);
static uint32_t suiOverCurrentDischargeFaultDetect(int16_t shMeasuredCurrent, int16_t shDischargeCurrentLimit);
static uint32_t suiExtremeOverCurrentFaultDetect(bool bI2tExceededFlag);
static uint32_t suiCellTempDifferenceFaultDetect(int16_t shMyMaxTemperature, int16_t shMyMinTemperature);
static uint32_t suiCellVoltageImbalanceFaultDetect(uint16_t ushMaxCellV, uint16_t ushMinCellV);
static uint32_t suiSelfTestFaultDetect(void);
static uint32_t suiFuseBlownFaultDetect(int16_t shMeasuredCurrentAvg, uint16_t ushSumOfCellsVoltage, uint16_t ushNetworkVoltage);
static uint32_t suiRedundantSafetyMechanismActivationFaultDetect(void);

/*************************/
/*   Private Functions   */
/*************************/

/**
 * @brief suiCellOverTempFaultDetect
 *
 * Execute fault detection algorithm for over temperature fault.
 * Return proper flags for either Warning or Alarm.
 *
 * @param shMyMaxTemperature[in] Maximum temperature value from the sensors.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiCellOverTempFaultDetect(int16_t shMyMaxTemperature)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t seAlarmState = FAULT_OFF_STATE;
    static eFault_States_t seWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(seAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_OVER_TEMP_ALARM);

        /* Check for temperature exceeding Alarm threshold */
        if(shMyMaxTemperature > const_shCfgCellOverTempAlarmThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            seAlarmState = FAULT_ACTIVE_STATE;
            seWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_CELL_OVER_TEMP_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_CELL_OVER_TEMP_ALARM);

        /* Check for hysteresis condition */
        if(shMyMaxTemperature <= (const_shCfgCellOverTempAlarmThreshold - const_shCfgCellTempFaultHysteresis))
        {
            seAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_CELL_OVER_TEMP_ALARM);

        /* Check that the Hysteresis condition still met */
        if(shMyMaxTemperature <= (const_shCfgCellOverTempAlarmThreshold - const_shCfgCellTempFaultHysteresis))
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgCellTempFaultDelay)
            {
                seAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_OVER_TEMP_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            seAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (seWarningState)
    {
        case FAULT_OFF_STATE:
        /* Clear OT warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_OVER_TEMP_WARNING);

        /* Check for temperature exceeding Warning threshold */
        if(shMyMaxTemperature > const_shCfgCellOverTempWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            seWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_OVER_TEMP_WARNING);

        /* Verify temp still exceeding threshold */
        if(shMyMaxTemperature > const_shCfgCellOverTempWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgCellTempFaultDelay)
            {
                /* Switch state to warning active */
                seWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_CELL_OVER_TEMP_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            seWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_CELL_OVER_TEMP_WARNING);

        /* Verify the hysteresis condition is met */
        if(shMyMaxTemperature <= (const_shCfgCellOverTempWarningThreshold - const_shCfgCellTempFaultHysteresis))
        {
            /* Switch state to recovering */
            seWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_CELL_OVER_TEMP_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(shMyMaxTemperature <= (const_shCfgCellOverTempWarningThreshold - const_shCfgCellTempFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgCellTempFaultDelay)
            {
                /* Warning has gone away */
                seWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_OVER_TEMP_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            seWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiCellUnderTempFaultDetect
 *
 * Execute fault detection algorithm for under temperature fault.
 * Return proper flags for either Warning or Alarm
 *
 * @param shMyMinTemperature[in] Minimum temperature value from the sensors.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiCellUnderTempFaultDetect(int16_t shMyMinTemperature)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t ucAlarmState = FAULT_OFF_STATE;
    static eFault_States_t ucWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(ucAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_UNDER_TEMP_ALARM);

        /* Check for temperature exceeding Alarm threshold */
        if(shMyMinTemperature < const_shCfgCellUnderTempAlarmThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            ucAlarmState = FAULT_ACTIVE_STATE;
            ucWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_CELL_UNDER_TEMP_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_CELL_UNDER_TEMP_ALARM);

        /* Check for hysteresis condition */
        if(shMyMinTemperature >= (const_shCfgCellUnderTempAlarmThreshold + const_shCfgCellTempFaultHysteresis))
        {
            ucAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_CELL_UNDER_TEMP_ALARM);

        /* Check that the Hysteresis condition still met */
        if(shMyMinTemperature >= (const_shCfgCellUnderTempAlarmThreshold + const_shCfgCellTempFaultHysteresis))
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgCellTempFaultDelay)
            {
                ucAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_UNDER_TEMP_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            ucAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (ucWarningState)
    {
        case FAULT_OFF_STATE:

        /* Clear OT warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_UNDER_TEMP_WARNING);

        /* Check for temperature exceeding Warning threshold */
        if(shMyMinTemperature < const_shCfgCellUnderTempWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            ucWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_UNDER_TEMP_WARNING);

        /* Verify temp still exceeding threshold */
        if(shMyMinTemperature < const_shCfgCellUnderTempWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgCellTempFaultDelay)
            {
                /* Switch state to warning active */
                ucWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_CELL_UNDER_TEMP_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            ucWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_CELL_UNDER_TEMP_WARNING);

        /* Verify the hysteresis condition is met */
        if(shMyMinTemperature >= (const_shCfgCellUnderTempWarningThreshold + const_shCfgCellTempFaultHysteresis))
        {
            /* Switch state to recovering */
            ucWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_CELL_UNDER_TEMP_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(shMyMinTemperature >= (const_shCfgCellUnderTempWarningThreshold + const_shCfgCellTempFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgCellTempFaultDelay)
            {
                /* Warning has gone away */
                ucWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_UNDER_TEMP_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            ucWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiElectronicsOverTempFaultDetect
 *
 * Execute fault detection algorithm for over temperature fault.
 * Return proper flags for either Warning or Alarm.
 *
 * @param shMyMaxTemperature[in] Maximum temperature value from the sensors.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiElectronicsOverTempFaultDetect(int16_t shMyMaxTemperature)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t seAlarmState = FAULT_OFF_STATE;
    static eFault_States_t seWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(seAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_ELECTRONICS_OVER_TEMP_ALARM);

        /* Check for temperature exceeding Alarm threshold */
        if(shMyMaxTemperature > const_shCfgElectronicsOverTempAlarmThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            seAlarmState = FAULT_ACTIVE_STATE;
            seWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_ELECTRONICS_OVER_TEMP_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_ELECTRONICS_OVER_TEMP_ALARM);

        /* Check for hysteresis condition */
        if(shMyMaxTemperature <= (const_shCfgElectronicsOverTempAlarmThreshold - const_shCfgElectronicsTempFaultHysteresis))
        {
            seAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_ELECTRONICS_OVER_TEMP_ALARM);

        /* Check that the Hysteresis condition still met */
        if(shMyMaxTemperature <= (const_shCfgElectronicsOverTempAlarmThreshold - const_shCfgElectronicsTempFaultHysteresis))
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgElectronicsTempFaultDelay)
            {
                seAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_ELECTRONICS_OVER_TEMP_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            seAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (seWarningState)
    {
        case FAULT_OFF_STATE:
        /* Clear OT warning bit from return value */
        uiReturnValue &= ~(1 << FLT_ELECTRONICS_OVER_TEMP_WARNING);

        /* Check for temperature exceeding Warning threshold */
        if(shMyMaxTemperature > const_shCfgElectronicsOverTempWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            seWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_ELECTRONICS_OVER_TEMP_WARNING);

        /* Verify temp still exceeding threshold */
        if(shMyMaxTemperature > const_shCfgElectronicsOverTempWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgElectronicsTempFaultDelay)
            {
                /* Switch state to warning active */
                seWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_ELECTRONICS_OVER_TEMP_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            seWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_ELECTRONICS_OVER_TEMP_WARNING);

        /* Verify the hysteresis condition is met */
        if(shMyMaxTemperature <= (const_shCfgElectronicsOverTempWarningThreshold - const_shCfgElectronicsTempFaultHysteresis))
        {
            /* Switch state to recovering */
            seWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_ELECTRONICS_OVER_TEMP_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(shMyMaxTemperature <= (const_shCfgElectronicsOverTempWarningThreshold - const_shCfgElectronicsTempFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgElectronicsTempFaultDelay)
            {
                /* Warning has gone away */
                seWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_ELECTRONICS_OVER_TEMP_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            seWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiCellOverVoltageFaultDetect
 *
 * Execute fault detection algorithm for over voltage fault.
 * Return proper flags for either Warning or Alarm.
 *
 * @param ushMaxCellV[in] Maximum cell voltage.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiCellOverVoltageFaultDetect(uint16_t ushMaxCellV)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t seAlarmState = FAULT_OFF_STATE;
    static eFault_States_t seWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(seAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_OVER_VOLTAGE_ALARM);

        /* Check for voltage exceeding Alarm threshold */
        if(ushMaxCellV > const_ushCfgCellOverVoltageAlarmThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            seAlarmState = FAULT_ACTIVE_STATE;
            seWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_CELL_OVER_VOLTAGE_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_CELL_OVER_VOLTAGE_ALARM);

        /* Check for hysteresis condition */
        if(ushMaxCellV <= (const_ushCfgCellOverVoltageAlarmThreshold - const_ushCfgCellVoltageFaultHysteresis))
        {
            seAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_CELL_OVER_VOLTAGE_ALARM);

        /* Check that the Hysteresis condition still met */
        if(ushMaxCellV <= (const_ushCfgCellOverVoltageAlarmThreshold - const_ushCfgCellVoltageFaultHysteresis))
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgCellVoltageFaultDelay)
            {
                seAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_OVER_VOLTAGE_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            seAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;

        return uiReturnValue;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (seWarningState)
    {
        case FAULT_OFF_STATE:

        /* Clear OT warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_OVER_VOLTAGE_WARNING);

        /* Check for temperature exceeding Warning threshold */
        if(ushMaxCellV > const_ushCfgCellOverVoltageWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            seWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_OVER_VOLTAGE_WARNING);

        /* Verify temp still exceeding threshold */
        if(ushMaxCellV > const_ushCfgCellOverVoltageWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgCellVoltageFaultDelay)
            {
                /* Switch state to warning active */
                seWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_CELL_OVER_VOLTAGE_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            seWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_CELL_OVER_VOLTAGE_WARNING);

        /* Verify the hysteresis condition is met */
        if(ushMaxCellV <= (const_ushCfgCellOverVoltageWarningThreshold - const_ushCfgCellVoltageFaultHysteresis))
        {
            /* Switch state to recovering */
            seWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_CELL_OVER_VOLTAGE_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(ushMaxCellV <= (const_ushCfgCellOverVoltageWarningThreshold - const_ushCfgCellVoltageFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgCellVoltageFaultDelay)
            {
                /* Warning has gone away */
                seWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_OVER_VOLTAGE_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            seWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiCellUnderVoltageFaultDetect
 *
 * Execute fault detection algorithm for under voltage fault.
 * Return proper flags for either Warning or Alarm.
 *
 * @param ushMinCellV[in] Minimum cell voltage.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiCellUnderVoltageFaultDetect(uint16_t ushMinCellV)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t ucAlarmState = FAULT_OFF_STATE;
    static eFault_States_t ucWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(ucAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_UNDER_VOLTAGE_ALARM);

        /* Check for temperature exceeding Alarm threshold */
        if(ushMinCellV < const_ushCfgCellUnderVoltageAlarmThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            ucAlarmState = FAULT_ACTIVE_STATE;
            ucWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_CELL_UNDER_VOLTAGE_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_CELL_UNDER_VOLTAGE_ALARM);

        /* Check for hysteresis condition */
        if(ushMinCellV >= (const_ushCfgCellUnderVoltageAlarmThreshold + const_ushCfgCellVoltageFaultHysteresis))
        {
            ucAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_CELL_UNDER_VOLTAGE_ALARM);

        /* Check that the Hysteresis condition still met */
        if(ushMinCellV >= (const_ushCfgCellUnderVoltageAlarmThreshold + const_ushCfgCellVoltageFaultHysteresis))
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgCellVoltageFaultDelay)
            {
                ucAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_UNDER_VOLTAGE_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            ucAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;

        return uiReturnValue;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (ucWarningState)
    {
        case FAULT_OFF_STATE:

        /* Clear UV warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_UNDER_VOLTAGE_WARNING);

        /* Check for temperature exceeding Warning threshold */
        if(ushMinCellV < const_ushCfgCellUnderVoltageWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            ucWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_UNDER_VOLTAGE_WARNING);

        /* Verify temp still exceeding threshold */
        if(ushMinCellV < const_ushCfgCellUnderVoltageWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgCellVoltageFaultDelay)
            {
                /* Switch state to warning active */
                ucWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_CELL_UNDER_VOLTAGE_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            ucWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_CELL_UNDER_VOLTAGE_WARNING);

        /* Verify the hysteresis condition is met */
        if(ushMinCellV >= (const_ushCfgCellUnderVoltageWarningThreshold + const_ushCfgCellVoltageFaultHysteresis))
        {
            /* Switch state to recovering */
            ucWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= FLT_CELL_UNDER_VOLTAGE_WARNING;

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(ushMinCellV >= (const_ushCfgCellUnderVoltageWarningThreshold + const_ushCfgCellVoltageFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgCellVoltageFaultDelay)
            {
                /* Warning has gone away */
                ucWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_UNDER_VOLTAGE_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            ucWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiBatteryOverVoltageFaultDetect
 *
 * Execute fault detection algorithm for over voltage fault.
 * Return proper flags for either Warning or Alarm.
 *
 * @param ushBattVoltage[in] Battery voltage.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiBatteryOverVoltageFaultDetect(uint16_t ushBattVoltage)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t eOVAlarmState = FAULT_OFF_STATE;
    static eFault_States_t eOVWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(eOVAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_OVER_VOLTAGE_ALARM);

        /* Check for voltage exceeding Alarm threshold */
        if(ushBattVoltage > const_ushCfgBatteryOverVoltageAlarmThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            eOVAlarmState = FAULT_ACTIVE_STATE;
            eOVWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_BATTERY_OVER_VOLTAGE_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_BATTERY_OVER_VOLTAGE_ALARM);

        /* Check for hysteresis condition */
        if(ushBattVoltage <= (const_ushCfgBatteryOverVoltageAlarmThreshold - const_ushCfgBatteryVoltageFaultHysteresis))
        {
            eOVAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_BATTERY_OVER_VOLTAGE_ALARM);

        /* Check that the Hysteresis condition still met */
        if(ushBattVoltage <= (const_ushCfgBatteryOverVoltageAlarmThreshold - const_ushCfgBatteryVoltageFaultHysteresis))
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgBatteryVoltageFaultDelay)
            {
                eOVAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_BATTERY_OVER_VOLTAGE_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            eOVAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;

        return uiReturnValue;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (eOVWarningState)
    {
        case FAULT_OFF_STATE:

        /* Clear OT warning bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_OVER_VOLTAGE_WARNING);

        /* Check for temperature exceeding Warning threshold */
        if(ushBattVoltage > const_ushCfgBatteryOverVoltageWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            eOVWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_OVER_VOLTAGE_WARNING);

        /* Verify temp still exceeding threshold */
        if(ushBattVoltage > const_ushCfgBatteryOverVoltageWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgBatteryVoltageFaultDelay)
            {
                /* Switch state to warning active */
                eOVWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_BATTERY_OVER_VOLTAGE_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            eOVWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_BATTERY_OVER_VOLTAGE_WARNING);

        /* Verify the hysteresis condition is met */
        if(ushBattVoltage <= (const_ushCfgBatteryOverVoltageWarningThreshold - const_ushCfgBatteryVoltageFaultHysteresis))
        {
            /* Switch state to recovering */
            eOVWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_BATTERY_OVER_VOLTAGE_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(ushBattVoltage <= (const_ushCfgBatteryOverVoltageWarningThreshold - const_ushCfgBatteryVoltageFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgBatteryVoltageFaultDelay)
            {
                /* Warning has gone away */
                eOVWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_BATTERY_OVER_VOLTAGE_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            eOVWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiBatteryUnderVoltageFaultDetect
 *
 * Execute fault detection algorithm for under voltage fault.
 * Return proper flags for either Warning or Alarm.
 *
 * @param ushBattVoltage[in] Battery voltage.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiBatteryUnderVoltageFaultDetect(uint16_t ushBattVoltage)
{
    static uint16_t ushUTAlarmTimer = 0;
    static uint16_t ushUTWarningTimer = 0;
    static eFault_States_t ucUVAlarmState = FAULT_OFF_STATE;
    static eFault_States_t ucUVWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(ucUVAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_VOLTAGE_ALARM);

        /* Check for temperature exceeding Alarm threshold */
        if(ushBattVoltage < const_ushCfgBatteryUnderVoltageAlarmThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            ucUVAlarmState = FAULT_ACTIVE_STATE;
            ucUVWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_BATTERY_UNDER_VOLTAGE_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_BATTERY_UNDER_VOLTAGE_ALARM);

        /* Check for hysteresis condition */
        if(ushBattVoltage >= (const_ushCfgBatteryUnderVoltageAlarmThreshold + const_ushCfgBatteryVoltageFaultHysteresis))
        {
            ucUVAlarmState = FAULT_RECOVERING_STATE;
            ushUTAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_BATTERY_UNDER_VOLTAGE_ALARM);

        /* Check that the Hysteresis condition still met */
        if(ushBattVoltage >= (const_ushCfgBatteryUnderVoltageAlarmThreshold + const_ushCfgBatteryVoltageFaultHysteresis))
        {
            /* Fault delay check */
            if(++ushUTAlarmTimer >= const_ushCfgBatteryVoltageFaultDelay)
            {
                ucUVAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_VOLTAGE_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            ucUVAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;

        return uiReturnValue;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (ucUVWarningState)
    {
        case FAULT_OFF_STATE:

        /* Clear UV warning bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_VOLTAGE_WARNING);

        /* Check for temperature exceeding Warning threshold */
        if(ushBattVoltage < const_ushCfgBatteryUnderVoltageWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            ucUVWarningState = FAULT_PENDING_STATE;
            ushUTWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_VOLTAGE_WARNING);

        /* Verify temp still exceeding threshold */
        if(ushBattVoltage < const_ushCfgBatteryUnderVoltageWarningThreshold)
        {
            if(++ushUTWarningTimer >= const_ushCfgBatteryVoltageFaultDelay)
            {
                /* Switch state to warning active */
                ucUVWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_BATTERY_UNDER_VOLTAGE_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            ucUVWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_BATTERY_UNDER_VOLTAGE_WARNING);

        /* Verify the hysteresis condition is met */
        if(ushBattVoltage >= (const_ushCfgBatteryUnderVoltageWarningThreshold + const_ushCfgBatteryVoltageFaultHysteresis))
        {
            /* Switch state to recovering */
            ucUVWarningState = FAULT_RECOVERING_STATE;
            ushUTWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_BATTERY_UNDER_VOLTAGE_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(ushBattVoltage >= (const_ushCfgBatteryUnderVoltageWarningThreshold + const_ushCfgBatteryVoltageFaultHysteresis))
        {
            /* Delay */
            if(++ushUTWarningTimer >= const_ushCfgBatteryVoltageFaultDelay)
            {
                /* Warning has gone away */
                ucUVWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_VOLTAGE_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            ucUVWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiUnderSOCFaultDetect
 *
 * Execute fault detection algorithm for under-SOC faults.
 * Return proper flags for either Warning or Alarm.
 *
 * @param ushSOC[in] Battery SOC.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiUnderSOCFaultDetect(uint16_t ushSOC)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t ucUSOCAlarmState = FAULT_OFF_STATE;
    static eFault_States_t ucUSOCWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(ucUSOCAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_SOC_ALARM);

        /* Check for temperature exceeding Alarm threshold */
        if(ushSOC < const_ushCfgBatteryUnderSOCAlarmThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            ucUSOCAlarmState = FAULT_ACTIVE_STATE;
            ucUSOCWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_BATTERY_UNDER_SOC_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_BATTERY_UNDER_SOC_ALARM);

        /* Check for hysteresis condition */
        if(ushSOC >= (const_ushCfgBatteryUnderSOCAlarmThreshold + const_ushCfgBatterySOCFaultHysteresis))
        {
            ucUSOCAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_BATTERY_UNDER_SOC_ALARM);

        /* Check that the Hysteresis condition still met */
        if(ushSOC >= (const_ushCfgBatteryUnderSOCAlarmThreshold + const_ushCfgBatterySOCFaultHysteresis))
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgBatterySOCFaultDelay)
            {
                ucUSOCAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_SOC_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            ucUSOCAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (ucUSOCWarningState)
    {
        case FAULT_OFF_STATE:

        /* Clear OT warning bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_SOC_WARNING);

        /* Check for temperature exceeding Warning threshold */
        if(ushSOC < const_ushCfgBatteryUnderSOCWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            ucUSOCWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_SOC_WARNING);

        /* Verify temp still exceeding threshold */
        if(ushSOC < const_ushCfgBatteryUnderSOCWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgBatterySOCFaultDelay)
            {
                /* Switch state to warning active */
                ucUSOCWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_BATTERY_UNDER_SOC_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            ucUSOCWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_BATTERY_UNDER_SOC_WARNING);

        /* Verify the hysteresis condition is met */
        if(ushSOC >= (const_ushCfgBatteryUnderSOCWarningThreshold + const_ushCfgBatterySOCFaultHysteresis))
        {
            /* Switch state to recovering */
            ucUSOCWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_BATTERY_UNDER_SOC_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(ushSOC >= (const_ushCfgBatteryUnderSOCWarningThreshold + const_ushCfgBatterySOCFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgBatterySOCFaultDelay)
            {
                /* Warning has gone away */
                ucUSOCWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_BATTERY_UNDER_SOC_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            ucUSOCWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiOverSOCFaultDetect
 *
 * Execute fault detection algorithm for over-SOC faults.
 * Return proper flags for either Warning or Alarm.
 *
 * @param ushSOC[in] Battery SOC.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiOverSOCFaultDetect(uint16_t ushSOC)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t ucUSOCAlarmState = FAULT_OFF_STATE;
    static eFault_States_t ucUSOCWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(ucUSOCAlarmState)
    {
        case FAULT_OFF_STATE:
        uiReturnValue &= ~(1 << FLT_BATTERY_OVER_SOC_ALARM);

        if(ushSOC > const_ushCfgBatteryOverSOCAlarmThreshold)
        {
            ucUSOCAlarmState = FAULT_ACTIVE_STATE;
            ucUSOCWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_BATTERY_OVER_SOC_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        uiReturnValue |= (1 << FLT_BATTERY_OVER_SOC_ALARM);

        if(ushSOC <= (const_ushCfgBatteryOverSOCAlarmThreshold - const_ushCfgBatterySOCFaultHysteresis))
        {
            ucUSOCAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        uiReturnValue |= (1 << FLT_BATTERY_OVER_SOC_ALARM);

        if(ushSOC <= (const_ushCfgBatteryOverSOCAlarmThreshold - const_ushCfgBatterySOCFaultHysteresis))
        {
            if(++sushAlarmTimer >= const_ushCfgBatterySOCFaultDelay)
            {
                ucUSOCAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_BATTERY_OVER_SOC_ALARM);
            }
        }
        else
        {
            ucUSOCAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (ucUSOCWarningState)
    {
        case FAULT_OFF_STATE:
        uiReturnValue &= ~(1 << FLT_BATTERY_OVER_SOC_WARNING);

        if(ushSOC > const_ushCfgBatteryOverSOCWarningThreshold)
        {
            ucUSOCWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        uiReturnValue &= ~(1 << FLT_BATTERY_OVER_SOC_WARNING);

        if(ushSOC > const_ushCfgBatteryOverSOCWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgBatterySOCFaultDelay)
            {
                ucUSOCWarningState = FAULT_ACTIVE_STATE;

                uiReturnValue |= (1 << FLT_BATTERY_OVER_SOC_WARNING);
            }
        }
        else
        {
            ucUSOCWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        uiReturnValue |= (1 << FLT_BATTERY_OVER_SOC_WARNING);

        if(ushSOC <= (const_ushCfgBatteryOverSOCWarningThreshold - const_ushCfgBatterySOCFaultHysteresis))
        {
            ucUSOCWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        uiReturnValue |= (1 << FLT_BATTERY_OVER_SOC_WARNING);

        if(ushSOC <= (const_ushCfgBatteryOverSOCWarningThreshold - const_ushCfgBatterySOCFaultHysteresis))
        {
            if(++sushWarningTimer >= const_ushCfgBatterySOCFaultDelay)
            {
                ucUSOCWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_BATTERY_OVER_SOC_WARNING);
            }
        }
        else
        {
            ucUSOCWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    return  uiReturnValue;
}

/**
 * @brief suiOverCurrentChargeFaultDetect
 *
 * Execute fault detection algorithm for over current in charge fault.
 * Return proper flags for either Warning or Alarm.
 *
 * @param shMeasuredCurrent[in] Measured current value.
 * @param shChargeCurrentLimit[in] Maximum allowable current in charge.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiOverCurrentChargeFaultDetect(int16_t shMeasuredCurrent, int16_t shChargeCurrentLimit)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t seAlarmState = FAULT_OFF_STATE;
    static eFault_States_t seWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;
    int16_t shTemporaryThreshold;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(seAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_CHARGE_OVER_CURRENT_ALARM);

        /* Check for value exceeding Alarm threshold */
        shTemporaryThreshold = max(((shChargeCurrentLimit * const_ushCfgOverCurrentInChargeAlarmThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInChargeMinimumCurrent);
        if(shMeasuredCurrent > shTemporaryThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            seAlarmState = FAULT_ACTIVE_STATE;
            seWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_CHARGE_OVER_CURRENT_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_CHARGE_OVER_CURRENT_ALARM);

        /* Check for hysteresis condition */
        shTemporaryThreshold = max(((shChargeCurrentLimit * const_ushCfgOverCurrentInChargeAlarmThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase) - ((shChargeCurrentLimit * const_ushCfgOverCurrentFaultHysteresis)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInChargeMinimumCurrent);
        if(shMeasuredCurrent <= shTemporaryThreshold)
        {
            seAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_CHARGE_OVER_CURRENT_ALARM);

        /* Check that the Hysteresis condition still met */
        shTemporaryThreshold = max(((shChargeCurrentLimit * const_ushCfgOverCurrentInChargeAlarmThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase) - ((shChargeCurrentLimit * const_ushCfgOverCurrentFaultHysteresis)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInChargeMinimumCurrent);
        if(shMeasuredCurrent <= shTemporaryThreshold)
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgOverCurrentFaultDelay)
            {
                seAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CHARGE_OVER_CURRENT_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            seAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (seWarningState)
    {
        case FAULT_OFF_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CHARGE_OVER_CURRENT_WARNING);

        /* Check for value exceeding Warning threshold */
        shTemporaryThreshold = max(((shChargeCurrentLimit * const_ushCfgOverCurrentInChargeWarningThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInChargeMinimumCurrent);
        if(shMeasuredCurrent > shTemporaryThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            seWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CHARGE_OVER_CURRENT_WARNING);

        /* Verify value still exceeding threshold */
        shTemporaryThreshold = max(((shChargeCurrentLimit * const_ushCfgOverCurrentInChargeWarningThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInChargeMinimumCurrent);
        if(shMeasuredCurrent > shTemporaryThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgOverCurrentFaultDelay)
            {
                /* Switch state to warning active */
                seWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_CHARGE_OVER_CURRENT_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            seWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_CHARGE_OVER_CURRENT_WARNING);

        /* Verify the hysteresis condition is met */
        shTemporaryThreshold = max(((shChargeCurrentLimit * const_ushCfgOverCurrentInChargeWarningThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase) - ((shChargeCurrentLimit * const_ushCfgOverCurrentFaultHysteresis)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInChargeMinimumCurrent);
        if(shMeasuredCurrent <= shTemporaryThreshold)
        {
            /* Switch state to recovering */
            seWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_CHARGE_OVER_CURRENT_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        shTemporaryThreshold = max(((shChargeCurrentLimit * const_ushCfgOverCurrentInChargeWarningThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase) - ((shChargeCurrentLimit * const_ushCfgOverCurrentFaultHysteresis)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInChargeMinimumCurrent);
        if(shMeasuredCurrent <= shTemporaryThreshold)
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgOverCurrentFaultDelay)
            {
                /* Warning has gone away */
                seWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CHARGE_OVER_CURRENT_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            seWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiOverCurrentDischargeFaultDetect
 *
 * Execute fault detection algorithm for over current in charge fault.
 * Return proper flags for either Warning or Alarm.
 *
 * @param shMeasuredCurrent[in] Measured current value.
 * @param shChargeCurrentLimit[in] Maximum allowable current in charge.
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiOverCurrentDischargeFaultDetect(int16_t shMeasuredCurrent, int16_t shDischargeCurrentLimit)
{
    static uint16_t sushAlarmTimer = 0;
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t seAlarmState = FAULT_OFF_STATE;
    static eFault_States_t seWarningState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;
    int16_t shTemporaryThreshold;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(seAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_DISCHARGE_OVER_CURRENT_ALARM);

        /* Check for value exceeding Alarm threshold */
        shTemporaryThreshold = min(((shDischargeCurrentLimit * const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInDischargeMinimumCurrent);
        if(shMeasuredCurrent < shTemporaryThreshold)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            seAlarmState = FAULT_ACTIVE_STATE;
            seWarningState = FAULT_ACTIVE_STATE;  /* This activates the Warning Flag, whenever the Alarm is activated */
            uiReturnValue |= (1 << FLT_DISCHARGE_OVER_CURRENT_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_DISCHARGE_OVER_CURRENT_ALARM);

        /* Check for hysteresis condition */
        shTemporaryThreshold = min(((shDischargeCurrentLimit * const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase) - ((shDischargeCurrentLimit * const_ushCfgOverCurrentFaultHysteresis)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInDischargeMinimumCurrent);
        if(shMeasuredCurrent >= shTemporaryThreshold)
        {
            seAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_DISCHARGE_OVER_CURRENT_ALARM);

        /* Check that the Hysteresis condition still met */
        shTemporaryThreshold = min(((shDischargeCurrentLimit * const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase) - ((shDischargeCurrentLimit * const_ushCfgOverCurrentFaultHysteresis)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInDischargeMinimumCurrent);
        if(shMeasuredCurrent >= shTemporaryThreshold)
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgOverCurrentFaultDelay)
            {
                seAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_DISCHARGE_OVER_CURRENT_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            seAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (seWarningState)
    {
        case FAULT_OFF_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_DISCHARGE_OVER_CURRENT_WARNING);

        /* Check for value not exceeding Warning threshold */
        shTemporaryThreshold = min(((shDischargeCurrentLimit * const_ushCfgOverCurrentInDischargeWarningThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInDischargeMinimumCurrent);
        if(shMeasuredCurrent < shTemporaryThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            seWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_DISCHARGE_OVER_CURRENT_WARNING);

        /* Verify value still exceeding threshold */
        shTemporaryThreshold = min(((shDischargeCurrentLimit * const_ushCfgOverCurrentInDischargeWarningThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInDischargeMinimumCurrent);
        if(shMeasuredCurrent < shTemporaryThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgOverCurrentFaultDelay)
            {
                /* Switch state to warning active */
                seWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_DISCHARGE_OVER_CURRENT_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            seWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_DISCHARGE_OVER_CURRENT_WARNING);

        /* Verify the hysteresis condition is met */
        shTemporaryThreshold = min(((shDischargeCurrentLimit * const_ushCfgOverCurrentInDischargeWarningThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase) - ((shDischargeCurrentLimit * const_ushCfgOverCurrentFaultHysteresis)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInDischargeMinimumCurrent);
        if(shMeasuredCurrent >= shTemporaryThreshold)
        {
            /* Switch state to recovering */
            seWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_DISCHARGE_OVER_CURRENT_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        shTemporaryThreshold = min(((shDischargeCurrentLimit * const_ushCfgOverCurrentInDischargeWarningThresholdPercentage)/const_ushCfgOverCurrentThresholdPercentageBase) - ((shDischargeCurrentLimit * const_ushCfgOverCurrentFaultHysteresis)/const_ushCfgOverCurrentThresholdPercentageBase),const_shCfgOverCurrentInDischargeMinimumCurrent);
        if(shMeasuredCurrent >= shTemporaryThreshold)
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgOverCurrentFaultDelay)
            {
                /* Warning has gone away */
                seWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_DISCHARGE_OVER_CURRENT_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            seWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiExtremeOverCurrentFaultDetect
 *
 * Execute fault detection algorithm for over current in charge fault.
 * Return proper flags for Alarm.
 *
 * @param bI2tExceededFlag[in] Flag from fast task to tell if I2t exceeded.
 * @return Bit array of alarms.
 */
static uint32_t suiExtremeOverCurrentFaultDetect(bool bI2tExceededFlag)
{
    static uint16_t sushAlarmTimer = 0;
    static eFault_States_t seAlarmState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;

    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(seAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_EXTREME_OVER_CURRENT_ALARM);

        /* Check for flag */
        if(bI2tExceededFlag == true)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            seAlarmState = FAULT_ACTIVE_STATE;
            uiReturnValue |= (1 << FLT_EXTREME_OVER_CURRENT_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_EXTREME_OVER_CURRENT_ALARM);

        /* Check for deactivation condition */
        if(bI2tExceededFlag == false)
        {
            seAlarmState = FAULT_RECOVERING_STATE;
            sushAlarmTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_EXTREME_OVER_CURRENT_ALARM);

        /* Check that the deactivation condition still met */
        if(bI2tExceededFlag == false)
        {
            /* Fault delay check */
            if(++sushAlarmTimer >= const_ushCfgOverCurrentFaultDelay)
            {
                seAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_EXTREME_OVER_CURRENT_ALARM);
            }
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            seAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiCellTempDifferenceFaultDetect
 *
 * Execute fault detection algorithm for cell temperature difference
 * warning. Return proper flags for Warning
 *
 * @param shMyMaxTemperature[in] Maximum temperature value from the sensor.
 * @param shMyMinTemperature[in] Minimum temperature value from the sensors.
 * @return Bit array of warnings.
 */
static uint32_t suiCellTempDifferenceFaultDetect(int16_t shMyMaxTemperature, int16_t shMyMinTemperature)
{
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t seWarningState = FAULT_OFF_STATE;
    int16_t shCellTempDelta;
    uint32_t uiReturnValue = 0;

    /* Calculate temperature delta */
    shCellTempDelta = shMyMaxTemperature - shMyMinTemperature;

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (seWarningState)
    {
        case FAULT_OFF_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_TEMP_DIFFERENCE_WARNING);

        /* Check for exceeding Warning threshold */
        if(shCellTempDelta > const_shCfgCellTempDifferenceWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            seWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_TEMP_DIFFERENCE_WARNING);

        /* Verify still exceeding threshold */
        if(shCellTempDelta > const_shCfgCellTempDifferenceWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgCellTempDifferenceFaultDelay)
            {
                /* Switch state to warning active */
                seWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_CELL_TEMP_DIFFERENCE_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            seWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_CELL_TEMP_DIFFERENCE_WARNING);

        /* Verify the hysteresis condition is met */
        if(shCellTempDelta <= (const_shCfgCellTempDifferenceWarningThreshold - const_shCfgCellTempDifferenceFaultHysteresis))
        {
            /* Switch state to recovering */
            seWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_CELL_TEMP_DIFFERENCE_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(shCellTempDelta <= (const_shCfgCellTempDifferenceWarningThreshold - const_shCfgCellTempDifferenceFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgCellTempDifferenceFaultDelay)
            {
                /* Warning has gone away */
                seWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_TEMP_DIFFERENCE_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            seWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiCellVoltageImbalanceFaultDetect
 *
 * Execute fault detection algorithm for cell temperature difference
 * warning. Return proper flags for Warning
 *
 * @param ushMaxCellV[in] Maximum cell voltage.
 * @param ushMinCellV[in] Minimum cell voltage.
 * @return Bit array of warnings.
 */
static uint32_t suiCellVoltageImbalanceFaultDetect(uint16_t ushMaxCellV, uint16_t ushMinCellV)
{
    static uint16_t sushWarningTimer = 0;
    static eFault_States_t seWarningState = FAULT_OFF_STATE;
    uint16_t ushCellVoltageDelta;
    uint32_t uiReturnValue = 0;

    /* Calculate voltage delta */
    ushCellVoltageDelta = ushMaxCellV - ushMinCellV;

    /*************************/
    /* Warning State Machine */
    /*************************/
    switch (seWarningState)
    {
        case FAULT_OFF_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_IMBALANCE_WARNING);

        /* Check for exceeding Warning threshold */
        if(ushCellVoltageDelta > const_ushCfgCellVoltageImbalanceWarningThreshold)
        {
            /* Switch state to warning pending and start the countdown*/
            seWarningState = FAULT_PENDING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_PENDING_STATE:
        /* Clear warning bit from return value */
        uiReturnValue &= ~(1 << FLT_CELL_IMBALANCE_WARNING);

        /* Verify still exceeding threshold */
        if(ushCellVoltageDelta > const_ushCfgCellVoltageImbalanceWarningThreshold)
        {
            if(++sushWarningTimer >= const_ushCfgCellVoltageImbalanceFaultDelay)
            {
                /* Switch state to warning active */
                seWarningState = FAULT_ACTIVE_STATE;

                /* Set warning bit on return value */
                uiReturnValue |= (1 << FLT_CELL_IMBALANCE_WARNING);
            }
        }
        else
        {
            /* Threshold no longer exceeded */
            seWarningState = FAULT_OFF_STATE;
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set warning bit on return value */
        uiReturnValue |= (1 << FLT_CELL_IMBALANCE_WARNING);

        /* Verify the hysteresis condition is met */
        if(ushCellVoltageDelta <= (const_ushCfgCellVoltageImbalanceWarningThreshold - const_ushCfgCellVoltageImbalanceFaultHysteresis))
        {
            /* Switch state to recovering */
            seWarningState = FAULT_RECOVERING_STATE;
            sushWarningTimer = 0;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* Set warning bit in return value */
        uiReturnValue |= (1 << FLT_CELL_IMBALANCE_WARNING);

        /* Hysteresis condition must be met continuously for the full length of the delay */
        if(ushCellVoltageDelta <= (const_ushCfgCellVoltageImbalanceWarningThreshold - const_ushCfgCellVoltageImbalanceFaultHysteresis))
        {
            /* Delay */
            if(++sushWarningTimer >= const_ushCfgCellVoltageImbalanceFaultDelay)
            {
                /* Warning has gone away */
                seWarningState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_CELL_IMBALANCE_WARNING);
            }
        }
        else
        {
            /* Hysteresis no longer met, going back to warning active */
            seWarningState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }

    /* Return bit array */
    return  uiReturnValue;
}

/**
 * @brief suiSelfTestFaultDetect
 *
 * Execute fault detection algorithm for Self-test alarm.
 * Return proper flags for either Warning or Alarm
 *
 * @return Bit array of warnings and alarms.
 */
static uint32_t suiSelfTestFaultDetect(void)
{
    static sSelfTestResults_t* spstSelfTestResultsLocal;
    bool bSelfTestFail = false;
    uint32_t uiReturnValue = 0;

    spstSelfTestResultsLocal = spGetSelfTestResultsBuffer();
    /* Create bit array of self-test flags */
    bSelfTestFail = !(spstSelfTestResultsLocal->sADC1abFuncTestResults.bAdc1abTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sCellVoltFuncVerTestResults.bCVFVTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sCTOpenDetTestResults.bCTOpenDetTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sCTOpenDetTestResults.bCTShortDetTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sCTOpenDetTestResults.bCTOpenOutRangeTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sCTOpenDetTestResults.bCTCloseOutRangeTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sCTLeakageTestResults.bCTLeakageTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sCellBalanceTestResults.bCBOpenTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sCellBalanceTestResults.bCBShortTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sOvUvFuncTestResults.bUvFaultTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sOvUvFuncTestResults.bOvFaultTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sIMeashAmpInputGndTestResults.bIMeashAmpInputGndTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sVrefDiagTestResults.bVrefDiagTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sGpioOpenTestResults.bGpioOpenTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sStackVsVolTestResults.bStackVsSumTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sISenseMeasChainOpenTestResults.bISenseMeasChainOpenTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sSupplyVoltagesTestResults.bSupply_3_3VTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sSupplyVoltagesTestResults.bSupply_12VTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sSupplyVoltagesTestResults.bSupply_5VTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sFaultPinDetectTestResults.bFaultPinDetectTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sTempSensorTestResults.bTempSensorTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sConfigurationAndParameterRangeTestResults.bConfigurationVariablesRangeTestPass);
    bSelfTestFail |= !(spstSelfTestResultsLocal->sConfigurationAndParameterRangeTestResults.bParameterVariablesRangeTestPass);
    //bSelfTestFail |= !(spstSelfTestResultsLocal->sCurrentInDeadzoneTestResults.bCurrentInDeadzoneTestPass);

    if (bSelfTestFail == true)
    {
        uiReturnValue |= 1<<FLT_SELF_TEST_FAILURE_ALARM;
    }

    return uiReturnValue;
}

/**
 * @brief suiFuseBlownFaultDetect
 *
 * Execute fault detection algorithm for fuse blown fault.
 * Return proper flags for Alarm.
 *
 * @param shMeasuredCurrentAvg[in] Average measured current.
 * @param ushSumOfCellsVoltage[in] Battery stack voltage sum of cell voltages.
 * @param ushNetworkVoltage[in] Network voltage.
 * @return Bit array of alarms.
 */
static uint32_t suiFuseBlownFaultDetect(int16_t shMeasuredCurrentAvg, uint16_t ushSumOfCellsVoltage, uint16_t ushNetworkVoltage)
{
    static eFault_States_t seAlarmState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;
    uint8_t uchFullChargeDischarge = FULL_CHARGE_FET_ENABLE_MASK | FULL_DISCHARGE_FET_ENABLE_MASK | 
                                    SAFETY_CHARGE_FET_ENABLE_MASK | REG_DISCHARGE_FET_ENABLE_MASK | 
                                    REG_CHARGE_FET_ENABLE_MASK | REGULATED_CURRENT_LVL_1_MASK | 
                                    REGULATED_CURRENT_LVL_2_MASK | REGULATED_CURRENT_LVL_3_MASK;
    bool bFuseBlownFlag = false;
    
    /* Check all FETs are closed */
    if(vGetFetStatus() == uchFullChargeDischarge)
    {
        /* Fuse blown if average measured current < 50mA (TBD) and network to battery stack voltage delta > 2V (TBD) */
        bFuseBlownFlag = (abs(shMeasuredCurrentAvg) < 5) && (abs(ushSumOfCellsVoltage - ushNetworkVoltage) > 2);
        
        /***********************/
        /* Alarm State Machine */
        /***********************/
        switch(seAlarmState)
        {
            case FAULT_OFF_STATE:
            /* Clear alarm bit from return value */
            uiReturnValue &= ~(1 << FLT_FUSE_BLOWN_ALARM);

            /* Check for flag */
            if(bFuseBlownFlag == true)
            {
                /* Switch state to alarm active and set the corresponding fault bit */
                seAlarmState = FAULT_ACTIVE_STATE;
                uiReturnValue |= (1 << FLT_FUSE_BLOWN_ALARM);
            }
            break;

            case FAULT_ACTIVE_STATE:
            /* Set alarm bit on return value */
            uiReturnValue |= (1 << FLT_FUSE_BLOWN_ALARM);

            /* Check for deactivation condition */
            if(bFuseBlownFlag == false)
            {
                seAlarmState = FAULT_RECOVERING_STATE;
            }
            break;

            case FAULT_RECOVERING_STATE:
            /* In recovery, but alarm is still present until time limit is reached */
            uiReturnValue |= (1 << FLT_FUSE_BLOWN_ALARM);

            /* Check that the deactivation condition still met */
            if(bFuseBlownFlag == false)
            {
                seAlarmState = FAULT_OFF_STATE;
                uiReturnValue &= ~(1 << FLT_FUSE_BLOWN_ALARM);
            }
            else
            {
                /* Oops the hysteresis condition failed, back to alarm active */
                seAlarmState = FAULT_ACTIVE_STATE;
            }
            break;

            default:
            break;
        }
    }

    /* Return bit array */
    return uiReturnValue;
}

/**
 * @brief suiRedundantSafetyMechanismActivationFaultDetect
 *
 * Execute fault detection algorithm for redundant safety 
 * mechanism activation fault.
 * Return proper flags for Alarm.
 *
 * @return Bit array of alarms.
 */
static uint32_t suiRedundantSafetyMechanismActivationFaultDetect(void)
{
    static eFault_States_t seAlarmState = FAULT_OFF_STATE;
    uint32_t uiReturnValue = 0;
    
    /* Short circuit mechanism */
    bool bShortCircuit = (port_pin_get_input_level(OVER_CURRENT_400A_NOTIFICATION_PIN) == OVER_CURRENT_400A_NOTIFICATION_ACTIVE && 
                        port_pin_get_input_level(OVER_CURRENT_192A_NOTIFICATION_PIN) == OVER_CURRENT_192A_NOTIFICATION_ACTIVE);
    
    /* Cell over/under voltage and over-temperature mechanisms */
    bool bFaultFlag = (port_pin_get_input_level(REDUNDANT_OV_NOTIFICATION_PIN) == REDUNDANT_OV_NOTIFICATION_ACTIVE ||
                        port_pin_get_input_level(REDUNDANT_UV_NOTIFICATION_PIN) == REDUNDANT_UV_NOTIFICATION_ACTIVE ||
                        port_pin_get_input_level(OVER_TEMPERATURE_NOTIFICATION_PIN) == OVER_TEMPERATURE_NOTIFICATION_ACTIVE ||
                        bShortCircuit);
    
    /***********************/
    /* Alarm State Machine */
    /***********************/
    switch(seAlarmState)
    {
        case FAULT_OFF_STATE:
        /* Clear alarm bit from return value */
        uiReturnValue &= ~(1 << FLT_REDUNDANT_SAFETY_MECH_ACTIVATION_ALARM);

        /* Check for flag */
        if(bFaultFlag == true)
        {
            /* Switch state to alarm active and set the corresponding fault bit */
            seAlarmState = FAULT_ACTIVE_STATE;
            uiReturnValue |= (1 << FLT_REDUNDANT_SAFETY_MECH_ACTIVATION_ALARM);
        }
        break;

        case FAULT_ACTIVE_STATE:
        /* Set alarm bit on return value */
        uiReturnValue |= (1 << FLT_REDUNDANT_SAFETY_MECH_ACTIVATION_ALARM);

        /* Check for deactivation condition */
        if(bFaultFlag == false)
        {
            seAlarmState = FAULT_RECOVERING_STATE;
        }
        break;

        case FAULT_RECOVERING_STATE:
        /* In recovery, but alarm is still present until time limit is reached */
        uiReturnValue |= (1 << FLT_REDUNDANT_SAFETY_MECH_ACTIVATION_ALARM);

        /* Check that the deactivation condition still met */
        if(bFaultFlag == false)
        {
            seAlarmState = FAULT_OFF_STATE;
            uiReturnValue &= ~(1 << FLT_REDUNDANT_SAFETY_MECH_ACTIVATION_ALARM);
        }
        else
        {
            /* Oops the hysteresis condition failed, back to alarm active */
            seAlarmState = FAULT_ACTIVE_STATE;
        }
        break;

        default:
        break;
    }
    
    if (bShortCircuit == true)
    {
        /* INT_400A latch reset */
        port_pin_set_output_level(OVER_CURRENT_LATCH_RESET_PIN, OVER_CURRENT_LATCH_RESET_ACTIVE);
    }        

    /* Return bit array */
    return uiReturnValue;
}


/**
* @brief PRIVATE sbEEPROMCommFaultDetect()
*
* Count the number of fault occurrences and  
* trigger the fault after count reaches the threshold
* value.  
*
* @param ushFaultCntThreshold Number of errors that will trigger the fault
* @return boolean, fault status = TRUE or FALSE
*
*/
static bool sbEEPROMCommFaultDetect(uint16_t ushFaultCntThreshold)
{
    static uint16_t ushEEPROMFaultCounter = 0; /* EEPROM comm fault counter, static value initialized to 0 */
    static uint32_t uiEEPROMTotalCounter = 0;  /* Incremented every time the function is called */
    static bool sbReturnValue = false;  /* Return value initialized to FALSE */
    SemaphoreHandle_t pstMySem_EEPROM_Fault_Hdl; /* Semaphore handle */
    
    /* Get semaphore Handle */
    pstMySem_EEPROM_Fault_Hdl = sGetSemaphoreHandle(EEPROM_SPI_FAULT);
    
    /* This is a counting semaphore, loop until false is returned */
    while(xSemaphoreTake(pstMySem_EEPROM_Fault_Hdl, NOWAIT) == pdPASS)
    {
        ushEEPROMFaultCounter++;
        uiEEPROMTotalCounter = 0;
    }
    
    /* Determine whether a fault is detected or not, if it has, set return value to TRUE
       and restart counters */
    if(ushEEPROMFaultCounter >= ushFaultCntThreshold)
    {
        sbReturnValue = true;        
    }    
    
    /* Clear fault counter and restart total counter if fault timeout has elapsed */
    if(uiEEPROMTotalCounter >= FAULT_EXPIRED_HALF_HR)
    {
        sbReturnValue = false;
        ushEEPROMFaultCounter = 0;
        uiEEPROMTotalCounter = 0;
    }
    
    /* Increment total counter */    
    uiEEPROMTotalCounter++;
    
    return sbReturnValue;
}

/**
* @brief PRIVATE sbBMSICCommFaultDetect()
*
* Count the number of fault occurrences and
* trigger the fault after count reaches the threshold
* value.
*
* @param ushFaultCntThreshold Number of errors that will trigger the fault
* @return boolean, fault status = TRUE or FALSE
*
*/
static bool sbBMSICCommFaultDetect(uint16_t ushFaultCntThreshold)
{
    static uint16_t ushBMSICFaultCounter = 0; /* BMSIC comm fault counter, static value initialized to 0 */
    static uint32_t uiBMSICTotalCounter = 0;  /* Incremented every time the function is called */
    static bool sbReturnValue = false; /* Return value initialized to FALSE */
    SemaphoreHandle_t pstMySem_BMSIC_Fault_Hdl; /* Semaphore handle */    
    
    /* Get semaphore Handle */
    pstMySem_BMSIC_Fault_Hdl = sGetSemaphoreHandle(BMSIC_SPI_FAULT);
    
    /* This is a counting semaphore, loop until false is returned */
    while(xSemaphoreTake(pstMySem_BMSIC_Fault_Hdl, NOWAIT) == pdPASS)
    {
        ushBMSICFaultCounter++;
        uiBMSICTotalCounter = 0;
    }
    
    /* Determine whether a fault is detected or not, if it has, set return value to TRUE
       and restart counters */
    if(ushBMSICFaultCounter >= ushFaultCntThreshold)
    {
        sbReturnValue = true;
    }    
    
    /* Clear fault counter and restart total counter if timeout has elapsed */
    if(uiBMSICTotalCounter >= FAULT_EXPIRED_HALF_HR)
    {
        sbReturnValue = false;
        ushBMSICFaultCounter = 0;
        uiBMSICTotalCounter = 0;
    }
    
    /* Increment total counter */    
    uiBMSICTotalCounter++;
    
    return sbReturnValue;
}


/**
* @brief PRIVATE sbTempSenseCommFaultDetect()
*
* Count the number of fault occurrences and
* trigger the fault after count reaches the threshold
* value.
*
* @param ushFaultCntThreshold Number of errors that will trigger the fault
* @return boolean, fault status = TRUE or FALSE
*
*/
static bool sbTempSenseCommFaultDetect(uint16_t ushFaultCntThreshold)
{
    static uint16_t ushTempSenseFaultCounter = 0; /* TempSense comm fault counter, static value initialized to 0 */
    static uint32_t uiTempSenseTotalCounter = 0;  /* Incremented every time the function is called */
    static bool sbReturnValue = false; /* Return value initialized to FALSE */
    SemaphoreHandle_t pstMySem_TempSense_Fault_Hdl; /* Semaphore handle */

    
    /* Get semaphore Handle */
    pstMySem_TempSense_Fault_Hdl = sGetSemaphoreHandle(TEMPSENSE_I2C_FAULT);
    
    /* This is a counting semaphore, loop until false is returned */
    while(xSemaphoreTake(pstMySem_TempSense_Fault_Hdl, NOWAIT) == pdPASS)
    {
        ushTempSenseFaultCounter++;
        uiTempSenseTotalCounter = 0;
    }
    
    /* Determine whether a fault is detected or not, if it has, set return value to TRUE
       and restart counters */
    if(ushTempSenseFaultCounter >= ushFaultCntThreshold)
    {
        sbReturnValue = true;        
    }    
    
    /* Clear fault counter and restart total counter if timeout has elapsed */
    if(uiTempSenseTotalCounter >= FAULT_EXPIRED_HALF_HR)
    {
        sbReturnValue = false;   
        ushTempSenseFaultCounter = 0;
        uiTempSenseTotalCounter = 0;
    }
    
    /* Increment total counter */    
    uiTempSenseTotalCounter++;
    
    return sbReturnValue;
}


/**
* @brief PRIVATE sbLEDCNTLCommFaultDetect()
*
* Count the number of fault occurrences and
* trigger the fault after count reaches the threshold
* value.
*
* @param ushFaultCntThreshold Number of errors that will trigger the fault
* @return boolean, fault status = TRUE or FALSE
*
*/
static bool sbLEDCNTLCommFaultDetect(uint16_t ushFaultCntThreshold)
{
    static uint16_t ushLEDCNTLFaultCounter = 0; /* LEDCNTL comm fault counter, static value initialized to 0 */
    static uint32_t uiLEDCNTLTotalCounter = 0;  /* Incremented every time the function is called */
    static bool sbReturnValue = false; /* Return value initialized to FALSE */
    SemaphoreHandle_t pstMySem_LEDCNTL_Fault_Hdl; /* Semaphore handle */    
    
    /* Get semaphore Handle */
    pstMySem_LEDCNTL_Fault_Hdl = sGetSemaphoreHandle(LEDCNTL_I2C_FAULT);
    
    /* This is a counting semaphore, loop until false is returned */
    while(xSemaphoreTake(pstMySem_LEDCNTL_Fault_Hdl, NOWAIT) == pdPASS)
    {
        ushLEDCNTLFaultCounter++;        
        uiLEDCNTLTotalCounter = 0;
    }
    
    /* Determine whether a fault is detected or not, if it has, set return value to TRUE
       and restart counters */
    if(ushLEDCNTLFaultCounter >= ushFaultCntThreshold)
    {
        sbReturnValue = true;       
    }    
    
    /* Clear fault counter and restart total counter if  timeout has elapsed */
    if(uiLEDCNTLTotalCounter >= FAULT_EXPIRED_HALF_HR)
    {
        sbReturnValue = false;
        ushLEDCNTLFaultCounter = 0;
        uiLEDCNTLTotalCounter = 0;
    }
    
    /* Increment total counter */    
    uiLEDCNTLTotalCounter++;
    
    return sbReturnValue;
}

/*************************/
/*   Public Functions    */
/*************************/

/**
 * @brief vFault_Handler
 *
 * Run through all the fault detection routines and update the BMS Data.
 *
 * @param pstMyBmsData[in] Structure containing most recent BMS data.
 * @return None.
 */
void vFault_Handler(stBmsData_t* pstMyBmsData)
{
    bool bMyTempValue;
    
    /* Detect SPI Communication Faults */
    bMyTempValue = sbEEPROMCommFaultDetect(EEPROM_SPI_FAULT_THRESHOLD);
    bMyTempValue = sbBMSICCommFaultDetect(BMSIC_SPI_FAULT_THRESHOLD);
    
    /* Detect I2C Communication Faults */
    bMyTempValue = sbTempSenseCommFaultDetect(TEMPSENSE_I2C_FAULT_THRESHOLD);
    bMyTempValue = sbLEDCNTLCommFaultDetect(LEDCNTL_I2C_FAULT_THRESHOLD);
    
    /* Detect Cell Over/Under Temperature Faults */
    pstMyBmsData->uiFaultFlagsEvt1 = suiCellOverTempFaultDetect(pstMyBmsData->shCellTemperatureMax);
    pstMyBmsData->uiFaultFlagsEvt1 |= suiCellUnderTempFaultDetect(pstMyBmsData->shCellTemperatureMin);

    /* Detect Over-current Faults */
    pstMyBmsData->uiFaultFlagsEvt1 |= suiOverCurrentChargeFaultDetect(pstMyBmsData->shMeasuredCurrentAvg, pstMyBmsData->shChargeCurrentLimit);
    pstMyBmsData->uiFaultFlagsEvt1 |= suiOverCurrentDischargeFaultDetect(pstMyBmsData->shMeasuredCurrentAvg, pstMyBmsData->shDischargeCurrentLimit);

    /* Detect Battery Over/Under Voltage Faults */
    pstMyBmsData->uiFaultFlagsEvt1 |= suiBatteryOverVoltageFaultDetect(pstMyBmsData->ushSumOfCellsVoltage);
    pstMyBmsData->uiFaultFlagsEvt1 |= suiBatteryUnderVoltageFaultDetect(pstMyBmsData->ushSumOfCellsVoltage);

    /* Detect Under SOC Faults */
    pstMyBmsData->uiFaultFlagsEvt1 |= suiUnderSOCFaultDetect(pstMyBmsData->ushSOC);

    /* Detect Over SOC Faults */
    pstMyBmsData->uiFaultFlagsEvt1 |= suiOverSOCFaultDetect(pstMyBmsData->ushSOC);

    /* Detect Self-test Alarm */
    pstMyBmsData->uiFaultFlagsEvtVnd1 = suiSelfTestFaultDetect();

    /* Detect Extreme over-current Alarm */
    pstMyBmsData->uiFaultFlagsEvtVnd1 |= suiExtremeOverCurrentFaultDetect(pstMyBmsData->bI2tExceededFlag);

    /* Detect Cell Over/Under Voltage Faults */
    pstMyBmsData->uiFaultFlagsEvtVnd1 |= suiCellOverVoltageFaultDetect(pstMyBmsData->ushMaxCellV);
    pstMyBmsData->uiFaultFlagsEvtVnd1 |= suiCellUnderVoltageFaultDetect(pstMyBmsData->ushMinCellV);

    /* Detect Electronics Over Temperature Faults */
    pstMyBmsData->uiFaultFlagsEvtVnd1 |= suiElectronicsOverTempFaultDetect(pstMyBmsData->shElectronicsTemperatureMax);

    /* Detect Cell Temperature Difference Warning */
    pstMyBmsData->uiFaultFlagsEvtVnd1 |= suiCellTempDifferenceFaultDetect(pstMyBmsData->shCellTemperatureMax, pstMyBmsData->shCellTemperatureMin);

    /* Detect Cell Voltage Imbalance Warning */
    pstMyBmsData->uiFaultFlagsEvtVnd1 |= suiCellVoltageImbalanceFaultDetect(pstMyBmsData->ushMaxCellV, pstMyBmsData->ushMinCellV);

    /* Detect Fuse Blown Alarm */
    pstMyBmsData->uiFaultFlagsEvtVnd1 |= suiFuseBlownFaultDetect(pstMyBmsData->shMeasuredCurrentAvg, pstMyBmsData->ushSumOfCellsVoltage, pstMyBmsData->ushAnalogVoltage[ANALOG_IN_6]);

    /* Detect Fuse Blown Alarm */
    pstMyBmsData->uiFaultFlagsEvtVnd1 |= suiRedundantSafetyMechanismActivationFaultDetect();

    /* Assemble Legacy Fault Mapping */
    pstMyBmsData->uiFaultFlagsLegacy = ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_EXTREME_OVER_CURRENT_ALARM) & 1) << FLT_LEGACY_EXTREME_OVER_CURRENT_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvt1 >> FLT_CELL_OVER_TEMP_WARNING) & 1) << FLT_LEGACY_CELL_OVER_TEMP_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvt1 >> FLT_CELL_OVER_TEMP_ALARM) & 1) << FLT_LEGACY_CELL_OVER_TEMP_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvt1 >> FLT_CELL_UNDER_TEMP_WARNING) & 1) << FLT_LEGACY_CELL_UNDER_TEMP_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvt1 >> FLT_CELL_UNDER_TEMP_ALARM) & 1) << FLT_LEGACY_CELL_UNDER_TEMP_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_CELL_OVER_VOLTAGE_WARNING) & 1) << FLT_LEGACY_CELL_OVER_VOLTAGE_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_CELL_OVER_VOLTAGE_ALARM) & 1) << FLT_LEGACY_CELL_OVER_VOLTAGE_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_CELL_UNDER_VOLTAGE_WARNING) & 1) << FLT_LEGACY_CELL_UNDER_VOLTAGE_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_CELL_UNDER_VOLTAGE_ALARM) & 1) << FLT_LEGACY_CELL_UNDER_VOLTAGE_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvt1 >> FLT_CHARGE_OVER_CURRENT_WARNING) & 1) << FLT_LEGACY_CHARGE_OVER_CURRENT_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvt1 >> FLT_DISCHARGE_OVER_CURRENT_ALARM) & 1) << FLT_LEGACY_DISCHARGE_OVER_CURRENT_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_FUSE_BLOWN_ALARM) & 1) << FLT_LEGACY_FUSE_BLOWN_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_BATTERY_UNDER_SOC_WARNING) & 1) << FLT_LEGACY_BATTERY_UNDER_SOC_LVL1_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_BATTERY_UNDER_SOC_WARNING) & 1) << FLT_LEGACY_BATTERY_UNDER_SOC_LVL2_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvt1 >> FLT_BATTERY_OVER_VOLTAGE_WARNING) & 1) << FLT_LEGACY_BATTERY_OVER_VOLTAGE_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvt1 >> FLT_BATTERY_UNDER_VOLTAGE_ALARM) & 1) << FLT_LEGACY_BATTERY_UNDER_VOLTAGE_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_ELECTRONICS_OVER_TEMP_WARNING) & 1) << FLT_LEGACY_ELECTRONICS_OVER_TEMP_WARNING;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_ELECTRONICS_OVER_TEMP_ALARM) & 1) << FLT_LEGACY_ELECTRONICS_OVER_TEMP_ALARM;
    pstMyBmsData->uiFaultFlagsLegacy |= ((pstMyBmsData->uiFaultFlagsEvtVnd1 >> FLT_SELF_TEST_FAILURE_ALARM) & 1) << FLT_LEGACY_SELF_TEST_FAILURE_ALARM;
}

/*------------------------------- end of file ------------------------------*/
