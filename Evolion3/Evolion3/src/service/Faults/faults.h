#ifndef FAULTS_H_
#define FAULTS_H_
/**
 * @copyright (C) Copyright 2017 by SAFT	
 *            All rights reserved	
 *             
 * @copyright This program is the property of Saft America, Inc. and can only be	
 *            used and copied with the prior written authorization of SAFT.	
 *            Any whole or partial copy of this program in either its original form or 
 *            in a modified form must mention this copyright and its owner.	
 *
 * @file faults.h	
 *
 * @author Creator: Adnorin L. Mendez
 *
 * @author Last Modifier: Gerald Percherancier
 *
 * @date File Creation: 2018-01-22
 *
 * @date Last Modification: 2018-03-05
 *
 * @version xx.yy.zz
 *
 * @brief Header file for faults.c, which provides prototypes for the
 *        public interface functions. Also includes types and enumeration
 *        definitions.	
 *	
 * Detailed description starting with 
 * Evolion 3 Software
 *	
 */ 

/**
 * @brief eFault_States_t
 *
 * Definition of the fault machine states
 */
typedef enum
{
    FAULT_OFF_STATE,
    FAULT_PENDING_STATE,
    FAULT_ACTIVE_STATE,
    FAULT_RECOVERING_STATE,
}eFault_States_t;

/* Fault Flags Lo Register (MESA Defined) */
#define FLT_COMMUNICATION_ERROR             0
#define FLT_CELL_OVER_TEMP_ALARM            1
#define FLT_CELL_OVER_TEMP_WARNING          2
#define FLT_CELL_UNDER_TEMP_ALARM           3
#define FLT_CELL_UNDER_TEMP_WARNING         4
#define FLT_CHARGE_OVER_CURRENT_ALARM       5
#define FLT_CHARGE_OVER_CURRENT_WARNING     6
#define FLT_DISCHARGE_OVER_CURRENT_ALARM    7
#define FLT_DISCHARGE_OVER_CURRENT_WARNING  8
#define FLT_BATTERY_OVER_VOLTAGE_ALARM      9
#define FLT_BATTERY_OVER_VOLTAGE_WARNING    10
#define FLT_BATTERY_UNDER_VOLTAGE_ALARM     11
#define FLT_BATTERY_UNDER_VOLTAGE_WARNING   12
#define FLT_BATTERY_UNDER_SOC_ALARM         13
#define FLT_BATTERY_UNDER_SOC_WARNING       14
#define FLT_BATTERY_OVER_SOC_ALARM          15
#define FLT_BATTERY_OVER_SOC_WARNING        16
#define FLT_MESA_ALARM_MASK                 0x0000AAAA  /* TO DO: Update when remaining faults are added */

/* Fault Flags Hi Register (Vendor Defined) */
#define FLT_SELF_TEST_FAILURE_ALARM                 0
#define FLT_EXTREME_OVER_CURRENT_ALARM              1
#define FLT_CELL_OVER_VOLTAGE_ALARM                 2
#define FLT_CELL_OVER_VOLTAGE_WARNING               3
#define FLT_CELL_UNDER_VOLTAGE_ALARM                4
#define FLT_CELL_UNDER_VOLTAGE_WARNING              5
#define FLT_ELECTRONICS_OVER_TEMP_ALARM             6
#define FLT_ELECTRONICS_OVER_TEMP_WARNING           7
#define FLT_CELL_TEMP_DIFFERENCE_WARNING            8
#define FLT_CELL_IMBALANCE_WARNING                  9
#define FLT_FUSE_BLOWN_ALARM                        10
#define FLT_REDUNDANT_SAFETY_MECH_ACTIVATION_ALARM  11
#define FLT_VEND_ALARM_MASK                         0x00000457 /* TO DO: Update when remaining faults are added */

/* Fault Flags Legacy Compatible Registers */
#define FLT_LEGACY_EXTREME_OVER_CURRENT_ALARM       1
#define FLT_LEGACY_CELL_OVER_TEMP_WARNING           2
#define FLT_LEGACY_CELL_OVER_TEMP_ALARM             3
#define FLT_LEGACY_CELL_UNDER_TEMP_WARNING          4
#define FLT_LEGACY_CELL_UNDER_TEMP_ALARM            5
#define FLT_LEGACY_CELL_OVER_VOLTAGE_WARNING        6
#define FLT_LEGACY_CELL_OVER_VOLTAGE_ALARM          7
#define FLT_LEGACY_CELL_UNDER_VOLTAGE_WARNING       8
#define FLT_LEGACY_CELL_UNDER_VOLTAGE_ALARM         9
#define FLT_LEGACY_CHARGE_OVER_CURRENT_WARNING      15
#define FLT_LEGACY_DISCHARGE_OVER_CURRENT_ALARM     16
#define FLT_LEGACY_FUSE_BLOWN_ALARM                 20
#define FLT_LEGACY_BATTERY_UNDER_SOC_LVL1_WARNING   21
#define FLT_LEGACY_BATTERY_UNDER_SOC_LVL2_WARNING   22
#define FLT_LEGACY_BATTERY_OVER_VOLTAGE_WARNING     23
#define FLT_LEGACY_BATTERY_UNDER_VOLTAGE_ALARM      24
#define FLT_LEGACY_ELECTRONICS_OVER_TEMP_WARNING    26
#define FLT_LEGACY_ELECTRONICS_OVER_TEMP_ALARM      27
#define FLT_LEGACY_SELF_TEST_FAILURE_ALARM          30

/* Fault related defines */
#define FAULT_EXPIRED_HALF_HR         18000
#define EEPROM_SPI_FAULT_THRESHOLD    3
#define BMSIC_SPI_FAULT_THRESHOLD     2
#define TEMPSENSE_I2C_FAULT_THRESHOLD 3
#define LEDCNTL_I2C_FAULT_THRESHOLD   3

/**
 * @brief vFault_Handler
 *
 * Run through all the fault detection routines and update the BMS Data.
 *
 * @param pstMyBmsData[in] Structure containing most recent BMS data.
 * @return None.
 */
void vFault_Handler(stBmsData_t* pstMyBmsData);

#endif /* FAULTS_H_ */
/*------------------------------- end of file ------------------------------*/
