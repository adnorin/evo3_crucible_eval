#ifndef HMI_BUTTON_READ_H_
#define HMI_BUTTON_READ_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: hmi_button_read.h                                              */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for hmi_button_read.c, which provides prototypes for   */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define BUTTON_DOWN_DEBOUNCE    2
#define BUTTON_UP_DEBOUNCE      0
#define BUTTON_POWERON_SET      0x01
#define BUTTON_POWERON_CLR      0xFE
#define BUTTON_SOC_SET          0x02
#define BUTTON_SOC_CLR          0xFD

/*********************************************************************
* PUBLIC void vReadAndDebounceButtons()
*---------------------------------------------------------------------
*  Reads and debounces buttons (HMI)
*
* Inputs:
*   stMyBMSData - Current data for BMS
* Returns:
*   uint8_t - Button status
*
**********************************************************************/

uint8_t vReadAndDebounceButtons(stBmsData_t* stMyBMSData);


#endif /* HMI_BUTTON_READ_H_ */