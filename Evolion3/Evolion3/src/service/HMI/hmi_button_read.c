/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: hmi_button_read.c                                              */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: N/A                                                        */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* GPIO pins (buttons) read and debounce.                                    */
/*                                                                           */
/*****************************************************************************/

#include <asf.h>
#include <FreeRTOS.h>
#include <queue.h>
#include <iface_rtos_start.h>
#include <conf_extint_custom.h>
#include <conf_gpio.h>
#include <self_tests.h>
#include <hmi_controller.h>
#include <sleepmgr.h>
#include <bat_cell_controller_ic.h>
#include <hmi_button_read.h>

//const uint8_t ucSOCTestArray[7] = {80, 60, 40, 20, 10, 5, 0};
//uint8_t ucSOCArrayIdx = 0;
uint8_t ucFLTIdx = 0;

/*********************************************************************
* PUBLIC void vReadAndDebounceButtons()
*---------------------------------------------------------------------
*  Reads and debounces buttons (HMI)
*
* Inputs:
*   stMyBMSData - Current data for BMS
* Returns:
*   uint8_t - Button status
*
**********************************************************************/

uint8_t vReadAndDebounceButtons(stBmsData_t* stMyBMSData)
{
    static int8_t cButtonPowerOn = 0;
    static int8_t cButtonSOC = 0;
    static uint8_t ucHMIInputs = 0;
    static bool bSOCIsDown = false;
    static bool bPWRONIsDown = false;

    /**************************************
    * POWER ON Button                     *
    ***************************************/
    /* Check to see if button is pressed */
	if(port_pin_get_input_level(HMI_POWER_ON_PIN) == false)
    {
        cButtonPowerOn++;

        /* Change status of button only if it has been held down for the down debounce period */
        if(cButtonPowerOn >= BUTTON_DOWN_DEBOUNCE)
        {
            cButtonPowerOn = BUTTON_DOWN_DEBOUNCE;
            ucHMIInputs |= BUTTON_POWERON_SET;

            /* Only execute this once every press */
            if(bPWRONIsDown == false)
            {                

                /*----------------------------------*/
                /* CALL BUTTON PWR ON DOWN FUNCTION */
                /*----------------------------------*/

                bPWRONIsDown = true;
            }

        }
    }
    else /* Button is not pressed */
    {
        cButtonPowerOn--;

        /* Change status of button only if it has been released for the up debounce period */
        if(cButtonPowerOn <= BUTTON_UP_DEBOUNCE)
        {
            cButtonPowerOn = BUTTON_UP_DEBOUNCE;
            ucHMIInputs &= BUTTON_POWERON_CLR;

            if(bPWRONIsDown == true)
            {
                /*--------------------------------*/
                /* CALL BUTTON PWR ON UP FUNCTION */
                /*--------------------------------*/

                bPWRONIsDown = false;
            }
        }
    }

    /**************************************
    * SOC Button                          *
    ***************************************/
    /* Check to see if button is pressed */
	if(port_pin_get_input_level(HMI_SOC_PIN) == false)
    {
        cButtonSOC++;

        /* Change status of button only if it has been held down for the down debounce period */
        if(cButtonSOC >= BUTTON_DOWN_DEBOUNCE)
        {
            cButtonSOC = BUTTON_DOWN_DEBOUNCE;
            ucHMIInputs |= BUTTON_SOC_SET;

            /* Only execute this once every press */
            if(bSOCIsDown == false)
            {

                /*----------------------------------*/
                /* CALL BUTTON SOC DOWN FUNCTION    */
                /*----------------------------------*/

                bSOCIsDown = true;
            }
        }
    }
    else /* Button is not pressed */
    {
        cButtonSOC--;

        /* Change status of button only if it has been released for the up debounce period */
        if(cButtonSOC <= BUTTON_UP_DEBOUNCE)
        {
            cButtonSOC = BUTTON_UP_DEBOUNCE;
            ucHMIInputs &= BUTTON_SOC_CLR;

            /* Only execute this once every release */
            if(bSOCIsDown == true)
            {
                /*----------------------------------*/
                /* CALL BUTTON SOC UP FUNCTION      */
                /*----------------------------------*/

                bSOCIsDown = false;
            }
        }
    }

    /* Save button status */
    stMyBMSData->ucButtonStatus = ucHMIInputs;

    return (ucHMIInputs);
}
/******************************** End of File ********************************/
