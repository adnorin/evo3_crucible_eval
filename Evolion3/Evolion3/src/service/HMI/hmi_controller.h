#ifndef HMI_CONTROLLER_H_
#define HMI_CONTROLLER_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: hmi_controller.h                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for bat_cell_controller_ic.c, which provides prototypes for   */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>

/* Macros and definitions */
#define SOCSOH_LED_1_MASK     0x0001
#define SOCSOH_LED_2_MASK     0x0002
#define SOCSOH_LED_3_MASK     0x0004
#define SOCSOH_LED_4_MASK     0x0008
#define SOCSOH_LED_5_MASK     0x0010
#define ALM_WARN_GRN_LED_MASK 0x0001
#define ALM_WARN_RED_LED_MASK 0x0002
#define PWR_LED_MASK          0x0001

/* Custom types */
typedef struct stLEDStatus
{
    bool bSOCLEDStatusChange;   /* SOC LEDs status change notification */
    uint8_t ucSOCLEDStatus;     /* SOC LEDs status ON/OFF 5 LEDs bitwise encoded */
    bool bFaultLEDStatusChange; /* Fault LEDs status change notification */
    uint8_t ucFaultLEDStatus;       /* Fault LEDs status ON/OFF 2 LEDs bitwise encoded */
    bool bPowerLEDStatusChange; /* Power LED status change notification */
    uint8_t ucPowerLEDStatus;       /* Power LED status ON/OFF 1 LEDs bitwise encoded */
    bool bBlinkLED;
} stLEDStatus_t;


/*********************************************************************
* PUBLIC vManageHMI()
*---------------------------------------------------------------------
* MHI management routine
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vManageHMI(void);


/*********************************************************************
* PUBLIC vSetSOCLEDs()
*---------------------------------------------------------------------
* Determines SOC LEDs needed to be lit in order to display the appropriate
* percentage range.
*
* Inputs:
*     ucSOCValue - State of charge from 0 to 100 percent
* Returns: None
*
**********************************************************************/
void vSetSOCLEDs(uint8_t ucSOCValue);


/*********************************************************************
* PUBLIC vSetFaultLEDs()
*---------------------------------------------------------------------
* Determines Fault LEDs needed to be lit in order to display the appropriate
* fault (OK=Green, Med=Orange, High=Red)
*
* Inputs:
*     ucFaultLevel - 0 = OK, 1 = Medium, 2 = High
* Returns: None
*
**********************************************************************/
void vSetFaultLEDs(uint8_t ucFaultLevel);


/*********************************************************************
* PUBLIC vSetPwrLED()
*---------------------------------------------------------------------
* Sets Power LED On or Off.
*
* Inputs:
*     bPwrLedOnOff - true = ON, false = OFF
* Returns: None
*
**********************************************************************/
void vSetPwrLED(uint8_t bPwrLedOnOff);


/*********************************************************************
* PUBLIC vSetLEDsONOFF()
*---------------------------------------------------------------------
* Sets LEDs On or Off individually based on a 8 bit array
*
* Inputs:
*     uchLedFlags - 8 bit array
* Returns: None
*
**********************************************************************/
void vSetLEDsONOFF(uint8_t uchLedFlags);

#endif /* HMI_CONTROLLER_H_ */