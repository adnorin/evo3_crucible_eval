/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: hmi_controller.c                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* This file implements functions to turn ON or OFF the LEDs on the HMI      */
/* panel of the Evolion 3 battery.                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <hmi_controller.h>
#include <led_driver_ic.h>
#include <conf_gpio.h>
#include <iface_rtos_start.h>

/* Private variables */
uint8_t ucSOCLEDValue = 0;
uint8_t ucFLTLEDValue = 0;
bool    bPWRLEDValue = false;


/*********************************************************************
* PUBLIC vManageHMI()
*---------------------------------------------------------------------
* MHI management routine
*
* Inputs:  None     
* Returns: None
*
**********************************************************************/
void vManageHMI(void)
{       
    BaseType_t sStatus;
    static stLEDStatus_t ssHmiStates;
    static stLEDStatus_t ssLEDStatus;
    static uint16_t ushLEDWord = 0; 
    static uint16_t ushPrevLEDWord = 0x55;  
    static uint8_t ucBlinkCnt = 0;
    static QueueHandle_t spLocalQueue_HMI_Outputs_Hdl1;
    bool   bQueueEmpty;
    /* Retrieve data from queue */          
    
    bQueueEmpty = false;
    spLocalQueue_HMI_Outputs_Hdl1 = sGetQueueHandle(HMI_OUTPUT_QUEUE);
    
    while(bQueueEmpty == false)
    {   
        sStatus = xQueueReceive(spLocalQueue_HMI_Outputs_Hdl1 , &ssLEDStatus, NOWAIT);
        if( sStatus == pdPASS )
        {   
            if(ssLEDStatus.bSOCLEDStatusChange == true)
            {
                /* If data is retrieved, first check for blinking LED */
                if (ssLEDStatus.bBlinkLED == false)
                {           
                    /* Copy new SOC states only if Blink is False */
                    ssHmiStates.bSOCLEDStatusChange   = ssLEDStatus.bSOCLEDStatusChange;
                    ssHmiStates.ucSOCLEDStatus        = ssLEDStatus.ucSOCLEDStatus;                 
                }
        
                /* Copy new States into local states */
                ssHmiStates.bBlinkLED             = ssLEDStatus.bBlinkLED;  
            }
        
            if(ssLEDStatus.bFaultLEDStatusChange == true)
            {   
                ssHmiStates.bFaultLEDStatusChange = ssLEDStatus.bFaultLEDStatusChange;
                ssHmiStates.ucFaultLEDStatus      = ssLEDStatus.ucFaultLEDStatus;
            }
        
            if(ssLEDStatus.bPowerLEDStatusChange == true)
            {
                ssHmiStates.bPowerLEDStatusChange = ssLEDStatus.bPowerLEDStatusChange;
                ssHmiStates.ucPowerLEDStatus      = ssLEDStatus.ucPowerLEDStatus;       
            }
        }
        else
        {
            bQueueEmpty = true;
        }
    }
    
    /* Process the blinking LED */      
    if(ssHmiStates.bBlinkLED == true)       
    {       
        /* This counter will dictate the blinking rate in multiple of 100ms */
        if(ucBlinkCnt >= 2)
        {                           
            /* Mask LED status to only use LED1 */
            ssHmiStates.ucSOCLEDStatus &= SOCSOH_LED_1_MASK;
            
            if(ssHmiStates.ucSOCLEDStatus != 0)
            {
                /* Clear Bit */
                ssHmiStates.ucSOCLEDStatus = 0;
            }
            else
            {
                /* Set Bit */
                ssHmiStates.ucSOCLEDStatus = SOCSOH_LED_1_MASK;
            }           
            
            /* Force status change */
            ssHmiStates.bSOCLEDStatusChange = true;
            
            /* Reset blink rate counter */
            ucBlinkCnt = 0;
        }
        else
        {
            /* Prevent SOC status change */
            ssHmiStates.bSOCLEDStatusChange = false;
            
            /* Increment blink rate counter */
            ucBlinkCnt++;
        }       
    }
            
    /* Only update LED states if the change flag is set */
    if(ssHmiStates.bSOCLEDStatusChange == true)
    {
        /* Turn HMI GPIOs ON/OFF based on input */
        /* LED #1 */
        if((SOCSOH_LED_1_MASK & ssHmiStates.ucSOCLEDStatus)==0)
        {
            ushLEDWord &= ~0x0040;
        }
        else
        {
            ushLEDWord |= 0x0040;
        }
        
        /* LED #2 */
        if((SOCSOH_LED_2_MASK & ssHmiStates.ucSOCLEDStatus)==0)
        {
            ushLEDWord &= ~0x0100;
        }
        else
        {
            ushLEDWord |= 0x0100;
        }
        
        /* LED #3 */
        if((SOCSOH_LED_3_MASK & ssHmiStates.ucSOCLEDStatus)==0)
        {
            ushLEDWord &= ~0x0400;
        }
        else
        {
            ushLEDWord |= 0x0400;
        }

        /* LED #4 */
        if((SOCSOH_LED_4_MASK & ssHmiStates.ucSOCLEDStatus)==0)
        {
            ushLEDWord &= ~0x1000;
        }
        else
        {
            ushLEDWord |= 0x1000;
        }

        /* LED #5 */
        if((SOCSOH_LED_5_MASK & ssHmiStates.ucSOCLEDStatus)==0)
        {
            ushLEDWord &= ~0x4000;
        }
        else
        {
            ushLEDWord |= 0x4000;
        }
        
        ssHmiStates.bSOCLEDStatusChange = false;
    }
    
    /* Only update LED states if the change flag is set */
    if(ssHmiStates.bFaultLEDStatusChange == true)
    {
        /* Alarm/Warning Green LED */
        if((ALM_WARN_GRN_LED_MASK & ssHmiStates.ucFaultLEDStatus)==0)
        {
            ushLEDWord &= ~0x0004;
        }
        else
        {
            ushLEDWord |= 0x0004;
        }
        
        /* Alarm/Warning Red LED */
        if((ALM_WARN_RED_LED_MASK & ssHmiStates.ucFaultLEDStatus)==0)
        {
            ushLEDWord &= ~0x0010;
        }
        else
        {
            ushLEDWord |= 0x0010;
        } 
        
        ssHmiStates.bFaultLEDStatusChange = false;
    }
    
    /* Only update LED states if the change flag is set */
    if(ssHmiStates.bPowerLEDStatusChange == true)
    {
        /* PWR LED */
        if((PWR_LED_MASK & ssHmiStates.ucPowerLEDStatus)==0)
        {
            ushLEDWord &= ~0x0001;
        }
        else
        {
            ushLEDWord |= 0x0001;
        }
        
        ssHmiStates.bPowerLEDStatusChange = false;
    }       
    
    /* Only transmit data if it has changed */
    if(ushLEDWord != ushPrevLEDWord)
    {           
        /* Send data to LED driver */
        vTurnLEDsOnOff(ushLEDWord);
        
        /* Save previous LED word */
        ushPrevLEDWord = ushLEDWord;
    }
    
}
/******************************** End of File ********************************/


/*********************************************************************
* PUBLIC vSetSOCLEDs()
*---------------------------------------------------------------------
* Determines SOC LEDs needed to be lit in order to display the appropriate
* percentage range.
*
* Inputs:
*     ucSOCValue - State of charge from 0 to 100 percent
* Returns: None
*
**********************************************************************/
void vSetSOCLEDs(uint8_t ucSOCValue)
{       
        static uint8_t stMySOCLEDs;
        static stLEDStatus_t myLEDStatusSOC;    
        static QueueHandle_t spLocalQueue_HMI_Outputs_Hdl2;
        
        /* Set the Status Change Flags */
        myLEDStatusSOC.bFaultLEDStatusChange = false;
        myLEDStatusSOC.bPowerLEDStatusChange = false;
        myLEDStatusSOC.bSOCLEDStatusChange = true;  
        myLEDStatusSOC.bBlinkLED = false;   
        
        if(ucSOCValue == 0)
        {
            stMySOCLEDs = 0;
        }
        else if(ucSOCValue < 10)
        {
            stMySOCLEDs = SOCSOH_LED_1_MASK;
            myLEDStatusSOC.bBlinkLED = true;
        } 
        else if (ucSOCValue < 20)
        {
            stMySOCLEDs = SOCSOH_LED_1_MASK;
    
        }
        else if (ucSOCValue < 40)
        {
            stMySOCLEDs = SOCSOH_LED_1_MASK | SOCSOH_LED_2_MASK;
        }
        else if (ucSOCValue < 60)
        {
            stMySOCLEDs = SOCSOH_LED_1_MASK | SOCSOH_LED_2_MASK | SOCSOH_LED_3_MASK;
            
        }
        else if (ucSOCValue < 80)
        {
            stMySOCLEDs = SOCSOH_LED_1_MASK | SOCSOH_LED_2_MASK | SOCSOH_LED_3_MASK | SOCSOH_LED_4_MASK;
        }
        else /* > 80 */
        {
            stMySOCLEDs = SOCSOH_LED_1_MASK | SOCSOH_LED_2_MASK | SOCSOH_LED_3_MASK | SOCSOH_LED_4_MASK | SOCSOH_LED_5_MASK;
        }               
                
        myLEDStatusSOC.ucSOCLEDStatus = stMySOCLEDs;
            
        /* QUEUE Update to LEDs */      
        spLocalQueue_HMI_Outputs_Hdl2 = sGetQueueHandle(HMI_OUTPUT_QUEUE);
        xQueueSendToBack( spLocalQueue_HMI_Outputs_Hdl2, &myLEDStatusSOC, TIME_10MS);                           
}


/*********************************************************************
* PUBLIC vSetFaultLEDs()
*---------------------------------------------------------------------
* Determines Fault LEDs needed to be lit in order to display the appropriate
* fault (OK=Green, Med=Orange, High=Red)
*
* Inputs:
*     ucFaultLevel - 0 = OK, 1 = Medium, 2 = High
* Returns: None
*
**********************************************************************/
void vSetFaultLEDs(uint8_t ucFaultLevel)
{
        static uint8_t stMyFLTLEDs;
        static stLEDStatus_t myLEDStatusFLT;    
        static QueueHandle_t spLocalQueue_HMI_Outputs_Hdl3;
        
        /* Set the Status Change Flags */
        myLEDStatusFLT.bFaultLEDStatusChange = true;
        myLEDStatusFLT.bPowerLEDStatusChange = false;
        myLEDStatusFLT.bSOCLEDStatusChange = false; 
        myLEDStatusFLT.bBlinkLED = false;   
        
        if(ucFaultLevel == 0)
        {
            stMyFLTLEDs = 0 ;
        }
        else if (ucFaultLevel == 1)
        {
            stMyFLTLEDs = ALM_WARN_GRN_LED_MASK;            
        } 
        else if (ucFaultLevel == 2)
        {
            stMyFLTLEDs = ALM_WARN_GRN_LED_MASK | ALM_WARN_RED_LED_MASK;    
        }
        else /* ucFaultLevel == 3 */
        {
            stMyFLTLEDs = ALM_WARN_RED_LED_MASK;
        }   
        
        myLEDStatusFLT.ucFaultLEDStatus = stMyFLTLEDs;
        
        /* QUEUE Update to LEDs */      
        spLocalQueue_HMI_Outputs_Hdl3 = sGetQueueHandle(HMI_OUTPUT_QUEUE);
        xQueueSendToBack( spLocalQueue_HMI_Outputs_Hdl3, &myLEDStatusFLT, TIME_10MS);                                   
}


/*********************************************************************
* PUBLIC vSetPwrLED()
*---------------------------------------------------------------------
* Sets Power LED On or Off.
*
* Inputs:
*     bPwrLedOnOff - true = ON, false = OFF
* Returns: None
*
**********************************************************************/
void vSetPwrLED(uint8_t bPwrLedOnOff)
{
    static uint8_t stMyPWRLED;
    static stLEDStatus_t myLEDStatusPWR;
    static QueueHandle_t spLocalQueue_HMI_Outputs_Hdl4;
    
    /* Set the Status Change Flags */
    myLEDStatusPWR.bFaultLEDStatusChange = false;
    myLEDStatusPWR.bPowerLEDStatusChange = true;
    myLEDStatusPWR.bSOCLEDStatusChange = false;
    myLEDStatusPWR.bBlinkLED = false;
    
    if(bPwrLedOnOff == 0)
    {
        stMyPWRLED  = 0 ;
    }
    else 
    {
        stMyPWRLED = PWR_LED_MASK;
    }   
    
    myLEDStatusPWR.ucPowerLEDStatus= stMyPWRLED;
    
    /* QUEUE Update to LEDs */
    spLocalQueue_HMI_Outputs_Hdl4 = sGetQueueHandle(HMI_OUTPUT_QUEUE);
    xQueueSendToBack( spLocalQueue_HMI_Outputs_Hdl4, &myLEDStatusPWR, TIME_10MS);
}


/*********************************************************************
* PUBLIC vSetLEDsONOFF()
*---------------------------------------------------------------------
* Sets LEDs On or Off individually based on a 8 bit array
*
* Inputs:
*     uchLedFlags - 8 bit array
* Returns: None
*
**********************************************************************/
void vSetLEDsONOFF(uint8_t uchLedFlags)
{   
    static stLEDStatus_t myLEDStatusFlags;
    static QueueHandle_t spLocalQueue_HMI_Outputs_Hdl5;
    
    /* Set the Status Change Flags */
    myLEDStatusFlags.bFaultLEDStatusChange = true;
    myLEDStatusFlags.bPowerLEDStatusChange = true;
    myLEDStatusFlags.bSOCLEDStatusChange = true;
    myLEDStatusFlags.bBlinkLED = false;
    
    myLEDStatusFlags.ucPowerLEDStatus = uchLedFlags;
    myLEDStatusFlags.ucFaultLEDStatus = uchLedFlags>>1;
    myLEDStatusFlags.ucSOCLEDStatus = uchLedFlags>>3;
    
    /* QUEUE Update to LEDs */
    spLocalQueue_HMI_Outputs_Hdl5 = sGetQueueHandle(HMI_OUTPUT_QUEUE);
    xQueueSendToBack( spLocalQueue_HMI_Outputs_Hdl5, &myLEDStatusFlags, TIME_10MS);
}


