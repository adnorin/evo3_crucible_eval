/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: self_tests.c                                                   */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/

#include <asf.h>
#include <main.h>
#include <bat_cell_controller_ic.h>
#include <iface_spi_dma.h>
#include <port.h>
#include <conf_gpio.h>
#include <self_tests.h>
#include <temp_sensor_ic.h>
#include <conf_extint_custom.h>
#include <conf_battery_settings.h>
#include <arm_math.h>

/********************************
* Variables
*********************************/
sSelfTestResults_t sSelfTestResults;


/* Sensor I2C addresses */
/* NOTE: During debugging ONLY using eval board use sensor 0 as 1st sensor, otherwise replace with sensor 1 */

#ifndef I2C_TEST_ADR_SETUP
const uint8_t uchTempSensorAddressTest[NUM_TEMP_SENSORS] = {CELL_TEMP_SENSOR_0_ADDR,
    CELL_TEMP_SENSOR_1_ADDR,
    CELL_TEMP_SENSOR_2_ADDR,
    CELL_TEMP_SENSOR_3_ADDR,
    ELEC_TEMP_SENSOR_0_ADDR,
    ELEC_TEMP_SENSOR_1_ADDR,
    ELEC_TEMP_SENSOR_2_ADDR,
ELEC_TEMP_SENSOR_3_ADDR};
#else
#if (I2C_TEST_ADR_SETUP == 1)
const uint8_t uchTempSensorAddressTest[NUM_TEMP_SENSORS] = {ELEC_TEMP_SENSOR_0_ADDR,
    ELEC_TEMP_SENSOR_1_ADDR,
    ELEC_TEMP_SENSOR_2_ADDR,
    ELEC_TEMP_SENSOR_3_ADDR,
    ELEC_TEMP_SENSOR_0_ADDR,
    ELEC_TEMP_SENSOR_1_ADDR,
    ELEC_TEMP_SENSOR_2_ADDR,
ELEC_TEMP_SENSOR_3_ADDR};
#else
#if (I2C_TEST_ADR_SETUP == 2)
const uint8_t uchTempSensorAddressTest[NUM_TEMP_SENSORS] = {CELL_B_SAMPLE_TEMP_SENSOR_0_ADDR,
    CELL_B_SAMPLE_TEMP_SENSOR_1_ADDR,
    CELL_B_SAMPLE_TEMP_SENSOR_2_ADDR,
    CELL_B_SAMPLE_TEMP_SENSOR_3_ADDR,
    CELL_B_SAMPLE_TEMP_SENSOR_4_ADDR,
    CELL_B_SAMPLE_TEMP_SENSOR_5_ADDR,
    ELEC_B_SAMPLE_TEMP_SENSOR_0_ADDR,
    ELEC_B_SAMPLE_TEMP_SENSOR_1_ADDR,
    ELEC_B_SAMPLE_TEMP_SENSOR_2_ADDR,
ELEC_B_SAMPLE_TEMP_SENSOR_3_ADDR};
#else
const uint8_t uchTempSensorAddressTest[NUM_TEMP_SENSORS] = {CELL_TEMP_SENSOR_2_ADDR,
    CELL_TEMP_SENSOR_3_ADDR,
    CELL_TEMP_SENSOR_2_ADDR,
    CELL_TEMP_SENSOR_3_ADDR,
    CELL_TEMP_SENSOR_2_ADDR,
    CELL_TEMP_SENSOR_3_ADDR,
    CELL_TEMP_SENSOR_2_ADDR,
CELL_TEMP_SENSOR_3_ADDR};
#endif
#endif
#endif


/********************************
* Private Functions
*********************************/

/*********************************************************************
* PRIVATE sbAdc1ABFuncTest()
*---------------------------------------------------------------------
* Execute self test ADC1a and ADC1b functional verification
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbAdc1ABFuncTest(void)
{
    uint16_t ushFault2Reg;
    bool bfaultPresent;

    /* Initiate a conversion */
    vStartConversion();

    /* Read fault register */
    ushFault2Reg = ushReadRegisterData(BC_REG_FAULT2_STATUS, READ_REGISTER_DATA);

    /* Check if any of the ADC1a or ADC1b fault bits are present */
    sSelfTestResults.sADC1abFuncTestResults.bAdc1aFaultBit = (ushFault2Reg & 0x0400) == 0x0400;
    sSelfTestResults.sADC1abFuncTestResults.bAdc1bFaultBit = (ushFault2Reg & 0x0800) == 0x0800;
    bfaultPresent = (sSelfTestResults.sADC1abFuncTestResults.bAdc1aFaultBit || sSelfTestResults.sADC1abFuncTestResults.bAdc1bFaultBit);

    /* Read the ADC1a and ADC1b Vbg Diagnostic values */
    sSelfTestResults.sADC1abFuncTestResults.Adc1aVbgValue = ushReadRegisterData(BC_REG_MEAS_VBG_DIAG_ADC1A, READ_VOLTAGE_VALUE);
    sSelfTestResults.sADC1abFuncTestResults.Adc1bVbgValue = ushReadRegisterData(BC_REG_MEAS_VBG_DIAG_ADC1B, READ_VOLTAGE_VALUE);

    /* If fault is present return false meaning test fails otherwise return true */
    sSelfTestResults.sADC1abFuncTestResults.bAdc1abTestPass = !bfaultPresent;

    /* Clear faults if present after test */
    if(bfaultPresent == true)
    {
        ushWriteRegisterData(BC_REG_FAULT2_STATUS,WRITE_CLEAR_REGISTER_DATA);
    }

    return (!bfaultPresent);
}


/*********************************************************************
* PRIVATE bCellVoltageChannelFunctionalTest()
*---------------------------------------------------------------------
* Execute self test Cell voltage channel
* functional verification
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbCellVoltageChannelFunctionalTest(void)
{
    uint8_t ucIdx;
    static float32_t sfMeasCell[14];
    static float32_t sfMeasCell2;
    static float32_t sfVErr;

    /* Initialize result variables */
    sSelfTestResults.sCellVoltFuncVerTestResults.ushCVFVFailedCells = 0;
    sSelfTestResults.sCellVoltFuncVerTestResults.bCVFVTestPass = true;

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE | GO2DIAG_MODE);

    /* SYS_DIAG Reg set DA_DIAG to 1 all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_DA_DIAG_REGISTER_DATA);

    /* Start a conversion */
    vStartConversion();

    /* Read measured voltage from Cell2 register */
    sfMeasCell2 = (float32_t)(ushReadRegisterData(BC_REG_MEAS_CELL2, READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT;

    /* Perform test for Cell3-Cell14 per specs */
    for(ucIdx=2;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        /* Read measured voltage from Cell2 register */
        sfMeasCell[ucIdx] = (float32_t)(ushReadRegisterData((BC_REG_MEAS_CELL1-ucIdx), READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT;

        /* Calculate V error */
        sfVErr = (sfMeasCell[ucIdx] - sfMeasCell2)/1000.0;

        /* If error is outside the acceptable range set the fault bit on the array bit */
        if( sfVErr < const_fCfgCellVoltageChannelErrorMin || const_fCfgCellVoltageChannelErrorMax < sfVErr)
        {
            sSelfTestResults.sCellVoltFuncVerTestResults.ushCVFVFailedCells |= (uint16_t)(0x0001 << ucIdx);
        }
    }

    /* Set the overall Pass/Fail flag */
    if(sSelfTestResults.sCellVoltFuncVerTestResults.ushCVFVFailedCells != 0)
    {
        sSelfTestResults.sCellVoltFuncVerTestResults.bCVFVTestPass = false;
    }

    /* Reset registers, cleanup and exit diagnostics mode */

    /* Clear SYS_DIAG Reg */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG1 Reg clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    return (sSelfTestResults.sCellVoltFuncVerTestResults.bCVFVTestPass);

}


/*********************************************************************
* PRIVATE sbCellTerminalOpenDetectTest()
*---------------------------------------------------------------------
* Execute self test Cell Terminal open detect
* functional verification
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbCellTerminalOpenDetectTest(void)
{
    bool bTestPass;
    bool bSWClosed;
    uint8_t ucIdx;
    uint16_t ushVoltage;
    uint16_t ushCompVolt;
    static uint16_t ushCellVoltage[14];

    /* Initialize result variables */
    sSelfTestResults.sCTOpenDetTestResults.bCTOpenDetTestPass = true;
    sSelfTestResults.sCTOpenDetTestResults.bCTShortDetTestPass = true;
    sSelfTestResults.sCTOpenDetTestResults.bCTOpenOutRangeTestPass = true;
    sSelfTestResults.sCTOpenDetTestResults.bCTCloseOutRangeTestPass = true;
    sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenFailedCells = 0;
    sSelfTestResults.sCTOpenDetTestResults.ushCTOdShortFailedCells = 0;
    sSelfTestResults.sCTOpenDetTestResults.ushCTOdClosedSWOutOfRangeCells = 0;
    sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenSWOutOfRangeCells = 0;
    bTestPass = true;

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE | GO2DIAG_MODE);

    /********** Baseline ************/

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg set CT_OL_ODD, CT_OL_EVEN to 00, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* Delay > 5 x tdiag is about 3.25 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_5MS));

    /* Start a conversion */
    vStartConversion();

    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        ushCellVoltage[ucIdx] = ushReadRegisterData((BC_REG_MEAS_CELL1-ucIdx), READ_VOLTAGE_VALUE);
    }

    /********** Part 1 ************/

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg set CT_OL_ODD, CT_OL_EVEN to 10, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CT_ODD_EVEN_REGISTER_DATA_10);

    /* Delay >5 x tdiag is about 3.25 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_5MS));

    /* Start a conversion */
    vStartConversion();

    /* Read cell terminal voltages and verify against test criteria */
    bSWClosed = true; /* Starting with SW1 closed */
    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        /* Read cell voltage from register */
        ushVoltage = ushReadRegisterData((BC_REG_MEAS_CELL1-ucIdx), READ_VOLTAGE_VALUE);

        /* Switch commanded ON (Closed) */
        if (bSWClosed == true)
        {
            /* PASS: Cell voltage in normal range for this condition (within +- 150mV) */
            ushCompVolt = (ushCellVoltage[ucIdx]*867)/10000;
            if( ((ushCompVolt - const_ushCfgCellTerminalOpenDetectionErrorMax) < ushVoltage) && (ushVoltage < (ushCompVolt + const_ushCfgCellTerminalOpenDetectionErrorMax)) )
            {
                bTestPass = bTestPass && true;
            }

            /* FAIL Short: Cell voltage comparable to baseline (within +- 150mV) */
            else if(((ushCellVoltage[ucIdx] - const_ushCfgCellTerminalOpenDetectionErrorMax) < ushVoltage) && (ushVoltage < (ushCellVoltage[ucIdx] + const_ushCfgCellTerminalOpenDetectionErrorMax)))
            {
                /* Set the corresponding bits in the fail bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdShortFailedCells |= (0x0001 << ucIdx);
                bTestPass = bTestPass && false;
            }

            /* FAIL Open: Cell voltage much higher than baseline (greater than baseline by at least 500 mV */
            else if (ushCellVoltage[ucIdx] + const_ushCfgCellTerminalOpenDetectionBaselineDiscrepancyMax < ushVoltage )
            {
                /* Set the corresponding bits in the fail bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenFailedCells |= (0x0001 << ucIdx);
                bTestPass = bTestPass && false;
            }

            /* Cell voltage outside of test range */
            else
            {
                /* Set the corresponding bits in the bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdClosedSWOutOfRangeCells|= (0x0001 << ucIdx);
                bTestPass = bTestPass && false;
            }
            bSWClosed = false;
        }
        /* Switch commanded OFF (Open) */
        else
        {
            /* PASS: Cell voltage in normal range for this condition (within 150mV) */
            ushCompVolt = CELL_TERMINAL_OPEN_DETECTION_RAIL_VOLTAGE; /* Rail voltage, will not get any higher reading than this */
            if((ushCompVolt - const_ushCfgCellTerminalOpenDetectionErrorMax) < ushVoltage)
            {
                bTestPass = bTestPass && true;
            }

            /* FAIL Open: Cell voltage comparable to baseline (within +- 150mV) */
            else if(((ushCellVoltage[ucIdx] - const_ushCfgCellTerminalOpenDetectionErrorMax) < ushVoltage) && (ushVoltage < (ushCellVoltage[ucIdx] + const_ushCfgCellTerminalOpenDetectionErrorMax)))
            {
                /* Set the corresponding bits in the fail bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenFailedCells |= (0x0001 << ucIdx);
                bTestPass = bTestPass && false;
            }

            /* FAIL Short: Cell voltage much lower than baseline (lower than baseline by at least 500 mV */
            else if (ushVoltage < ushCellVoltage[ucIdx] - const_ushCfgCellTerminalOpenDetectionBaselineDiscrepancyMax)
            {
                /* Set the corresponding bits in the fail bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdShortFailedCells |= (0x0001 << ucIdx);
                bTestPass = bTestPass && false;
            }

            /* Cell voltage outside of test range */
            else
            {
                /* Set the corresponding bits in the bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenSWOutOfRangeCells|= (0x0001 << ucIdx);
                bTestPass = bTestPass && false;
            }
            bSWClosed = true;
        }
    }

    /********** Part 2 ************/

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg set CT_OL_ODD, CT_OL_EVEN to 01, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CT_ODD_EVEN_REGISTER_DATA_01);

    /* Delay >5 x tdiag is about 3.25 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_5MS));

    /* Start a conversion */
    vStartConversion();

    /* Read cell terminal voltages and verify against test criteria */
    bSWClosed = false; /* Starting with SW1 open */
    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        /* Read cell voltage from register */
        ushVoltage = ushReadRegisterData((BC_REG_MEAS_CELL1-ucIdx), READ_VOLTAGE_VALUE);

        /* Switch commanded ON (Closed) */
        if (bSWClosed == true)
        {
            /* PASS: Cell voltage in normal range for this condition (within +- 150mV) */
            ushCompVolt = (ushCellVoltage[ucIdx]*867)/10000;
            if( ((ushCompVolt - const_ushCfgCellTerminalOpenDetectionErrorMax) < ushVoltage) && (ushVoltage < (ushCompVolt + const_ushCfgCellTerminalOpenDetectionErrorMax)) )
            {
                bTestPass = bTestPass && true;
            }

            /* FAIL Short: Cell voltage comparable to baseline (within +- 150mV) */
            else if(((ushCellVoltage[ucIdx] - const_ushCfgCellTerminalOpenDetectionErrorMax) < ushVoltage) && (ushVoltage < (ushCellVoltage[ucIdx] + const_ushCfgCellTerminalOpenDetectionErrorMax)))
            {
                /* Set the corresponding bits in the fail bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdShortFailedCells |= (uint16_t)(0x0001 << ucIdx);
                bTestPass = false;
            }

            /* FAIL Open: Cell voltage much higher than baseline (greater than baseline by at least 500 mV */
            else if (ushCellVoltage[ucIdx] + const_ushCfgCellTerminalOpenDetectionBaselineDiscrepancyMax < ushVoltage )
            {
                /* Set the corresponding bits in the fail bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenFailedCells |= (uint16_t)(0x0001 << ucIdx);
                bTestPass = false;
            }

            /* Cell voltage outside of test range */
            else
            {
                /* Set the corresponding bits in the bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdClosedSWOutOfRangeCells|= (uint16_t)(0x0001 << ucIdx);
                bTestPass = false;
            }
            bSWClosed = false;
        }
        /* Switch commanded OFF (Open) */
        else
        {
            /* PASS: Cell voltage in normal range for this condition (within 150mV) */
            ushCompVolt = CELL_TERMINAL_OPEN_DETECTION_RAIL_VOLTAGE; /* Rail voltage, will not get any higher reading than this */
            if((ushCompVolt - const_ushCfgCellTerminalOpenDetectionErrorMax) < ushVoltage)
            {
                bTestPass = bTestPass && true;
            }

            /* FAIL Open: Cell voltage comparable to baseline (within +- 150mV) */
            else if(((ushCellVoltage[ucIdx] - const_ushCfgCellTerminalOpenDetectionErrorMax) < ushVoltage) && (ushVoltage < (ushCellVoltage[ucIdx] + const_ushCfgCellTerminalOpenDetectionErrorMax)))
            {
                /* Set the corresponding bits in the fail bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenFailedCells |= (uint16_t)(0x0001 << ucIdx);
                bTestPass = false;
            }

            /* FAIL Short: Cell voltage much lower than baseline (lower than baseline by at least 500 mV */
            else if (ushVoltage < ushCellVoltage[ucIdx] - const_ushCfgCellTerminalOpenDetectionErrorMax)
            {
                /* Set the corresponding bits in the fail bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdShortFailedCells |= (uint16_t)(0x0001 << ucIdx);
                bTestPass = false;
            }

            /* Cell voltage outside of test range */
            else
            {
                /* Set the corresponding bits in the bit array */
                sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenSWOutOfRangeCells |= (uint16_t)(0x0001 << ucIdx);
                bTestPass = false;
            }
            bSWClosed = true;
        }
    }

    /*************** Clear registers in order to exit Diagnostic Mode ***********/

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg set CT_OL_ODD, CT_OL_EVEN to 00, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* Clear fault registers before exiting test */
    ushWriteRegisterData(BC_REG_CELL_OV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_CELL_UV_FLT, WRITE_CLEAR_REGISTER_DATA);

    /* Delay >10 x tdiag is about 6.5 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* SYS_CFG1 Reg clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    /* Save overall test results */
    if(sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenFailedCells != 0)
    {
        sSelfTestResults.sCTOpenDetTestResults.bCTOpenDetTestPass = false;;
    }

    if(sSelfTestResults.sCTOpenDetTestResults.ushCTOdShortFailedCells != 0)
    {
        sSelfTestResults.sCTOpenDetTestResults.bCTShortDetTestPass = false;
    }

    if(sSelfTestResults.sCTOpenDetTestResults.ushCTOdOpenSWOutOfRangeCells != 0)
    {
        sSelfTestResults.sCTOpenDetTestResults.bCTOpenOutRangeTestPass = false;
    }

    if(sSelfTestResults.sCTOpenDetTestResults.ushCTOdShortFailedCells != 0)
    {
        sSelfTestResults.sCTOpenDetTestResults.bCTCloseOutRangeTestPass = false;
    }

    return (bTestPass);

}


/*********************************************************************
* PRIVATE sbCellTerminalLeakageTest()
*---------------------------------------------------------------------
* Execute self test Cell terminal leakage
* test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbCellTerminalLeakageTest(void)
{
    uint8_t ucIdx;
    uint8_t ucIdx2;
    static float32_t sfMeasCell[14];
    static float32_t sfIND0[15];
    static float32_t sfIND1[15];
    static float32_t sfVLeak[15];
    static float32_t sfMeasStack;

    /* Initialize result variables */
    sSelfTestResults.sCTLeakageTestResults.bCTLeakageTestPass = true;
    sSelfTestResults.sCTLeakageTestResults.ushCTLeakageFailedCells = 0;

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE | GO2DIAG_MODE);

    /* SYS_CFG1 Reg set Cell Balancing in pause */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE | GO2DIAG_MODE | CB_MANUAL_PAUSE);

    /* Initialize variable */
    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        sfVLeak[ucIdx] = 0;
    }

    /* Calculate the leakage over 20 samples taken 25ms appart */
    for(ucIdx2=0;ucIdx2<20;ucIdx2++)
    {
        /*********** Part 1 ************/

        /* SYS_DIAG Reg set CT_LEAK_DIAG (bit 5) to 1 and POLARITY (bit 6) to 0 all other bits in default */
        ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CT_LEAK_POLARITY_REGISTER_DATA_10);

        /* Start a conversion */
        vStartConversion();

        /* Read measured voltages from Cell registers */
        sfMeasStack = (float32_t)(ushReadRegisterData(BC_REG_MEAS_STACK, READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT;

        for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
        {
            sfMeasCell[ucIdx] = (float32_t)(ushReadRegisterData((BC_REG_MEAS_CELL1-ucIdx), READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT;
        }

        /* Calculate Leakage indices */
        for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
        {
            sfIND0[ucIdx] = sfMeasCell[ucIdx+1] - sfMeasCell[ucIdx];
        }

        sfIND0[NUM_OF_CELLS] = (-1.0)*sfMeasStack - sfMeasCell[NUM_OF_CELLS-1];

        /*********** Part 2 ************/

        /* SYS_DIAG Reg set CT_LEAK_DIAG (bit 5) to 1 and POLARITY (bit 6) to 1 all other bits in default */
        ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CT_LEAK_POLARITY_REGISTER_DATA_11);

        /* Start a conversion */
        vStartConversion();

        /* Read measured voltages from Cell registers */
        sfMeasStack = (float32_t)(ushReadRegisterData(BC_REG_MEAS_STACK, READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT;

        for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
        {
            sfMeasCell[ucIdx] = (float32_t)(ushReadRegisterData((BC_REG_MEAS_CELL1-ucIdx), READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT;
        }

        /* Calculate Leakage indices */
        for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
        {
            sfIND1[ucIdx] = sfMeasCell[ucIdx] - sfMeasCell[ucIdx+1];
        }

        sfIND1[NUM_OF_CELLS] = sfMeasStack + sfMeasCell[NUM_OF_CELLS-1];

        /*********** Part 3 ************/

        /* Calculate Vleak(k) */
        for(ucIdx=0;ucIdx<=NUM_OF_CELLS;ucIdx++)
        {
            sfVLeak[ucIdx] += sfIND0[ucIdx] + sfIND1[ucIdx];
        }

        /* Wait 25ms between samples */
            vTaskDelay(pdMS_TO_TICKS(TIME_5MS + TIME_20MS));
    }

    /* Calculate VLeak Average */
    for(ucIdx=0;ucIdx<=NUM_OF_CELLS;ucIdx++)
    {
        /* Calculate average and convert to mV */
        sfVLeak[ucIdx] /= 20000.0;

        /* Fault condition check */
        if(sfVLeak[ucIdx] < (-const_shCfgCellTerminalLeakageErrorMax) || const_shCfgCellTerminalLeakageErrorMax < sfVLeak[ucIdx])
        {
            sSelfTestResults.sCTLeakageTestResults.ushCTLeakageFailedCells |= (uint16_t)(0x0001 << ucIdx);
        }
    }
    /* Overall test result (Pass or Fail) */
    if(sSelfTestResults.sCTLeakageTestResults.ushCTLeakageFailedCells != 0)
    {
        sSelfTestResults.sCTLeakageTestResults.bCTLeakageTestPass = false;
    }

    /* Reset registers, cleanup and exit diagnostics mode */

    /* Clear SYS_DIAG Reg */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG1 Reg clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    return(sSelfTestResults.sCTLeakageTestResults.bCTLeakageTestPass);
}


/*********************************************************************
* PRIVATE sbCellBalanceTest()
*---------------------------------------------------------------------
* Execute self test Cell balance test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbCellBalanceTest(void)
{
    bool bTestResult;
    uint8_t ucIdx;
    uint16_t ushCBOpenFltReg;
    uint16_t ushCBShortFltReg;

    /* Initialize result variables */
    bTestResult = true;
    sSelfTestResults.sCellBalanceTestResults.bCBOpenTestPass = true;
    sSelfTestResults.sCellBalanceTestResults.bCBShortTestPass = true;
    sSelfTestResults.sCellBalanceTestResults.ushCBOpenFailedCells = 0;
    sSelfTestResults.sCellBalanceTestResults.ushCShortFailedCells = 0;

    /********** Part 1 Short *********/

    /* SYS_CFG1 Reg, configure cyclic operation, enable global CB driver */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1201 | CB_DRVEN);

    /* Enable individual CB driver for all cells */
    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        ushWriteRegisterData(BC_REG_CB1_CFG+ucIdx, WRITE_CB_DRIVER_REGISTER_DATA);
    }

    /* Delay at least 3 cycles */
        vTaskDelay(pdMS_TO_TICKS(350));

    /* Read CB short fault flags */
    ushCBShortFltReg = ushReadRegisterData(BC_REG_CB_SHORT_FLT, READ_REGISTER_DATA);

    /* SYS_CFG1 Reg, configure cyclic operation, Disable global CB driver */
    ushWriteRegisterData(BC_REG_SYS_CFG1, WRITE_CB_DRIVER_DISABLE_CYCLIC_REGISTER_DATA);

    /* Disable individual CB driver for all cells */
    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        ushWriteRegisterData(BC_REG_CB1_CFG+ucIdx, WRITE_CLEAR_REGISTER_DATA);
    }

    /* Save fault flags into bit array */
    sSelfTestResults.sCellBalanceTestResults.ushCShortFailedCells = ushCBShortFltReg;

    /* Test Result */
    if(ushCBShortFltReg != 0)
    {
        sSelfTestResults.sCellBalanceTestResults.bCBShortTestPass = false;

        /* Clear faults */
        ushWriteRegisterData(BC_REG_CB_SHORT_FLT, WRITE_CLEAR_REGISTER_DATA);
    }

    /* Delay at least 3 cycles */
        vTaskDelay(pdMS_TO_TICKS(350));

    /********** Part 2 Open *********/

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE | GO2DIAG_MODE);

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_CFG1 Reg set Cell Balancing in pause */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE | GO2DIAG_MODE | CB_MANUAL_PAUSE);

    /* SYS_DIAG Reg set CB_OL_ODD = 1, CB_OL_EVEN = 0 all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CB_OL_ODD_EVEN_REGISTER_DATA_10);

    /* Read fault flags Odd */
    ushCBOpenFltReg = ushReadRegisterData(BC_REG_CB_OPEN_FLT, READ_REGISTER_DATA);

    /* SYS_DIAG Reg set CB_OL_ODD = 0, CB_OL_EVEN = 1 all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CB_OL_ODD_EVEN_REGISTER_DATA_01);

    /* Read fault flags Even */
    ushCBOpenFltReg |= ushReadRegisterData(BC_REG_CB_OPEN_FLT, READ_REGISTER_DATA);

    /* Save Combined fault flags into single bit array */
    sSelfTestResults.sCellBalanceTestResults.ushCBOpenFailedCells = ushCBOpenFltReg;

    /* Test Result */
    if(ushCBOpenFltReg != 0)
    {
        sSelfTestResults.sCellBalanceTestResults.bCBOpenTestPass = false;

        /* Clear faults */
        ushWriteRegisterData(BC_REG_CB_OPEN_FLT, WRITE_CLEAR_REGISTER_DATA);
    }

    /* Clear SYS_DIAG Reg */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG1 Reg clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    /* Overall test result */
    if(ushCBOpenFltReg != 0 || ushCBShortFltReg != 0)
    {
        bTestResult = false;
    }

    return(bTestResult);
}


/*********************************************************************
* PRIVATE sbCellTerminalOvUvFuncTest()
*---------------------------------------------------------------------
* Execute self test Cell Terminal Over-Voltage and Under-Voltage
* functional verification
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbCellTerminalOvUvFuncTest(void)
{
    bool bTestPass;
    uint16_t ushOVFaultRegister;
    uint16_t ushUVFaultRegister;

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE | GO2DIAG_MODE);

    /********** Part 1 ************/

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);


    /* SYS_DIAG Reg, set CT_OV_UV to 1, set CT_OL_ODD, CT_OL_EVEN to 01, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CT_OV_UV_OL_ODD_EVEN_REGISTER_DATA_101);

    /* Clear fault registers before starting the conversion */
    ushWriteRegisterData(BC_REG_CELL_OV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_CELL_UV_FLT, WRITE_CLEAR_REGISTER_DATA);

    /* Delay >5 x tdiag is about 3.25 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_5MS));

    /* Start a conversion */
    vStartConversion();

    /* Read Cell OV and UV fault registers */
    ushOVFaultRegister = ushReadRegisterData(BC_REG_CELL_OV_FLT, READ_REGISTER_DATA);
    ushUVFaultRegister = ushReadRegisterData(BC_REG_CELL_UV_FLT, READ_REGISTER_DATA);

    /********** Part 2 ************/

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg, set CT_OV_UV to 1, set CT_OL_ODD, CT_OL_EVEN to 10, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CT_OV_UV_OL_ODD_EVEN_REGISTER_DATA_110);

    /* Clear fault registers before starting the conversion */
    ushWriteRegisterData(BC_REG_CELL_OV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_CELL_UV_FLT, WRITE_CLEAR_REGISTER_DATA);

    /* Delay > 5 x tdiag is about 3.25 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_5MS));

    /* Start a conversion */
    vStartConversion();

    /* Read Cell OV and UV fault registers */
    ushOVFaultRegister |= ushReadRegisterData(BC_REG_CELL_OV_FLT, READ_REGISTER_DATA);
    ushUVFaultRegister |= ushReadRegisterData(BC_REG_CELL_UV_FLT, READ_REGISTER_DATA);

    /* Save test results - Overall test Pass/Fail */
    sSelfTestResults.sOvUvFuncTestResults.bOvFaultTestPass = (ushOVFaultRegister & 0x3FFF) == 0x3FFF;
    sSelfTestResults.sOvUvFuncTestResults.bUvFaultTestPass = (ushUVFaultRegister & 0x3FFF) == 0x3FFF;

    /* Save test results - Bit encoded cells that Pass/Fail */
    sSelfTestResults.sOvUvFuncTestResults.ushOvFailedCells = (~ushOVFaultRegister) & 0x3FFF;
    sSelfTestResults.sOvUvFuncTestResults.ushUvFailedCells = (~ushUVFaultRegister) & 0x3FFF;

    /*************** Clear registers in order to exit Diagnostic Mode ***********/

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg, set CT_OV_UV to 0, set CT_OL_ODD, CT_OL_EVEN to 00, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* Clear fault registers before starting the conversion */
    ushWriteRegisterData(BC_REG_CELL_OV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_CELL_UV_FLT, WRITE_CLEAR_REGISTER_DATA);

    /* Delay > 10x tdiag is about 6.5 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_10MS));


    /* SYS_CFG1 Reg Clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    /* Return general result */
    bTestPass = sSelfTestResults.sOvUvFuncTestResults.bOvFaultTestPass && sSelfTestResults.sOvUvFuncTestResults.bUvFaultTestPass;

    return (bTestPass);

}


/*********************************************************************
* PRIVATE sbISenseAmpInputsGroundedTest()
*---------------------------------------------------------------------
* Execute self test I Sense amplifier inputs grounded test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbISenseAmpInputsGroundedTest(void)
{
    bool bTestPass;
    uint16_t ushMeasISenseMSB;
    uint16_t ushMeasISenseLSB;
    uint32_t uiMeasISense;
    int32_t iMeasISense;

    /* Initialize result variables */
    bTestPass = true;

    /* SYS_CFG1 Reg disable current measurement */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001);

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001 | GO2DIAG_MODE);

    /* SYS_DIAG Reg, set I_MUX to 11, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_I_MUX_REGISTER_DATA_11);

    /* SYS_CFG1 Reg enable current measurement */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001 | GO2DIAG_MODE | I_MEAS_EN);

    /* Delay tazc_settle */
        vTaskDelay(pdMS_TO_TICKS(TIME_1MS));

    /* Reset coulomb counter and start a new conversion (Gain = 256) */
    ushWriteRegisterData(BC_REG_ADC_CFG, 0x0B3F);

    /* Wait for conversion */
        vTaskDelay(pdMS_TO_TICKS(TIME_1MS));

    /* Read MEAS_ISENSE2 */
    ushMeasISenseMSB = ushReadRegisterData(BC_REG_MEAS_ISENSE1, READ_REGISTER_DATA);
    ushMeasISenseLSB = ushReadRegisterData(BC_REG_MEAS_ISENSE2, READ_REGISTER_DATA);

    /* Read measured I sense registers, combine MSB with LSB, convert to signed, shift to extend sign, scale factor */
    uiMeasISense = ((uint32_t)(ushMeasISenseMSB & 0x7FFF) << 4) | (uint32_t)(ushMeasISenseLSB & 0x000F);
    iMeasISense = (int32_t)(uiMeasISense << 13);
    iMeasISense = (iMeasISense >> 13) * CURRENT_SENSE_SCALAR;

    /* Test fault condition 37.2uV */
    if(iMeasISense > const_shCfgCurrentSenseAmplifierShortedErrorMax || (-const_shCfgCurrentSenseAmplifierShortedErrorMax) > iMeasISense)
    {
        bTestPass = false;
    }

    /* Save test results */
    sSelfTestResults.sIMeashAmpInputGndTestResults.bIMeashAmpInputGndTestPass = bTestPass;
    sSelfTestResults.sIMeashAmpInputGndTestResults.iIMeasValue = iMeasISense;

    /*************** Exit Diagnostic Mode *************/

    /* Clear SYS_DIAG Reg */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG1 Reg clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    return (bTestPass);

}

/*********************************************************************
* PRIVATE bISenseVrefDiagTest()
*---------------------------------------------------------------------
* Execute self test I Sense VREF_DIAG test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbISenseVrefDiagTest(void)
{
    bool bTestPass;
    uint16_t ushMeasISenseMSB;
    uint16_t ushMeasISenseLSB;
    uint32_t uiVRefDiag;

    /* Initialize result variables */
    bTestPass = true;

    /* SYS_CFG1 Reg disable current measurement */
    ushWriteRegisterData(BC_REG_SYS_CFG1, WRITE_I_DISABLE_REGISTER_DATA);

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001 | GO2DIAG_MODE);

    /* SYS_DIAG Reg, set I_MUX to 10, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_I_MUX_REGISTER_DATA_10);

    /* SYS_CFG1 Reg enable current measurement */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001 | GO2DIAG_MODE | I_MEAS_EN);

    /* Delay tazc_settle */
        vTaskDelay(pdMS_TO_TICKS(TIME_1MS));

    /* Reset coulomb counter and start a new conversion (Gain = 4) */
    ushWriteRegisterData(BC_REG_ADC_CFG, WRITE_GAIN_RESET_COUNTER_REGISTER_DATA);

    /* Wait for conversion */
        vTaskDelay(pdMS_TO_TICKS(TIME_1MS));

    /* Read MEAS_ISENSE2 */
    ushMeasISenseMSB = ushReadRegisterData(BC_REG_MEAS_ISENSE1, READ_REGISTER_DATA);
    ushMeasISenseLSB = ushReadRegisterData(BC_REG_MEAS_ISENSE2, READ_REGISTER_DATA);

    /* Measured VRefDiag*/
    uiVRefDiag = ((uint32_t)(ushMeasISenseMSB & 0x7FFF) << 4) | (uint32_t)(ushMeasISenseLSB & 0x000F);
    uiVRefDiag = (uiVRefDiag * 6)/1000;

    /* Test fault condition*/
    if(uiVRefDiag > const_ushCfgVRefDiagMax || const_ushCfgVRefDiagMin > uiVRefDiag )
    {
        bTestPass = false;
    }

    /* Save test results */
    sSelfTestResults.sVrefDiagTestResults.bVrefDiagTestPass = bTestPass;
    sSelfTestResults.sVrefDiagTestResults.ushVrefDiagMeasValue = (uint16_t)uiVRefDiag;

    /*************** Exit Diagnostic Mode *************/

    /* Clear SYS_DIAG Reg */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG1 Reg clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    return (bTestPass);
}


/*********************************************************************
* PRIVATE sbGpioTerminalOpenTest()
*---------------------------------------------------------------------
* Execute self test GPIO Open terminal test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbGpioTerminalOpenTest(void)
{
    bool bTestPass;
    uint16_t ushFaultStatusReg2;
    uint16_t ushGpioOpenFlags;

    /* Initialize result variables */
    bTestPass = true;

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001 | GO2DIAG_MODE);

    /* Set GPIOs as analog inputs */
    ushWriteRegisterData(BC_REG_GPIO_CFG1, WRITE_GPIO_ANALOG_REGISTER_DATA);

    /* Activate weak pull-downs */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_WEAK_PULL_DOWN_REGISTER_DATA);

    /* Read the GPIO Open Status Flags */
    ushGpioOpenFlags = ushReadRegisterData(BC_REG_GPIO_SHORT_ANX_OPEN_STS, READ_REGISTER_DATA);

    /* Read the Fault 2 Status Reg */
    ushFaultStatusReg2 = ushReadRegisterData(BC_REG_FAULT2_STATUS, READ_REGISTER_DATA);

    /* Check for AN5(MCM_MON_3.3V), AN4(MCM_MON_12V), AN3(MCM_MON_5V), AN1(MCM_MON_OCD) and Fault status */
    if( ((ushGpioOpenFlags & GPIO_OPEN_ANALOG_INPUT_MASK) != 0) && (ushFaultStatusReg2 & GPIO_OPEN_FAULT2_STATUS_MASK))
    {
        bTestPass = false;
    }

    /*************** Exit Diagnostic Mode *************/

    /* Clear SYS Diag Reg */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG1 Reg clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    /* Restore GPIO operational settings */
    ushWriteRegisterData(BC_REG_GPIO_CFG1, WRITE_GPIO_OPERATE_REGISTER_DATA);

    /* Save test results */
    sSelfTestResults.sGpioOpenTestResults.bGpioOpenTestPass = bTestPass;
    sSelfTestResults.sGpioOpenTestResults.ushGpioOpenFailedPins = ushGpioOpenFlags;

    return(bTestPass);
}


/*********************************************************************
* PRIVATE sbStackVsCellSumTest()
*---------------------------------------------------------------------
* Execute self test stack voltage vs. cell voltage sum terminal test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbStackVsCellSumTest(void)
{
    uint8_t ucIdx;
    static uint32_t suiMeasStack;
    static uint32_t suiCellVoltage[14];
    static uint32_t suiCellSum;
    static int32_t siVoltDifference;

    suiCellSum = 0;
    sSelfTestResults.sStackVsVolTestResults.ushCellVoltageSum = 0;
    sSelfTestResults.sStackVsVolTestResults.ushStackVoltage = 0;
    sSelfTestResults.sStackVsVolTestResults.bStackVsSumTestPass = true;

    /* Start a conversion */
    vStartConversion();

    /* Read stack voltage */
    suiMeasStack = (((ushReadRegisterData(BC_REG_MEAS_STACK, READ_REGISTER_DATA) & 0x7fff) * ADC_RAW_VOLTAGE_SCALE) >> STACKVOLTAGE_SCALE);

    /* Read measured voltages from Cell registers */
    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        suiCellVoltage[ucIdx] = (((ushReadRegisterData((BC_REG_MEAS_CELL1-ucIdx), READ_REGISTER_DATA) & 0x7fff) * ADC_RAW_VOLTAGE_SCALE) >> CELLVOLTAGE_SCALE);
        suiCellSum += suiCellVoltage[ucIdx];
    }

    /* Calculate the difference between the Stack measurement and the cell voltage sum */
    siVoltDifference = ((int32_t)suiCellSum - (int32_t)suiMeasStack);

    /* Failure criteria is if the Stack voltage > sum of cells voltage, or stack voltage is more than 3V less than sum of cells */
    if( siVoltDifference < const_iCfgStackToSumOfCellsCongruenceVoltageMin || const_iCfgStackToSumOfCellsCongruenceVoltageMax < siVoltDifference )
    {
        sSelfTestResults.sStackVsVolTestResults.bStackVsSumTestPass = false;
    }

    sSelfTestResults.sStackVsVolTestResults.ushCellVoltageSum = (uint16_t) suiCellSum;
    sSelfTestResults.sStackVsVolTestResults.ushStackVoltage = (uint16_t) suiMeasStack;

    return(sSelfTestResults.sStackVsVolTestResults.bStackVsSumTestPass);
}


/*********************************************************************
* PRIVATE sbISenseMeasChainOpenTest()
*---------------------------------------------------------------------
* Execute self test I Sense measurement chain open test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbISenseMeasChainOpenTest(void)
{
    bool bTestPass;
    uint16_t ushFlt1Status;

    /* Initialize result variables */
    bTestPass = true;

    /* SYS_CFG1 Reg disable current measurement */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001);

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001 | GO2DIAG_MODE);

    /* SYS_DIAG Reg, set ISENSE_OL_DIAG = 1 all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_ISENSE_OL_REGISTER_DATA);

    /* Delay tdiag */
        vTaskDelay(pdMS_TO_TICKS(TIME_10MS + TIME_20MS));

    ushFlt1Status = ushReadRegisterData(BC_REG_FAULT1_STATUS, READ_REGISTER_DATA);

    if((ushFlt1Status & 0x0020) != 0)
    {
        /* Fault flag set */
        bTestPass = false;
    }

    /* SYS_DIAG Reg, set ISENSE_OL_DIAG = 0 all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* Delay 10 times ti */
        vTaskDelay(pdMS_TO_TICKS(18));

    /*************** Exit Diagnostic Mode *************/

    /* SYS_CFG1 Reg disable current measurement */
    ushWriteRegisterData(BC_REG_SYS_CFG1, 0x1001);

    sSelfTestResults.sISenseMeasChainOpenTestResults.bISenseMeasChainOpenTestPass = bTestPass;

    return (bTestPass);
}


/*********************************************************************
* PRIVATE sbFaultPinDetectTest()
*---------------------------------------------------------------------
* Execute self test fault pin detect test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbFaultPinDetectTest(void)
{
    bool bTestPass;
    bool bFaultPinSet;

    /* Initialize result variables */
    bTestPass = true;

    /* SYS_CFG1 Reg set Go to Diagnostic Mode Bit (Enter Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE | GO2DIAG_MODE);

    /* Clear fault registers before starting the conversion */
    ushWriteRegisterData(BC_REG_CELL_OV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_CELL_UV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT1_STATUS, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT2_STATUS, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT3_STATUS, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg, clear all to 0 */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* Delay > 5 x tdiag is about 3.25 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_5MS));

    /* Start a conversion */
    vStartConversion();

    /* Read fault pin state before Generating Fault */
    bFaultPinSet = port_pin_get_input_level(BMS_IC_FAULT_NOTIFICATION_PIN);

    /* Check proper pin condition */
    if(bFaultPinSet == true)
    {
        bTestPass = false;
    }

    /* Clear fault registers before starting the conversion */
    ushWriteRegisterData(BC_REG_CELL_OV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_CELL_UV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT1_STATUS, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT2_STATUS, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT3_STATUS, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg, set CT_OV_UV to 1, set CT_OL_ODD, CT_OL_EVEN to 01, all other bits in default - generate OV/UV fault */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CT_OV_UV_OL_ODD_EVEN_REGISTER_DATA_101);

    /* Delay > 5 x tdiag is about 3.25 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_5MS));

    /* Start a conversion */
    vStartConversion();

    /* Read fault pin state after Generating Fault */
    bFaultPinSet = port_pin_get_input_level(BMS_IC_FAULT_NOTIFICATION_PIN);

    /* Check proper pin condition */
    if(bFaultPinSet == false)
    {
        bTestPass = false;
    }

    /* Clear fault registers before starting the conversion */
    ushWriteRegisterData(BC_REG_CELL_OV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_CELL_UV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT1_STATUS, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT2_STATUS, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT3_STATUS, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg, clear all to 0 */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* Delay > 5 x tdiag is about 3.25 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_5MS));

    /* Start a conversion */
    vStartConversion();

    /* Read fault pin state after clearing Fault */
    bFaultPinSet = port_pin_get_input_level(BMS_IC_FAULT_NOTIFICATION_PIN);

    /* Check proper pin condition */
    if(bFaultPinSet == true)
    {
        bTestPass = false;
    }

    /*************** Clear registers in order to exit Diagnostic Mode ***********/

    /* SYS_CFG2 Reg, number of cells is even, write 0 to NUMB_ODD (bit 1), all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_CFG2, WRITE_NUMB_ODD_REGISTER_DATA);

    /* SYS_DIAG Reg, set CT_OV_UV to 0, set CT_OL_ODD, CT_OL_EVEN to 00, all other bits in default */
    ushWriteRegisterData(BC_REG_SYS_DIAG, WRITE_CLEAR_REGISTER_DATA);

    /* Delay > 10x tdiag is about 6.5 ms */
        vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
        
    /* Clear fault registers before starting the conversion */
    ushWriteRegisterData(BC_REG_CELL_OV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_CELL_UV_FLT, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT1_STATUS, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT2_STATUS, WRITE_CLEAR_REGISTER_DATA);
    ushWriteRegisterData(BC_REG_FAULT3_STATUS, WRITE_CLEAR_REGISTER_DATA);

    /* SYS_CFG1 Reg Clear Go to Diagnostic Mode Bit (Exit Diagnostic Mode) */
    ushWriteRegisterData(BC_REG_SYS_CFG1, EXIT2DIAG_MODE);

    sSelfTestResults.sFaultPinDetectTestResults.bFaultPinDetectTestPass = bTestPass;
    return (bTestPass);
}


/*********************************************************************
* PRIVATE sbSupplyVoltageCheck()
*---------------------------------------------------------------------
* Execute self test supply voltages test.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbSupplyVoltageCheck(void)
{
    bool bTestPass;
    static float32_t sf3_3VSupplyValue;
    static float32_t sf5VSupplyValue;
    static float32_t sf12VSupplyValue;

    bTestPass = true;
    sSelfTestResults.sSupplyVoltagesTestResults.bSupply_3_3VTestPass = true;
    sSelfTestResults.sSupplyVoltagesTestResults.bSupply_5VTestPass = true;
    sSelfTestResults.sSupplyVoltagesTestResults.bSupply_12VTestPass = true;

    /* Start a conversion */
    vStartConversion();

    /* Read 3.3V supply voltage */
    sf3_3VSupplyValue = ((float32_t)(ushReadRegisterData(BC_REG_MEAS_AN5, READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT) / 1000000;

    /* Verify supply voltage range */
    if( sf3_3VSupplyValue < const_fCfgSupplyVoltage3V3Min || const_fCfgSupplyVoltage3V3Max < sf3_3VSupplyValue )
    {
        sSelfTestResults.sSupplyVoltagesTestResults.bSupply_3_3VTestPass = false;
        bTestPass = false;
    }

    /* Read 5V supply voltage */
    sf5VSupplyValue = (((float32_t)(ushReadRegisterData(BC_REG_MEAS_AN3, READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT) * SUPPLY_5V_SCALAR) / 1000000;

    /* Verify supply voltage range */
    if( sf5VSupplyValue < const_fCfgSupplyVoltage5VMin || const_fCfgSupplyVoltage5VMax < sf5VSupplyValue )
    {
        sSelfTestResults.sSupplyVoltagesTestResults.bSupply_5VTestPass = false;
        bTestPass = false;
    }

    /* Read 12V supply voltage */
    sf12VSupplyValue = (((float32_t)(ushReadRegisterData(BC_REG_MEAS_AN4, READ_REGISTER_DATA) & 0x7fff) * MEAS_RESOLUTION_FLOAT) * SUPPLY_12V_SCALAR) / 1000000;

    /* Verify supply voltage range */
    if( sf12VSupplyValue < const_fCfgSupplyVoltage12VMin || const_fCfgSupplyVoltage12VMax < sf12VSupplyValue )
    {
        sSelfTestResults.sSupplyVoltagesTestResults.bSupply_12VTestPass = false;
        bTestPass = false;
    }

    return(bTestPass);
}


/*********************************************************************
* PRIVATE sbTempSensorSelfTest()
*---------------------------------------------------------------------
* Execute Temperature sensor self tests.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbTempSensorSelfTest(void)
{
    bool bTestPass;
    bool bFaultPinSet;
    uint16_t ucFailedTempSensor;
    uint8_t ucIdx;
    int16_t shRawValue;

    /* Initialize variable */
    ucFailedTempSensor = 0;
    bTestPass = true;
    sSelfTestResults.sTempSensorTestResults.bTempSensorTestPass = true;


    /* Read Over Temp pin state */
    bFaultPinSet = port_pin_get_input_level(OVER_TEMPERATURE_NOTIFICATION_PIN);

    /* Check initial pin condition */
    if(bFaultPinSet == false)
    {
        bTestPass = false;
        ucFailedTempSensor = 0xFF;
    }
    else
    {
        /* Execute sensor pin test if the initial condition is met otherwise fail all */
        for(ucIdx=0;ucIdx<NUM_TEMP_SENSORS;ucIdx++)
        {
            /* Init for every sensor */
            bTestPass = true;

            /* Read current temperature - get raw data value */
            shRawValue = (int16_t)ushReadRawTempValue(uchTempSensorAddressTest[ucIdx]);

            /* Scale Value */
            shRawValue = (shRawValue >> 7);

            /* Set OT and Thyst to a value lower than current temperature */
            vSetOverTempAndHystLimit(uchTempSensorAddressTest[ucIdx], shRawValue - 20, T_HYST_REG);
            vSetOverTempAndHystLimit(uchTempSensorAddressTest[ucIdx], shRawValue - 10, T_OS_REG);

            /* Wait for the chip to change state of pin */
                vTaskDelay(pdMS_TO_TICKS(200));

            /* Read Over Temp pin state */
            bFaultPinSet = port_pin_get_input_level(OVER_TEMPERATURE_NOTIFICATION_PIN);

            /* Check proper pin condition */
            if(bFaultPinSet == true)
            {
                bTestPass = false;
            }

            /* Set OT and Thyst to desired temperatures */
            if (ucIdx < NUM_CELL_TEMP_SENSORS)
            {
                vSetOverTempAndHystLimit(uchTempSensorAddressTest[ucIdx], const_cCfgRedundantOverTempCellHysteresis * OVER_TEMP_AND_HYSTERESIS_SCALAR, T_HYST_REG);
                vSetOverTempAndHystLimit(uchTempSensorAddressTest[ucIdx], const_cCfgRedundantOverTempCellThreshold * OVER_TEMP_AND_HYSTERESIS_SCALAR, T_OS_REG);
            }
            else
            {
                vSetOverTempAndHystLimit(uchTempSensorAddressTest[ucIdx], const_cCfgRedundantOverTempElectronicsHysteresis * OVER_TEMP_AND_HYSTERESIS_SCALAR, T_HYST_REG);
                vSetOverTempAndHystLimit(uchTempSensorAddressTest[ucIdx], const_cCfgRedundantOverTempElectronicsThreshold * OVER_TEMP_AND_HYSTERESIS_SCALAR, T_OS_REG);
            }

            /* Wait for the chip to change state of pin */
                vTaskDelay(pdMS_TO_TICKS(200));

            /* Read Over Temp pin state */
            bFaultPinSet = port_pin_get_input_level(OVER_TEMPERATURE_NOTIFICATION_PIN);

            /* Check proper pin condition */
            if(bFaultPinSet == false)
            {
                bTestPass = false;
            }

            /* If any of the tests failed log the failure */
            if(bTestPass == false)
            {
                ucFailedTempSensor |= (uint8_t)(0x0001 << ucIdx);
            }
        }
    }

    /* General test results */
    if(ucFailedTempSensor != 0)
    {
        /* Save test results */
        sSelfTestResults.sTempSensorTestResults.bTempSensorTestPass = false;
        bTestPass = false;
    }

    /* Save all failed sensors in bit array */
    sSelfTestResults.sTempSensorTestResults.uchFailedSensors = ucFailedTempSensor;

    return (bTestPass);
}

/*********************************************************************
* PRIVATE sbConfigurationAndParameterRangeSelfTest()
*---------------------------------------------------------------------
* Execute configuration and parameter range self tests.
*
* Inputs: None
* Returns: true = pass or false = fail
*
**********************************************************************/
static bool sbConfigurationAndParameterRangeSelfTest(void)
{
    bool bConfigurationTestPass = true;
    bool bParameterTestPass = true;
    uint16_t ushIdx;

    /* Check I2T values */
    for (ushIdx = 0; ushIdx < I2T_ARRAY_LENGTH; ushIdx++)
    {
        if ( ( const_uiCfgFastOverCurrentI2tValues[ushIdx] > (uint32_t)CFG_FAST_OVER_CURRENT_I2T_VALUE_MAX ) || ( const_uiCfgFastOverCurrentI2tValues[ushIdx] < (uint32_t)CFG_FAST_OVER_CURRENT_I2T_VALUE_MIN ) )
        {
            bConfigurationTestPass = false;
        }
    }

    /* Check Current Limit configuration variables */
    if ( ( const_shCfgMinTcellToCalculateCurrentLimits > (int16_t)CFG_MIN_TCELL_TO_CALCULATE_CURRENT_LIMITS_MAX ) || ( const_shCfgMinTcellToCalculateCurrentLimits < (int16_t)CFG_MIN_TCELL_TO_CALCULATE_CURRENT_LIMITS_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgMaxTcellToCalculateCurrentLimits > (int16_t)CFG_MAX_TCELL_TO_CALCULATE_CURRENT_LIMITS_MAX ) || ( const_shCfgMaxTcellToCalculateCurrentLimits < (int16_t)CFG_MAX_TCELL_TO_CALCULATE_CURRENT_LIMITS_MIN ) || ( const_shCfgMaxTcellToCalculateCurrentLimits <= const_shCfgMinTcellToCalculateCurrentLimits ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVcellToStartDecreasingIMD > (uint16_t)CFG_VCELL_TO_START_DECREASAING_IMD_MAX ) || ( const_ushCfgVcellToStartDecreasingIMD < (uint16_t)CFG_VCELL_TO_START_DECREASAING_IMD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVcellToInhibitDischarge > (uint16_t)CFG_VCELL_TO_INHIBIT_DISCHARGE_MAX ) || ( const_ushCfgVcellToInhibitDischarge < (uint16_t)CFG_VCELL_TO_INHIBIT_DISCHARGE_MIN ) || ( const_ushCfgVcellToInhibitDischarge >= const_ushCfgVcellToStartDecreasingIMD ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVcellToStartDecreasingIMR > (uint16_t)CFG_VCELL_TO_START_DECREASAING_IMR_MAX ) || ( const_ushCfgVcellToStartDecreasingIMR < (uint16_t)CFG_VCELL_TO_START_DECREASAING_IMR_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVcellToInhibitCharge > (uint16_t)CFG_VCELL_TO_INHIBIT_CHARGE_MAX ) || ( const_ushCfgVcellToInhibitCharge < (uint16_t)CFG_VCELL_TO_INHIBIT_CHARGE_MIN ) || ( const_ushCfgVcellToInhibitCharge <= const_ushCfgVcellToStartDecreasingIMR ) )
    {
        bConfigurationTestPass = false;
    }
    for (ushIdx = 0; ushIdx < TCELL_TO_CURRENT_LIMIT_ARRAY_LENGTH; ushIdx++)
    {
        if ( ( const_iCfgIMDLUT[ushIdx] > (int32_t)CFG_IMD_LUT_MAX ) || ( const_iCfgIMDLUT[ushIdx] < (int32_t)CFG_IMD_LUT_MIN ) )
        {
            bConfigurationTestPass = false;
        }
        if ( ( const_iCfgIMRLUT[ushIdx] > (int32_t)CFG_IMR_LUT_MAX ) || ( const_iCfgIMRLUT[ushIdx] < (int32_t)CFG_IMR_LUT_MIN ) )
        {
            bConfigurationTestPass = false;
        }
    }
    if ( ( const_ucCfgNbCurrentLimitSamplesToAvg > (uint8_t)CFG_NB_CURRENT_LIMIT_SAMPLES_TO_AVG_MAX ) || ( const_ucCfgNbCurrentLimitSamplesToAvg < (uint8_t)CFG_NB_CURRENT_LIMIT_SAMPLES_TO_AVG_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Current Measurement configuration variables */
    if ( ( const_ucCfgNbCurrentSamplesToAvg > (uint8_t)CFG_NB_CURRENT_SAMPLES_TO_AVG_MAX ) || ( const_ucCfgNbCurrentSamplesToAvg < (uint8_t)CFG_NB_CURRENT_SAMPLES_TO_AVG_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Temperature Measurement configuration variables */
    if ( ( const_ucCfgNbTemperatureSamplesToAvg > (uint8_t)CFG_NB_TEMPERATURE_SAMPLES_TO_AVG_MAX ) || ( const_ucCfgNbTemperatureSamplesToAvg < (uint8_t)CFG_NB_TEMPERATURE_SAMPLES_TO_AVG_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check SOC configuration variables */
    if ( ( const_ushCfgCellVOneHundredSOC > (uint16_t)CFG_CELL_V_ONE_HUNDRED_SOC_MAX ) || ( const_ushCfgCellVOneHundredSOC < (uint16_t)CFG_CELL_V_ONE_HUNDRED_SOC_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellVZeroSOC > (uint16_t)CFG_CELL_V_ZERO_SOC_MAX ) || ( const_ushCfgCellVZeroSOC < (uint16_t)CFG_CELL_V_ZERO_SOC_MIN ) || ( const_ushCfgCellVZeroSOC >= const_ushCfgCellVOneHundredSOC ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgSOCMaximum > (uint16_t)CFG_SOC_MAX_MAX ) || ( const_ushCfgSOCMaximum < (uint16_t)CFG_SOC_MAX_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgSOCMinimum > (uint16_t)CFG_SOC_MIN_MAX ) || ( const_ushCfgSOCMinimum < (uint16_t)CFG_SOC_MIN_MIN ) || ( const_ushCfgSOCMinimum >= const_ushCfgSOCMaximum ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgSOCFullyCharged > (uint16_t)CFG_SOC_FULLY_CHARGED_MAX ) || ( const_ushCfgSOCFullyCharged < (uint16_t)CFG_SOC_FULLY_CHARGED_MIN ) || ( const_ushCfgSOCFullyCharged > const_ushCfgSOCMaximum ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgSOCFullyDischarged > (uint16_t)CFG_SOC_FULLY_DISCHARGED_MAX ) || ( const_ushCfgSOCFullyDischarged < (uint16_t)CFG_SOC_FULLY_DISCHARGED_MIN ) || ( const_ushCfgSOCFullyDischarged < const_ushCfgSOCMinimum ) || ( const_ushCfgSOCFullyDischarged >= const_ushCfgSOCFullyCharged ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgSOCRateOfChangeMax > (uint16_t)CFG_SOC_RATE_OF_CHANGE_MAX_MAX ) || ( const_ushCfgSOCRateOfChangeMax < (uint16_t)CFG_SOC_RATE_OF_CHANGE_MAX_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ucCfgNbSOCSamplesToAvg > (uint8_t)CFG_NB_SOC_SAMPLES_TO_AVG_MAX ) || ( const_ucCfgNbSOCSamplesToAvg < (uint8_t)CFG_NB_SOC_SAMPLES_TO_AVG_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    for (ushIdx = 0; ushIdx < VCELL_TO_SOC_ARRAY_LENGTH; ushIdx++)
    {
        if ( ( const_ushCfgSOCLUT[ushIdx] > (uint16_t)CFG_SOC_LUT_MAX ) || ( const_ushCfgSOCLUT[ushIdx] < (uint16_t)CFG_SOC_LUT_MIN ) )
        {
            bConfigurationTestPass = false;
        }
    }

    /* Check Cell Balancing configuration variables */
    if ( ( const_ushCfgCellBalancingDuration > (uint16_t)CFG_CELL_BALANCING_DURATION_MAX ) || ( const_ushCfgCellBalancingDuration < (uint16_t)CFG_CELL_BALANCING_DURATION_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellBalancingDelta > (uint16_t)CFG_CELL_BALANCING_DELTA_MAX ) || ( const_ushCfgCellBalancingDelta < (uint16_t)CFG_CELL_BALANCING_DELTA_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellBalancingVoltageMin > (uint16_t)CFG_CELL_BALANCING_VOLTAGE_MIN_MAX ) || ( const_ushCfgCellBalancingVoltageMin < (uint16_t)CFG_CELL_BALANCING_VOLTAGE_MIN_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellBalancingHysteresis > (uint16_t)CFG_CELL_BALANCING_HYSTERESIS_MAX ) || ( const_ushCfgCellBalancingHysteresis < (uint16_t)CFG_CELL_BALANCING_HYSTERESIS_MIN ) || ( const_ushCfgCellBalancingHysteresis > const_ushCfgCellBalancingDelta ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCellBalancingCellTMax > (int16_t)CFG_CELL_BALANCING_CELL_TMAX_MAX ) || ( const_shCfgCellBalancingCellTMax < (int16_t)CFG_CELL_BALANCING_CELL_TMAX_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCellBalancingElecTMax > (int16_t)CFG_CELL_BALANCING_ELEC_TMAX_MAX ) || ( const_shCfgCellBalancingElecTMax < (int16_t)CFG_CELL_BALANCING_ELEC_TMAX_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Electronics Over-temp configuration variables */
    if ( ( const_shCfgElectronicsOverTempAlarmThreshold > (int16_t)CFG_ELECTRONICS_OVER_TEMP_ALARM_THRESHOLD_MAX ) || ( const_shCfgElectronicsOverTempAlarmThreshold < (int16_t)CFG_ELECTRONICS_OVER_TEMP_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgElectronicsOverTempWarningThreshold > (int16_t)CFG_ELECTRONICS_OVER_TEMP_WARNING_THRESHOLD_MAX ) || ( const_shCfgElectronicsOverTempWarningThreshold < (int16_t)CFG_ELECTRONICS_OVER_TEMP_WARNING_THRESHOLD_MIN ) || ( const_shCfgElectronicsOverTempWarningThreshold > const_shCfgElectronicsOverTempAlarmThreshold ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgElectronicsTempFaultHysteresis > (int16_t)CFG_ELECTRONICS_OVER_TEMP_FAULT_HYSTERESIS_MAX ) || ( const_shCfgElectronicsTempFaultHysteresis < (int16_t)CFG_ELECTRONICS_OVER_TEMP_FAULT_HYSTERESIS_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgElectronicsTempFaultDelay > (uint16_t)CFG_ELECTRONICS_TEMP_FAULT_DELAY_MAX ) || ( const_ushCfgElectronicsTempFaultDelay < (uint16_t)CFG_ELECTRONICS_TEMP_FAULT_DELAY_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check SOC Fault configuration variables */
    if ( ( const_ushCfgBatteryUnderSOCAlarmThreshold > (uint16_t)CFG_BATTERY_UNDER_SOC_ALARM_THRESHOLD_MAX ) || ( const_ushCfgBatteryUnderSOCAlarmThreshold < (uint16_t)CFG_BATTERY_UNDER_SOC_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgBatteryUnderSOCWarningThreshold > (uint16_t)CFG_BATTERY_UNDER_SOC_WARNING_THRESHOLD_MAX ) || ( const_ushCfgBatteryUnderSOCWarningThreshold < (uint16_t)CFG_BATTERY_UNDER_SOC_WARNING_THRESHOLD_MIN ) || ( const_ushCfgBatteryUnderSOCWarningThreshold < const_ushCfgBatteryUnderSOCAlarmThreshold ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgBatteryOverSOCAlarmThreshold > (uint16_t)CFG_BATTERY_OVER_SOC_ALARM_THRESHOLD_MAX ) || ( const_ushCfgBatteryOverSOCAlarmThreshold < (uint16_t)CFG_BATTERY_OVER_SOC_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgBatteryOverSOCWarningThreshold > (uint16_t)CFG_BATTERY_OVER_SOC_WARNING_THRESHOLD_MAX ) || ( const_ushCfgBatteryOverSOCWarningThreshold < (uint16_t)CFG_BATTERY_OVER_SOC_WARNING_THRESHOLD_MIN ) || ( const_ushCfgBatteryOverSOCWarningThreshold > const_ushCfgBatteryOverSOCAlarmThreshold ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgBatterySOCFaultHysteresis > (uint16_t)CFG_BATTERY_SOC_FAULT_HYSTERESIS_MAX ) || ( const_ushCfgBatterySOCFaultHysteresis < (uint16_t)CFG_BATTERY_SOC_FAULT_HYSTERESIS_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgBatterySOCFaultDelay > (uint16_t)CFG_BATTERY_SOC_FAULT_DELAY_MAX ) || ( const_ushCfgBatterySOCFaultDelay < (uint16_t)CFG_BATTERY_SOC_FAULT_DELAY_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Over-Current Fault configuration variables */
    if ( ( const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage > (uint16_t)CFG_OVER_CURRENT_IN_DISCHARGE_ALARM_THRESHOLD_MAX ) || ( const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage < (uint16_t)CFG_OVER_CURRENT_IN_DISCHARGE_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgOverCurrentInDischargeWarningThresholdPercentage > (uint16_t)CFG_OVER_CURRENT_IN_DISCHARGE_WARNING_THRESHOLD_MAX ) || ( const_ushCfgOverCurrentInDischargeWarningThresholdPercentage < (uint16_t)CFG_OVER_CURRENT_IN_DISCHARGE_WARNING_THRESHOLD_MIN ) || ( const_ushCfgOverCurrentInDischargeWarningThresholdPercentage > const_ushCfgOverCurrentInDischargeAlarmThresholdPercentage ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgOverCurrentInDischargeMinimumCurrent > (int16_t)CFG_OVER_CURRENT_IN_DISCHARGE_MINIMUM_CURRENT_MAX ) || ( const_shCfgOverCurrentInDischargeMinimumCurrent < (int16_t)CFG_OVER_CURRENT_IN_DISCHARGE_MINIMUM_CURRENT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgOverCurrentInChargeAlarmThresholdPercentage > (uint16_t)CFG_OVER_CURRENT_IN_CHARGE_ALARM_THRESHOLD_MAX ) || ( const_ushCfgOverCurrentInChargeAlarmThresholdPercentage < (uint16_t)CFG_OVER_CURRENT_IN_CHARGE_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgOverCurrentInChargeWarningThresholdPercentage > (uint16_t)CFG_OVER_CURRENT_IN_CHARGE_WARNING_THRESHOLD_MAX ) || ( const_ushCfgOverCurrentInChargeWarningThresholdPercentage < (uint16_t)CFG_OVER_CURRENT_IN_CHARGE_WARNING_THRESHOLD_MIN ) || ( const_ushCfgOverCurrentInChargeWarningThresholdPercentage > const_ushCfgOverCurrentInChargeAlarmThresholdPercentage ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgOverCurrentInChargeMinimumCurrent > (int16_t)CFG_OVER_CURRENT_IN_CHARGE_MINIMUM_CURRENT_MAX ) || ( const_shCfgOverCurrentInChargeMinimumCurrent < (int16_t)CFG_OVER_CURRENT_IN_CHARGE_MINIMUM_CURRENT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgOverCurrentFaultHysteresis > (uint16_t)CFG_OVER_CURRENT_FAULT_HYSTERESIS_MAX ) || ( const_ushCfgOverCurrentFaultHysteresis < (uint16_t)CFG_OVER_CURRENT_FAULT_HYSTERESIS_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgOverCurrentFaultDelay > (uint16_t)CFG_OVER_CURRENT_FAULT_DELAY_MAX ) || ( const_ushCfgOverCurrentFaultDelay < (uint16_t)CFG_OVER_CURRENT_FAULT_DELAY_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgOverCurrentThresholdPercentageBase > (uint16_t)CFG_OVER_CURRENT_THRESHOLD_BASE_MAX ) || ( const_ushCfgOverCurrentThresholdPercentageBase < (uint16_t)CFG_OVER_CURRENT_THRESHOLD_BASE_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Cell Temperature Fault configuration variables */
    if ( ( const_shCfgCellOverTempAlarmThreshold > (int16_t)CFG_CELL_OVER_TEMP_ALARM_THRESHOLD_MAX ) || ( const_shCfgCellOverTempAlarmThreshold < (int16_t)CFG_CELL_OVER_TEMP_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCellOverTempWarningThreshold > (int16_t)CFG_CELL_OVER_TEMP_WARNING_THRESHOLD_MAX ) || ( const_shCfgCellOverTempWarningThreshold < (int16_t)CFG_CELL_OVER_TEMP_WARNING_THRESHOLD_MIN ) || ( const_shCfgCellOverTempWarningThreshold > const_shCfgCellOverTempAlarmThreshold ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCellUnderTempAlarmThreshold > (int16_t)CFG_CELL_UNDER_TEMP_ALARM_THRESHOLD_MAX ) || ( const_shCfgCellUnderTempAlarmThreshold < (int16_t)CFG_CELL_UNDER_TEMP_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCellUnderTempWarningThreshold > (int16_t)CFG_CELL_UNDER_TEMP_WARNING_THRESHOLD_MAX ) || ( const_shCfgCellUnderTempWarningThreshold < (int16_t)CFG_CELL_UNDER_TEMP_WARNING_THRESHOLD_MIN ) || ( const_shCfgCellUnderTempWarningThreshold < const_shCfgCellUnderTempAlarmThreshold ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCellTempFaultHysteresis > (int16_t)CFG_CELL_TEMP_FAULT_HYSTERESIS_MAX ) || ( const_shCfgCellTempFaultHysteresis < (int16_t)CFG_CELL_TEMP_FAULT_HYSTERESIS_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellTempFaultDelay > (uint16_t)CFG_CELL_TEMP_FAULT_DELAY_MAX ) || ( const_ushCfgCellTempFaultDelay < (uint16_t)CFG_CELL_TEMP_FAULT_DELAY_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Cell Voltage Fault configuration variables */
    if ( ( const_ushCfgCellOverVoltageAlarmThreshold > (uint16_t)CFG_CELL_OVER_VOLTAGE_ALARM_THRESHOLD_MAX ) || ( const_ushCfgCellOverVoltageAlarmThreshold < (uint16_t)CFG_CELL_OVER_VOLTAGE_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellOverVoltageWarningThreshold > (uint16_t)CFG_CELL_OVER_VOLTAGE_WARNING_THRESHOLD_MAX ) || ( const_ushCfgCellOverVoltageWarningThreshold < (uint16_t)CFG_CELL_OVER_VOLTAGE_WARNING_THRESHOLD_MIN ) || ( const_ushCfgCellOverVoltageWarningThreshold > const_ushCfgCellOverVoltageAlarmThreshold ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellUnderVoltageAlarmThreshold > (uint16_t)CFG_CELL_UNDER_VOLTAGE_ALARM_THRESHOLD_MAX ) || ( const_ushCfgCellUnderVoltageAlarmThreshold < (uint16_t)CFG_CELL_UNDER_VOLTAGE_ALARM_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellUnderVoltageWarningThreshold > (uint16_t)CFG_CELL_UNDER_VOLTAGE_WARNING_THRESHOLD_MAX ) || ( const_ushCfgCellUnderVoltageWarningThreshold < (uint16_t)CFG_CELL_UNDER_VOLTAGE_WARNING_THRESHOLD_MIN ) || ( const_ushCfgCellUnderVoltageWarningThreshold < const_ushCfgCellUnderVoltageAlarmThreshold ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellVoltageFaultHysteresis > (uint16_t)CFG_CELL_VOLTAGE_FAULT_HYSTERESIS_MAX ) || ( const_ushCfgCellVoltageFaultHysteresis < (uint16_t)CFG_CELL_VOLTAGE_FAULT_HYSTERESIS_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellVoltageFaultDelay > (uint16_t)CFG_CELL_VOLTAGE_FAULT_DELAY_MAX ) || ( const_ushCfgCellVoltageFaultDelay < (uint16_t)CFG_CELL_VOLTAGE_FAULT_DELAY_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Cell Temperature Difference Fault configuration variables */
    if ( ( const_shCfgCellTempDifferenceWarningThreshold > (int16_t)CFG_CELL_TEMP_DIFFERENCE_WARNING_THRESHOLD_MAX ) || ( const_shCfgCellTempDifferenceWarningThreshold < (int16_t)CFG_CELL_TEMP_DIFFERENCE_WARNING_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCellTempDifferenceFaultHysteresis > (int16_t)CFG_CELL_TEMP_DIFFERENCE_FAULT_HYSTERESIS_MAX ) || ( const_shCfgCellTempDifferenceFaultHysteresis < (int16_t)CFG_CELL_TEMP_DIFFERENCE_FAULT_HYSTERESIS_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellTempDifferenceFaultDelay > (uint16_t)CFG_CELL_TEMP_DIFFERENCE_FAULT_DELAY_MAX ) || ( const_ushCfgCellTempDifferenceFaultDelay < (uint16_t)CFG_CELL_TEMP_DIFFERENCE_FAULT_DELAY_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Cell Voltage Imbalance Fault configuration variables */
    if ( ( const_ushCfgCellVoltageImbalanceWarningThreshold > (uint16_t)CFG_CELL_VOLTAGE_IMBALANCE_WARNING_THRESHOLD_MAX ) || ( const_ushCfgCellVoltageImbalanceWarningThreshold < (uint16_t)CFG_CELL_VOLTAGE_IMBALANCE_WARNING_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellVoltageImbalanceFaultHysteresis > (uint16_t)CFG_CELL_VOLTAGE_IMBALANCE_FAULT_HYSTERESIS_MAX ) || ( const_ushCfgCellVoltageImbalanceFaultHysteresis < (uint16_t)CFG_CELL_VOLTAGE_IMBALANCE_FAULT_HYSTERESIS_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellVoltageImbalanceFaultDelay > (uint16_t)CFG_CELL_VOLTAGE_IMBALANCE_FAULT_DELAY_MAX ) || ( const_ushCfgCellVoltageImbalanceFaultDelay < (uint16_t)CFG_CELL_VOLTAGE_IMBALANCE_FAULT_DELAY_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Regulated Current Path Voltage and Current Limit configuration variables */
    if ( ( const_ushCfgVoltageLimit0 > (uint16_t)CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MAX ) || ( const_ushCfgVoltageLimit0 < (uint16_t)CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVoltageLimit1 > (uint16_t)CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MAX ) || ( const_ushCfgVoltageLimit1 < (uint16_t)CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MIN ) || ( const_ushCfgVoltageLimit1 > const_ushCfgVoltageLimit0 ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVoltageLimit2_3 > (uint16_t)CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MAX ) || ( const_ushCfgVoltageLimit2_3 < (uint16_t)CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MIN ) || ( const_ushCfgVoltageLimit2_3 > const_ushCfgVoltageLimit1 ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVoltageLimit4_7 > (uint16_t)CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MAX ) || ( const_ushCfgVoltageLimit4_7 < (uint16_t)CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MIN ) || ( const_ushCfgVoltageLimit4_7 > const_ushCfgVoltageLimit2_3 ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentLimit0 > (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX ) || ( const_shCfgCurrentLimit0 < (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentLimit1 > (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX ) || ( const_shCfgCurrentLimit1 < (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentLimit2 > (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX ) || ( const_shCfgCurrentLimit2 < (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentLimit3 > (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX ) || ( const_shCfgCurrentLimit3 < (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentLimit4 > (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX ) || ( const_shCfgCurrentLimit4 < (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentLimit5 > (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX ) || ( const_shCfgCurrentLimit5 < (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentLimit6 > (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX ) || ( const_shCfgCurrentLimit6 < (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentLimit7 > (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX ) || ( const_shCfgCurrentLimit7 < (int16_t)CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Redundant Over-temp configuration variables */
    if ( ( const_cCfgRedundantOverTempElectronicsThreshold > (int8_t)CFG_REDUNDANT_ELECTRONICS_OVER_TEMP_THRESHOLD_MAX ) || ( const_cCfgRedundantOverTempElectronicsThreshold < (int8_t)CFG_REDUNDANT_ELECTRONICS_OVER_TEMP_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_cCfgRedundantOverTempElectronicsHysteresis > (int8_t)CFG_REDUNDANT_ELECTRONICS_OVER_TEMP_HYSTERESIS_MAX ) || ( const_cCfgRedundantOverTempElectronicsHysteresis < (int8_t)CFG_REDUNDANT_ELECTRONICS_OVER_TEMP_HYSTERESIS_MIN ) || ( const_cCfgRedundantOverTempElectronicsHysteresis >= const_cCfgRedundantOverTempElectronicsThreshold ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_cCfgRedundantOverTempCellThreshold > (int8_t)CFG_REDUNDANT_CELL_OVER_TEMP_THRESHOLD_MAX ) || ( const_cCfgRedundantOverTempCellThreshold < (int8_t)CFG_REDUNDANT_CELL_OVER_TEMP_THRESHOLD_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_cCfgRedundantOverTempCellHysteresis > (int8_t)CFG_REDUNDANT_CELL_OVER_TEMP_HYSTERESIS_MAX ) || ( const_cCfgRedundantOverTempCellHysteresis < (int8_t)CFG_REDUNDANT_CELL_OVER_TEMP_HYSTERESIS_MIN ) || ( const_cCfgRedundantOverTempCellHysteresis >= const_cCfgRedundantOverTempCellThreshold ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check Self-test Criteria configuration variables */
    if ( ( const_fCfgCellVoltageChannelErrorMin > (float32_t)CFG_CELL_VOLTAGE_CHANNEL_ERROR_MIN_MAX ) || ( const_fCfgCellVoltageChannelErrorMin < (float32_t)CFG_CELL_VOLTAGE_CHANNEL_ERROR_MIN_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_fCfgCellVoltageChannelErrorMax > (float32_t)CFG_CELL_VOLTAGE_CHANNEL_ERROR_MAX_MAX ) || ( const_fCfgCellVoltageChannelErrorMax < (float32_t)CFG_CELL_VOLTAGE_CHANNEL_ERROR_MAX_MIN ) || ( const_fCfgCellVoltageChannelErrorMax < const_fCfgCellVoltageChannelErrorMin ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellTerminalOpenDetectionErrorMax > (uint16_t)CFG_CELL_TERMINAL_OPEN_DETECTION_ERROR_MAX_MAX ) || ( const_ushCfgCellTerminalOpenDetectionErrorMax < (uint16_t)CFG_CELL_TERMINAL_OPEN_DETECTION_ERROR_MAX_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgCellTerminalOpenDetectionBaselineDiscrepancyMax > (uint16_t)CFG_CELL_TERMINAL_OPEN_DETECTION_BASELINE_DISCREPANCY_MAX_MAX ) || ( const_ushCfgCellTerminalOpenDetectionBaselineDiscrepancyMax < (uint16_t)CFG_CELL_TERMINAL_OPEN_DETECTION_BASELINE_DISCREPANCY_MAX_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCellTerminalLeakageErrorMax > (int16_t)CFG_CELL_TERMINAL_LEAKAGE_ERROR_MAX_MAX ) || ( const_shCfgCellTerminalLeakageErrorMax < (int16_t)CFG_CELL_TERMINAL_LEAKAGE_ERROR_MAX_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_shCfgCurrentSenseAmplifierShortedErrorMax > (int16_t)CFG_CURRENT_SENSE_AMPLIFIER_SHORTED_ERROR_MAX_MAX ) || ( const_shCfgCurrentSenseAmplifierShortedErrorMax < (int16_t)CFG_CURRENT_SENSE_AMPLIFIER_SHORTED_ERROR_MAX_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVRefDiagMin > (uint16_t)CFG_VREF_DIAG_MAX ) || ( const_ushCfgVRefDiagMin < (uint16_t)CFG_VREF_DIAG_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_ushCfgVRefDiagMax > (uint16_t)CFG_VREF_DIAG_MAX ) || ( const_ushCfgVRefDiagMax < (uint16_t)CFG_VREF_DIAG_MIN ) || ( const_ushCfgVRefDiagMax <= const_ushCfgVRefDiagMin ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_iCfgStackToSumOfCellsCongruenceVoltageMin > (int32_t)CFG_STACK_TO_SUM_OF_CELLS_CONGRUENCE_VOLTAGE_MIN_MAX ) || ( const_iCfgStackToSumOfCellsCongruenceVoltageMin < (int32_t)CFG_STACK_TO_SUM_OF_CELLS_CONGRUENCE_VOLTAGE_MIN_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_iCfgStackToSumOfCellsCongruenceVoltageMax > (int32_t)CFG_STACK_TO_SUM_OF_CELLS_CONGRUENCE_VOLTAGE_MAX_MAX ) || ( const_iCfgStackToSumOfCellsCongruenceVoltageMax < (int32_t)CFG_STACK_TO_SUM_OF_CELLS_CONGRUENCE_VOLTAGE_MAX_MIN ) || ( const_iCfgStackToSumOfCellsCongruenceVoltageMax <= const_iCfgStackToSumOfCellsCongruenceVoltageMin ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_fCfgSupplyVoltage3V3Min > (float32_t)CFG_SUPPLY_VOLTAGE_3V3_MIN_MAX ) || ( const_fCfgSupplyVoltage3V3Min < (float32_t)CFG_SUPPLY_VOLTAGE_3V3_MIN_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_fCfgSupplyVoltage3V3Max > (float32_t)CFG_SUPPLY_VOLTAGE_3V3_MAX_MAX ) || ( const_fCfgSupplyVoltage3V3Max < (float32_t)CFG_SUPPLY_VOLTAGE_3V3_MAX_MIN ) || ( const_fCfgSupplyVoltage3V3Max <= const_fCfgSupplyVoltage3V3Min ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_fCfgSupplyVoltage5VMin > (float32_t)CFG_SUPPLY_VOLTAGE_5V_MIN_MAX ) || ( const_fCfgSupplyVoltage5VMin < (float32_t)CFG_SUPPLY_VOLTAGE_5V_MIN_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_fCfgSupplyVoltage5VMax > (float32_t)CFG_SUPPLY_VOLTAGE_5V_MAX_MAX ) || ( const_fCfgSupplyVoltage5VMax < (float32_t)CFG_SUPPLY_VOLTAGE_5V_MAX_MIN ) || ( const_fCfgSupplyVoltage5VMax <= const_fCfgSupplyVoltage5VMin ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_fCfgSupplyVoltage12VMin > (float32_t)CFG_SUPPLY_VOLTAGE_12V_MIN_MAX ) || ( const_fCfgSupplyVoltage12VMin < (float32_t)CFG_SUPPLY_VOLTAGE_12V_MIN_MIN ) )
    {
        bConfigurationTestPass = false;
    }
    if ( ( const_fCfgSupplyVoltage12VMax > (float32_t)CFG_SUPPLY_VOLTAGE_12V_MAX_MAX ) || ( const_fCfgSupplyVoltage12VMax < (float32_t)CFG_SUPPLY_VOLTAGE_12V_MAX_MIN ) || ( const_fCfgSupplyVoltage12VMax <= const_fCfgSupplyVoltage12VMin ) )
    {
        bConfigurationTestPass = false;
    }

    /* Check External Communication parameter variables */
    if ( ( const_ucParExternalCommSlaveId > (uint8_t)PAR_EXTERNAL_COMM_SLAVE_ID_MAX ) || ( const_ucParExternalCommSlaveId < (uint8_t)PAR_EXTERNAL_COMM_SLAVE_ID_MIN ) )
    {
        bParameterTestPass = false;
    }
    if ( ( const_ucParExternalCommBaudRate > (uint8_t)PAR_EXTERNAL_COMM_BAUD_RATE_MAX ) || ( const_ucParExternalCommBaudRate < (uint8_t)PAR_EXTERNAL_COMM_BAUD_RATE_MIN ) )
    {
        bParameterTestPass = false;
    }
    if ( ( const_ucParExternalCommParity > (uint8_t)PAR_EXTERNAL_COMM_PARITY_MAX ) || ( const_ucParExternalCommParity < (uint8_t)PAR_EXTERNAL_COMM_PARITY_MIN ) )
    {
        bParameterTestPass = false;
    }
    if ( ( const_ucParExternalCommStopBits > (uint8_t)PAR_EXTERNAL_COMM_STOP_BITS_MAX ) || ( const_ucParExternalCommStopBits < (uint8_t)PAR_EXTERNAL_COMM_STOP_BITS_MIN ) )
    {
        bParameterTestPass = false;
    }
    if ( ( const_ucParExternalCommType > (uint8_t)PAR_EXTERNAL_COMM_TYPE_MAX ) || ( const_ucParExternalCommType < (uint8_t)PAR_EXTERNAL_COMM_TYPE_MIN ) )
    {
        bParameterTestPass = false;
    }

    /* Save test results */
    sSelfTestResults.sConfigurationAndParameterRangeTestResults.bConfigurationVariablesRangeTestPass = bConfigurationTestPass;
    sSelfTestResults.sConfigurationAndParameterRangeTestResults.bParameterVariablesRangeTestPass = bParameterTestPass;

    return ( bConfigurationTestPass & bParameterTestPass );
}

/********************************
* Public Functions
*********************************/

/*********************************************************************
* PUBLIC vExecuteSelfTests()
*---------------------------------------------------------------------
* Execute self tests
*
* Inputs: uiSelfTestFlags - Bitwise encoded variable, tests to be executed
* Returns:
* uint32_t bit array. Each bit corresponds to a test. PASS = 1, FAIL = 0.
*
**********************************************************************/
uint32_t vExecuteSelfTests(uint32_t uiSelfTestFlags)
{
    bool bTestResult;
    uint32_t uiTestResultsFlags;

    /* Initialize result variables */
    uiTestResultsFlags = 0;

    /* Self test #1 & 2 */
    if((uiSelfTestFlags & CFG_RANGE_TEST) || (uiSelfTestFlags & PARAM_RANGE_TEST))
    {
        bTestResult = sbConfigurationAndParameterRangeSelfTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= (CFG_RANGE_TEST | PARAM_RANGE_TEST);
        }
    }

    /* Self test #3 */
    if(uiSelfTestFlags & ADC1_A_B_FUNC_TEST)
    {
        bTestResult = sbAdc1ABFuncTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= ADC1_A_B_FUNC_TEST;
        }
    }

    /* Self test #4 */
    if(uiSelfTestFlags & CELL_VOLT_CHAN_FUNCT_TEST)
    {
        bTestResult = sbCellVoltageChannelFunctionalTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= CELL_VOLT_CHAN_FUNCT_TEST;
        }
    }

    /* Self test #5 */
    if(uiSelfTestFlags & CT_OPEN_DET_TEST)
    {
        bTestResult = sbCellTerminalOpenDetectTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= CT_OPEN_DET_TEST;
        }
    }

    /* Self test #6 */
    if(uiSelfTestFlags & CT_LEAKAGE_TEST)
    {
        bTestResult = sbCellTerminalLeakageTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= CT_OPEN_DET_TEST;
        }
    }

    /* Self test #7 & 8 */
    if((uiSelfTestFlags & CELL_BAL_OPEN_TEST) || (uiSelfTestFlags & CELL_BAL_SHORT_TEST))
    {
        bTestResult = sbCellBalanceTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= (CELL_BAL_OPEN_TEST | CELL_BAL_SHORT_TEST);
        }
    }

    /* Self test #9 & 10 */
    if( (uiSelfTestFlags & CT_OV_FUNCT_TEST) || (uiSelfTestFlags & CT_UV_FUNCT_TEST))
    {
        bTestResult = sbCellTerminalOvUvFuncTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= (CT_OV_FUNCT_TEST | CT_UV_FUNCT_TEST);
        }
    }

    /* Self test #11 */
    if(uiSelfTestFlags & AMP_INPUTS_GND_TEST)
    {
        bTestResult = sbISenseAmpInputsGroundedTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= AMP_INPUTS_GND_TEST;
        }
    }

    /* Self test #12 */
    if(uiSelfTestFlags & VREF_DIAG_CHECK_TEST)
    {
        bTestResult = sbISenseVrefDiagTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= VREF_DIAG_CHECK_TEST;
        }
    }

    /* Self test #13 */
    if(uiSelfTestFlags & GPIO_TERM_OPEN_TEST)
    {
        bTestResult = sbGpioTerminalOpenTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= GPIO_TERM_OPEN_TEST;
        }
    }

    /* Self test #14 */
    if(uiSelfTestFlags & STACK_TO_CELL_SUM_TEST)
    {
        bTestResult = sbStackVsCellSumTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= STACK_TO_CELL_SUM_TEST;
        }
    }

    /* Self test #15 */
    if(uiSelfTestFlags & CUR_SENSE_MEAS_CHN_OPEN_TEST)
    {
        bTestResult = sbISenseMeasChainOpenTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= CUR_SENSE_MEAS_CHN_OPEN_TEST;
        }
    }

    /* Self test #16 */
    if(uiSelfTestFlags & FAULT_PIN_DETECT_TEST)
    {
        bTestResult = sbFaultPinDetectTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= FAULT_PIN_DETECT_TEST;
        }
    }

    /* Self test #17, 18, 19 */
    if(uiSelfTestFlags & VOL_3_3_SUPPLY_TEST || uiSelfTestFlags & VOL_12_SUPPLY_TEST || uiSelfTestFlags & VOL_5_SUPPLY_TEST)
    {
        bTestResult = sbSupplyVoltageCheck();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= (VOL_3_3_SUPPLY_TEST | VOL_12_SUPPLY_TEST | VOL_5_SUPPLY_TEST);
        }
    }

    /* Self test #20 */
    if(uiSelfTestFlags & LM75_OVER_TEMP_TEST)
    {
        bTestResult = sbTempSensorSelfTest();

        if(bTestResult == true)
        {
            uiTestResultsFlags |= LM75_OVER_TEMP_TEST;
        }
    }

    /* Make sure all registers are reset to the required default conditions for this system */
    //vInitializeBatteryCellCtl();


    return (uiTestResultsFlags);
}


/*********************************************************************
* PUBLIC spGetSelfTestResultsBuffer()
*---------------------------------------------------------------------
* Get the reference (Pointer) to the buffer containing the self test results
*
* Inputs: None
* Returns:
* sSelfTestResults_t* - Pointer to the self test results buffer
*
**********************************************************************/
sSelfTestResults_t* spGetSelfTestResultsBuffer(void)
{
    return (&sSelfTestResults);
}



/******************************** End of File ********************************/
