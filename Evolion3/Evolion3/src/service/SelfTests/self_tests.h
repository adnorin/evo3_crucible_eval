#ifndef SELF_TESTS_H_
#define SELF_TESTS_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc and can only be         */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: self_tests.h                                                   */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for self_tests.c, which provides prototypes for the           */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#include <current_limits.h>
#include <SOC_calculation.h>
#include <current_meas.h>
#include <temp_meas.h>

/* Macros and Definitions */
#define ADC1_A_B_FUNC_TEST              0x00000001
#define CELL_VOLT_CHAN_FUNCT_TEST       0x00000002
#define CT_OPEN_DET_TEST                0x00000004
#define CT_LEAKAGE_TEST                 0x00000040
#define CELL_BAL_OPEN_TEST              0x00000080
#define CELL_BAL_SHORT_TEST             0x00000100
#define CT_UV_FUNCT_TEST                0x00000200
#define CT_OV_FUNCT_TEST                0x00000400
#define AMP_INPUTS_GND_TEST             0x00000800
#define VREF_DIAG_CHECK_TEST            0x00001000
#define GPIO_TERM_OPEN_TEST             0x00002000
#define STACK_TO_CELL_SUM_TEST          0x00004000
#define CUR_SENSE_MEAS_CHN_OPEN_TEST    0x00008000
#define VOL_3_3_SUPPLY_TEST             0x00010000
#define VOL_12_SUPPLY_TEST              0x00020000
#define VOL_5_SUPPLY_TEST               0x00040000
#define FAULT_PIN_DETECT_TEST           0x00080000
#define LM75_OVER_TEMP_TEST             0x00100000
#define CFG_RANGE_TEST                  0x00200000
#define PARAM_RANGE_TEST                0x00400000
#define CURRENT_IN_DEADZONE_TEST        0x00800000

#define SELF_TEST_BIT_ADC_CHK                       ( 0x00 )    //( 0x000001 )
#define SELF_TEST_BIT_CELL_V_CHK                    ( 0x01 )    //( 0x000002 )
#define SELF_TEST_BIT_CELL_TERM_OPEN_CHK            ( 0x02 )    //( 0x000004 )
#define SELF_TEST_BIT_CELL_TERM_SHORT_CHK           ( 0x03 )    //( 0x000008 )
#define SELF_TEST_BIT_CELL_TERM_SW_OPEN_OOR_CHK     ( 0x04 )    //( 0x000010 )
#define SELF_TEST_BIT_CELL_TERM_SW_CLOSE_OOR_CHK    ( 0x05 )    //( 0x000020 )
#define SELF_TEST_BIT_CELL_TERM_LEAK_CHK            ( 0x06 )    //( 0x000040 )
#define SELF_TEST_BIT_CELL_BALL_OPEN_CHK            ( 0x07 )    //( 0x000080 )
#define SELF_TEST_BIT_CELL_BALL_SHORT_CHK           ( 0x08 )    //( 0x000100 )
#define SELF_TEST_BIT_CELL_UV_CHK                   ( 0x09 )    //( 0x000200 )
#define SELF_TEST_BIT_CELL_OV_CHK                   ( 0x0A )    //( 0x000400 )
#define SELF_TEST_BIT_AMP_IN_GND_CHK                ( 0x0B )    //( 0x000800 )
#define SELF_TEST_BIT_VREF_DIAG_CHK                 ( 0x0C )    //( 0x001000 )
#define SELF_TEST_BIT_GPIO_TERM_OPEN_CHK            ( 0x0D )    //( 0x002000 )
#define SELF_TEST_BIT_STACK_TO_SUM_CHK              ( 0x0E )    //( 0x004000 )
#define SELF_TEST_BIT_CURR_SENS_OPEN_CHK            ( 0x0F )    //( 0x008000 )
#define SELF_TEST_BIT_3V3_CHK                       ( 0x10 )    //( 0x010000 )
#define SELF_TEST_BIT_12V_CHK                       ( 0x11 )    //( 0x020000 )
#define SELF_TEST_BIT_5V_CHK                        ( 0x12 )    //( 0x040000 )
#define SELF_TEST_MC33771_FAULT_CHK                 ( 0x13 )    //( 0x080000 )
#define SELF_TEST_TEMP_OT_FAULT_CHK                 ( 0x14 )    //( 0x100000 )
#define SELF_TEST_CFG_RANGE_CHK                     ( 0x15 )    //( 0x200000 )
#define SELF_TEST_PARAM_RANGE_CHK                   ( 0x16 )    //( 0x400000 )
#define SELF_TEST_CURRENT_IN_DEADZONE_CHK           ( 0x17 )    //( 0x800000 )

#define GO2DIAG_MODE            0x0040
#define CB_MANUAL_PAUSE         0x0020
#define CB_DRVEN                0x0080
#define I_MEAS_EN               0x0200
#define SYS_CFG_REG_DEFAULT     0x5381 /* CYCLIC_TIMER: 100ms, I_MEAS_EN Enabled, CB_AUTO_PAUSE Enabled, CB Driver Enabled, OSC_MON Enabled */
#define EXIT2DIAG_MODE          0x1201
#define CURRENT_SENSE_SCALAR    6
#define MEAS_RESOLUTION_FLOAT   152.587890625           /* 152.58789 uV per LSB */
#define CELL_TERMINAL_OPEN_DETECTION_RAIL_VOLTAGE 5000  /* 5000 mV */
#define GPIO_OPEN_ANALOG_INPUT_MASK     0x003A
#define GPIO_OPEN_FAULT2_STATUS_MASK    0x0040
#define SUPPLY_12V_SCALAR       2.5037593841552734375   /* 166.5kOhm / 66.5kOhm */
#define SUPPLY_5V_SCALAR        1.200400829315185546875 /* 599kOhm / 499kOhm */

#define CFG_FAST_OVER_CURRENT_I2T_VALUE_MAX                 4294967295UL    /* uint32_t data type max */
#define CFG_FAST_OVER_CURRENT_I2T_VALUE_MIN                 0               /* uint32_t data type min */
#define CFG_MIN_TCELL_TO_CALCULATE_CURRENT_LIMITS_MAX       250     /*  25.0 degC */
#define CFG_MIN_TCELL_TO_CALCULATE_CURRENT_LIMITS_MIN       -550    /* -55.0 degC */
#define CFG_MAX_TCELL_TO_CALCULATE_CURRENT_LIMITS_MAX       900     /*  90.0 degC */
#define CFG_MAX_TCELL_TO_CALCULATE_CURRENT_LIMITS_MIN       250     /*  25.0 degC */
#define CFG_VCELL_TO_START_DECREASAING_IMD_MAX              3600    /*   3.600 V */
#define CFG_VCELL_TO_START_DECREASAING_IMD_MIN              1200    /*   1.200 V */
#define CFG_VCELL_TO_INHIBIT_DISCHARGE_MAX                  3600    /*   3.600 V */
#define CFG_VCELL_TO_INHIBIT_DISCHARGE_MIN                  1200    /*   1.200 V */
#define CFG_VCELL_TO_START_DECREASAING_IMR_MAX              4200    /*   4.200 V */
#define CFG_VCELL_TO_START_DECREASAING_IMR_MIN              3600    /*   3.600 V */
#define CFG_VCELL_TO_INHIBIT_CHARGE_MAX                     4200    /*   4.200 V */
#define CFG_VCELL_TO_INHIBIT_CHARGE_MIN                     3600    /*   3.600 V */
#define CFG_IMD_LUT_MAX                                     0       /*   0.000 A */
#define CFG_IMD_LUT_MIN                                     -80000  /* -80.000 A */
#define CFG_IMR_LUT_MAX                                     80000   /*  80.000 A */
#define CFG_IMR_LUT_MIN                                     0       /*   0.000 A */
#define CFG_NB_CURRENT_LIMIT_SAMPLES_TO_AVG_MAX             NB_IMX_SAMPLES_TO_AVG_MAX
#define CFG_NB_CURRENT_LIMIT_SAMPLES_TO_AVG_MIN             1       /*   1 sample */
#define CFG_NB_CURRENT_SAMPLES_TO_AVG_MAX                   NB_CURRENT_SAMPLES_TO_AVG_MAX
#define CFG_NB_CURRENT_SAMPLES_TO_AVG_MIN                   1       /*   1 sample */
#define CFG_NB_TEMPERATURE_SAMPLES_TO_AVG_MAX               NB_TEMP_SAMPLES_TO_AVG_MAX
#define CFG_NB_TEMPERATURE_SAMPLES_TO_AVG_MIN               1       /*   1 sample */
#define CFG_CELL_V_ONE_HUNDRED_SOC_MAX                      4200    /*   4.200 V */
#define CFG_CELL_V_ONE_HUNDRED_SOC_MIN                      3600    /*   3.600 V */
#define CFG_CELL_V_ZERO_SOC_MAX                             3600    /*   3.600 V */
#define CFG_CELL_V_ZERO_SOC_MIN                             1200    /*   1.200 V */
#define CFG_SOC_MAX_MAX                                     1000    /* 100.0 % */
#define CFG_SOC_MAX_MIN                                     500     /*  50.0 % */
#define CFG_SOC_MIN_MAX                                     500     /*  50.0 % */
#define CFG_SOC_MIN_MIN                                     0       /*   0.0 % */
#define CFG_SOC_FULLY_CHARGED_MAX                           1000    /* 100.0 % */
#define CFG_SOC_FULLY_CHARGED_MIN                           500     /*  50.0 % */
#define CFG_SOC_FULLY_DISCHARGED_MAX                        500     /*  50.0 % */
#define CFG_SOC_FULLY_DISCHARGED_MIN                        0       /*   0.0 % */
#define CFG_SOC_RATE_OF_CHANGE_MAX_MAX                      5       /*   0.5 % */
#define CFG_SOC_RATE_OF_CHANGE_MAX_MIN                      1       /*   0.1 % */
#define CFG_NB_SOC_SAMPLES_TO_AVG_MAX                       NB_SOC_SAMPLES_TO_AVG_MAX
#define CFG_NB_SOC_SAMPLES_TO_AVG_MIN                       1       /*   1 sample */
#define CFG_SOC_LUT_MAX                                     1000    /* 100.0 % */
#define CFG_SOC_LUT_MIN                                     0       /*   0.0 % */
#define CFG_CELL_BALANCING_DURATION_MAX                     600     /*  60.0 s */
#define CFG_CELL_BALANCING_DURATION_MIN                     1       /*   0.1 s */
#define CFG_CELL_BALANCING_DELTA_MAX                        100     /*   0.100 V */
#define CFG_CELL_BALANCING_DELTA_MIN                        5       /*   0.005 V */
#define CFG_CELL_BALANCING_VOLTAGE_MIN_MAX                  4200    /*   4.200 V */
#define CFG_CELL_BALANCING_VOLTAGE_MIN_MIN                  1200    /*   1.200 V */
#define CFG_CELL_BALANCING_HYSTERESIS_MAX                   100     /*   0.100 V */
#define CFG_CELL_BALANCING_HYSTERESIS_MIN                   0       /*   0.000 V */
#define CFG_CELL_BALANCING_CELL_TMAX_MAX                    850     /*  85.0 degC */
#define CFG_CELL_BALANCING_CELL_TMAX_MIN                    -550    /* -55.0 degC */
#define CFG_CELL_BALANCING_ELEC_TMAX_MAX                    1250    /* 125.0 degC */
#define CFG_CELL_BALANCING_ELEC_TMAX_MIN                    -550    /* -55.0 degC */
#define CFG_ELECTRONICS_OVER_TEMP_ALARM_THRESHOLD_MAX       1250    /* 125.0 degC */
#define CFG_ELECTRONICS_OVER_TEMP_ALARM_THRESHOLD_MIN       550     /*  55.0 degC */
#define CFG_ELECTRONICS_OVER_TEMP_WARNING_THRESHOLD_MAX     1250    /* 125.0 degC */
#define CFG_ELECTRONICS_OVER_TEMP_WARNING_THRESHOLD_MIN     550     /*  55.0 degC */
#define CFG_ELECTRONICS_OVER_TEMP_FAULT_HYSTERESIS_MAX      200     /*  20.0 degC */
#define CFG_ELECTRONICS_OVER_TEMP_FAULT_HYSTERESIS_MIN      0       /*   0.0 degC */
#define CFG_ELECTRONICS_TEMP_FAULT_DELAY_MAX                600     /*  60.0 s */
#define CFG_ELECTRONICS_TEMP_FAULT_DELAY_MIN                0       /*   0.0 s */
#define CFG_BATTERY_UNDER_SOC_ALARM_THRESHOLD_MAX           500     /*  50.0 % */
#define CFG_BATTERY_UNDER_SOC_ALARM_THRESHOLD_MIN           0       /*   0.0 % */
#define CFG_BATTERY_UNDER_SOC_WARNING_THRESHOLD_MAX         500     /*  50.0 % */
#define CFG_BATTERY_UNDER_SOC_WARNING_THRESHOLD_MIN         0       /*   0.0 % */
#define CFG_BATTERY_OVER_SOC_ALARM_THRESHOLD_MAX            1000    /* 100.0 % */
#define CFG_BATTERY_OVER_SOC_ALARM_THRESHOLD_MIN            500     /*  50.0 % */
#define CFG_BATTERY_OVER_SOC_WARNING_THRESHOLD_MAX          1000    /* 100.0 % */
#define CFG_BATTERY_OVER_SOC_WARNING_THRESHOLD_MIN          500     /*  50.0 % */
#define CFG_BATTERY_SOC_FAULT_HYSTERESIS_MAX                500     /*  50.0 % */
#define CFG_BATTERY_SOC_FAULT_HYSTERESIS_MIN                0       /*   0.0 % */
#define CFG_BATTERY_SOC_FAULT_DELAY_MAX                     600     /*  60.0 s */
#define CFG_BATTERY_SOC_FAULT_DELAY_MIN                     0       /*   0.0 s */
#define CFG_OVER_CURRENT_IN_DISCHARGE_ALARM_THRESHOLD_MAX   200     /* 200 % */
#define CFG_OVER_CURRENT_IN_DISCHARGE_ALARM_THRESHOLD_MIN   50      /*  50 % */
#define CFG_OVER_CURRENT_IN_DISCHARGE_WARNING_THRESHOLD_MAX 200     /* 200 % */
#define CFG_OVER_CURRENT_IN_DISCHARGE_WARNING_THRESHOLD_MIN 50      /*  50 % */
#define CFG_OVER_CURRENT_IN_DISCHARGE_MINIMUM_CURRENT_MAX   0       /*   0.00 A */
#define CFG_OVER_CURRENT_IN_DISCHARGE_MINIMUM_CURRENT_MIN   -8000   /* -80.00 A */
#define CFG_OVER_CURRENT_IN_CHARGE_ALARM_THRESHOLD_MAX      200     /* 200 % */
#define CFG_OVER_CURRENT_IN_CHARGE_ALARM_THRESHOLD_MIN      50      /*  50 % */
#define CFG_OVER_CURRENT_IN_CHARGE_WARNING_THRESHOLD_MAX    200     /* 200 % */
#define CFG_OVER_CURRENT_IN_CHARGE_WARNING_THRESHOLD_MIN    50      /*  50 % */
#define CFG_OVER_CURRENT_IN_CHARGE_MINIMUM_CURRENT_MAX      8000    /*  80.00 A */
#define CFG_OVER_CURRENT_IN_CHARGE_MINIMUM_CURRENT_MIN      0       /*   0.00 A */
#define CFG_OVER_CURRENT_FAULT_HYSTERESIS_MAX               100     /* 100 % */
#define CFG_OVER_CURRENT_FAULT_HYSTERESIS_MIN               0       /*   0 % */
#define CFG_OVER_CURRENT_FAULT_DELAY_MAX                    600     /*  60.0 s */
#define CFG_OVER_CURRENT_FAULT_DELAY_MIN                    0       /*   0.0 s */
#define CFG_OVER_CURRENT_THRESHOLD_BASE_MAX                 200     /* 200 % */
#define CFG_OVER_CURRENT_THRESHOLD_BASE_MIN                 50      /*  50 % */
#define CFG_CELL_OVER_TEMP_ALARM_THRESHOLD_MAX              850     /*  85.0 degC */
#define CFG_CELL_OVER_TEMP_ALARM_THRESHOLD_MIN              250     /*  25.0 degC */
#define CFG_CELL_OVER_TEMP_WARNING_THRESHOLD_MAX            850     /*  85.0 degC */
#define CFG_CELL_OVER_TEMP_WARNING_THRESHOLD_MIN            250     /*  25.0 degC */
#define CFG_CELL_UNDER_TEMP_ALARM_THRESHOLD_MAX             250     /*  25.0 degC */
#define CFG_CELL_UNDER_TEMP_ALARM_THRESHOLD_MIN             -550    /* -55.0 degC */
#define CFG_CELL_UNDER_TEMP_WARNING_THRESHOLD_MAX           250     /*  25.0 degC */
#define CFG_CELL_UNDER_TEMP_WARNING_THRESHOLD_MIN           -550    /* -55.0 degC */
#define CFG_CELL_TEMP_FAULT_HYSTERESIS_MAX                  100     /*  10.0 degC */
#define CFG_CELL_TEMP_FAULT_HYSTERESIS_MIN                  0       /*   0.0 degC */
#define CFG_CELL_TEMP_FAULT_DELAY_MAX                       600     /*  60.0 s */
#define CFG_CELL_TEMP_FAULT_DELAY_MIN                       0       /*   0.0 s */
#define CFG_CELL_OVER_VOLTAGE_ALARM_THRESHOLD_MAX           4250    /*   4.250 V */
#define CFG_CELL_OVER_VOLTAGE_ALARM_THRESHOLD_MIN           3600    /*   3.600 V */
#define CFG_CELL_OVER_VOLTAGE_WARNING_THRESHOLD_MAX         4250    /*   4.250 V */
#define CFG_CELL_OVER_VOLTAGE_WARNING_THRESHOLD_MIN         3600    /*   3.600 V */
#define CFG_CELL_UNDER_VOLTAGE_ALARM_THRESHOLD_MAX          3600    /*   3.600 V */
#define CFG_CELL_UNDER_VOLTAGE_ALARM_THRESHOLD_MIN          500     /*   0.500 V */
#define CFG_CELL_UNDER_VOLTAGE_WARNING_THRESHOLD_MAX        3600    /*   3.600 V */
#define CFG_CELL_UNDER_VOLTAGE_WARNING_THRESHOLD_MIN        500     /*   0.500 V */
#define CFG_CELL_VOLTAGE_FAULT_HYSTERESIS_MAX               1000    /*   1.000 V */
#define CFG_CELL_VOLTAGE_FAULT_HYSTERESIS_MIN               0       /*   0.000 V */
#define CFG_CELL_VOLTAGE_FAULT_DELAY_MAX                    600     /*  60.0 s */
#define CFG_CELL_VOLTAGE_FAULT_DELAY_MIN                    0       /*   0.0 s */
#define CFG_CELL_TEMP_DIFFERENCE_WARNING_THRESHOLD_MAX      500     /*  50.0 degC */
#define CFG_CELL_TEMP_DIFFERENCE_WARNING_THRESHOLD_MIN      10      /*   1.0 degC */
#define CFG_CELL_TEMP_DIFFERENCE_FAULT_HYSTERESIS_MAX       100     /*  10.0 degC */
#define CFG_CELL_TEMP_DIFFERENCE_FAULT_HYSTERESIS_MIN       0       /*   0.0 degC */
#define CFG_CELL_TEMP_DIFFERENCE_FAULT_DELAY_MAX            600     /*  60.0 s */
#define CFG_CELL_TEMP_DIFFERENCE_FAULT_DELAY_MIN            0       /*   0.0 s */
#define CFG_CELL_VOLTAGE_IMBALANCE_WARNING_THRESHOLD_MAX    350     /*   0.350 V */
#define CFG_CELL_VOLTAGE_IMBALANCE_WARNING_THRESHOLD_MIN    10      /*   0.010 V */
#define CFG_CELL_VOLTAGE_IMBALANCE_FAULT_HYSTERESIS_MAX     350     /*   0.350 V */
#define CFG_CELL_VOLTAGE_IMBALANCE_FAULT_HYSTERESIS_MIN     0       /*   0.000 V */
#define CFG_CELL_VOLTAGE_IMBALANCE_FAULT_DELAY_MAX          600     /*  60.0 s */
#define CFG_CELL_VOLTAGE_IMBALANCE_FAULT_DELAY_MIN          0       /*   0.0 s */
#define CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MAX             6000    /*  60.00 V */
#define CFG_REGULATED_CURRENT_VOLTAGE_LIMIT_MIN             0       /*   0.00 V */
#define CFG_REGULATED_CURRENT_CURRENT_LIMIT_MAX             8000    /*  80.00 A */
#define CFG_REGULATED_CURRENT_CURRENT_LIMIT_MIN             0       /*   0.00 A */
#define CFG_REDUNDANT_ELECTRONICS_OVER_TEMP_THRESHOLD_MAX   125     /* 125 degC */
#define CFG_REDUNDANT_ELECTRONICS_OVER_TEMP_THRESHOLD_MIN   -55     /* -55 degC */
#define CFG_REDUNDANT_ELECTRONICS_OVER_TEMP_HYSTERESIS_MAX  125     /* 125 degC */
#define CFG_REDUNDANT_ELECTRONICS_OVER_TEMP_HYSTERESIS_MIN  -55     /* -55 degC */
#define CFG_REDUNDANT_CELL_OVER_TEMP_THRESHOLD_MAX          125     /* 125 degC */
#define CFG_REDUNDANT_CELL_OVER_TEMP_THRESHOLD_MIN          -55     /* -55 degC */
#define CFG_REDUNDANT_CELL_OVER_TEMP_HYSTERESIS_MAX         125     /* 125 degC */
#define CFG_REDUNDANT_CELL_OVER_TEMP_HYSTERESIS_MIN         -55     /* -55 degC */
#define CFG_CELL_VOLTAGE_CHANNEL_ERROR_MIN_MAX              0                       /*   0 mV */
#define CFG_CELL_VOLTAGE_CHANNEL_ERROR_MIN_MIN              -65.1999969482421875    /* -32.6 mV * 2 */
#define CFG_CELL_VOLTAGE_CHANNEL_ERROR_MAX_MAX              25.200000762939453125   /*  12.6 mV * 2 */
#define CFG_CELL_VOLTAGE_CHANNEL_ERROR_MAX_MIN              0                       /*   0 mV */
#define CFG_CELL_TERMINAL_OPEN_DETECTION_ERROR_MAX_MAX      300 /* 150 mV * 2 */
#define CFG_CELL_TERMINAL_OPEN_DETECTION_ERROR_MAX_MIN      0   /*   0 mV */
#define CFG_CELL_TERMINAL_OPEN_DETECTION_BASELINE_DISCREPANCY_MAX_MAX   1000    /* 500 mV * 2 */
#define CFG_CELL_TERMINAL_OPEN_DETECTION_BASELINE_DISCREPANCY_MAX_MIN   0       /*   0 mV */
#define CFG_CELL_TERMINAL_LEAKAGE_ERROR_MAX_MAX             60  /* 30 mV * 2 */
#define CFG_CELL_TERMINAL_LEAKAGE_ERROR_MAX_MIN             0   /*  0 mV */
#define CFG_CURRENT_SENSE_AMPLIFIER_SHORTED_ERROR_MAX_MAX   744 /* 37.2 uV *2 */
#define CFG_CURRENT_SENSE_AMPLIFIER_SHORTED_ERROR_MAX_MIN   0   /*  0.0 uV */
#define CFG_VREF_DIAG_MAX                                   1512    /* 126.0 mV + 20% */
#define CFG_VREF_DIAG_MIN                                   1008    /* 126.0 mV - 20% */
#define CFG_STACK_TO_SUM_OF_CELLS_CONGRUENCE_VOLTAGE_MIN_MAX    1000    /*  1.000 V */
#define CFG_STACK_TO_SUM_OF_CELLS_CONGRUENCE_VOLTAGE_MIN_MIN    -1000   /* -1.000 V */
#define CFG_STACK_TO_SUM_OF_CELLS_CONGRUENCE_VOLTAGE_MAX_MAX    4000    /*  4.000 V */
#define CFG_STACK_TO_SUM_OF_CELLS_CONGRUENCE_VOLTAGE_MAX_MIN    0       /*  0.000 V */
#define CFG_SUPPLY_VOLTAGE_3V3_MIN_MAX                      3.2999999523162841796875    /*  3.3 V */
#define CFG_SUPPLY_VOLTAGE_3V3_MIN_MIN                      2.6400001049041748046875    /*  3.3 V - 20% */
#define CFG_SUPPLY_VOLTAGE_3V3_MAX_MAX                      3.96000003814697265625      /*  3.3 V + 20% */
#define CFG_SUPPLY_VOLTAGE_3V3_MAX_MIN                      3.2999999523162841796875    /*  3.3 V */
#define CFG_SUPPLY_VOLTAGE_5V_MIN_MAX                       5                           /*  5 V */
#define CFG_SUPPLY_VOLTAGE_5V_MIN_MIN                       4                           /*  5 V - 20% */
#define CFG_SUPPLY_VOLTAGE_5V_MAX_MAX                       6                           /*  5 V + 20% */
#define CFG_SUPPLY_VOLTAGE_5V_MAX_MIN                       5                           /*  5 V */
#define CFG_SUPPLY_VOLTAGE_12V_MIN_MAX                      12                          /* 12 V */
#define CFG_SUPPLY_VOLTAGE_12V_MIN_MIN                      9.6000003814697265625       /* 12 V - 20% */
#define CFG_SUPPLY_VOLTAGE_12V_MAX_MAX                      14.3999996185302734375      /* 12 V + 20% */
#define CFG_SUPPLY_VOLTAGE_12V_MAX_MIN                      12                          /* 12 V */
#define PAR_EXTERNAL_COMM_SLAVE_ID_MAX                      247                         /* 247 is max node ID for a Modbus slave */
#define PAR_EXTERNAL_COMM_SLAVE_ID_MIN                      1                           /*   1 is min node ID for a Modbus slave */
#define PAR_EXTERNAL_COMM_BAUD_RATE_MAX                     EXTCOMM_BAUD_RATE_115200    /* 115200 is max supported baud rate, enum as 3 */
#define PAR_EXTERNAL_COMM_BAUD_RATE_MIN                     EXTCOMM_BAUD_RATE_9600      /*   9600 is min supported baud rate, enum as 0 */
#define PAR_EXTERNAL_COMM_PARITY_MAX                        EXTCOMM_PARITY_EVEN         /*   Even is "max" supported parity, enum as 2 */
#define PAR_EXTERNAL_COMM_PARITY_MIN                        EXTCOMM_PARITY_NONE         /*   None is "min" supported parity, enum as 0 */
#define PAR_EXTERNAL_COMM_STOP_BITS_MAX                     EXTCOMM_STOP_BITS_2         /*      2 stop bits is the max supported, enum as 2 */
#define PAR_EXTERNAL_COMM_STOP_BITS_MIN                     EXTCOMM_STOP_BITS_1         /*      1 stop bits is the max supported, enum as 1 */
#define PAR_EXTERNAL_COMM_TYPE_MAX                          EXTCOMM_TYPE_SINGLE_VOICE   /* "single voice" is max supported communication type, enum as 2 */
#define PAR_EXTERNAL_COMM_TYPE_MIN                          EXTCOMM_TYPE_LEGACY         /*       "legacy" is min supported communication type, enum as 1 */

/* Custom types */

/* Self test #1 & 2 */
typedef struct
{
    bool bConfigurationVariablesRangeTestPass;
    bool bParameterVariablesRangeTestPass;
}sConfigurationAndParameterRangeTestResults_t;

/* Self test #3 */
typedef struct
{
    bool bAdc1abTestPass;
    bool bAdc1aFaultBit;
    bool bAdc1bFaultBit;
    uint16_t Adc1aVbgValue;
    uint16_t Adc1bVbgValue;
}sADC1abFuncTestResults_t;

/* Self test #4 & 5 */
typedef struct
{
    bool bOvFaultTestPass;
    bool bUvFaultTestPass;
    uint16_t ushOvFailedCells;
    uint16_t ushUvFailedCells;
}sOvUvFuncTestResults_t;

/* Self test #6 */
typedef struct
{
    bool bCTOpenDetTestPass;
    bool bCTShortDetTestPass;
    bool bCTCloseOutRangeTestPass;
    bool bCTOpenOutRangeTestPass;
    uint16_t ushCTOdShortFailedCells;
    uint16_t ushCTOdOpenFailedCells;
    uint16_t ushCTOdClosedSWOutOfRangeCells;
    uint16_t ushCTOdOpenSWOutOfRangeCells;
}sCTOpenDetTestResults_t;

/* Self test #7 */
typedef struct
{
    bool bCVFVTestPass;
    uint16_t ushCVFVFailedCells;
}sCellVoltFuncVerTestResults_t;

/* Self test #8 */
typedef struct
{
    bool bCTLeakageTestPass;
    uint16_t ushCTLeakageFailedCells;
}sCTLeakageTestResults_t;

/* Self test #9 & 10 */
typedef struct
{
    bool bCBOpenTestPass;
    bool bCBShortTestPass;
    uint16_t ushCBOpenFailedCells;
    uint16_t ushCShortFailedCells;
}sCellBalanceTestResults_t;

/* Self test #11 */
typedef struct
{
    bool bIMeashAmpInputGndTestPass;
    int32_t iIMeasValue;
}sIMeashAmpInputGndTestResults_t;

/* Self test #12 */
typedef struct
{
    bool bVrefDiagTestPass;
    uint16_t ushVrefDiagMeasValue;
}sVrefDiagTestResults_t;

/* Self test #13 */
typedef struct
{
    bool bGpioOpenTestPass;
    uint16_t ushGpioOpenFailedPins;
}sGpioOpenTestResults_t;

/* Self test #14 */
typedef struct
{
    bool bStackVsSumTestPass;
    uint16_t ushStackVoltage;
    uint16_t ushCellVoltageSum;
}sStackVsVolTestResults_t;

/* Self test #15 */
typedef struct
{
    bool bISenseMeasChainOpenTestPass;
}sISenseMeasChainOpenTestResults_t;

/* Self test #16 */
typedef struct
{
    bool bFaultPinDetectTestPass;
}sFaultPinDetectTestResults_t;


/* Self test #17, 18, 19 */
typedef struct
{
    bool bSupply_3_3VTestPass;
    bool bSupply_5VTestPass;
    bool bSupply_12VTestPass;
}sSupplyVoltagesTestResults_t;

/* Self test #20 */
typedef struct
{
    bool bTempSensorTestPass;
    uint16_t uchFailedSensors;
}sTempSensorTestResults_t;

/* Self test #21 */
typedef struct
{
    bool bCurrentInDeadzoneTestPass;
    int16_t shMeasuredCurrentValue;
}sCurrentInDeadzoneTestResults_t;


/* Self test results */
typedef struct
{
    /* Self Test #1 & 2 */
    sConfigurationAndParameterRangeTestResults_t sConfigurationAndParameterRangeTestResults;

    /* Self Test #3 */
    sADC1abFuncTestResults_t sADC1abFuncTestResults;

    /* Self Test #4 & 5 */
    sOvUvFuncTestResults_t sOvUvFuncTestResults;

    /* Self test #6 */
    sCTOpenDetTestResults_t sCTOpenDetTestResults;

    /* Self test #7 */
    sCellVoltFuncVerTestResults_t sCellVoltFuncVerTestResults;

    /* Self test #8 */
    sCTLeakageTestResults_t sCTLeakageTestResults;

    /* Self Test #9 & 10 */
    sCellBalanceTestResults_t sCellBalanceTestResults;

    /* Self Test #11 */
    sIMeashAmpInputGndTestResults_t sIMeashAmpInputGndTestResults;

    /* Self Test #12 */
    sVrefDiagTestResults_t sVrefDiagTestResults;

    /* Self Test #13 */
    sGpioOpenTestResults_t sGpioOpenTestResults;

    /* Self Test #14 */
    sStackVsVolTestResults_t sStackVsVolTestResults;

    /* Self Test #15 */
    sISenseMeasChainOpenTestResults_t sISenseMeasChainOpenTestResults;

    /* Self Test #16 */
    sFaultPinDetectTestResults_t sFaultPinDetectTestResults;

    /* Self Test #17, 18, 19 */
    sSupplyVoltagesTestResults_t sSupplyVoltagesTestResults;

    /* Self Test #20 */
    sTempSensorTestResults_t sTempSensorTestResults;

    /* Self Test #21 */
    sCurrentInDeadzoneTestResults_t sCurrentInDeadzoneTestResults;

}sSelfTestResults_t;


/* Prototypes and interfaces */
/*********************************************************************
* PUBLIC vExecuteSelfTests()
*---------------------------------------------------------------------
* Execute self tests
*
* Inputs: uiSelfTestFlags - Bitwise encoded variable, tests to be executed
* Returns:
*   uint32_t bit array. Each bit corresponds to a test. PASS = 1, FAIL = 0.
*
**********************************************************************/
uint32_t vExecuteSelfTests(uint32_t uiSelfTestFlags);


/*********************************************************************
* PUBLIC spGetSelfTestResultsBuffer()
*---------------------------------------------------------------------
* Get the reference (Pointer) to the buffer containing the self test results
*
* Inputs: None
* Returns:
*   sSelfTestResults_t* - Pointer to the self test results buffer
*
**********************************************************************/
sSelfTestResults_t* spGetSelfTestResultsBuffer(void);

#endif /* SELF_TESTS_H_ */