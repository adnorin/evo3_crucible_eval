/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: test_flash.c                                                   */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Flash test calculates a 32bit CRC value of the contents of the flash that */
/* includes the Evolion 3 application (excluding bootloader). This CRC32     */
/* value is compared to the CRC32 value programmed by the boot loader        */
/* the value must match in order to have a successful test result.           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <stdlib.h>
#include <status_codes.h>
#include <crc32.h>
#include <test_flash.h>

/********************************************************************
* PRIVATE uiCalculateFlashCRC()
*--------------------------------------------------------------------
* Calculates a 32 bit CRC from Flash programmed data.
*
* Inputs: None
* Returns:
*      Returns 32 bit CRC (uint32 word).
*
*********************************************************************/
static uint32_t uiCalculateFlashCRC(void)
{   
    uint32_t uiDataBlockCRC; /* Location to store the newly calculated CRC value */
    
    /* Calculate FLash CRC */
    crc32_calculate((void*)(FLASH_DATA_ADDRESS), FLASH_DATA_LEN, &uiDataBlockCRC);  
        
    /* Return the calculated CRC */
    return uiDataBlockCRC;
}


/********************************************************************
* PUBLIC bVerifyFlashCRC()
*--------------------------------------------------------------------
* Calculates a 32 bit CRC from Flash programmed data and compares it
* to the programmed CRC.
*
* Inputs: None
* Returns:
*      true  - Success flash CRC matches expected value
*      false - Failure flash CRC does not match expected value
*
*********************************************************************/
bool bVerifyFlashCRC(void)
{
    uint32_t *uipFlashCrcAddress;   /* Programmed CRC value address from flash */
    uint32_t  uiFlashCrcCalculated; /* Calculated CRC value from flash */
    bool bReturnValue = false;      /* Return value (default = false) */
    
    /* Calculate CRC from flash and also retrieve the programmed CRC value */
    uiFlashCrcCalculated = uiCalculateFlashCRC();
    uipFlashCrcAddress = (uint32_t *)(FLASH_CRC_ADDRESS);
    
    /* Compare CRC values */
    if(*uipFlashCrcAddress == uiFlashCrcCalculated)
    {
        bReturnValue = true;    
    }
    else
    {
        /* CRC does not match return false (default value) */
		SetBootloaderFlag();
		// To Do enable system reset function when CRC is implemented
		//system_reset();
    }
    
    /* Results */
    return bReturnValue;
}

/********************************************************************
* PUBLIC SetBootloaderFlag()
*--------------------------------------------------------------------
* Set the bootloader flag
*
*
* Inputs: None
* Returns: None
*
*
*********************************************************************/

void SetBootloaderFlag(void)
{
	
	//To Do
	
}



/******************************** End of File ********************************/
