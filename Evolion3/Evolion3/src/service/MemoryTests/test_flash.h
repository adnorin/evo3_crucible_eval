#ifndef TEST_FLASH_H_
#define TEST_FLASH_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc and can only be         */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: test_flash.h                                                   */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for test_ram.c, which provides prototypes for the             */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define FLASH_DATA_ADDRESS  0x00006000
#define FLASH_DATA_LEN      0x39FF8
#define FLASH_CRC_ADDRESS   0x0003FFF8


/********************************************************************
* PUBLIC bVerifyFlashCRC()
*--------------------------------------------------------------------
* Calculates a 32 bit CRC from Flash programmed data and compares it
* to the programmed CRC.
*
* Inputs: None
* Returns:
*      true  - Success flash CRC matches expected value
*      false - Failure flash CRC does not match expected value
*
*********************************************************************/
bool bVerifyFlashCRC(void);
void SetBootloaderFlag(void);

#endif /* TEST_FLASH_H_ */