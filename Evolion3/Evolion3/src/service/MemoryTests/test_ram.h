#ifndef TEST_RAM_H_
#define TEST_RAM_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc and can only be         */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: test_ram.h                                                     */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for test_ram.c, which provides prototypes for the             */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define RAM_TEST_RESULTS_ADDRESS 0x20007FFC

/********************************************************************
* PUBLIC uiGetRamTestResults()
*--------------------------------------------------------------------
* Reads and returns the RAM test results from static RAM location.
* Test results are stored on the last 4 bytes of RAM (uint32 word).
*
* Inputs: None
* Returns:
*     uint32 : Number of failures during testing Read/Writes to RAM.
*
*********************************************************************/
uint32_t uiGetRamTestResults(void);

#endif /* TEST_RAM_H_ */