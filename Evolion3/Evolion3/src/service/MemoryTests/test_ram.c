/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: test_ram.c                                                     */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Ram Test is executed inside the Reset Handler function before any RAM     */
/* sections are loaded. It is executed there to prevent the test from        */
/* overwriting any pre-initialized data or ramfunctions.                     */
/* The RAM tests writes the following values to each RAM location and reads  */
/* them back for comparison, 0xAA, 0x55 and 0x00. Reads and writes 4 bytes   */ 
/* at a time. Increments a counter for each value that does not match.       */
/* The function in this file reads and returns the RAM test results which    */
/* are saved in a pre-determined fixed RAM location.                         */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <test_ram.h>


/********************************************************************
* PUBLIC uiGetRamTestResults()
*--------------------------------------------------------------------
* Reads and returns the RAM test results from static RAM location.
* Test results are stored on the last 4 bytes of RAM (uint32 word).
*
* Inputs: None
* Returns: 
*     Number of failures during testing Read/Writes to RAM.
*
*********************************************************************/
uint32_t uiGetRamTestResults(void)
{
    uint32_t *pRamTestResults;
    
    pRamTestResults = (uint32_t *)RAM_TEST_RESULTS_ADDRESS;
    
    return(*pRamTestResults);
}

/******************************** End of File ********************************/