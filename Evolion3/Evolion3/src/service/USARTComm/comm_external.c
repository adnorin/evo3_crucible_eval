/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: comm_external.c                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Nicolas Aumetre                                            */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* GPIO pins (buttons) read and debounce.                                    */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <usart.h>
#include <iface_usart_dma.h>
#include <comm_external.h>
#include <iface_rtos_start.h>
#include <conf_battery_settings.h>
#include <mbtypes.h>
/*************************/
/*   Private Functions   */
/*************************/
static eMBErrorCode eExtCommParameters(uint8_t ushExtCommSlaveIdIn, eExtCommsBaudRate eExtCommBaudRateIn,
                                eExtCommsStopBits eExtCommStopBitsIn, eExtCommsParity eExtCommParityIn,
                                uint8_t *ushExtCommSlaveIdOut, eUsartPortSpeed_t *ulExtCommBaudRateOut,
                                enum usart_stopbits *ushExtCommStopBitsOut, enum usart_parity *eExtCommParityOut);
/*********************************************************************
* PRIVATE svUsart2CommRx_CallBack()
*---------------------------------------------------------------------
* Callback for USART2 Rx
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
static void svUsart2CommRx_CallBack(void)
{
    SemaphoreHandle_t mySemHandle;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    /* Get Semaphore Handle */
    mySemHandle = sGetSemaphoreHandle(USART2_COMM);

    /* Post Semaphore */
    xSemaphoreGiveFromISR(mySemHandle, &xHigherPriorityTaskWoken);
}


/*********************************************************************
* PRIVATE svUsart2CommTx_CallBack()
*---------------------------------------------------------------------
* Callback for USART2 Tx
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
static void svUsart2CommTx_CallBack(void)
{
    ;
}


/*************************/
/*    Public Functions   */
/*************************/

/*********************************************************************
* PUBLIC vUsartExtCommInitialize()
*---------------------------------------------------------------------
* Initializes USART2 for external communications
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
void vUsartExtCommInitialize(void)
{
    uint8_t             __attribute__((unused)) ucLocalSlaveId;
    enum usart_stopbits      eLocalStopBits;
    eUsartPortSpeed_t   eLocalBaudRate;
    enum usart_parity        eLocalParity;

    /* conversion between flash memory values and expected configuration values */
    eExtCommParameters(const_ucParExternalCommSlaveId, const_ucParExternalCommBaudRate,
                        const_ucParExternalCommStopBits, const_ucParExternalCommParity,
                        &ucLocalSlaveId, &eLocalBaudRate,
                        &eLocalStopBits,  &eLocalParity);

    /* Configure USART Port */
    bConfigureUsartPort(USART_PORT_2, eLocalBaudRate, eLocalParity, eLocalStopBits);

    /* Register Callbacks for USART TX & RX */
    vSetUsartRxCallback(USART_PORT_2, svUsart2CommRx_CallBack);
    vSetUsartTxCallback(USART_PORT_2, svUsart2CommTx_CallBack);
}


/*********************************************************************
* PUBLIC vUsartExtCommRx_Parms()
*---------------------------------------------------------------------
* Initializes USART2 data buffer and size and starts Rx
*
* Inputs:
*     chRxUSART2Buffer - Input Data Buffer
*     ushDataLen       - Rx Data Len Expected/Buffer Size
* Returns:
*     None
*
**********************************************************************/
void vUsartExtCommRx_Parms(char * chRxUSART2Buffer, uint16_t ushDataLen)
{
    /* Setup USART2 RX Buffers */
    vSetupUsartRx(USART_PORT_2, chRxUSART2Buffer, ushDataLen);

    /* Start RX */
    vStartUsartRx(USART_PORT_2);
}


/*********************************************************************
* PUBLIC vUsartExtCommTx_Parms()
*---------------------------------------------------------------------
* Initializes USART2 data buffer and size and starts Tx
*
* Inputs:
*     chTxUSART2Buffer - Buffer Containing Output Data
*     ushDataLen       - Rx Data Len/Buffer Size
* Returns:
*     None
*
**********************************************************************/
void vUsartExtCommTx_Parms(char * chTxUSART2Buffer, uint16_t ushDataLen)
{
    /* Setup USART Port and TX & RX Buffers */
    vSetupUsartTx(USART_PORT_2, chTxUSART2Buffer, ushDataLen);

    /* Start TX */
    vStartUsartTx(USART_PORT_2);
}


/*********************************************************************
* PUBLIC vUsartExtCommRx()
*---------------------------------------------------------------------
* This starts the USART/DMA Receive. Requires input data buffer and
* length to be initialized prior to calling this function, by using
* vUsartExtCommRx_Parms() first. Then as long as the data buffer and
* length does not change this function can be used indefinitely.
*
* Inputs:
*     None
* Returns:
*   true  - Successfully Started and new USART transfer
*   false - USART was busy, previous transfer has not completed
*
**********************************************************************/
bool vUsartExtCommRx(void)
{
    bool bStatus;
    /* Start RX */
    bStatus = vStartUsartRx(USART_PORT_2);

    return bStatus;
}


/*********************************************************************
* PUBLIC vUsartExtCommTx()
*---------------------------------------------------------------------
* This starts the USART/DMA Transmit. Requires output data buffer and
* length to be initialized prior to calling this function, by using
* vUsartExtCommTx_Parms() first. Then as long as the data buffer and
* length does not change this function can be used indefinitely.
*
* Inputs:
*     None
* Returns:
*   true  - Successfully Started and new USART transfer
*   false - USART was busy, previous transfer has not completed
*
**********************************************************************/
bool vUsartExtCommTx(void)
{
    bool bStatus;
    /* Start TX */
    bStatus = vStartUsartTx(USART_PORT_2);

    return bStatus;
}

/*********************************************************************
* PRIVATE eMBErrorCode eExtCommParameters()
*---------------------------------------------------------------------
*
*
* Inputs:
*   ushExtCommSlaveIdIn : slave Id parameter from flash memory
*   eExtCommBaudRateIn : Baud rate parameter from flash memory
*   eExtCommStopBitsIn : Stop Bits parameter from flash memory
*   eExtCommParityIn : Parity parameter from flash memory
*
* Outputs:
*   ushExtCommSlaveIdOut : slave Id parameter used for Modbus communication
*   ulExtCommBaudRateOut : Baud rate parameter used for Modbus communication
*   ushExtCommStopBitsOut : Stop Bits parameter used for Modbus communication
*   eExtCommParityOut : Parity parameter used for Modbus communication
*
* Returns:
*   Error code :    MB_ENOERR : parameter conversion ok
*                   MB_EINVAL : SlaveId out of range
*
**********************************************************************/
static eMBErrorCode eExtCommParameters(uint8_t ushExtCommSlaveIdIn, eExtCommsBaudRate eExtCommBaudRateIn,
                                eExtCommsStopBits eExtCommStopBitsIn, eExtCommsParity eExtCommParityIn,
                                uint8_t *ushExtCommSlaveIdOut, eUsartPortSpeed_t *ulExtCommBaudRateOut,
                                enum usart_stopbits *ushExtCommStopBitsOut, enum usart_parity *eExtCommParityOut)
{
    eMBErrorCode eParamStatus = MB_ENOERR;

    if ( ( ushExtCommSlaveIdIn < 1 ) || ( ushExtCommSlaveIdIn > 247 ) )
    {
        *ushExtCommSlaveIdOut = 1; /* default behavior to be defined */
        eParamStatus = MB_EINVAL;
    }
    else
    {
        *ushExtCommSlaveIdOut = ushExtCommSlaveIdIn;
    }

    switch (eExtCommBaudRateIn)
    {
        case EXTCOMM_BAUD_RATE_9600:
        {
            *ulExtCommBaudRateOut = E_9600_BAUD;
            break;
        }
        case EXTCOMM_BAUD_RATE_19200:
        {
            *ulExtCommBaudRateOut = E_19200_BAUD;
            break;
        }
        case EXTCOMM_BAUD_RATE_57600:
        {
            *ulExtCommBaudRateOut = E_57600_BAUD;
            break;
        }
        case EXTCOMM_BAUD_RATE_115200:
        {
            *ulExtCommBaudRateOut = E_115200_BAUD;
            break;
        }
        default:
        {
            *ulExtCommBaudRateOut = E_115200_BAUD;/* default behavior to be defined */
            break;
        }
    }

    switch (eExtCommStopBitsIn)
    {
/*        case EXTCOMM_STOP_BITS_1:
        {
            *ushExtCommStopBitsOut = 1;
            break;
        }*/
        case EXTCOMM_STOP_BITS_2:
        {
            *eExtCommParityOut = USART_PARITY_NONE; /* SWRS_00320 */
            *ushExtCommStopBitsOut = USART_STOPBITS_2;
            break;
        }
        default: /* EXTCOMM_STOP_BITS_1 is the default value */
        {
            switch (eExtCommParityIn) /* SWRS_00320 */
            {
                case EXTCOMM_PARITY_ODD:
                {
                    *eExtCommParityOut = USART_PARITY_ODD;
                    break;
                }
                case EXTCOMM_PARITY_EVEN:
                {
                    *eExtCommParityOut = USART_PARITY_EVEN;
                    break;
                }
                default:
                {
                    *eExtCommParityOut = USART_PARITY_NONE;
                    break;
                }
            }

            *ushExtCommStopBitsOut = USART_STOPBITS_1; /* default behavior to be defined */
            break;
        }
    }
    return eParamStatus;
}

/******************************** End of File ********************************/