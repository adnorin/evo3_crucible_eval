/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: comm_internal.c                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: N/A                                                        */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* GPIO pins (buttons) read and debounce.                                    */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <usart.h>
#include <iface_usart_dma.h>
#include <comm_internal.h>
#include <iface_rtos_start.h>

/*************************/
/*    Private Functions   */
/*************************/

/*********************************************************************
* PRIVATE vUsart1CommRx_CallBack()
*---------------------------------------------------------------------
* Callback for USART1 Rx
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
static void svUsart1CommRx_CallBack(void)
{
    SemaphoreHandle_t mySemHandle;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    
    /* Get Semaphore Handle */
    mySemHandle = sGetSemaphoreHandle(USART1_COMM);
    
    /* Post Semaphore */
    xSemaphoreGiveFromISR(mySemHandle, xHigherPriorityTaskWoken);
}

/*********************************************************************
* PRIVATE vUsart1CommTx_CallBack()
*---------------------------------------------------------------------
* Callback for USART1 Tx
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
static void svUsart1CommTx_CallBack(void)
{
    ;
}

/*************************/
/*    Public Functions   */
/*************************/

/*********************************************************************
* PUBLIC vUsartIntCommInitialize()
*---------------------------------------------------------------------
* Initializes USART1 for internal communications
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommInitialize(void)
{
    /* Configure USART Port */
    bConfigureUsartPort(USART_PORT_1, E_115200_BAUD, USART_PARITY_NONE, USART_STOPBITS_1);
    
    /* Register Callbacks for USART TX & RX */
    vSetUsartRxCallback(USART_PORT_1, svUsart1CommRx_CallBack);
    vSetUsartTxCallback(USART_PORT_1, svUsart1CommTx_CallBack);
    
}

/*********************************************************************
* PUBLIC vUsartIntCommRx_Parms()
*---------------------------------------------------------------------
* Initializes USART1 data buffer and size and starts Rx
*
* Inputs:
*     chRxUSART1Buffer - Input Data Buffer
*     ushDataLen       - Rx Data Len Expected/Buffer Size 
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommRx_Parms(char * chRxUSART1Buffer, uint16_t ushDataLen)
{
    /* Setup USART1 RX Buffers */
    vSetupUsartRx(USART_PORT_1, chRxUSART1Buffer, ushDataLen);
    
    /* Start RX */
    vStartUsartRx(USART_PORT_1);
}


/*********************************************************************
* PUBLIC vUsartIntCommTx_Parms()
*---------------------------------------------------------------------
* Initializes USART1 data buffer and size and starts Tx
*
* Inputs:
*     chTxUSART1Buffer - Buffer Containing Output Data 
*     ushDataLen       - Tx Data Len/Buffer Size
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommTx_Parms(char * chTxUSART1Buffer, uint16_t ushDataLen)
{
    /* Setup USART1 TX Buffers */
    vSetupUsartTx(USART_PORT_1, chTxUSART1Buffer, ushDataLen);
    
    /* Start TX */
    vStartUsartTx(USART_PORT_1);
}


/*********************************************************************
* PUBLIC vUsartIntCommRx()
*---------------------------------------------------------------------
* This starts the USART/DMA Receive. Requires input data buffer and
* length to be initialized prior to calling this function, by using
* vUsartIntCommRx_Parms() first. Then as long as the data buffer and
* length does not change this function can be used indefinitely.
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommRx(void)
{
    /* Start RX */
    vStartUsartRx(USART_PORT_1);
}


/*********************************************************************
* PUBLIC vUsartIntCommTx()
*---------------------------------------------------------------------
* This starts the USART/DMA Transmit. Requires output data buffer and
* length to be initialized prior to calling this function, by using
* vUsartIntCommTx_Parms() first. Then as long as the data buffer and
* length does not change this function can be used indefinitely.
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommTx(void)
{
    /* Start TX */
    vStartUsartTx(USART_PORT_1);
}


/******************************** End of File ********************************/