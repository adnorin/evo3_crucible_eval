#ifndef COMM_EXTERNAL_H_
#define COMM_EXTERNAL_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: comm_external.h                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for comm_external.c, which provides prototypes for            */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#include <usart.h>
#include <iface_usart_dma.h>
#include <conf_battery_settings.h>
#include <mbtypes.h>

/*********************************************************************
* PUBLIC vUsartExtCommInitialize()
*---------------------------------------------------------------------
* Initializes USART2 for external communications
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
void vUsartExtCommInitialize(void);


/*********************************************************************
* PUBLIC vUsartExtCommRx_Parms()
*---------------------------------------------------------------------
* Initializes USART2 data buffer and size and starts Rx
*
* Inputs:
*     chRxUSART2Buffer - Input Data Buffer
*     ushDataLen       - Rx Data Len Expected/Buffer Size
* Returns:
*     None
*
**********************************************************************/
void vUsartExtCommRx_Parms(char * chRxUSART2Buffer, uint16_t ushDataLen);


/*********************************************************************
* PUBLIC vUsartExtCommTx_Parms()
*---------------------------------------------------------------------
* Initializes USART2 data buffer and size and starts Tx
*
* Inputs:
*     chTxUSART2Buffer - Buffer Containing Output Data
*     ushDataLen       - Rx Data Len Expected/Buffer Size
* Returns:
*     None
*
**********************************************************************/
void vUsartExtCommTx_Parms(char * chTxUSART2Buffer, uint16_t ushDataLen);


/*********************************************************************
* PUBLIC vUsartExtCommRx()
*---------------------------------------------------------------------
* This starts the USART/DMA Receive. Requires input data buffer and
* length to be initialized prior to calling this function, by using
* vUsartExtCommRx_Parms() first. Then as long as the data buffer and
* length does not change this function can be used indefinitely.
*
* Inputs:
*     None
* Returns:
*   true  - Successfully Started and new USART transfer
*   false - USART was busy, previous transfer has not completed
*
**********************************************************************/
bool vUsartExtCommRx(void);


/*********************************************************************
* PUBLIC vUsartExtCommTx()
*---------------------------------------------------------------------
* This starts the USART/DMA Transmit. Requires output data buffer and
* length to be initialized prior to calling this function, by using
* vUsartExtCommTx_Parms() first. Then as long as the data buffer and
* length does not change this function can be used indefinitely.
*
* Inputs:
*     None
* Returns:
*   true  - Successfully Started and new USART transfer
*   false - USART was busy, previous transfer has not completed
*
**********************************************************************/
bool vUsartExtCommTx(void);

#endif /* COMM_EXTERNAL_H_ */