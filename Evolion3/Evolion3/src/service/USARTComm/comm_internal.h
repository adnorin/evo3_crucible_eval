#ifndef COMM_INTERNAL_H_
#define COMM_INTERNAL_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: comm_internal.h                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for comm_internal.c, which provides prototypes for            */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

/*********************************************************************
* PUBLIC vUsartIntCommInitialize()
*---------------------------------------------------------------------
* Initializes USART1 for internal communications
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommInitialize(void);


/*********************************************************************
* PUBLIC vUsartIntCommRx_Parms()
*---------------------------------------------------------------------
* Initializes USART1 data buffer and size and starts Rx
*
* Inputs:
*     chRxUSART1Buffer - Input Data Buffer
*     ushDataLen       - Rx Data Len Expected/Buffer Size
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommRx_Parms(char * chRxUSART1Buffer, uint16_t ushDataLen);


/*********************************************************************
* PUBLIC vUsartIntCommTx_Parms()
*---------------------------------------------------------------------
* Initializes USART1 data buffer and size and starts Tx
*
* Inputs:
*     chTxUSART1Buffer - Buffer Containing Output Data
*     ushDataLen       - Tx Data Len/Buffer Size
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommTx_Parms(char * chTxUSART1Buffer, uint16_t ushDataLen);


/*********************************************************************
* PUBLIC vUsartIntCommRx()
*---------------------------------------------------------------------
* This starts the USART/DMA Receive. Requires input data buffer and
* length to be initialized prior to calling this function, by using
* vUsartIntCommRx_Parms() first. Then as long as the data buffer and
* length does not change this function can be used indefinitely.
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommRx(void);


/*********************************************************************
* PUBLIC vUsartIntCommTx()
*---------------------------------------------------------------------
* This starts the USART/DMA Transmit. Requires output data buffer and
* length to be initialized prior to calling this function, by using
* vUsartIntCommTx_Parms() first. Then as long as the data buffer and
* length does not change this function can be used indefinitely.
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
void vUsartIntCommTx(void);

#endif /* COMM_INTERNAL_H_*/

