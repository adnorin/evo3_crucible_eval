#ifndef HEATER_H_
#define HEATER_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: heater.h                                                       */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for heater.c, which provides prototypes for                   */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/


/*********************************************************************
* PUBLIC vManageHeater()
*---------------------------------------------------------------------
* Heater management routine
*
* Inputs:  None
* Returns:
*     Retrieve data success  = True or False
*
**********************************************************************/
bool vManageHeater(void);


/*********************************************************************
* PUBLIC vSetHeaterONOFF()
*---------------------------------------------------------------------
* Queues the Heater On or Off request.
*
* Inputs:
*     ucHeaterOnOff - bit array, 1-ON, 0-OFF
* Returns: None
*
**********************************************************************/
void vSetHeaterONOFF(uint8_t ucHeaterOnOff);



#endif /* HEATER_H_ */