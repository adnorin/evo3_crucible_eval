/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: gpio_outputs.c                                                 */
/*****************************************************************************/
/* AUTHOR : Mayank Raj                                                       */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  Reads GPIO input status for diagnostics.                                 */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <gpio_controller.h>
#include <conf_gpio.h>
#include <gpio_outputs.h>


static uint16_t ushSavedGPIOStatus;

/*********************************************************************
* Private vSaveGPIOStatus()
*---------------------------------------------------------------------
* Saves the current GPIO status
*
* Inputs:  ushGPIOStatus - Current GPIO status
* Returns: None
*
**********************************************************************/
static void vSaveGPIOStatus(uint16_t ushGPIOStatus)
{
	ushSavedGPIOStatus = ushGPIOStatus;
}

/*********************************************************************
* PUBLIC vManageGPIOs()
*---------------------------------------------------------------------
* GPIO management routine
*
* Inputs:  None
* Returns:
*     Retrieve data success  = True or False
*
**********************************************************************/
bool bManageGPIOs (void)
{
	BaseType_t sStatus;
	static QueueHandle_t spLocalQueue_GPIOs_Hdl1;
	static uint16_t ushGPIOsStatus;
	
	/* Retrieve data from queue */
	spLocalQueue_GPIOs_Hdl1 = sGetQueueHandle(GPIO_IN_QUEUE);
	sStatus = xQueueReceive(spLocalQueue_GPIOs_Hdl1 , &ushGPIOsStatus, NOWAIT);
	if( sStatus == pdPASS )
	{
		vSetGPIOState(ushGPIOsStatus);
		vSaveGPIOStatus(ushGPIOsStatus);
	}
	
	return(sStatus);
}

/*********************************************************************
* PUBLIC vSetGPIOsONOFF()
*---------------------------------------------------------------------
* Queues the GPIO States (On or Off) request.
*
* Inputs:
*     ushGPIOsOnOff - bit array, 1-ON, 0-OFF
* Returns: None
*
**********************************************************************/
void vSetGPIOsONOFF(uint16_t ushGPIOsOnOff)
{
	static uint16_t sushMyGPIOsOnOff;
	static QueueHandle_t spLocalQueue_GPIOs_Hdl2;
	
	sushMyGPIOsOnOff = ushGPIOsOnOff;
	
	/* QUEUE Update to GPIOs */
	spLocalQueue_GPIOs_Hdl2 = sGetQueueHandle(GPIO_IN_QUEUE);
	xQueueOverwrite( spLocalQueue_GPIOs_Hdl2, &sushMyGPIOsOnOff);
}

/*********************************************************************
* PUBLIC vGetGPIOStatus()
*---------------------------------------------------------------------
* Returns the current GPIO status
*
* Inputs:  None
* Returns:
*     uint16_t GPIO status bit array
*
**********************************************************************/
uint16_t ushGetGPIOStatus(void)
{
	return (ushSavedGPIOStatus);
}



/******************************** End of File ********************************/
