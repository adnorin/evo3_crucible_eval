/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: fets.c                                                         */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  Controls GPIO to turn Heater On/Off                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <conf_gpio.h>
#include <fets.h>
#include <fet_controller.h>

static uint16_t ushSavedFetStatus;

/*********************************************************************
* Private vSaveFetStatus()
*---------------------------------------------------------------------
* Saves the current FET status
*
* Inputs:  ushFetStatus - Current fet status
* Returns: None
*
**********************************************************************/
static void vSaveFetStatus(uint16_t ushFetStatus)
{
    ushSavedFetStatus = ushFetStatus;
}


/*********************************************************************
* PUBLIC vManageFets()
*---------------------------------------------------------------------
* FET management routine
*
* Inputs:  None
* Returns:
*     Retrieve data success  = True or False
*
**********************************************************************/
bool vManageFets(void)
{
    BaseType_t sStatus;
    static QueueHandle_t spLocalQueue_FETs_Hdl1;
    static uint16_t ushFETsStatus;
    
    /* Retrieve data from queue */
    spLocalQueue_FETs_Hdl1 = sGetQueueHandle(FET_STATE_QUEUE);
    sStatus = xQueueReceive(spLocalQueue_FETs_Hdl1 , &ushFETsStatus, NOWAIT);
    if( sStatus == pdPASS )
    {
        vSetFetState(ushFETsStatus);
        vSaveFetStatus(ushFETsStatus);
    }
    
    return(sStatus);
}


/*********************************************************************
* PUBLIC vSetFetsONOFF()
*---------------------------------------------------------------------
* Queues the FET States (On or Off) request.
*
* Inputs:
*     ushFetsOnOff - bit array, 1-ON, 0-OFF
* Returns: None
*
**********************************************************************/
void vSetFetsONOFF(uint16_t ushFetsOnOff)
{
    static uint16_t sushMyFetsOnOff;
    static QueueHandle_t spLocalQueue_FETs_Hdl2;
    
    sushMyFetsOnOff = ushFetsOnOff;
    
    /* QUEUE Update to FETs */
    spLocalQueue_FETs_Hdl2 = sGetQueueHandle(FET_STATE_QUEUE);
    xQueueOverwrite( spLocalQueue_FETs_Hdl2, &sushMyFetsOnOff);
}


/*********************************************************************
* PUBLIC vGetFetStatus()
*---------------------------------------------------------------------
* Returns the current FET status
*
* Inputs:  None
* Returns:
*     uint16_t Fet status bit array
*
**********************************************************************/
uint16_t vGetFetStatus(void)
{
    return (ushSavedFetStatus);
}

/******************************** End of File ********************************/