/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: dry_contacts.c                                                 */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  Controls GPIO to turn Heater On/Off                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <conf_gpio.h>
#include <heater_controller.h>
#include <dry_contacts.h>
#include <dry_contacts_controller.h>

/*********************************************************************
* PUBLIC vManageDryContacts()
*---------------------------------------------------------------------
* Dry contacts management routine
*
* Inputs:  None
* Returns:
*     Retrieve data success  = True or False
*
**********************************************************************/
bool vManageDryContacts(void)
{
    BaseType_t sStatus;
    static QueueHandle_t spLocalQueue_DC_Hdl1;
    static uint8_t ucDCStatus;
    
    /* Retrieve data from queue */
    spLocalQueue_DC_Hdl1 =  sGetQueueHandle(DRY_CONTACTS_QUEUE);
    sStatus = xQueueReceive(spLocalQueue_DC_Hdl1 , &ucDCStatus, NOWAIT);
    if( sStatus == pdPASS )
    {
        vTurnDryContactsOnOFF(ucDCStatus);
    }
    
    return(sStatus);
}


/*********************************************************************
* PUBLIC vSetDCsONOFF()
*---------------------------------------------------------------------
* Queues the Heater On or Off request.
*
* Inputs:
*     ucSetDCsONOFF - bit array, 1-ON, 0-OFF (TB and RJ45)
* Returns: None
*
**********************************************************************/
void vSetDCsONOFF(uint8_t ucSetDCsONOFF)
{
    static uint8_t sucMySetDCsONOFF;
    static QueueHandle_t spLocalQueue_DC_Hdl2;
    
    sucMySetDCsONOFF = ucSetDCsONOFF;
    
    /* QUEUE Update to LEDs */
    spLocalQueue_DC_Hdl2 =  sGetQueueHandle(DRY_CONTACTS_QUEUE);
    xQueueOverwrite( spLocalQueue_DC_Hdl2, &sucMySetDCsONOFF);
}

/******************************** End of File ********************************/