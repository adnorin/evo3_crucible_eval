#ifndef INCFILE1_H_
#define INCFILE1_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: dry_contacts.h                                                 */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for dry_contacts.c, which provides prototypes for             */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/


/*********************************************************************
* PUBLIC vManageDryContacts()
*---------------------------------------------------------------------
* Dry contacts management routine
*
* Inputs:  None
* Returns:
*     Retrieve data success  = True or False
*
**********************************************************************/
bool vManageDryContacts(void);


/*********************************************************************
* PUBLIC vSetDCsONOFF()
*---------------------------------------------------------------------
* Queues the Heater On or Off request.
*
* Inputs:
*     ucSetDCsONOFF - bit array, 1-ON, 0-OFF (TB and RJ45)
* Returns: None
*
**********************************************************************/
void vSetDCsONOFF(uint8_t ucSetDCsONOFF);



#endif /* INCFILE1_H_ */