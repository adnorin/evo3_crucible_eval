#ifndef GPIO_INPUTS_H_
#define GPIO_INPUTS_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: gpio_inputs.h                                                  */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for gpio_inputs.c, which provides prototypes for              */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define GPIO_FUSE_BLOWN_MASK    0x01
#define GPIO_NETWORK_VOLT_MASK  0x02
#define GPIO_BMS_FAULT_MASK     0x04
#define GPIO_OVER_CURRENT_MASK  0x08
#define GPIO_OVER_TEMP_MASK     0x10

/*********************************************************************
* PUBLIC ucReadInputGPIOStatus()
*---------------------------------------------------------------------
* Read Input GPIO and queue their status for diagnostics
*
* Inputs:
*     stMyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void ucReadInputGPIOStatus(stBmsData_t* stMyBMSData);

#endif /* GPIO_INPUTS_H_ */