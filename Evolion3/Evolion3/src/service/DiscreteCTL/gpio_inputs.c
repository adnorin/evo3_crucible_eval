/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: gpio_inputs.c                                              */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  Reads GPIO input status for diagnostics.                                 */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <conf_gpio.h>
#include <conf_extint_custom.h>
#include <gpio_inputs.h>

/*********************************************************************
* PUBLIC ucReadInputGPIOStatus()
*---------------------------------------------------------------------
* Read Input GPIO and queue their status for diagnostics
*
* Inputs:
*     stMyBMSData - Current data for BMS
* Returns: None
*
**********************************************************************/
void ucReadInputGPIOStatus(stBmsData_t* stMyBMSData)
{
    uint8_t ucGPIOStatus;

    /* Init variable */
    ucGPIOStatus = 0;

    /* Read Fuse Blown pin state */
    ucGPIOStatus |= port_pin_get_input_level(NETWORK_MEASUREMENT_ENABLE_PIN)?GPIO_FUSE_BLOWN_MASK:0x00;

    /* Read Network Voltage Detected pin state */
    ucGPIOStatus |= port_pin_get_input_level(NETWORK_WAKEUP_NOTIFICATION_PIN)?GPIO_NETWORK_VOLT_MASK:0x00;

    /* Read BMS Fault pin state */
    ucGPIOStatus |= port_pin_get_input_level(BMS_IC_FAULT_NOTIFICATION_PIN)?GPIO_BMS_FAULT_MASK:0x00;

    /* Read Over Current Charge pin state */
    ucGPIOStatus |= port_pin_get_input_level(OVER_CURRENT_192A_NOTIFICATION_PIN)?GPIO_OVER_CURRENT_MASK:0x00;

    /* Read Over Temp pin state */
    ucGPIOStatus |= port_pin_get_input_level(OVER_TEMPERATURE_NOTIFICATION_PIN)?GPIO_OVER_TEMP_MASK:0x00;

    stMyBMSData->ucGPIOStatus = ucGPIOStatus;

}

/******************************** End of File ********************************/
