#ifndef FETS_H_
#define FETS_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: fet.h                                                          */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for fet.c, which provides prototypes for                      */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/


/*********************************************************************
* PUBLIC vManageFets()
*---------------------------------------------------------------------
* FET management routine
*
* Inputs:  None
* Returns:
*     Retrieve data success  = True or False
*
**********************************************************************/
bool vManageFets(void);


/*********************************************************************
* PUBLIC vSetFetsONOFF()
*---------------------------------------------------------------------
* Queues the FET States (On or Off) request.
*
* Inputs:
*     ushFetsOnOff - bit array, 1-ON, 0-OFF
* Returns: None
*
**********************************************************************/
void vSetFetsONOFF(uint16_t ushFetsOnOff);


/*********************************************************************
* PUBLIC vGetFetStatus()
*---------------------------------------------------------------------
* Returns the current FET status
*
* Inputs:  None
* Returns:
*     uint16_t Fet status bit array
*
**********************************************************************/
uint16_t vGetFetStatus(void);


#endif /* FETS_H_ */