/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: heater_controller.c                                            */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  Controls GPIO to turn Heater On/Off                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <iface_rtos_start.h>
#include <conf_gpio.h>
#include <heater_controller.h>
#include <heater.h>

/*********************************************************************
* PUBLIC vManageHeater()
*---------------------------------------------------------------------
* Heater management routine
*
* Inputs:  None
* Returns: 
*     Retrieve data success  = True or False
*
**********************************************************************/
bool vManageHeater(void)
{
    BaseType_t sStatus;
    static QueueHandle_t spLocalQueue_Heater_Hdl1;
    static uint8_t ucHeaterStatus;
    
    /* Retrieve data from queue */
    spLocalQueue_Heater_Hdl1 =  sGetQueueHandle(HEATER_CTL_QUEUE);
    sStatus = xQueueReceive(spLocalQueue_Heater_Hdl1 , &ucHeaterStatus, NOWAIT);
    if( sStatus == pdPASS )
    {
        vTurnHeaterOnOFF(ucHeaterStatus);
    }   
    
    return(sStatus);
}


/*********************************************************************
* PUBLIC vSetHeaterONOFF()
*---------------------------------------------------------------------
* Queues the Heater On or Off request.
*
* Inputs:
*     ucHeaterOnOff - bit array, 1-ON, 0-OFF
* Returns: None
*
**********************************************************************/
void vSetHeaterONOFF(uint8_t ucHeaterOnOff)
{
    static uint8_t sucMyHeaterOnOff;
    static QueueHandle_t spLocalQueue_Heater_Hdl2;
    
    sucMyHeaterOnOff = ucHeaterOnOff;
    
    /* QUEUE Update to Heater */
    spLocalQueue_Heater_Hdl2 =  sGetQueueHandle(HEATER_CTL_QUEUE);
    xQueueOverwrite( spLocalQueue_Heater_Hdl2, &sucMyHeaterOnOff);
}

/******************************** End of File ********************************/