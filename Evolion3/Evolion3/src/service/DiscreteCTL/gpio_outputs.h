#ifndef GPIO_OUTPUTS_H_
#define GPIO_OUTPUTS_H_

/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: gpio_outputs.h                                                 */
/*****************************************************************************/
/* AUTHOR : Mayank Raj                                                       */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for gpio_inputs.c, which provides prototypes for              */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/


/*********************************************************************
* PUBLIC bManageGPIOs()
*---------------------------------------------------------------------
* GPIO management routine
*
* Inputs:  None
* Returns:
*     Retrieve data success  = True or False
*
**********************************************************************/
bool bManageGPIOs(void);

/*********************************************************************
* PUBLIC vSetGPIOsONOFF()
*---------------------------------------------------------------------
* Queues the GPIO States (On or Off) request.
*
* Inputs:
*     ushGPIOsOnOff - bit array, 1-ON, 0-OFF
* Returns: None
*
**********************************************************************/
void vSetGPIOsONOFF(uint16_t ushGPIOsOnOff);

/*********************************************************************
* PUBLIC ushGetGPIOStatus()
*---------------------------------------------------------------------
* Returns the current GPIO status
*
* Inputs:  None
* Returns:
*     uint16_t GPIO status bit array
*
**********************************************************************/
uint16_t ushGetGPIOStatus(void);

#endif /* GPIO_OUTPUTS_H_ */