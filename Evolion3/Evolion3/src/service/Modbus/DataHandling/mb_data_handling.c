/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: mb_data_handling.c                                             */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <limits.h>
#include <iface_rtos_start.h>
#include <conf_usb.h>
#include <conf_gpio.h>
#include <bat_cell_controller_ic.h>
#include <iface_usart_dma.h>
#include <iface_i2c_dma.h>
#include <iface_spi_dma.h>
#include <hmi_button_read.h>
#include <hmi_controller.h>
#include <mbs.h>
#include <fet_controller.h>
#include <mbportlayer.h>
#include <mbutils.h>
#include <mbport.h>
#include <comm_external.h>
#include <comm_internal.h>
#include <current_meas.h>
#include <gpio_outputs.h>
#include <volt_meas.h>
#include <self_tests.h>
#include <cell_balancing.h>
#include <fets.h>
#include <heater.h>
#include <dry_contacts.h>
#include <gpio_inputs.h>
#include <gpio_controller.h>
#include <eeprom_controller.h>
#include <temp_meas.h>
#include <iface_rtos_start.h>
#include <conf_battery_settings.h>
#include <mb_data_handling.h>
#include <FreeRTOS.h>
#include "mbsiframe.h"
#include "mbsi.h"

//extern UBaseType_t uxHighWaterMarkMB;

static uint16_t   ushModernRegHoldingValue[234] = {[0 ... 233] = 0};
static uint16_t   ushTestModeRegHoldingValue[73] = {[0 ... 68] = 0};
static uint16_t   ushLegacyRegHoldingValue[16] = {[0 ... 15] = 0};
static eMBException eMyRegHoldingCB( UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs, eMBSRegisterMode eRegMode );
static eMBErrorCode eDataHdlExtCommParameters(uint8_t ushExtCommSlaveIdIn, eExtCommsBaudRate eExtCommBaudRateIn,
                                                eExtCommsStopBits eExtCommStopBitsIn, eExtCommsParity eExtCommParityIn,
                                                uint8_t *ushExtCommSlaveIdOut, uint32_t *ulExtCommBaudRateOut,
                                                uint8_t *ushExtCommStopBitsOut, eMBSerialParity *eExtCommParityOut);

/*********************************************************************
* PUBLIC void vModbusTask()
*---------------------------------------------------------------------
*  Initialize and execute Modbus routines
*
* Inputs: None
*
* Returns: None
*
*
**********************************************************************/
void vModbusTask(void)
{
    static xMBSHandle   xMBSHdlUSB, __attribute__((unused)) xMBSHdlExt;
    xMBSInternalHandle *xMBSLocInternalHdl;
    static eMBErrorCode eStatusUSB, eStatusExt, __attribute__((unused)) eStatusConv;
    stBmsData_t         sstBmsIcDataLocal;
    eBatteryOperatingMode_t eMyBatteryOperatingMode;
    BaseType_t          sStatus;
    static sSelfTestResults_t* spSelfTestResultsLocal;
    static uint32_t            siSelfTestPassBits;
    static stUsartData_t       stRxUSARTBufferLocal;
    static uint16_t            sushSelfTestExecutePrevious;
    static TickType_t          xLastWakeTime;
    static QueueHandle_t       spLocalDiag_Data_Hdl = NULL;
    static QueueHandle_t       spLocalUSART1_Data_Hdl = NULL;
    static QueueHandle_t       spLocalUSART2_Data_Hdl = NULL;
    static QueueHandle_t       spLocalTest_Mode_Request_Hdl = NULL;
    static uint8_t             sushPreviousWakeByte;
    uint8_t             sushNewWakeByte;
    uint32_t            uiLegacySumOfCellsVoltage;
    uint32_t            uiLegacyCurrent;
    uint32_t            uiLegacyIMD;
    uint32_t            uiLegacyIMR;
    uint8_t             ushTempChargeMode = 0; /* Temporary, to be removed once legacy state mapping is completed */
    uint8_t             ushTempBatteryeMode = 3; /* Temporary, to be removed once legacy state mapping is completed */
    uint8_t             __attribute__((unused)) ucLocalSlaveId;
    uint8_t             __attribute__((unused)) ucLocalStopBits;
    uint32_t            __attribute__((unused)) ulLocalBaudRate;
    eMBSerialParity     __attribute__((unused)) eLocalParity;
    uint16_t            ushReg;
    QueueHandle_t       spQueue_SelfTests_Hdl;
    uint32_t            uiSelfTestFlags;
    if (spLocalDiag_Data_Hdl == NULL)
    {
        spLocalDiag_Data_Hdl = sGetQueueHandle(DIAG_DATA_QUEUE);
    }
    if (spLocalUSART1_Data_Hdl == NULL)
    {
        spLocalUSART1_Data_Hdl = sGetQueueHandle(USART1_DATA_QUEUE);
    }
    if (spLocalUSART2_Data_Hdl == NULL)
    {
        spLocalUSART2_Data_Hdl = sGetQueueHandle(USART2_DATA_QUEUE);
    }
    if (spLocalTest_Mode_Request_Hdl == NULL)
    {
        spLocalTest_Mode_Request_Hdl = sGetQueueHandle(TEST_MODE_REQUEST_QUEUE);
    }
    /* initialize and execute serial handler task for USB diagnostic port */
    eStatusUSB = eMBSSerialInit( &xMBSHdlUSB, MB_RTU, MBS_SERIAL_ADDRESS, MBS_SERIAL_PORT, MBS_SERIAL_BAUDRATE, MBS_SERIAL_PARITY );

    /*eStatusConv = eDataHdlExtCommParameters(const_ucParExternalCommSlaveId, const_ucParExternalCommBaudRate,
                        const_ucParExternalCommStopBits, const_ucParExternalCommParity,
                        &ucLocalSlaveId, &ulLocalBaudRate,
                        &ucLocalStopBits,  &eLocalParity);  */

    /* initialize and execute serial handler task for external communication port USART2 */
    /*eStatusExt = eMBSSerialInit( &xMBSHdlExt, MB_RTU, ucLocalSlaveId, USART_PORT_2,
                                    ulLocalBaudRate, eLocalParity );   */

    /* Testing Only!!! */
    eStatusExt = MB_ENOERR;

    if( MB_ENOERR != (eMBErrorCode)( eStatusUSB | eStatusExt ) )
    {
    }
    else
    {
        eStatusUSB = eMBSRegisterHoldingCB( xMBSHdlUSB, eMyRegHoldingCB );
        //eStatusExt = eMBSRegisterHoldingCB( xMBSHdlExt, eMyRegHoldingCB );
        if( MB_ENOERR != (eMBErrorCode)( eStatusUSB | eStatusExt ) )
        {
        }
        else
        {
            do
            {
                /* Poll the communication stack for both USB and External USART2 . */
                eStatusUSB = eMBSPoll( xMBSHdlUSB );
                //eStatusExt = eMBSPoll( xMBSHdlExt );
                if( MB_ENOERR != (eMBErrorCode)( eStatusUSB | eStatusExt ) )
                {
                }
                /* Update holding registers */
                /* Get data from NXP MC33771 */
                if (pdPASS == xQueueReceive(spLocalDiag_Data_Hdl,&sstBmsIcDataLocal,0))
                {
                    if (xMBSHdlUSB != NULL)
                    {
                        xMBSLocInternalHdl = (xMBSInternalHandle *)&xMBSHdlUSB;
                    }
                    /********************************************/
                    /************ Modern interface **************/
                    /********************************************/
                    /* SunS identifier */
                    ushModernRegHoldingValue[MB_MODERN_REG_001] = 0x5375;
                    ushModernRegHoldingValue[MB_MODERN_REG_002] = 0x6e53;
                    /* Manufacturer : "SAFT" */
                    for (ushReg = (uint16_t) MB_MODERN_REG_003 ; ushReg < (uint16_t) MB_MODERN_REG_017 ; ushReg++)
                    {
                        ushModernRegHoldingValue[ushReg] = 0;
                    }
                    ushModernRegHoldingValue[MB_MODERN_REG_017] = 0x5341;
                    ushModernRegHoldingValue[MB_MODERN_REG_018] = 0x4654;
                    /* Model : "EVO3" */
                    for (ushReg = (uint16_t) MB_MODERN_REG_019 ; ushReg < (uint16_t) MB_MODERN_REG_033 ; ushReg++)
                    {
                        ushModernRegHoldingValue[ushReg] = 0;
                    }
                    ushModernRegHoldingValue[MB_MODERN_REG_033] = 0x4556;
                    ushModernRegHoldingValue[MB_MODERN_REG_034] = 0x4F33;
                    /* Options */
                    for (ushReg = (uint16_t) MB_MODERN_REG_035 ; ushReg < (uint16_t) MB_MODERN_REG_043 ; ushReg++)
                    {
                        ushModernRegHoldingValue[ushReg] = 0;
                    }
                    /* Version */
                    for (ushReg = (uint16_t) MB_MODERN_REG_043 ; ushReg < (uint16_t) MB_MODERN_REG_051 ; ushReg++)
                    {
                        ushModernRegHoldingValue[ushReg] = 0;
                    }
                    /* Serial number : TBD */
                    for (ushReg = (uint16_t) MB_MODERN_REG_051 ; ushReg < (uint16_t) MB_MODERN_REG_067 ; ushReg++)
                    {
                        ushModernRegHoldingValue[ushReg] = 0;
                    }
                    /* Device address */
                    ushModernRegHoldingValue[MB_MODERN_REG_067] = xMBSLocInternalHdl->ubSlaveAddress;
                    /* pad */
                    ushModernRegHoldingValue[MB_MODERN_REG_068] = (uint16_t)DEFAULT_VALUE_PAD;
                    /* ID - Battery Base Model ID */
                    ushModernRegHoldingValue[MB_MODERN_REG_069] = 802;
                    /* L - Model Length */
                    ushModernRegHoldingValue[MB_MODERN_REG_070] = 62;
                    /* AHRtg - Nameplate Charge Capacity (Ah) */
                    ushModernRegHoldingValue[MB_MODERN_REG_071] = 74;
                    /* WHRtg - Nameplate Energy Capacity (Wh) */ /* SRS_00040 : 3,9kWh */
                    ushModernRegHoldingValue[MB_MODERN_REG_072] = 3900;
                    /* WChaRteMax - Nameplate Max Charge Rate (W) */
                    ushModernRegHoldingValue[MB_MODERN_REG_073] = 1950;
                    /* WDisChaRteMax - Nameplate Max Discharge Rate (W) */
                    ushModernRegHoldingValue[MB_MODERN_REG_074] = 1950;
                    /* DisChaRte - Self Discharge Rate (%WHRtg) */
                    ushModernRegHoldingValue[MB_MODERN_REG_075] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* SocMax - Nameplate Max SoC (%) */
                    ushModernRegHoldingValue[MB_MODERN_REG_076] = const_ushCfgBatteryOverSOCAlarmThreshold;
                    /* SocMin - Nameplate Min SoC (%) */
                    ushModernRegHoldingValue[MB_MODERN_REG_077] = const_ushCfgBatteryUnderSOCAlarmThreshold;
                    /* SocRsvMax - Max Reserve SoC (%) */
                    ushModernRegHoldingValue[MB_MODERN_REG_078] = const_ushCfgBatteryOverSOCWarningThreshold;
                    /* SocRsvMin - Min Reserve SoC (%) */
                    ushModernRegHoldingValue[MB_MODERN_REG_079] = const_ushCfgBatteryUnderSOCWarningThreshold;
                    /* SoC - State of Charge (%) */
                    ushModernRegHoldingValue[MB_MODERN_REG_080] = sstBmsIcDataLocal.ushSOC;
                    /* Depth of Discharge*/
                    /* SoC Testing Only */
                    ushModernRegHoldingValue[MB_MODERN_REG_081] = sstBmsIcDataLocal.ushSOCMin;
                    /* State Of Health */
                    /* SoC Testing Only */
                    ushModernRegHoldingValue[MB_MODERN_REG_082] = sstBmsIcDataLocal.ushSOCAvg;
                    /* Cycle Count */
                    /* SoC Testing Only */
                    ushModernRegHoldingValue[MB_MODERN_REG_083] = sstBmsIcDataLocal.ushSOCMax;
                    ushModernRegHoldingValue[MB_MODERN_REG_084] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* Charge Status */
                    ushModernRegHoldingValue[MB_MODERN_REG_085] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* Control Mode */
                    ushModernRegHoldingValue[MB_MODERN_REG_086] = (uint16_t)DEFAULT_VALUE_TBD;

                    /* Hb - Battery Heartbeat */
                    ushModernRegHoldingValue[MB_MODERN_REG_087] = sstBmsIcDataLocal.ushBatteryHeartbeat;
                    /* CtrlHb - Controller Heartbeat */
                    ushModernRegHoldingValue[MB_MODERN_REG_088] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* AlmRst - Alarm Reset */
                    ushModernRegHoldingValue[MB_MODERN_REG_089] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* Typ - Battery Type */
                    ushModernRegHoldingValue[MB_MODERN_REG_090] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* State - State of the Battery Bank */
                    ushModernRegHoldingValue[MB_MODERN_REG_091] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* pad */
                    ushModernRegHoldingValue[MB_MODERN_REG_092] = (uint16_t)DEFAULT_VALUE_PAD;
                    /* WarrDt - Warranty Date */
                    ushModernRegHoldingValue[MB_MODERN_REG_093] = (uint16_t)DEFAULT_VALUE_TBD;
                    ushModernRegHoldingValue[MB_MODERN_REG_094] = (uint16_t)DEFAULT_VALUE_TBD;

                    /* Evt1 - Battery Event 1 Bitfield */
                    ushModernRegHoldingValue[MB_MODERN_REG_095] = (uint16_t) sstBmsIcDataLocal.uiFaultFlagsEvt1;
                    /* Evt1 - Battery Event 1 Bitfield */
                    ushModernRegHoldingValue[MB_MODERN_REG_096] = (uint16_t) (sstBmsIcDataLocal.uiFaultFlagsEvt1>>16);
                    /* Evt2 - Battery Event 2 Bitfield (reserve)*/
                    ushModernRegHoldingValue[MB_MODERN_REG_097] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    ushModernRegHoldingValue[MB_MODERN_REG_098] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;

                    /* EvtVnd1 - Vendor Event 1 Bitfield */
                    ushModernRegHoldingValue[MB_MODERN_REG_099] = (uint16_t) sstBmsIcDataLocal.uiFaultFlagsEvtVnd1;
                    /* EvtVnd1 - Vendor Event 1 Bitfield */
                    ushModernRegHoldingValue[MB_MODERN_REG_100] = (uint16_t) (sstBmsIcDataLocal.uiFaultFlagsEvtVnd1>>16);
                    /* EvtVnd2 - Vendor Event 2 Bitfield (reserve)*/
                    ushModernRegHoldingValue[MB_MODERN_REG_101] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    ushModernRegHoldingValue[MB_MODERN_REG_102] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;

                    /* V - External Battery Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_103] = sstBmsIcDataLocal.ushSumOfCellsVoltage;
                    /* VMax - Maximum Battery Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_104] = const_ushCfgBatteryOverVoltageWarningThreshold;
                    /* VMin - Minimum Battery Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_105] = const_ushCfgBatteryUnderVoltageWarningThreshold;
                    /* CellVMax - Max Cell Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_106] = sstBmsIcDataLocal.ushMaxCellV;
                    /* CellVMaxStr - Max Cell Voltage String */
                    ushModernRegHoldingValue[MB_MODERN_REG_107] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* CellVMaxMod - Max Cell Voltage Module */
                    ushModernRegHoldingValue[MB_MODERN_REG_108] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* CellVMin - Min Cell Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_109] = sstBmsIcDataLocal.ushMinCellV;
                    /* CellVMinStr - Min Cell Voltage String */
                    ushModernRegHoldingValue[MB_MODERN_REG_110] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* CellVMinMod - Min Cell Voltage Module */
                    ushModernRegHoldingValue[MB_MODERN_REG_111] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* CellVAvg - Average Cell Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_112] = sstBmsIcDataLocal.ushAvgCellV;
                    /* A - Total DC Current */
                    ushModernRegHoldingValue[MB_MODERN_REG_113] = sstBmsIcDataLocal.shMeasuredCurrentAvg;
                    /* AChaMax - Max Charge Current */
                    ushModernRegHoldingValue[MB_MODERN_REG_114] = sstBmsIcDataLocal.shChargeCurrentLimit;
                    /* ADisChaMax - Max Discharge Current */
                    ushModernRegHoldingValue[MB_MODERN_REG_115] = abs(sstBmsIcDataLocal.shDischargeCurrentLimit);
                    /* W - Total Power */
                    ushModernRegHoldingValue[MB_MODERN_REG_116] = (int16_t) (((int32_t) sstBmsIcDataLocal.ushSumOfCellsVoltage) * ((int32_t) sstBmsIcDataLocal.shMeasuredCurrentAvg) / INSTANTANEOUS_POWER_SCALAR);
                    /* ReqInvState - Inverter State Request */
                    ushModernRegHoldingValue[MB_MODERN_REG_117] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* ReqW - Battery Power Request */
                    ushModernRegHoldingValue[MB_MODERN_REG_118] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* SetOp - Set Operation */
                    ushModernRegHoldingValue[MB_MODERN_REG_119] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* SetInvState - Set Inverter State */
                    ushModernRegHoldingValue[MB_MODERN_REG_120] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* AHRtg_SF - Charge Capacity Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_121] = (uint16_t)SCALE_FACTOR_DEFAULT;
                    /* WHRtg_SF - Energy Capacity Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_122] = (uint16_t)SCALE_FACTOR_DEFAULT;
                    /* WChaDisChaMax_SF - Maximum Charge and Discharge Rate Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_123] = (uint16_t)SCALE_FACTOR_DEFAULT;
                    /* DisChaRte_SF - Self Discharge Rate Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_124] = (uint16_t)SCALE_FACTOR_DEFAULT;
                    /* SoC_SF - State of Charge Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_125] = (uint16_t)SCALE_FACTOR_SOC;
                    /* DoD_SF - Depth of Discharge Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_126] = (uint16_t)SCALE_FACTOR_DEFAULT;
                    /* SoH_SF - State of Health Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_127] = (uint16_t)SCALE_FACTOR_DEFAULT;
                    /* V_SF - Voltage Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_128] = (uint16_t)SCALE_FACTOR_V;
                    /* CellV_SF - Cell Voltage Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_129] = (uint16_t)SCALE_FACTOR_CELLV;
                    /* A_SF - Current Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_130] = (uint16_t)SCALE_FACTOR_CURRENT;
                    /* AMax_SF - Instantaneous Charge/Discharge Current Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_131] = (uint16_t)SCALE_FACTOR_CURRENT;
                    /* W_SF - Power Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_132] = (uint16_t)SCALE_FACTOR_DEFAULT;

                    /* ID - Lithium-Ion Module Model ID */
                    ushModernRegHoldingValue[MB_MODERN_REG_133] = 805;
                    /* L - Model Length */
                    ushModernRegHoldingValue[MB_MODERN_REG_134] = 98;
                    /* Idx - Module Index */
                    ushModernRegHoldingValue[MB_MODERN_REG_135] = 0;
                    /* NCell - Module Cell Count */
                    ushModernRegHoldingValue[MB_MODERN_REG_136] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* SoC - Module State of Charge (%) */
                    ushModernRegHoldingValue[MB_MODERN_REG_137] = sstBmsIcDataLocal.ushSOC;
                    /* DoD - Depth of Discharge (%) */
                    ushModernRegHoldingValue[MB_MODERN_REG_138] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* NCyc - Cycle Count */
                    ushModernRegHoldingValue[MB_MODERN_REG_139] = (uint16_t)DEFAULT_VALUE_TBD;
                    ushModernRegHoldingValue[MB_MODERN_REG_140] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* SoH - State of Health (%) */
                    ushModernRegHoldingValue[MB_MODERN_REG_141] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* V - Module Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_142] = sstBmsIcDataLocal.ushSumOfCellsVoltage;
                    /* CellVMax - Max Cell Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_143] = sstBmsIcDataLocal.ushMaxCellV;
                    /* CellVMaxCell - Max Cell Voltage Cell */
                    ushModernRegHoldingValue[MB_MODERN_REG_144] = sstBmsIcDataLocal.ucMaxCellVIdx;
                    /* CellVMin - Min Cell Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_145] = sstBmsIcDataLocal.ushMinCellV;
                    /* CellVMinCell - Min Cell Voltage Cell */
                    ushModernRegHoldingValue[MB_MODERN_REG_146] = sstBmsIcDataLocal.ucMinCellVIdx;
                    /* CellVAvg - Average Cell Voltage */
                    ushModernRegHoldingValue[MB_MODERN_REG_147] = sstBmsIcDataLocal.ushAvgCellV;
                    /* CellTmpMax - Max Cell Temperature */
                    ushModernRegHoldingValue[MB_MODERN_REG_148] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* CellTmpMaxCell - Max Cell Temperature Cell */
                    ushModernRegHoldingValue[MB_MODERN_REG_149] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* CellTmpMin - Min Cell Temperature */
                    ushModernRegHoldingValue[MB_MODERN_REG_150] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* CellTmpMinCell - Min Cell Temperature Cell */
                    ushModernRegHoldingValue[MB_MODERN_REG_151] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* CellTmpAvg - Average Cell Temperature */
                    ushModernRegHoldingValue[MB_MODERN_REG_152] = (uint16_t)DEFAULT_VALUE_TBD;
                    /* SN - Module Serial number */
                    for (ushReg = (uint16_t) MB_MODERN_REG_153 ; ushReg < (uint16_t) MB_MODERN_REG_169 ; ushReg++)
                    {
                        ushModernRegHoldingValue[ushReg] = (uint16_t)DEFAULT_VALUE_TBD;
                    }
                    /* NCellBal - Balancing Cell Count */
                    ushModernRegHoldingValue[MB_MODERN_REG_169] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* SoC_SF - State of Charge Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_170] = (uint16_t)SCALE_FACTOR_SOC;
                    /* SoH_SF - State of Health Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_171] = (uint16_t)SCALE_FACTOR_DEFAULT;
                    /* DoD_SF - Depth of Discharge Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_172] = (uint16_t)SCALE_FACTOR_DEFAULT;
                    /* V_SF - Voltage Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_173] = (uint16_t)SCALE_FACTOR_V;
                    /* CellV_SF - Cell Voltage Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_174] = (uint16_t)SCALE_FACTOR_CELLV;
                    /* Tmp_SF - Module Temperature Scale Factor */
                    ushModernRegHoldingValue[MB_MODERN_REG_175] = (uint16_t)SCALE_FACTOR_TEMPERATURE;
                    /* pad */
                    ushModernRegHoldingValue[MB_MODERN_REG_176] = (uint16_t)DEFAULT_VALUE_PAD;
                    /* Cells Voltage, Temperature and Status */
                    for (ushReg = 0 ; ushReg < NUM_OF_CELLS ; ushReg++)
                    {
                        /* CellV - Cell [ushReg] Voltage */
                        ushModernRegHoldingValue[MB_MODERN_REG_177 + (4 * ushReg)] = sstBmsIcDataLocal.ushCellVoltage[CELL_1 + ushReg];
                        /* CellTmp - Cell [ushReg] Temperature */
                        switch (ushReg)
                        {
                            case CELL_1:
                            case CELL_2:
                                ushModernRegHoldingValue[MB_MODERN_REG_178 + (4 * ushReg)] = sstBmsIcDataLocal.shTemperatureSensor[0];
                                break;
                            case CELL_3:
                            case CELL_4:
                            case CELL_5:
                                ushModernRegHoldingValue[MB_MODERN_REG_178 + (4 * ushReg)] = sstBmsIcDataLocal.shTemperatureSensor[1];
                                break;
                            case CELL_6:
                            case CELL_7:
                                ushModernRegHoldingValue[MB_MODERN_REG_178 + (4 * ushReg)] = sstBmsIcDataLocal.shTemperatureSensor[2];
                                break;
                            case CELL_8:
                            case CELL_9:
                                ushModernRegHoldingValue[MB_MODERN_REG_178 + (4 * ushReg)] = sstBmsIcDataLocal.shTemperatureSensor[3];
                                break;
                            case CELL_10:
                            case CELL_11:
                            case CELL_12:
                                ushModernRegHoldingValue[MB_MODERN_REG_178 + (4 * ushReg)] = sstBmsIcDataLocal.shTemperatureSensor[4];
                                break;
                            case CELL_13:
                            case CELL_14:
                                ushModernRegHoldingValue[MB_MODERN_REG_178 + (4 * ushReg)] = sstBmsIcDataLocal.shTemperatureSensor[5];
                                break;
                            default:
                                ushModernRegHoldingValue[MB_MODERN_REG_178 + (4 * ushReg)] = sstBmsIcDataLocal.shTemperatureSensor[0];
                                break;
                        }
                        /* CellSt - Cell [ushReg] Balancing Status */
                        ushModernRegHoldingValue[MB_MODERN_REG_179 + (4 * ushReg)] = (sstBmsIcDataLocal.ushCellsToBalance>>ushReg) & (uint16_t)1;
                        ushModernRegHoldingValue[MB_MODERN_REG_180 + (4 * ushReg)] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    }
                    /* ID - End Model ID */
                    ushModernRegHoldingValue[MB_MODERN_REG_233] = 0xFFFF;
                    /* L - Model Length */
                    ushModernRegHoldingValue[MB_MODERN_REG_234] = 0;

                    /********************************************/
                    /********** Test mode interface *************/
                    /********************************************/
                    /* Copy sum of cells voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1001] = sstBmsIcDataLocal.ushSumOfCellsVoltage;
                    /* Copy stack voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1002] = sstBmsIcDataLocal.ushStackVoltage;
                    /* Copy network voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1003] = sstBmsIcDataLocal.ushAnalogVoltage[ANALOG_IN_6];
                    /* Copy measured current */
                    ushTestModeRegHoldingValue[MB_TM_REG_1004] = sstBmsIcDataLocal.shMeasuredCurrentAvg;
                    /* Copy cell 1 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1005] = sstBmsIcDataLocal.ushCellVoltage[CELL_1];
                    /* Copy cell 2 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1006] = sstBmsIcDataLocal.ushCellVoltage[CELL_2];
                    /* Copy cell 3 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1007] = sstBmsIcDataLocal.ushCellVoltage[CELL_3];
                    /* Copy cell 4 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1008] = sstBmsIcDataLocal.ushCellVoltage[CELL_4];
                    /* Copy cell 5 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1009] = sstBmsIcDataLocal.ushCellVoltage[CELL_5];
                    /* Copy cell 6 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1010] = sstBmsIcDataLocal.ushCellVoltage[CELL_6];
                    /* Copy cell 7 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1011] = sstBmsIcDataLocal.ushCellVoltage[CELL_7];
                    /* Copy cell 8 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1012] = sstBmsIcDataLocal.ushCellVoltage[CELL_8];
                    /* Copy cell 9 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1013] = sstBmsIcDataLocal.ushCellVoltage[CELL_9];
                    /* Copy cell 10 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1014] = sstBmsIcDataLocal.ushCellVoltage[CELL_10];
                    /* Copy cell 11 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1015] = sstBmsIcDataLocal.ushCellVoltage[CELL_11];
                    /* Copy cell 12 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1016] = sstBmsIcDataLocal.ushCellVoltage[CELL_12];
                    /* Copy cell 13 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1017] = sstBmsIcDataLocal.ushCellVoltage[CELL_13];
                    /* Copy cell 14 voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1018] = sstBmsIcDataLocal.ushCellVoltage[CELL_14];
                    /* Copy 3.3V rail voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1019] = sstBmsIcDataLocal.ushAnalogVoltage[ANALOG_IN_5];
                    /* Copy 12V switched rail voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1020] = sstBmsIcDataLocal.ushAnalogVoltage[ANALOG_IN_4];
                    /* Copy 5V switched rail voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1021] = sstBmsIcDataLocal.ushAnalogVoltage[ANALOG_IN_3];
                    /* Copy midpoint voltage (for FET self-test) */
                    ushTestModeRegHoldingValue[MB_TM_REG_1022] = sstBmsIcDataLocal.ushAnalogVoltage[ANALOG_IN_2];
                    /* Copy over-current detection voltage */
                    ushTestModeRegHoldingValue[MB_TM_REG_1023] = sstBmsIcDataLocal.ushAnalogVoltage[ANALOG_IN_1];
                    /* Reserved for unused analog in */
                    ushTestModeRegHoldingValue[MB_TM_REG_1024] = sstBmsIcDataLocal.ushAnalogVoltage[ANALOG_IN_0];
                    /* Copy temp sensor 0 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1025] = sstBmsIcDataLocal.shTemperatureSensor[0];
                    /* Copy temp sensor 1 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1026] = sstBmsIcDataLocal.shTemperatureSensor[1];
                    /* Copy temp sensor 2 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1027] = sstBmsIcDataLocal.shTemperatureSensor[2];
                    /* Copy temp sensor 3 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1028] = sstBmsIcDataLocal.shTemperatureSensor[3];
                    /* Copy temp sensor 4 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1029] = sstBmsIcDataLocal.shTemperatureSensor[4];
                    /* Copy temp sensor 5 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1030] = sstBmsIcDataLocal.shTemperatureSensor[5];
                    /* Copy temp sensor 6 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1031] = sstBmsIcDataLocal.shTemperatureSensor[6];
                    /* Copy temp sensor 7 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1032] = sstBmsIcDataLocal.shTemperatureSensor[7];
                    /* Copy temp sensor 8 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1033] = sstBmsIcDataLocal.shTemperatureSensor[8];
                    /* Copy temp sensor 9 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1034] = sstBmsIcDataLocal.shTemperatureSensor[9];
                    /* Copy temp sensor 10 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1035] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* Copy temp sensor 11 value */
                    ushTestModeRegHoldingValue[MB_TM_REG_1036] = (uint16_t)DEFAULT_VALUE_UNUSED_REGISTER;
                    /* Get data from HMI inputs (buttons) */
                    ushTestModeRegHoldingValue[MB_TM_REG_1037] = (uint16_t)sstBmsIcDataLocal.ucButtonStatus;
                    /* GPIO input state reading */
                    ushTestModeRegHoldingValue[MB_TM_REG_1038] = (uint16_t)sstBmsIcDataLocal.ucGPIOStatus;
                    /* MC33771 cell under-voltage status bits */
                    ushTestModeRegHoldingValue[MB_TM_REG_1039] = sstBmsIcDataLocal.ushCellUnderVoltageFlags;
                    /* MC33771 cell over-voltage status bits */
                    ushTestModeRegHoldingValue[MB_TM_REG_1040] = sstBmsIcDataLocal.ushCellOverVoltageFlags;
                    /* MC33771 thermal shutdown status bit */
                    ushTestModeRegHoldingValue[MB_TM_REG_1041] = sstBmsIcDataLocal.ucThermalShutdownFlag;
                    /* Battery Heartbeat - watchdog counter */
                    ushTestModeRegHoldingValue[MB_TM_REG_1058] = sstBmsIcDataLocal.ushBatteryHeartbeat;
                    

                    /********************************************/
                    /************ Legacy interface **************/
                    /********************************************/
                    /* Scale sum of cells voltage to legacy interface value */
                    uiLegacySumOfCellsVoltage = DIV10_U16( sstBmsIcDataLocal.ushSumOfCellsVoltage );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23765] = (uint16_t) ( uiLegacySumOfCellsVoltage );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23766] = (uint16_t) ( uiLegacySumOfCellsVoltage >> 16 );
                    /* Scale measured current to legacy interface value */
                    uiLegacyCurrent = DIV10_I16( sstBmsIcDataLocal.shMeasuredCurrentAvg );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23767] = (uint16_t) ( uiLegacyCurrent );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23768] = (uint16_t) ( uiLegacyCurrent >> 16 );
                    /* Scale discharge current limit to legacy interface value */
                    uiLegacyIMD = DIV10_U16( Abs( sstBmsIcDataLocal.shDischargeCurrentLimit ) );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23769] = (uint16_t) ( uiLegacyIMD );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23770] = (uint16_t) ( uiLegacyIMD >> 16 );
                    /* Scale charge current limit to legacy interface value */
                    uiLegacyIMR = DIV10_U16( sstBmsIcDataLocal.shChargeCurrentLimit );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23771] = (uint16_t) ( uiLegacyIMR );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23772] = (uint16_t) ( uiLegacyIMR >> 16 );
                    /* Copy fault values */
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23773] = (uint16_t) ( sstBmsIcDataLocal.uiFaultFlagsLegacy );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23774] = (uint16_t) ( sstBmsIcDataLocal.uiFaultFlagsLegacy >> 16);
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23775] = (uint16_t) ( 0 );
                    /* Create Battery Mode and Charge Mode mappings */
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23776] = (uint8_t) ( ushTempChargeMode );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23776] |= ( ( (uint8_t) ( ushTempBatteryeMode ) ) << 8 );
                    /* Copy SOH and derated charge current limit values */
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23777] = (uint8_t) ( 100 );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23777] |= ( ( (uint8_t) DIV10_U16(sstBmsIcDataLocal.shChargeCurrentLimitDerated) ) << 8 );
                    /* Copy battery capacity value */
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23778] = (uint16_t) ( 74 );
                    /* Copy SOC and max cell temperature */
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23779] = (uint8_t) ( DIV10_U16(sstBmsIcDataLocal.ushSOC) );
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23779] |= ( ( (uint8_t) DIV10_I16(sstBmsIcDataLocal.shTemperatureSensor[0]) ) << 8 );
                    /* Copy major/minor fault flags */
                    ushLegacyRegHoldingValue[MB_LEGACY_REG_23780] = (uint16_t) 0;
                }

                /* Get Self test results and create pass bit array if the results change */
                if (spSelfTestResultsLocal != spGetSelfTestResultsBuffer())
                {
                    spSelfTestResultsLocal = spGetSelfTestResultsBuffer();
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_ADC_CHK)) | \
                    (spSelfTestResultsLocal->sADC1abFuncTestResults.bAdc1abTestPass << SELF_TEST_BIT_ADC_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_V_CHK)) | \
                    (spSelfTestResultsLocal->sCellVoltFuncVerTestResults.bCVFVTestPass << SELF_TEST_BIT_CELL_V_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_TERM_OPEN_CHK)) | \
                    (spSelfTestResultsLocal->sCTOpenDetTestResults.bCTOpenDetTestPass << SELF_TEST_BIT_CELL_TERM_OPEN_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_TERM_SHORT_CHK)) | \
                    (spSelfTestResultsLocal->sCTOpenDetTestResults.bCTShortDetTestPass << SELF_TEST_BIT_CELL_TERM_SHORT_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_TERM_SW_OPEN_OOR_CHK)) | \
                    (spSelfTestResultsLocal->sCTOpenDetTestResults.bCTOpenOutRangeTestPass << SELF_TEST_BIT_CELL_TERM_SW_OPEN_OOR_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_TERM_SW_CLOSE_OOR_CHK)) | \
                    (spSelfTestResultsLocal->sCTOpenDetTestResults.bCTCloseOutRangeTestPass << SELF_TEST_BIT_CELL_TERM_SW_CLOSE_OOR_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_TERM_LEAK_CHK)) | \
                    (spSelfTestResultsLocal->sCTLeakageTestResults.bCTLeakageTestPass << SELF_TEST_BIT_CELL_TERM_LEAK_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_BALL_OPEN_CHK)) | \
                    (spSelfTestResultsLocal->sCellBalanceTestResults.bCBOpenTestPass << SELF_TEST_BIT_CELL_BALL_OPEN_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_BALL_SHORT_CHK)) | \
                    (spSelfTestResultsLocal->sCellBalanceTestResults.bCBShortTestPass << SELF_TEST_BIT_CELL_BALL_SHORT_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_UV_CHK)) | \
                    (spSelfTestResultsLocal->sOvUvFuncTestResults.bUvFaultTestPass << SELF_TEST_BIT_CELL_UV_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CELL_OV_CHK)) | \
                    (spSelfTestResultsLocal->sOvUvFuncTestResults.bOvFaultTestPass << SELF_TEST_BIT_CELL_OV_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_AMP_IN_GND_CHK)) | \
                    (spSelfTestResultsLocal->sIMeashAmpInputGndTestResults.bIMeashAmpInputGndTestPass << SELF_TEST_BIT_AMP_IN_GND_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_VREF_DIAG_CHK)) | \
                    (spSelfTestResultsLocal->sVrefDiagTestResults.bVrefDiagTestPass << SELF_TEST_BIT_VREF_DIAG_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_GPIO_TERM_OPEN_CHK)) | \
                    (spSelfTestResultsLocal->sGpioOpenTestResults.bGpioOpenTestPass << SELF_TEST_BIT_GPIO_TERM_OPEN_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_STACK_TO_SUM_CHK)) | \
                    (spSelfTestResultsLocal->sStackVsVolTestResults.bStackVsSumTestPass << SELF_TEST_BIT_STACK_TO_SUM_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_CURR_SENS_OPEN_CHK)) | \
                    (spSelfTestResultsLocal->sISenseMeasChainOpenTestResults.bISenseMeasChainOpenTestPass << SELF_TEST_BIT_CURR_SENS_OPEN_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_3V3_CHK)) | \
                    (spSelfTestResultsLocal->sSupplyVoltagesTestResults.bSupply_3_3VTestPass << SELF_TEST_BIT_3V3_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_12V_CHK)) | \
                    (spSelfTestResultsLocal->sSupplyVoltagesTestResults.bSupply_12VTestPass << SELF_TEST_BIT_12V_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_BIT_5V_CHK)) | \
                    (spSelfTestResultsLocal->sSupplyVoltagesTestResults.bSupply_5VTestPass << SELF_TEST_BIT_5V_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_MC33771_FAULT_CHK)) | \
                    (spSelfTestResultsLocal->sFaultPinDetectTestResults.bFaultPinDetectTestPass << SELF_TEST_MC33771_FAULT_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_TEMP_OT_FAULT_CHK)) | \
                    (spSelfTestResultsLocal->sTempSensorTestResults.bTempSensorTestPass << SELF_TEST_TEMP_OT_FAULT_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_CFG_RANGE_CHK)) | \
                    (spSelfTestResultsLocal->sConfigurationAndParameterRangeTestResults.bConfigurationVariablesRangeTestPass << SELF_TEST_CFG_RANGE_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_PARAM_RANGE_CHK)) | \
                    (spSelfTestResultsLocal->sConfigurationAndParameterRangeTestResults.bParameterVariablesRangeTestPass << SELF_TEST_PARAM_RANGE_CHK);
                    siSelfTestPassBits = (siSelfTestPassBits & ~(1 << SELF_TEST_CURRENT_IN_DEADZONE_CHK)) | \
                    (spSelfTestResultsLocal->sCurrentInDeadzoneTestResults.bCurrentInDeadzoneTestPass << SELF_TEST_CURRENT_IN_DEADZONE_CHK);
                }

                ushTestModeRegHoldingValue[MB_TM_REG_1042] = (uint16_t) siSelfTestPassBits;
                ushTestModeRegHoldingValue[MB_TM_REG_1043] = (uint16_t) (siSelfTestPassBits >> 16);

                ushTestModeRegHoldingValue[MB_TM_REG_1044] = (uint16_t) (spSelfTestResultsLocal->sADC1abFuncTestResults.bAdc1bFaultBit<<1) | spSelfTestResultsLocal->sADC1abFuncTestResults.bAdc1aFaultBit;
                ushTestModeRegHoldingValue[MB_TM_REG_1045] = spSelfTestResultsLocal->sCellVoltFuncVerTestResults.ushCVFVFailedCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1046] = spSelfTestResultsLocal->sCTOpenDetTestResults.ushCTOdOpenFailedCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1047] = spSelfTestResultsLocal->sCTOpenDetTestResults.ushCTOdShortFailedCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1048] = spSelfTestResultsLocal->sCTOpenDetTestResults.ushCTOdOpenSWOutOfRangeCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1049] = spSelfTestResultsLocal->sCTOpenDetTestResults.ushCTOdClosedSWOutOfRangeCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1050] = spSelfTestResultsLocal->sCTLeakageTestResults.ushCTLeakageFailedCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1051] = spSelfTestResultsLocal->sCellBalanceTestResults.ushCBOpenFailedCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1052] = spSelfTestResultsLocal->sCellBalanceTestResults.ushCShortFailedCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1053] = spSelfTestResultsLocal->sOvUvFuncTestResults.ushUvFailedCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1054] = spSelfTestResultsLocal->sOvUvFuncTestResults.ushOvFailedCells;
                ushTestModeRegHoldingValue[MB_TM_REG_1055] = (int16_t) spSelfTestResultsLocal->sIMeashAmpInputGndTestResults.iIMeasValue;
                ushTestModeRegHoldingValue[MB_TM_REG_1056] = spSelfTestResultsLocal->sVrefDiagTestResults.ushVrefDiagMeasValue;
                ushTestModeRegHoldingValue[MB_TM_REG_1057] = spSelfTestResultsLocal->sGpioOpenTestResults.ushGpioOpenFailedPins;

                /* Get character from USART1 */
                if (pdPASS == xQueueReceive(spLocalUSART1_Data_Hdl,&stRxUSARTBufferLocal,0))
                {
                    ushTestModeRegHoldingValue[MB_TM_REG_1059] = (uint16_t) stRxUSARTBufferLocal.cSerialData[0];
                }

                /* Get character from USART2 */
                if (pdPASS == xQueueReceive(spLocalUSART2_Data_Hdl,&stRxUSARTBufferLocal,0))
                {
                    ushTestModeRegHoldingValue[MB_TM_REG_1060] = (uint16_t) stRxUSARTBufferLocal.cSerialData[0];
                }

                /* Get current battery operating mode */
                eMyBatteryOperatingMode = eGetBatteryOperatingMode();

                /* Execute code based on the operating mode */
                switch(eMyBatteryOperatingMode)
                {
                    case TEST_MODE:
                    /* GPIO output state control */
					vSetGPIOsONOFF((uint8_t) ushTestModeRegHoldingValue[MB_TM_REG_1061]);
                    vSetDCsONOFF((uint8_t) ushTestModeRegHoldingValue[MB_TM_REG_1062]);
                    vSetLEDsONOFF((uint8_t) ushTestModeRegHoldingValue[MB_TM_REG_1063]);
                    vSetFetsONOFF((uint8_t) ushTestModeRegHoldingValue[MB_TM_REG_1064]);

                    /* Execute self-tests on change of execute register */
                    if (sushSelfTestExecutePrevious != ushTestModeRegHoldingValue[MB_TM_REG_1068])
                    {
                        uiSelfTestFlags = ((uint32_t) ushTestModeRegHoldingValue[MB_TM_REG_1067]<<16 ) | ((uint32_t) ushTestModeRegHoldingValue[MB_TM_REG_1066]);
                        spQueue_SelfTests_Hdl = sGetQueueHandle(SELF_TESTS_QUEUE);
                        
                        /* Unblock self-tests task */
                        xQueueOverwrite(spQueue_SelfTests_Hdl, &uiSelfTestFlags);
                        sushSelfTestExecutePrevious = ushTestModeRegHoldingValue[MB_TM_REG_1068];
                    }

                    ushTestModeRegHoldingValue[MB_TM_REG_1069] = 0;
                    
                    /* Cell Balancing Manual */
                    vCellBalancingRequest(ushTestModeRegHoldingValue[MB_TM_REG_1065]);
                    
                    break;

                    case NORMAL_MODE:
                    /* Check for Test Mode Request Sequence and queue value only if it has changed from the last one */
                    sushNewWakeByte = (uint8_t)ushTestModeRegHoldingValue[MB_TM_REG_1069];
                    if(sushPreviousWakeByte != sushNewWakeByte)
                    {
                        /* Queue new value, if successful save as previous to prevent re-queuing, but if unsuccessful do not update previous in order to re-try queuing */
                        sStatus = xQueueSendToBack(spLocalTest_Mode_Request_Hdl, &sushNewWakeByte, NOWAIT);
                        if( sStatus == pdPASS )
                        {
                            sushPreviousWakeByte = sushNewWakeByte;
                        }
                    }
                    
                    /* Cell Balancing*/
                    ushTestModeRegHoldingValue[MB_TM_REG_1065] = sstBmsIcDataLocal.ushCellsToBalance;
                    
                    break;

                    default:
                    break;
                }

                /* Task delay */
                vTaskDelayUntil(&xLastWakeTime, pdMS_TO_TICKS(TIME_10MS));
            }
            while( MB_ENOERR == (eMBErrorCode)( eStatusUSB | eStatusExt ) );

        }
        eStatusUSB = eMBSClose( xMBSHdlUSB );
        //eStatusExt = eMBSClose( xMBSHdlExt );
        if( MB_ENOERR != ( eStatusUSB = eMBSClose( xMBSHdlUSB ) ) )
        {
        }
        //if( MB_ENOERR != ( eStatusExt = eMBSClose( xMBSHdlExt ) ) )
        //{
        //}
    }
    if( MB_ENOERR != (eMBErrorCode)( eStatusUSB | eStatusExt ) )
    {
        vTaskDelay( 500 );
    }
}

/*********************************************************************
* PRIVATE void eMyRegHoldingCB()
*---------------------------------------------------------------------
*  Callback for reading or writing holding registers
*
* Inputs:
*   pubRegBuffer: pointer to register buffer
*   usAddress: start address
*   usNRegs: number of registers
*   eRegMode: register access mode (read or write)
*
* Returns:
*   eMBException: exception code
*
*
**********************************************************************/
static eMBException eMyRegHoldingCB( UBYTE * pubRegBuffer, USHORT usAddress, USHORT usNRegs, eMBSRegisterMode eRegMode )
{
    eMBException    eException = MB_PDU_EX_ILLEGAL_DATA_ADDRESS;
    static const ULONG usModernRegsMappedAt = ( MB_MODERN_REG_START_ADR );
    static const ULONG usTestModeRegsMappedAt = ( MB_TEST_MODE_REG_START_ADR );
    static const ULONG usLegacyRegsMappedAt = ( MB_LEGACY_REG_START_ADR );
    ULONG           usRegStart = usAddress;
    ULONG           usRegEnd = usAddress + usNRegs - 1;
    USHORT          usIndex;
    USHORT          usIndexEnd;

    if( usNRegs > 0 )
    {
        if (( usRegStart >= usModernRegsMappedAt ) && ( usRegEnd <= ( usModernRegsMappedAt + MB_UTILS_NARRSIZE( ushModernRegHoldingValue ) ) ))
        {
            usIndex = ( USHORT ) ( usRegStart - usModernRegsMappedAt );
            usIndexEnd = ( USHORT ) ( usRegEnd - usModernRegsMappedAt );
            switch ( eRegMode )
            {
                case MBS_REGISTER_WRITE:
                for( ; usIndex <= usIndexEnd; usIndex++ )
                {
                    ushModernRegHoldingValue[usIndex] = ( USHORT ) * pubRegBuffer++ << 8;
                    ushModernRegHoldingValue[usIndex] |= ( USHORT ) * pubRegBuffer++;
                }
                break;

                default:
                case MBS_REGISTER_READ:

                for( ; usIndex <= usIndexEnd; usIndex++ )
                {
                    *pubRegBuffer++ = ( UBYTE ) ( ushModernRegHoldingValue[usIndex] >> 8 );
                    *pubRegBuffer++ = ( UBYTE ) ( ushModernRegHoldingValue[usIndex] & 0xFF );
                }
                break;
            }
            eException = MB_PDU_EX_NONE;
        }
        else if (( usRegStart >= usTestModeRegsMappedAt ) && ( usRegEnd <= ( usTestModeRegsMappedAt + MB_UTILS_NARRSIZE( ushTestModeRegHoldingValue ) ) ))
        {
            usIndex = ( USHORT ) ( usRegStart - usTestModeRegsMappedAt );
            usIndexEnd = ( USHORT ) ( usRegEnd - usTestModeRegsMappedAt );
            switch ( eRegMode )
            {
                case MBS_REGISTER_WRITE:
                for( ; usIndex <= usIndexEnd; usIndex++ )
                {
                    ushTestModeRegHoldingValue[usIndex] = ( USHORT ) * pubRegBuffer++ << 8;
                    ushTestModeRegHoldingValue[usIndex] |= ( USHORT ) * pubRegBuffer++;
                }
                break;

                default:
                case MBS_REGISTER_READ:

                for( ; usIndex <= usIndexEnd; usIndex++ )
                {
                    *pubRegBuffer++ = ( UBYTE ) ( ushTestModeRegHoldingValue[usIndex] >> 8 );
                    *pubRegBuffer++ = ( UBYTE ) ( ushTestModeRegHoldingValue[usIndex] & 0xFF );
                }
                break;
            }
            eException = MB_PDU_EX_NONE;
        }
        else if (( usRegStart >= usLegacyRegsMappedAt ) && ( usRegEnd <= ( usLegacyRegsMappedAt + MB_UTILS_NARRSIZE( ushLegacyRegHoldingValue ) ) ))
        {
            usIndex = ( USHORT ) ( usRegStart - usLegacyRegsMappedAt );
            usIndexEnd = ( USHORT ) ( usRegEnd - usLegacyRegsMappedAt );
            switch ( eRegMode )
            {
                case MBS_REGISTER_WRITE:
                for( ; usIndex <= usIndexEnd; usIndex++ )
                {
                    ushLegacyRegHoldingValue[usIndex] = ( USHORT ) * pubRegBuffer++ << 8;
                    ushLegacyRegHoldingValue[usIndex] |= ( USHORT ) * pubRegBuffer++;
                }
                break;

                default:
                case MBS_REGISTER_READ:

                for( ; usIndex <= usIndexEnd; usIndex++ )
                {
                    *pubRegBuffer++ = ( UBYTE ) ( ushLegacyRegHoldingValue[usIndex] >> 8 );
                    *pubRegBuffer++ = ( UBYTE ) ( ushLegacyRegHoldingValue[usIndex] & 0xFF );
                }
                break;
            }
            eException = MB_PDU_EX_NONE;
        }
    }
    return eException;
}

/*********************************************************************
* PUBLIC ushGetRegisterValue()
*---------------------------------------------------------------------
* Cell management routines
*
* Inputs:
*       pushRegisterOffset - pointer to value with register offset to be returned
* Returns:
*       Register value at offset passed to function
*
**********************************************************************/
uint16_t ushGetRegisterValue(uint16_t ushRegisterOffset)
{
    uint16_t ushMyRegHoldingValue = 0;
    /* verify that the address offset being passed falls within the memory space of the register buffer */
    if ( ushRegisterOffset <= MB_UTILS_NARRSIZE( ushTestModeRegHoldingValue ) )
    {
        ushMyRegHoldingValue = ushTestModeRegHoldingValue[ushRegisterOffset];
    }
    return ushMyRegHoldingValue;
}

/*********************************************************************
* PRIVATE eMBErrorCode eDataHdlExtCommParameters()
*---------------------------------------------------------------------
*
*
* Inputs:
*   ushExtCommSlaveIdIn : slave Id parameter from flash memory
*   eExtCommBaudRateIn : Baud rate parameter from flash memory
*   eExtCommStopBitsIn : Stop Bits parameter from flash memory
*   eExtCommParityIn : Parity parameter from flash memory
*
* Outputs:
*   ushExtCommSlaveIdOut : slave Id parameter used for Modbus communication
*   ulExtCommBaudRateOut : Baud rate parameter used for Modbus communication
*   ushExtCommStopBitsOut : Stop Bits parameter used for Modbus communication
*   eExtCommParityOut : Parity parameter used for Modbus communication
*
* Returns:
*   Error code :    MB_ENOERR : parameter conversion ok
*                   MB_EINVAL : SlaveId out of range
*
**********************************************************************/
static eMBErrorCode eDataHdlExtCommParameters(uint8_t ushExtCommSlaveIdIn, eExtCommsBaudRate eExtCommBaudRateIn,
                                eExtCommsStopBits eExtCommStopBitsIn, eExtCommsParity eExtCommParityIn,
                                uint8_t *ushExtCommSlaveIdOut, uint32_t *ulExtCommBaudRateOut,
                                uint8_t *ushExtCommStopBitsOut, eMBSerialParity *eExtCommParityOut)
{
    eMBErrorCode eParamStatus = MB_ENOERR;

    if ( ( ushExtCommSlaveIdIn < 1 ) || ( ushExtCommSlaveIdIn > 247 ) )
    {
        *ushExtCommSlaveIdOut = 1; /* default behavior to be defined */
        eParamStatus = MB_EINVAL;
    }
    else
    {
        *ushExtCommSlaveIdOut = ushExtCommSlaveIdIn;
    }

    switch (eExtCommBaudRateIn)
    {
        case EXTCOMM_BAUD_RATE_9600:
        {
            *ulExtCommBaudRateOut = 9600;
            break;
        }
        case EXTCOMM_BAUD_RATE_19200:
        {
            *ulExtCommBaudRateOut = 19200;
            break;
        }
        case EXTCOMM_BAUD_RATE_57600:
        {
            *ulExtCommBaudRateOut = 57600;
            break;
        }
        case EXTCOMM_BAUD_RATE_115200:
        {
            *ulExtCommBaudRateOut = 115200;
            break;
        }
        default:
        {
            *ulExtCommBaudRateOut = 115200;/* default behavior to be defined */
            break;
        }
    }

    switch (eExtCommStopBitsIn)
    {
/*        case EXTCOMM_STOP_BITS_1:
        {
            *ushExtCommStopBitsOut = 1;
            break;
        }*/
        case EXTCOMM_STOP_BITS_2:
        {
            *eExtCommParityOut = MB_PAR_NONE; /* SWRS_00320 */
            *ushExtCommStopBitsOut = 2;
            break;
        }
        default: /* EXTCOMM_STOP_BITS_1 is the default value */
        {
            switch (eExtCommParityIn)
            {
                case EXTCOMM_PARITY_ODD:
                {
                    *eExtCommParityOut = MB_PAR_ODD;
                    break;
                }
                case EXTCOMM_PARITY_EVEN:
                {
                    *eExtCommParityOut = MB_PAR_EVEN;
                    break;
                }
                default:
                {
                    *eExtCommParityOut = MB_PAR_EVEN;  /* default behavior to be defined */
                    break;
                }
            }

            *ushExtCommStopBitsOut = 1; /* default behavior to be defined */
            break;
        }
    }
    return eParamStatus;
}

/******************************** End of File ********************************/
