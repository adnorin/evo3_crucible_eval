/*
 * MODBUS Library: FreeRTOS port
 * Copyright (c) 2016 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbportother.c,v 1.1 2008-04-06 07:47:26 cwalter Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

/* ----------------------- Platform includes --------------------------------*/
#include "service/Modbus/port/mbport.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "service/Modbus/include/common/mbtypes.h"
#include "service/Modbus/include/common/mbportlayer.h"
#include "service/Modbus/include/common/mbframe.h"
#include "service/Modbus/include/common/mbutils.h"

/* ----------------------- Defines ------------------------------------------*/

/* ----------------------- Type definitions ---------------------------------*/

/* ----------------------- Static variables ---------------------------------*/
STATIC xSemaphoreHandle xCritSection;

/* ----------------------- Static functions ---------------------------------*/

/* ----------------------- Function prototypes ------------------------------*/

/* ----------------------- Start implementation -----------------------------*/

void
vMBPInit( void )
{
    xCritSection = xSemaphoreCreateRecursiveMutex(  );
    MBP_ASSERT( NULL != xCritSection );
}

void
vMBPEnterCritical( void )
{
    signed portBASE_TYPE xResult;
    xResult = xSemaphoreTakeRecursive( xCritSection, portMAX_DELAY );
    MBP_ASSERT( pdTRUE == xResult );
}

void
vMBPExitCritical( void )
{
    signed portBASE_TYPE xResult;
    xResult = xSemaphoreGiveRecursive( xCritSection );
    MBP_ASSERT( pdTRUE == xResult );
}

void
vMBPAssert( const char *pszFile, int iLineNo )
{
    __disable_irq(  );
    /* Let the watchdog trigger a reset here. */
    for( ;; );
}
