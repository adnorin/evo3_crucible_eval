/*
 * MODBUS Library: FreeRTOS port
 * Copyright (c)  2016 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbsconfig.h 1068 2016-08-26 16:05:09Z cwalter $
 */

#ifndef _MBSCONFIG_H
#define _MBSCONFIG_H

#ifdef __cplusplus
PR_BEGIN_EXTERN_C
#endif

/* ----------------------- Defines ------------------------------------------*/
#define MBS_ASCII_ENABLED               ( 0 )
#define MBS_RTU_ENABLED                 ( 1 )
#define MBS_TCP_ENABLED                 ( 0 )
#define MBS_UDP_ENABLED                 ( 0 )
#define MBS_SERIAL_RTU_MAX_INSTANCES    ( 1 )
#define MBS_SERIAL_ASCII_MAX_INSTANCES  ( 0 )
#define MBS_SERIAL_API_VERSION          ( 2 )
#define MBS_SERIAL_APIV2_RTU_TIMEOUT_MS ( 20 )
#define MBS_SERIAL_DROPFRAME_TIMEOUT_CB ( 0 )
#define MBS_RTU_NEEDS_INTHDL            ( 1 )
#define MBS_SERIAL_APIV2_RTU_DYNAMIC_TIMEOUT_MS( ulBaudRate )   usMPSerialTimeout( ulBaudRate )
#define MBS_TCP_MAX_INSTANCES                           ( 0 )
#define MBS_UDP_MAX_INSTANCES                           ( 0 )
#define MBS_TCP_MAX_CLIENTS                             ( 0 )
#define MBS_ASCII_TIMEOUT_SEC                           ( 1 )
#define MBS_ASCII_BACKOF_TIME_MS                        ( 5 )
#define MBS_ENABLE_FULL_API_CHECKS                      ( 1 )
#define MBS_NCUSTOM_FUNCTION_HANDLERS                   ( 0 )
#define MBS_FUNC_READ_INPUT_REGISTERS_ENABLED           ( 0 )
#define MBS_FUNC_READ_HOLDING_REGISTERS_ENABLED         ( 1 )
#define MBS_FUNC_WRITE_SINGLE_REGISTER_ENABLED          ( 1 )
#define MBS_FUNC_WRITE_MULTIPLE_REGISTERS_ENABLED       ( 1 )
#define MBS_FUNC_MASK_WRITE_REGISTER_ENABLED            ( 0 )
#define MBS_FUNC_READWRITE_MULTIPLE_REGISTERS_ENABLED   ( 0 )
#define MBS_FUNC_READ_DISCRETE_ENABLED                  ( 0 )
#define MBS_FUNC_READ_COILS_ENABLED                     ( 0 )
#define MBS_FUNC_WRITE_SINGLE_COIL_ENABLED              ( 0 )
#define MBS_FUNC_WRITE_MULTIPLE_COILS_ENABLED           ( 0 )
#define MBS_FUNC_REPORT_SLAVE_ID_ENABLED                ( 0 )
#define MBS_FUNC_READ_EXCEPTION_STATUS_ENABLED          ( 0 )
#define MBS_RTU_WAITAFTERSEND_ENABLED                   ( 0 )
#define MBS_ASCII_WAITAFTERSEND_ENABLED                 ( 0 )
#define MBS_ENABLE_STATISTICS_INTERFACE                 ( 0 )
#define MBS_ENABLE_PROT_ANALYZER_INTERFACE              ( 0 )
#define MBS_ENABLE_SER_DIAG                             ( 0 )
#define MBS_CALLBACK_ENABLE_CONTEXT                     ( 0 )
#define MBS_TRACK_SLAVEADDRESS                          ( 0 )
#define MBS_ENABLE_GATEWAY_MODE                         ( 0 )
#define MBS_POLL_SINGLE_CYCLE                           ( 1 )
#define MBS_TCP_IGNORE_UNIT_ID                          ( 0 )
#define MBS_IS_EVALUATION_BUILD                         ( 0 )
#ifdef __cplusplus
PR_END_EXTERN_C
#endif

#endif
