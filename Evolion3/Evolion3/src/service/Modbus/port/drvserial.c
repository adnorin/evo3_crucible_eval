/*
 * MODBUS Library: FreeRTOS, Serial Driver
 * Copyright (c) 2010 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: drvserial.c,v 1.6 2010-06-13 17:08:43 embedded-so.embedded-solutions.1 Exp $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <string.h>
#include <FreeRTOS.h>
#include <task.h>
#include <semphr.h>

/* ----------------------- Platform includes --------------------------------*/
#include "service/Modbus/port/mbport.h"
#include "config/conf_usb.h"
#include <iface_rtos_start.h>
#include <iface_usart_dma.h>
#include <comm_external.h>
#include <comm_internal.h>

/* ----------------------- MODBUS -------------------------------------------*/
#include "service/Modbus/include/common/mbtypes.h"
#include "service/Modbus/include/common/mbframe.h"
#include "service/Modbus/include/common/mbutils.h"
#include "service/Modbus/include/common/mbportlayer.h"

#include "drvserial.h"

/* ----------------------- Hardware mapping - Must come first ---------------*/

/* ----------------------- Defines ------------------------------------------*/

#define DRV_SERIAL_IS_VALID( ubPort, xHdls )	\
	( ( ubPort < DRV_SERIAL_MAX_INSTANCES ) && ( xHdls[ ubPort ].bIsInitialized  ) )


#define HDL_RESET( x )						do { \
	( x )->bIsInitialized = FALSE; \
	( x )->xSemHdl = NULL; \
	( x )->usEventMask = 0; \
} while( 0 )

/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    BOOL            bIsInitialized;
    xSemaphoreHandle xSemHdl;
    USHORT          usEventMask;
} xDrvSerialHandle;

/* ----------------------- Static functions ---------------------------------*/

/* ----------------------- Static variables ---------------------------------*/
STATIC xDrvSerialHandle xDrvSerialHdls[DRV_SERIAL_MAX_INSTANCES];
static SemaphoreHandle_t spSemBinary_USB_Hdl;
static SemaphoreHandle_t spSemBinary_USART2_HdlMB;
//static SemaphoreHandle_t spSemBinary_USB_Rx_Hdl;
//static SemaphoreHandle_t spSemBinary_USB_Tx_Empty_Hdl;

/* ----------------------- Start implementation -----------------------------*/
void
vDrvSerialCloseInt( UBYTE ubPort )
{
    vPortEnterCritical(  );
    if( NULL != xDrvSerialHdls[ubPort].xSemHdl )
    {
        vQueueDelete( xDrvSerialHdls[ubPort].xSemHdl );
    }
    HDL_RESET( &xDrvSerialHdls[ubPort] );
    vPortEnterCritical(  );
}

eMBErrorCode
eDrvSerialInit( UBYTE ubPort, ULONG ulBaudRate, UBYTE ucDataBits, eMBSerialParity eParity, UBYTE ucStopBits )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    MBP_ENTER_CRITICAL_SECTION(  );

    if( ( ubPort < DRV_SERIAL_MAX_INSTANCES ) && ( !xDrvSerialHdls[ubPort].bIsInitialized ) )
    {
        HDL_RESET( &xDrvSerialHdls[ubPort] );
        xDrvSerialHdls[ubPort].bIsInitialized = TRUE;

        eStatus = MB_ENOERR;
        //switch ( ucDataBits )
        //{
        //case 8:
            //xOptions.charlength = 8;
            //break;
        //case 7:
            //xOptions.charlength = 7;
            //break;
        //default:
            //eStatus = MB_EINVAL;
            //break;
        //}
        //switch ( ucStopBits )
        //{
        //case 2:
            //xOptions.stopbits = USART_2_STOPBITS;
            //break;
        //case 1:
            //xOptions.stopbits = USART_1_STOPBIT;
            //break;
        //default:
            //eStatus = MB_EINVAL;
            //break;
        //}
        //switch ( eParity )
        //{
        //case MB_PAR_EVEN:
            //xOptions.paritytype = USART_EVEN_PARITY;
            //break;
        //case MB_PAR_ODD:
            //xOptions.paritytype = USART_ODD_PARITY;
            //break;
        //case MB_PAR_NONE:
            //xOptions.paritytype = USART_NO_PARITY;
            //break;
        //default:
            //eStatus = MB_EINVAL;
            //break;
        //}
        if( MB_EINVAL != eStatus )
        {
            vSemaphoreCreateBinary( xDrvSerialHdls[ubPort].xSemHdl );
            if( NULL == xDrvSerialHdls[ubPort].xSemHdl )
            {
                eStatus = MB_ENORES;
            }
            else
            {
                //vPortEnterCritical(  );
				/* Do any HW stuff here */
				//vPortExitCritical(  );
				eStatus = MB_ENOERR;
            }
        }

        if( MB_ENOERR != eStatus )
        {
            vDrvSerialCloseInt( ubPort );
        }
    }

    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

eMBErrorCode
eDrvSerialClose( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    MBP_ENTER_CRITICAL_SECTION(  );
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        vDrvSerialCloseInt( ubPort );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

eMBErrorCode
eDrvSerialWaitEvent( UBYTE ubPort, USHORT * pusEvents, USHORT usTimeOut )
{
    spSemBinary_USB_Hdl = sGetSemaphoreHandle(USB_COMM);
    spSemBinary_USART2_HdlMB = sGetSemaphoreHandle(USART2_COMM);
    
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        if( ubPort == USB_PORT_ID )
        {
            if( pdTRUE == xSemaphoreTake( spSemBinary_USB_Hdl, usTimeOut / portTICK_RATE_MS ) )
            {
                vPortEnterCritical(  );
                *pusEvents = xDrvSerialHdls[ubPort].usEventMask;
			    /*if( ubPort == 0 )*/
			    {
				    if ( uiUSBGetTxFree() > MBP_SERIAL_BUFFER_SIZE )
				    {
					    *pusEvents |= DRV_SERIAL_EVENT_TXRDY;
                    }
                    //else
                    //{
                        //*pusEvents = *pusEvents & 0<<DRV_SERIAL_EVENT_TXRDY;
                    //}
				    if ( uiUSBGetRxCount() > 0 )
				    {
                        //xSemaphoreTake(spSemBinary_USB_Rx_Hdl, 0);
					    *pusEvents |= DRV_SERIAL_EVENT_RXRDY;
				    }
                    //else
                    //{
                        //*pusEvents = *pusEvents & 0<<DRV_SERIAL_EVENT_RXRDY;
                    //}
			    }
                xDrvSerialHdls[ubPort].usEventMask = 0;
                vPortExitCritical(  );
            }
            else
            {
                *pusEvents = 0;
            }
            eStatus = MB_ENOERR;
        }
        else if( ubPort == USART_PORT_2 )
        {
            if( pdTRUE == xSemaphoreTake( spSemBinary_USART2_HdlMB, usTimeOut / portTICK_RATE_MS ) )
            {
                vPortEnterCritical(  );
                *pusEvents = xDrvSerialHdls[ubPort].usEventMask;

                if ( uiUsartGetTxFree(USART_PORT_2) > 0 )
                {
                    *pusEvents |= DRV_SERIAL_EVENT_TXRDY;    
                }
                
                if ( uiUsartGetRxCount(USART_PORT_2) > 0)
                {
                    *pusEvents |= DRV_SERIAL_EVENT_RXRDY;    
                }

                xDrvSerialHdls[ubPort].usEventMask = 0;
                vPortExitCritical(  );
            }
            else
            {
                *pusEvents = 0;
            }
            eStatus = MB_ENOERR;
        }
    }
    return eStatus;
}

eMBErrorCode
eDrvSerialAbort( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        vPortEnterCritical(  );
        xDrvSerialHdls[ubPort].usEventMask |= DRV_SERIAL_EVENT_ABORT;
        vPortExitCritical(  );
        if (ubPort == USB_PORT_ID)
        {
            ( void )xSemaphoreGive( spSemBinary_USB_Hdl );
        }
        else if( ubPort == USART_PORT_2 )
        {
             ( void )xSemaphoreGive( spSemBinary_USART2_HdlMB );       
        }                    
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

eMBErrorCode
eDrvSerialTransmitEnable( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        if( ubPort == USB_PORT_ID )
		{
			if ( uiUSBGetTxFree() > 0 )
			{
				( void )xSemaphoreGive( spSemBinary_USB_Hdl );
			}
		}
        else if( ubPort == USART_PORT_2 )
        {
            if ( uiUsartGetTxFree(USART_PORT_2) > 0 )
            {        
                ( void )xSemaphoreGive( spSemBinary_USART2_HdlMB );
            }                    
        }        
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

eMBErrorCode
eDrvSerialTransmitDisable( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        /* Not implemented - Transmission stops automatically when no
         * more characters are sent.
         */
        //vPortEnterCritical(  );
		/* Do any HW stuff here */
        //vPortExitCritical(  );
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

eMBErrorCode
eDrvSerialTransmitFree( UBYTE ubPort, USHORT * pusNFreeBytes )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    
    if( ( NULL != pusNFreeBytes ) && DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        vPortEnterCritical(  );
		if( ubPort == USB_PORT_ID )
		{
        	*pusNFreeBytes = uiUSBGetTxFree();
		}
        else if( ubPort == USART_PORT_2 )
        {
            *pusNFreeBytes = uiUsartGetTxFree(USART_PORT_2); 
        }        
        vPortExitCritical(  );
		eStatus = MB_ENOERR;
    }
    return eStatus;
}

eMBErrorCode
eDrvSerialTransmit( UBYTE ubPort, const UBYTE * pubBuffer, USHORT usLength )
{
    /* USHORT          usIdx; */
    eMBErrorCode    eStatus = MB_EINVAL;

    if( ( NULL != pubBuffer ) && DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {

		if( ubPort == USB_PORT_ID )
		{
	        if( usLength <= uiUSBGetTxFree() )
	        {
	            vPortEnterCritical(  );
                uiUSBTxDataBuffer((void *) pubBuffer, usLength);
                eStatus = MB_ENOERR;
	            //for( usIdx = 0; usIdx < usLength; usIdx++ )
	            //{
	                //if( bUSBTxChar(pubBuffer[usIdx]) )
					//{
						//eStatus = MB_ENORES;
					//}
					//else
					//{
						//eStatus = MB_ENOERR;
					//}
	            //}
	            vPortExitCritical(  );

	        }          
	        else
	        {
	            eStatus = MB_ENORES;
	        }

	        vPortEnterCritical(  );
	        if( uiUSBGetTxFree() > 0 )
	        {
	            ( void )xSemaphoreGive( spSemBinary_USB_Hdl );
	        }
			vPortExitCritical(  );
		}
        else if( ubPort == USART_PORT_2 )
        {
            if( usLength <= uiUsartGetTxFree(USART_PORT_2) )
            {
                //vPortEnterCritical(  );
                
                /* Disable RS485 transceiver receive driver and enable output driver */
                //OUTPUT_SET(RS485_EXT_TX_ENABLE_RX_DISABLE_PIN,RS485_EXT_TX_ENABLE_RX_DISABLE_ACTIVE);
                
                vUsartExtCommTx_Parms((char *) pubBuffer, usLength);
                
                 /* Wait a little bit so that the DMA can shuttle data to the peripheral and the peripheral can send it out */
                 //vTaskDelay(TIME_1MS);

                 /* Enable RS485 transceiver receive driver and disable output driver */
                 //OUTPUT_SET(RS485_EXT_TX_ENABLE_RX_DISABLE_PIN,RS485_EXT_TX_ENABLE_RX_DISABLE_INACTIVE);

                 /* Re-start Usart2 data Rx */
                 //vUsartExtCommRx();
                
                //vPortExitCritical(  );
            
                eStatus = MB_ENOERR;   

            }
	        else
	        {
    	        eStatus = MB_ENORES;
	        }
                        
            vPortEnterCritical(  );
            if( uiUsartGetTxFree(USART_PORT_2) > 0)
            {
                ( void )xSemaphoreGive( spSemBinary_USART2_HdlMB );
            }
            vPortExitCritical(  );
        }
    }
    return eStatus;
}

eMBErrorCode
eDrvSerialReceiveEnable( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
		/* Do any HW stuff here */
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

eMBErrorCode
eDrvSerialReceiveDisable( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
        //vPortEnterCritical(  );
		/* Do any HW stuff here */
        //vPortExitCritical(  );
        eStatus = MB_ENOERR;
    }
    return eStatus;
}

eMBErrorCode
eDrvSerialReceive( UBYTE ubPort, UBYTE * pubBuffer, USHORT usLengthMax, USHORT * pusNBytesReceived )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    
    if( ( NULL != pubBuffer ) && ( NULL != pusNBytesReceived ) && DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
		vPortEnterCritical(  );
        if ( ubPort == USB_PORT_ID)
		{
            *pusNBytesReceived = min(usLengthMax, uiUSBGetRxCount());
            uiUSBRxDataBuffer(pubBuffer, *pusNBytesReceived);

			//for( *pusNBytesReceived = 0; ( *pusNBytesReceived < usLengthMax ) && ( uiUSBGetRxCount() > 0 );  ( *pusNBytesReceived )++ )
			//{
				//pubBuffer[*pusNBytesReceived] = ucUSBRxChar();
	        //}
            eStatus = MB_ENOERR;

		}
        else if( ubPort == USART_PORT_2 )
        {
            //vPortEnterCritical(  );
            *pusNBytesReceived = min(usLengthMax, uiUsartGetRxCount(USART_PORT_2));
            vUsartExtCommRx_Parms((char *) pubBuffer, *pusNBytesReceived);
            //vPortExitCritical(  );
            
            eStatus = MB_ENOERR;
            
        }
        
        vPortExitCritical(  );
        if(( uiUSBGetRxCount() > 0 ) && ubPort == USB_PORT_ID)
        {
            ( void )xSemaphoreGive( spSemBinary_USB_Hdl );
        }
        if(( uiUsartGetRxCount(USART_PORT_2) > 0 ) && ubPort == USART_PORT_2)
        {
            ( void )xSemaphoreGive( spSemBinary_USART2_HdlMB );
        }
        

    }
    return eStatus;
}

eMBErrorCode
eDrvSerialReceiveReset( UBYTE ubPort )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    UCHAR           __attribute__((__unused__)) ucDummyRx = 0;
    CHAR            __attribute__((__unused__)) cDummyRx = 0;
    //uint32_t   uiDummyBytesToRx = 0;
    BOOL            __attribute__((__unused__)) bStatus = FALSE;
    
    if( DRV_SERIAL_IS_VALID( ubPort, xDrvSerialHdls ) )
    {
		vPortEnterCritical(  );
        if ( ubPort == USB_PORT_ID)
		{
            //uiDummyBytesToRx = uiUSBGetRxCount();
			while( uiUSBGetRxCount() > 0 )
	        {
				ucDummyRx = ucUSBRxChar();
                //uiDummyBytesToRx = uiUSBGetRxCount();
	        }
		}
        else if( ubPort == USART_PORT_2 )
        {
            vUsartExtCommRx_Parms( &cDummyRx, (uint16_t) sizeof(char));    
            
            while ( uiUsartGetRxCount(USART_PORT_2) > 0 )
            {
                bStatus = vUsartExtCommRx();
            }
        }        
		vPortExitCritical(  );
        eStatus = MB_ENOERR;
    }
    return eStatus;
}
