/*
 * MODBUS Library: FreeRTOS port
 * Copyright (c)  2016 Christian Walter <cwalter@embedded-solutions.at>
 * All rights reserved.
 *
 * $Id: mbporttimer.c 1068 2016-08-26 16:05:09Z cwalter $
 */

/* ----------------------- System includes ----------------------------------*/
#include <stdlib.h>
#include <FreeRTOS.h>
#include <task.h>

/* ----------------------- Platform includes --------------------------------*/
#include "service/Modbus/port/mbport.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "service/Modbus/include/common/mbtypes.h"
#include "service/Modbus/include/common/mbportlayer.h"
#include "service/Modbus/include/common/mbframe.h"
#include "service/Modbus/include/common/mbutils.h"

/* ----------------------- Defines ------------------------------------------*/
#define TIMER_TASK_PRIORITY             ( MBP_TASK_PRIORITY )
#define TIMER_TASK_STACKSIZE            ( 128 )
#define TIMER_TICKRATE_MS               ( 10 / portTICK_RATE_MS )
#define MAX_TIMER_HDLS                  ( 3 )
#define IDX_INVALID                     ( 255 )

#define RESET_HDL( x ) do { \
    ( x )->ubIdx = IDX_INVALID; \
    ( x )->bIsRunning = FALSE; \
    ( x )->xNTimeoutTicks = 0; \
    ( x )->xNExpiryTimeTicks = 0; \
    ( x )->xMBMHdl = MB_HDL_INVALID; \
    ( x )->pbMBPTimerExpiredFN = NULL; \
} while( 0 );

/* ----------------------- Type definitions ---------------------------------*/
typedef struct
{
    UBYTE           ubIdx;
    BOOL            bIsRunning;
    portTickType    xNTimeoutTicks;
    portTickType    xNExpiryTimeTicks;
    xMBHandle       xMBMHdl;
    pbMBPTimerExpiredCB pbMBPTimerExpiredFN;
} xTimerInternalHandle;

/* ----------------------- Static variables ---------------------------------*/
STATIC xTimerInternalHandle arxTimerHdls[MAX_TIMER_HDLS];

STATIC BOOL     bIsInitalized = FALSE;

static TaskHandle_t  spTaskPeriodic_MBTimer_Hdl= NULL;

/* ----------------------- Static functions ---------------------------------*/
void            vMBPTimerTask( void *pvParameters );

/* ----------------------- Start implementation -----------------------------*/


TaskHandle_t 
pstGetMBTimerTaskHandle(void)
{
    return (spTaskPeriodic_MBTimer_Hdl);
}

eMBErrorCode
eMBPTimerInit( xMBPTimerHandle * xTimerHdl, USHORT usTimeOut1ms,
               pbMBPTimerExpiredCB pbMBPTimerExpiredFN, xMBHandle xHdl )
{
    eMBErrorCode    eStatus = MB_EPORTERR;
    UBYTE           ubIdx;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( ( NULL != xTimerHdl ) && ( NULL != pbMBPTimerExpiredFN ) && ( MB_HDL_INVALID != xHdl ) )
    {
        if( !bIsInitalized )
        {
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
            {
                RESET_HDL( &arxTimerHdls[ubIdx] );
            }
            if( pdPASS !=
                 xTaskCreate( vMBPTimerTask, ( const portCHAR * )"MBP-TIMER", TIMER_TASK_STACKSIZE, NULL,
                             TIMER_TASK_PRIORITY, &spTaskPeriodic_MBTimer_Hdl) )
            {
                eStatus = MB_EPORTERR;
            }
            else
            {
                bIsInitalized = TRUE;
            }
        }
        if( bIsInitalized )
        {
            for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
            {
                if( IDX_INVALID == arxTimerHdls[ubIdx].ubIdx )
                {
                    break;
                }
            }
            if( MAX_TIMER_HDLS != ubIdx )
            {
                arxTimerHdls[ubIdx].ubIdx = ubIdx;
                arxTimerHdls[ubIdx].bIsRunning = FALSE;
                arxTimerHdls[ubIdx].xNTimeoutTicks = MB_INTDIV_CEIL( ( portTickType ) usTimeOut1ms, portTICK_RATE_MS );
                arxTimerHdls[ubIdx].xNExpiryTimeTicks = 0;
                arxTimerHdls[ubIdx].xMBMHdl = xHdl;
                arxTimerHdls[ubIdx].pbMBPTimerExpiredFN = pbMBPTimerExpiredFN;

                *xTimerHdl = &arxTimerHdls[ubIdx];
                eStatus = MB_ENOERR;
            }
            else
            {
                eStatus = MB_ENORES;
            }
        }
    }
    else
    {
        eStatus = MB_EINVAL;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

void
vMBPTimerClose( xMBPTimerHandle xTimerHdl )
{
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        RESET_HDL( pxTimerIntHdl );
    }
}

eMBErrorCode
eMBPTimerSetTimeout( xMBPTimerHandle xTimerHdl, USHORT usTimeOut1ms )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) && ( usTimeOut1ms > 0 ) )
    {
        pxTimerIntHdl->xNTimeoutTicks = MB_INTDIV_CEIL( ( portTickType ) usTimeOut1ms, portTICK_RATE_MS );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

eMBErrorCode
eMBPTimerStart( xMBPTimerHandle xTimerHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;

    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        pxTimerIntHdl->bIsRunning = TRUE;
        pxTimerIntHdl->xNExpiryTimeTicks = xTaskGetTickCount(  );
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

eMBErrorCode
eMBPTimerStop( xMBPTimerHandle xTimerHdl )
{
    eMBErrorCode    eStatus = MB_EINVAL;
    xTimerInternalHandle *pxTimerIntHdl = xTimerHdl;

    MBP_ENTER_CRITICAL_SECTION(  );
    if( MB_IS_VALID_HDL( pxTimerIntHdl, arxTimerHdls ) )
    {
        pxTimerIntHdl->bIsRunning = FALSE;
        pxTimerIntHdl->xNExpiryTimeTicks = 0;
        eStatus = MB_ENOERR;
    }
    MBP_EXIT_CRITICAL_SECTION(  );
    return eStatus;
}

void
vMBPTimerTask( void *pvParameters )
{
    UBYTE           ubIdx;
    xTimerInternalHandle *pxTmrHdl;
    portTickType    xLastWakeTime;
    portTickType    xCurrentTime;

    xLastWakeTime = xTaskGetTickCount(  );
    for( ;; )
    {
        vTaskDelayUntil( &xLastWakeTime, ( portTickType ) TIMER_TICKRATE_MS );
        xCurrentTime = xTaskGetTickCount(  );
        MBP_ENTER_CRITICAL_SECTION(  );
        for( ubIdx = 0; ubIdx < MB_UTILS_NARRSIZE( arxTimerHdls ); ubIdx++ )
        {
            pxTmrHdl = &arxTimerHdls[ubIdx];
            if( ( IDX_INVALID != pxTmrHdl->ubIdx ) && ( pxTmrHdl->bIsRunning ) )
            {
                if( ( xCurrentTime - pxTmrHdl->xNExpiryTimeTicks ) >= pxTmrHdl->xNTimeoutTicks )
                {
                    pxTmrHdl->bIsRunning = FALSE;
                    if( NULL != pxTmrHdl->pbMBPTimerExpiredFN )
                    {
                        ( void )pxTmrHdl->pbMBPTimerExpiredFN( pxTmrHdl->xMBMHdl );
                    }
                }
            }
        }
        MBP_EXIT_CRITICAL_SECTION(  );
    }
}
