/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: RomDataStorage.c                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: N/A                                                        */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Real time data collection storage in EEPROM  access routines.             */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <RomDataStorage.h>
#include <RamDataStorage.h>
#include <eeprom_controller.h>
#include <iface_rtos_start.h>

/*********************************************************************
* PUBLIC vStartRoutineDataSaving()
*---------------------------------------------------------------------
* Initiate routine data saving to EEPROM
*
* Inputs:
*   TBD
* Returns:
*   TBD
*
**********************************************************************/
void vStartRoutineDataSaving(st_RTDataCollection_t * pst_myRTDataCollection)
{
    BaseType_t sStatus;
    static stEEPROMAccess_t stMyEEPROMAccess;
    static QueueHandle_t stMyEEPROM_Queue_Hdl;
    
    stMyEEPROM_Queue_Hdl = sGetQueueHandle(EEPROM_ACCESS_QUEUE);
    
    stMyEEPROMAccess.ucOperationRequest = EEPROM_ROUTINE_DATA_WRITE;
    stMyEEPROMAccess.pvDataBuffer = pst_myRTDataCollection;
        
    sStatus = xQueueSendToBack(stMyEEPROM_Queue_Hdl, &stMyEEPROMAccess, NOWAIT);
    if( sStatus != pdPASS )
    {
        /* The send operation could not complete because the queue was full */
    }
    
}

/*********************************************************************
* PUBLIC vPerformRoutineDataSaving()
*---------------------------------------------------------------------
* Perform data saving to EEPROM
*
* Inputs:
*   TBD
* Returns: 
*   TBD
*
**********************************************************************/
void vPerformRoutineDataSaving(st_RTDataCollection_t * pst_myRTDataCollection)
{
    uint32_t uiDestAddress;            
    uint8_t * uiSourceAddress;
            
    /* P2 - Continuous Data 1 */     
    uiDestAddress = CONTINUOUS_DATA_START_ADDRESS;
    uiSourceAddress = (uint8_t *)(&pst_myRTDataCollection->stContinuousData);
    vEEPROM_WriteBlock(uiDestAddress,  uiSourceAddress, CONT_DATA_PAGE_1);              
       
    /* Wait 10ms */
     vTaskDelay(pdMS_TO_TICKS(TIME_10MS));        
    
    /* P3 - Continuous Data 2 */ 
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += CONT_DATA_PAGE_1;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, CONT_DATA_PAGE_2);
    
    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));     
    
    /* P4 - Histograms Data 1 */
    uiDestAddress = HISTOGRAMS_START_ADDRESS;
    uiSourceAddress = (uint8_t *)(&pst_myRTDataCollection->stHistograms);
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_1);
 
    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P5 - Histograms Data 2 */        
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_1;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_2);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
    
    /* P6 - Histograms Data 3 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_2;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_3);        

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
    
    /* P7 - Histograms Data 4 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_3;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_4);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P8 - Histograms Data 5 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_4;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_5);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P9 - Histograms Data 6 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_5;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_6);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P10 - Histograms Data 7 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_6;
    vEEPROM_WriteBlock(uiDestAddress , uiSourceAddress, HIST_DATA_PAGE_7);
    
     /* Wait 10ms */
     vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P11 - Histograms Data 8 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_7;
    vEEPROM_WriteBlock(uiDestAddress , uiSourceAddress, HIST_DATA_PAGE_8);
    
    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
    
    /* P12 - Histograms Data 9 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_8;
    vEEPROM_WriteBlock(uiDestAddress , uiSourceAddress, HIST_DATA_PAGE_9);
    
     /* Wait 10ms */
     vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P13 - Histograms Data 10 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_9;
    vEEPROM_WriteBlock(uiDestAddress , uiSourceAddress, HIST_DATA_PAGE_10);

    /* P14 - Histograms Data 11 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_10;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_11);
    
    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P15 - Histograms Data 12 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_11;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_12);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
    
    /* P16 - Histograms Data 13 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_12;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_13);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
    
    /* P17 - Histograms Data 14 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_13;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_14);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P18 - Histograms Data 15 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_14;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_15);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P19 - Histograms Data 16 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_15;
    vEEPROM_WriteBlock(uiDestAddress, uiSourceAddress, HIST_DATA_PAGE_16);

    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P20 - Histograms Data 17 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_16;
    vEEPROM_WriteBlock(uiDestAddress , uiSourceAddress, HIST_DATA_PAGE_17);
    
    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P21 - Histograms Data 18 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_17;
    vEEPROM_WriteBlock(uiDestAddress , uiSourceAddress, HIST_DATA_PAGE_18);
    
    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
    
    /* P22 - Histograms Data 19 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_18;
    vEEPROM_WriteBlock(uiDestAddress , uiSourceAddress, HIST_DATA_PAGE_19);
    
    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));

    /* P23 - Histograms Data 20 */
    uiDestAddress += PAGE_SIZE;
    uiSourceAddress += HIST_DATA_PAGE_19;
    vEEPROM_WriteBlock(uiDestAddress , uiSourceAddress, HIST_DATA_PAGE_20);
    
    /* Wait 10ms */
    vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
}

/*********************************************************************
* PUBLIC vPerformOperationalDataDataSaving()
*---------------------------------------------------------------------
* Perform data saving to EEPROM
*
* Inputs:
*   TBD
* Returns: 
*   TBD
*
**********************************************************************/
void vPerformOperationalDataDataSaving(st_RTDataCollection_t * pst_myRTDataCollection, eRTDC_Faults_t eMyRTDC_Faults)
{    
   static uint8_t  ucOverTempFltRecordCnt = 0;
   static uint8_t  ucAllOtherFltRecordCnt = 0;
   uint32_t uiDestAddress;
   uint8_t* uiSourceAddress;
   uint8_t  ucIndex;
   
   /* Source address is always the latest Operational Data Buffer */
   uiSourceAddress = (uint8_t *)(&pst_myRTDataCollection->stOperationalData);
   
   /* Select the destination address based on the Fault */
   if(eMyRTDC_Faults == RTDC_FUSE_BLOWN_FAULT)
   {       
       /* Only 1 record of Op Data */
       uiDestAddress = OP_DATA_FUSE_BLOWN_ADDRESS;       
   }
   else if (eMyRTDC_Faults == RTDC_COMM_ERROR_FAULT)
   {
       /* Only 1 record of Op Data */
       uiDestAddress = OP_DATA_COMM_ERROR_ADDRESS; 
   }
   else if (eMyRTDC_Faults == RTDC_CELL_OVER_TEMP_FAULT)
   {
       /* Keep 5 records of Op data for the last 5 OT faults */ 
       uiDestAddress = OP_DATA_OVER_TEMP_ADDRESS + (OP_DATA_PAGES_PER_RECORD * PAGE_SIZE * ucOverTempFltRecordCnt); 
       ucOverTempFltRecordCnt = (ucOverTempFltRecordCnt + 1) % OP_DATA_OVER_TEMP_RECORDS;
   }
   else 
   {
       /* Keep 10 records of Op data for the last 10 faults (all faults other than the above) */
       uiDestAddress = OP_DATA_ALL_OTHER_FLT_ADDRESS + (OP_DATA_PAGES_PER_RECORD * PAGE_SIZE * ucAllOtherFltRecordCnt); 
       ucAllOtherFltRecordCnt = (ucAllOtherFltRecordCnt + 1) % OP_DATA_ALL_OTHER_FLT_RECORDS;
   }      
   
   /* Save a record of Operational Data to EEPROM */      
   for(ucIndex=0;ucIndex<OP_DATA_PAGES_PER_RECORD;ucIndex++)
   {          
       /* Operational data */
       vEEPROM_WriteBlock(uiDestAddress,  uiSourceAddress, OP_DATA_DATA_PER_PAGE);
       
       /* Calculate next data cluster source and destination addresses */   
       uiDestAddress += PAGE_SIZE;
       uiSourceAddress += OP_DATA_DATA_PER_PAGE;
   
       /* Wait 10ms */
       vTaskDelay(pdMS_TO_TICKS(TIME_10MS));
   }            
}

/*********************************************************************
* PUBLIC vReadDataFromEEPROM()
*---------------------------------------------------------------------
* Reads data from EEPROM
*
* Inputs:
*   TBD
* Returns:
*   TBD
*
**********************************************************************/
void vReadDataFromEEPROM(void)
{   
    /* TBD */
}

/* Todo: Verify data written to EEPROM */
/******************************** End of File ********************************/