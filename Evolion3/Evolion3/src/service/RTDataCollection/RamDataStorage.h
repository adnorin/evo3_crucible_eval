#ifndef RAMDATASTORAGE_H_
#define RAMDATASTORAGE_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: RamDataStorage.h                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for RamDataStorage.c, which provides prototypes for           */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
#include <iface_rtos_start.h>

/* Voltage Histogram */
#define HIST_CELL_VOLTAGE_BOTTOM_BIN    0
#define HIST_CELL_VOLTAGE_TOP_BIN       51
#define HIST_CELL_VOLTAGE_INTERVAL      20
#define HIST_CELL_VOLTAGE_HALF_INTERVAL 10
#define HIST_CELL_VOLTAGE_MIN           3000
#define HIST_CELL_VOLTAGE_MAX           3999

/* Temperature Histogram */
#define HIST_CELL_TEMP_BOTTOM_BIN       0
#define HIST_CELL_TEMP_TOP_BIN          24
#define HIST_CELL_TEMP_INTERVAL         5
#define HIST_CELL_TEMP_MIN             -40
#define HIST_CELL_TEMP_MAX              74

/* Current Histogram */
#define HIST_BAT_CURR_BOTTOM_BIN       0
#define HIST_BAT_CURR_TOP_BIN          162
#define HIST_BAT_CURR_INTERVAL         1
#define HIST_BAT_CURR_MIN             -80
#define HIST_BAT_CURR_MAX              80

/* Operational Data */
#define OP_DATA_ONE_MINUTE_CNT         60
#define OP_DATA_NETWORK_VOLTAGE        0

/* EOC and EOD */
#define EOC_VOLTAGE_VALUE              4000
#define EOC_VOLTAGE_HYST_VALUE         3750
#define EOD_VOLTAGE_HYST_VALUE         3250
#define EOD_VOLTAGE_VALUE              3000
#define EOCD_NOT_DETECTED_STATE        0
#define EOCD_DETECTED_STATE            1
#define EOCD_RESOLUTION_STATE          2
#define EOCD_WAITING_PERIOD            600

/* Fault Counters */
#define FAULT_VEND_REG_OFFSET          32

/*
The SW shall store the following information in a format that can be read via the diagnostic SW:
�	Header of manufacturing data containing:
    o	Battery Module Part Number
    o	Battery Module Part Revision
    o	Battery Module Serial Number
    o	Embedded SW Version
    o	Embedded SW CRC: calculated using CRC-32 with 0x04C11DB7 / 0xEDB88320 (normal / reversed) generator polynomial
�	Continuously updated variables recording:
    o	Maximum Cell Temperature (one var. for all cells)
    o	Minimum Cell Temperature (one var. for all cells)
    o	Maximum Cell Voltage (one var. for all cells)
    o	Minimum Cell Voltage (one var. for all cells)
    o	Maximum Current (maximum current in charge)
    o	Minimum Current (maximum current in discharge)
    o	Ah throughput
    o	Total balancing ON time per cell
    o	Heater ON time
    o	Counter for each fault (incremented each time a given fault is activated)
�	Histograms containing:
    o	Time (in seconds) at cell voltage
        ?	Individual histogram per cell: 3000 to 4000mV in 10mV increments, 1 bin from 2999mV and below, 1 bin from 40001mV and above: 102 total bins per cell
    o	Time (in seconds) at cell temp
        ?	Individual histogram per cell: -40 to +75�C in 5�C increments, 1 bin from -41�C and below, 1 bin from +76�C and above: 25 total bins per cell
    o	Time (in seconds) at current
        ?	-80 to 80A in 1A increments, 1 bin from -81A and below, 1 bin from 81A and above: 162 total bins
    o	Counts of hitting end-of-charge first: 1 bin per cell
    o	Counts of hitting end-of-discharge first: 1 bin per cell
�	2 minutes of operational data preceding the most recent:
    o	Fuse Blown Alarm: 1x 2-minute log, overwritten by each new entry
    o	Communication Error Warning: 1x 2-minute log, overwritten by each new entry
    o	Cell Over-temperature Alarm: 5x 2-minute logs, oldest log overwritten by each new entry
    o	All other alarms: 10x 2-minute logs, oldest log overwritten by each new entry
    o	Each of these logs will consist of 1-second samples with the following data:
        ?	All cell voltages
        ?	Minimum, average, maximum cell temperatures
        ?	Battery voltage
        ?	Network voltage
        ?	Current
        ?	All faults
        ?	Heater status
        ?	FET statuses
*/

typedef struct
{
     char ushModulePartNumber[8];
     char ushModulePartRevision[1];
     char ushModulePartSerialNumber[9];
     char ushEmbeddedSwVersion[5];
     uint32_t ushEmbededSwCRC;
}stHeaderManufacturingData_t;


typedef struct
{
    uint16_t ushMaxCellTemperature;
    uint16_t ushMinCellTemperature;
    uint16_t ushMaxCellVoltage;
    uint16_t ushMinCellVoltage;
    uint16_t ushMaxCurrentCharge;
    uint16_t ushMaxCurrentDischarge;
    uint32_t uiAHThroughput;
    uint32_t uiTotalBalancingOnTime[NUM_OF_CELLS];
    uint32_t uiHeaterOnTime;
    uint32_t uiFaultCounter[64];
}stContinuousData_t;


typedef struct
{
    uint32_t uiCellSecondsAtVoltage[NUM_OF_CELLS][52]; /* 1 bin from 2999mV and below, bins for 3000 to 3999mV in 20mV increments, 1 bin from 4000mV and above */
    uint32_t uiCellSecondsAtTemp[8][25];    /* 1 bin from -41�C and below, bins for -40 to +74�C in 5�C increments, 1 bin from +75�C and above */
    uint32_t uiCellSecondsAtCurrent[163];    /* 1 bin from -81A and below, bins for -80 to 80A in 1A increments, 1 bin from 81A and above */
    uint32_t uiCellEndOfChargeFirstCnt[NUM_OF_CELLS];
    uint32_t uiCellEndOfDischargeFirstCnt[NUM_OF_CELLS];
}stHistograms_t;


typedef struct
{
    uint32_t uiDataCounter;         //4
    uint16_t ushCellVoltage[NUM_OF_CELLS];
    uint16_t ushCellMaxTemp;        //2
    uint16_t ushCellMinTemp;        //2
    uint16_t ushCellAverageTemp;    //2
    uint16_t ushBatteryVoltage;     //2
    uint16_t ushNetworkVoltage;     //2
    uint32_t uiBatteryCurrent;      //4
    uint32_t uiFaultRegisterLo;     //4
    uint32_t uiFaultRegisterHi;     //4
    eStates_machine_t eFetState;    //1 
    bool     bHeaterStatus;         //1
}stOperationalData_t;


typedef struct
{
    stHeaderManufacturingData_t stHeaderManufacturingData;
    stContinuousData_t stContinuousData;
    stHistograms_t stHistograms;
    stOperationalData_t stOperationalData[60];
}st_RTDataCollection_t;


/*********************************************************************
* PUBLIC vPerformRTDataCollection()
*---------------------------------------------------------------------
* Perform Real Time Data Collection.
*
* Inputs:
*   stBmsData_t - structure of BMS data
* Returns: None
*
**********************************************************************/
st_RTDataCollection_t * vPerformRTDataCollection(stBmsData_t * sstBmsData_RTDC);


/*********************************************************************
* PUBLIC vDetectEndOfChargeAndDischarge()
*---------------------------------------------------------------------
* Detect the end of charge and end of discharge. Log the cells that reach
* these states first.
*
* Inputs:
*       sstBmsData_EOCD - Battery data
* Returns: None
*
**********************************************************************/
void vDetectEndOfChargeAndDischarge(stBmsData_t * sstBmsData_EOCD);


/*********************************************************************
* PUBLIC vDetectFaultActivity()
*---------------------------------------------------------------------
* Updates the continuous data collection counter for faults. Updated
* when they happen.
*
* Inputs:
*       sstBmsData_Faults - Runtime battery data
*
* Returns: None
*
**********************************************************************/
void vDetectFaultActivity(stBmsData_t * sstBmsData_Faults);


/*********************************************************************
* PUBLIC vCalculateAmpHrThroughput()
*---------------------------------------------------------------------
* Accumulates raw current count taken at 100ms intervals.
*
* Inputs:
*   stBmsData_t - structure of BMS data
* Returns: None
*
**********************************************************************/
void vAccumulateAmpHrThroughput(stBmsData_t * sstBmsData_Ctpt);


/*********************************************************************
* PUBLIC vClearRTDataCollectionBuffers()
*---------------------------------------------------------------------
* Clear real time data collection buffers .
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vClearRTDataCollectionBuffers(void);

/*********************************************************************
* PUBLIC vDataLoggingDueToFaults()
*---------------------------------------------------------------------
* Updates the continuous data collection counter for faults. Updated
* when they happen.
*
* Inputs:
*       ushFaultNumber - Fault identifier as defined in faults.h
*       bMesaDefinedFault - true for Mesa defined fault, false for
*                           Vendor defined fault
* Returns: None
*
**********************************************************************/
void vDataLoggingDueToFaults(stBmsData_t * sstBmsData_Faults);

#endif /* RAMDATASTORAGE_H_ */