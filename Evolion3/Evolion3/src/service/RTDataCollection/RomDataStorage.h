#ifndef ROMDATASTORAGE_H_
#define ROMDATASTORAGE_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: RomDataStorage.h                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for RomDataStorage.c, which provides prototypes for           */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#include <RamDataStorage.h>


#define PAGE_SIZE               256

#define HEADER_PAGE             0   /* Requires 1  page  */
#define CONTINUOUS_DATA_PAGE    1   /* Requires 2  pages */
#define HISTOGRAMS_PAGE         3   /* Requires 20 pages */
#define OPERATIONAL_DATA_PAGE   23  /* Requires 15 pages per record */

#define HEADER_PAGE_START_ADDRESS      (0)
#define CONTINUOUS_DATA_START_ADDRESS  (CONTINUOUS_DATA_PAGE * PAGE_SIZE) 
#define HISTOGRAMS_START_ADDRESS       (HISTOGRAMS_PAGE * PAGE_SIZE) 
#define OPERATIONAL_DATA_START_ADDRESS (OPERATIONAL_DATA_PAGE * PAGE_SIZE) 

/* Continuous Data */
#define CONT_DATA_PAGE_1  76
#define CONT_DATA_PAGE_2  PAGE_SIZE

/* Histogram Data */
#define HIST_DATA_PAGE_1  PAGE_SIZE
#define HIST_DATA_PAGE_2  PAGE_SIZE
#define HIST_DATA_PAGE_3  PAGE_SIZE
#define HIST_DATA_PAGE_4  PAGE_SIZE
#define HIST_DATA_PAGE_5  PAGE_SIZE
#define HIST_DATA_PAGE_6  PAGE_SIZE
#define HIST_DATA_PAGE_7  PAGE_SIZE
#define HIST_DATA_PAGE_8  PAGE_SIZE
#define HIST_DATA_PAGE_9  PAGE_SIZE
#define HIST_DATA_PAGE_10 PAGE_SIZE
#define HIST_DATA_PAGE_11 PAGE_SIZE
#define HIST_DATA_PAGE_12 96
#define HIST_DATA_PAGE_13 PAGE_SIZE
#define HIST_DATA_PAGE_14 PAGE_SIZE
#define HIST_DATA_PAGE_15 PAGE_SIZE
#define HIST_DATA_PAGE_16 32
#define HIST_DATA_PAGE_17 PAGE_SIZE
#define HIST_DATA_PAGE_18 PAGE_SIZE
#define HIST_DATA_PAGE_19 140
#define HIST_DATA_PAGE_20 112

/* Operational Data */
#define OP_DATA_FUSE_BLOWN_RECORDS      1
#define OP_DATA_COMM_ERROR_RECORDS      1
#define OP_DATA_OVER_TEMP_RECORDS       5
#define OP_DATA_ALL_OTHER_FLT_RECORDS   10
#define OP_DATA_LENGTH                  56
#define OP_DATA_DATA_PER_PAGE           (OP_DATA_LENGTH * 4)
#define OP_DATA_PAGES_PER_RECORD        15
#define OP_DATA_FUSE_BLOWN_ADDRESS      (OPERATIONAL_DATA_START_ADDRESS)
#define OP_DATA_COMM_ERROR_ADDRESS      (OP_DATA_FUSE_BLOWN_ADDRESS + (PAGE_SIZE * OP_DATA_PAGES_PER_RECORD))
#define OP_DATA_OVER_TEMP_ADDRESS       (OP_DATA_COMM_ERROR_ADDRESS + (PAGE_SIZE * OP_DATA_PAGES_PER_RECORD))
#define OP_DATA_ALL_OTHER_FLT_ADDRESS   (OP_DATA_OVER_TEMP_ADDRESS + (PAGE_SIZE * OP_DATA_PAGES_PER_RECORD * 5))


/* EEPROM Access Request Types */
#define EEPROM_UNUSED_VALUE            0
#define EEPROM_ROUTINE_DATA_WRITE      1
#define EEPROM_OPERATIONAL_DATA_WRITE  2
#define EEPROM_DATA_READ               3


/*********************************************************************
* PUBLIC vStartRoutineDataSaving()
*---------------------------------------------------------------------
* Initiate routine data saving to EEPROM
*
* Inputs:
*   TBD
* Returns:
*   TBD
*
**********************************************************************/
void vStartRoutineDataSaving(st_RTDataCollection_t * pst_myRTDataCollection);


/*********************************************************************
* PUBLIC vPerformRoutineDataSaving()
*---------------------------------------------------------------------
* Perform data saving to EEPROM
*
* Inputs:
*   TBD
* Returns:
*   TBD
*
**********************************************************************/
void vPerformRoutineDataSaving(st_RTDataCollection_t * pst_myRTDataCollection);


/*********************************************************************
* PUBLIC vPerformOperationalDataDataSaving()
*---------------------------------------------------------------------
* Perform data saving to EEPROM
*
* Inputs:
*   TBD
* Returns:
*   TBD
*
**********************************************************************/
void vPerformOperationalDataDataSaving(st_RTDataCollection_t * pst_myRTDataCollection, eRTDC_Faults_t eMyRTDC_Faults);


/*********************************************************************
* PUBLIC vReadDataFromEEPROM()
*---------------------------------------------------------------------
* Reads data from EEPROM
*
* Inputs:
*   TBD
* Returns:
*   TBD
*
**********************************************************************/
void vReadDataFromEEPROM(void);

#endif /* ROMDATASTORAGE_H_ */