/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: RamDataStorage.c                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Real time data collection buffers (RAM) and access routines.              */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <RamDataStorage.h>
#include <RomDataStorage.h>
#include <fets.h>
#include <faults.h>

st_RTDataCollection_t st_RTDataCollection;
static uint8_t ucRoundIdx = 0;

/*************************/
/*   Private Functions   */
/*************************/
/*********************************************************************
* PRIVATE vUpdateFaultCounter()
*---------------------------------------------------------------------
* Updates the continuous data collection counter for faults. Updated
* when they happen.
*
* Inputs:
*       ushFaultNumber - Fault identifier as defined in faults.h
*       bMesaDefinedFault - true for Mesa defined fault, false for 
*                           Vendor defined fault
* Returns: None
*
**********************************************************************/
static void vUpdateFaultCounter(uint32_t uiFaultNumbers, bool bMesaDefinedFault)
{
    uint16_t ushFltIdx;   

    /* Update index by adding offset if it is a vendor defined fault. No offset needed for MESA defined faults */
    for(ushFltIdx=0; ushFltIdx<32; ushFltIdx++)
    {
        if((uiFaultNumbers >> ushFltIdx) && 0x00000001)
        {
            if(bMesaDefinedFault == true)
            {
                /* Increment fault counter */
                st_RTDataCollection.stContinuousData.uiFaultCounter[ushFltIdx]++;
            }
            else
            {
                /* Increment fault counter */
                st_RTDataCollection.stContinuousData.uiFaultCounter[ushFltIdx + FAULT_VEND_REG_OFFSET]++;
            }
        }
    }    
}

/*************************/
/* Public Functions      */
/*************************/
/*********************************************************************
* PUBLIC vDataLoggingDueToFaults()
*---------------------------------------------------------------------
* Updates the continuous data collection counter for faults. Updated
* when they happen.
*
* Inputs:
*       ushFaultNumber - Fault identifier as defined in faults.h
*       bMesaDefinedFault - true for Mesa defined fault, false for
*                           Vendor defined fault
* Returns: None
*
**********************************************************************/
void vDataLoggingDueToFaults(stBmsData_t * sstBmsData_Faults)
{    
    static uint32_t uiMyFaultsMesa_Prev = 0;
    static uint32_t uiMyFaultsVend_Prev = 0;
    static stEEPROMAccess_t stMyEEPROMAccess;     
    uint32_t uiNewlyDetectedFaults;    
    BaseType_t sStatus;
    QueueHandle_t stMyEEPROM_Queue_Hdl;
       
            
    /* Retrieve queue handle for EEPROM access */
    stMyEEPROM_Queue_Hdl = sGetQueueHandle(EEPROM_ACCESS_QUEUE);

    /* Start operational data logging due to special faults */
   
    /* Mesa defined faults */
    uiNewlyDetectedFaults  = sstBmsData_Faults->uiFaultFlagsEvt1 & ~uiMyFaultsMesa_Prev;
        
    /* Save faults for next time */
    uiMyFaultsMesa_Prev = sstBmsData_Faults->uiFaultFlagsEvt1;
        
    if((uiNewlyDetectedFaults & (1<<FLT_COMMUNICATION_ERROR)) != 0)
    {
        /* Queue fault to be written to EEPROM */
        stMyEEPROMAccess.uiFaultsMesa = (1<<FLT_COMMUNICATION_ERROR);
        stMyEEPROMAccess.ucOperationRequest = EEPROM_OPERATIONAL_DATA_WRITE;
        stMyEEPROMAccess.pvDataBuffer = st_RTDataCollection.stOperationalData;
        sStatus = xQueueSendToBack(stMyEEPROM_Queue_Hdl, &stMyEEPROMAccess, NOWAIT);
        if( sStatus != pdPASS )
        {
            /* The send operation could not complete because the queue was full */
        }
            
        /* Clear Communication Error Bit From Variable */
        uiNewlyDetectedFaults &= ~(1<<FLT_COMMUNICATION_ERROR);
    }

    if ((uiNewlyDetectedFaults & (1<<FLT_CELL_OVER_TEMP_ALARM)) != 0)
    {
        /* Queue fault to be written to EEPROM */
        stMyEEPROMAccess.uiFaultsMesa = (1<<FLT_CELL_OVER_TEMP_ALARM);
        stMyEEPROMAccess.ucOperationRequest = EEPROM_OPERATIONAL_DATA_WRITE;
        stMyEEPROMAccess.pvDataBuffer = st_RTDataCollection.stOperationalData;
        sStatus = xQueueSendToBack(stMyEEPROM_Queue_Hdl, &stMyEEPROMAccess, NOWAIT);
        if( sStatus != pdPASS )
        {
            /* The send operation could not complete because the queue was full */
        }
            
        /* Clear Communication Error Bit From Variable */
        uiNewlyDetectedFaults &= ~(1<<FLT_CELL_OVER_TEMP_ALARM);
    }

    /* All Other Alarms */
    if((uiNewlyDetectedFaults & FLT_MESA_ALARM_MASK) != 0)
    {
        /* Queue fault to be written to EEPROM */
        stMyEEPROMAccess.uiFaultsMesa = uiNewlyDetectedFaults;
        stMyEEPROMAccess.ucOperationRequest = EEPROM_OPERATIONAL_DATA_WRITE;
        stMyEEPROMAccess.pvDataBuffer = st_RTDataCollection.stOperationalData;
        sStatus = xQueueSendToBack(stMyEEPROM_Queue_Hdl, &stMyEEPROMAccess, NOWAIT);
        if( sStatus != pdPASS )
        {
            /* The send operation could not complete because the queue was full */
        }
    }
    
    /* Vend defined faults */
    uiNewlyDetectedFaults  = sstBmsData_Faults->uiFaultFlagsEvtVnd1 & ~uiMyFaultsVend_Prev;
        
    /* Save faults for next time */
    uiMyFaultsVend_Prev =  sstBmsData_Faults->uiFaultFlagsEvtVnd1;
        
    if (uiNewlyDetectedFaults & (1<<FLT_FUSE_BLOWN_ALARM))
    {
        // Queue fault to be written to EEPROM
        stMyEEPROMAccess.uiFaultsVend = (1<<FLT_FUSE_BLOWN_ALARM);
        stMyEEPROMAccess.ucOperationRequest = EEPROM_OPERATIONAL_DATA_WRITE;
        stMyEEPROMAccess.pvDataBuffer = st_RTDataCollection.stOperationalData;
        sStatus = xQueueSendToBack(stMyEEPROM_Queue_Hdl, &stMyEEPROMAccess, NOWAIT);
        if( sStatus != pdPASS )
        {
            // The send operation could not complete because the queue was full
        }
            
        //Clear Fuse Blown Alarm Bit From Variable
        uiNewlyDetectedFaults &= ~(1<<FLT_FUSE_BLOWN_ALARM);
    }

    /* All Other Alarms */
    if((uiNewlyDetectedFaults & FLT_VEND_ALARM_MASK) != 0)
    {
        /* Queue fault to be written to EEPROM */
        stMyEEPROMAccess.uiFaultsVend = uiNewlyDetectedFaults;
        stMyEEPROMAccess.ucOperationRequest = EEPROM_OPERATIONAL_DATA_WRITE;
        stMyEEPROMAccess.pvDataBuffer = st_RTDataCollection.stOperationalData;
        sStatus = xQueueSendToBack(stMyEEPROM_Queue_Hdl, &stMyEEPROMAccess, NOWAIT);
        if( sStatus != pdPASS )
        {
            /* The send operation could not complete because the queue was full */
        }
    }    
}

/*********************************************************************
* PUBLIC vPerformRTDataCollection()
*---------------------------------------------------------------------
* Perform Real Time Data Collection.
*
* Inputs:
*   stBmsData_t - structure of BMS data
* Returns: None
*
**********************************************************************/
st_RTDataCollection_t * vPerformRTDataCollection(stBmsData_t * sstBmsData_RTDC)
{
    static uint16_t ushIdx;
    static uint16_t ushBinNumber;
    static uint32_t uiTempAccumulator;
    SemaphoreHandle_t spSemMutex_EEPROM_Access_Lcl_Hdl;
    
    spSemMutex_EEPROM_Access_Lcl_Hdl = sGetSemaphoreHandle(EEPROM_ACCESS);

    if( xSemaphoreTake(spSemMutex_EEPROM_Access_Lcl_Hdl, pdMS_TO_TICKS(200)) == pdTRUE )
    {   
        /*******************************************************************/
        /* Continuous Data */
        /*******************************************************************/

        /* Min and Max Cell Temperatures */
        st_RTDataCollection.stContinuousData.ushMaxCellTemperature = sstBmsData_RTDC->shCellTemperatureMax;
        st_RTDataCollection.stContinuousData.ushMinCellTemperature = sstBmsData_RTDC->shCellTemperatureMin;

        /* Min and Max Cell Voltages */
        st_RTDataCollection.stContinuousData.ushMaxCellVoltage = sstBmsData_RTDC->ushMaxCellV;
        st_RTDataCollection.stContinuousData.ushMinCellVoltage = sstBmsData_RTDC->ushMinCellV;

        /* Max Charge and Max Discharge current */
        st_RTDataCollection.stContinuousData.ushMaxCurrentCharge = sstBmsData_RTDC->shMaxChargeCurrent;
        st_RTDataCollection.stContinuousData.ushMaxCurrentDischarge = sstBmsData_RTDC->shMaxDischargeCurrent;

        /* Throughput Amp-Hr raw accumulator count 100ms samples of (0.01amp * 100) scaling */
        st_RTDataCollection.stContinuousData.uiAHThroughput = sstBmsData_RTDC->uiCurrentThroughputRawCount;

        /* Add a sec count to heater counter if enabled */
        if(sstBmsData_RTDC->bHeaterEnable == true)
        {
            st_RTDataCollection.stContinuousData.uiHeaterOnTime++;
        }

        /* Add a sec of balancing time to each cell counter */
        for(ushIdx = 0; ushIdx < NUM_OF_CELLS; ushIdx++)
        {
            st_RTDataCollection.stContinuousData.uiTotalBalancingOnTime[ushIdx] += (sstBmsData_RTDC->ushCellsToBalance >> ushIdx) & 0x01;
        }

        /* Fault counts are updated by vDetectFaultActivity() which is called from the 100ms task */

        /*******************************************************************/
        /* Histograms */
        /*******************************************************************/

        /* Cell Voltages */
        for(ushIdx = 0; ushIdx < NUM_OF_CELLS; ushIdx++)
        {
            /* 1 bin from 2999mV and below */
            if(sstBmsData_RTDC->ushCellVoltage[ushIdx] < HIST_CELL_VOLTAGE_MIN)
            {
                st_RTDataCollection.stHistograms.uiCellSecondsAtVoltage[ushIdx][HIST_CELL_VOLTAGE_BOTTOM_BIN]++;
            }

            /* 1 bin from 4000mV and above */
            else if (sstBmsData_RTDC->ushCellVoltage[ushIdx] > HIST_CELL_VOLTAGE_MAX)
            {
                st_RTDataCollection.stHistograms.uiCellSecondsAtVoltage[ushIdx][HIST_CELL_VOLTAGE_TOP_BIN]++;
            }

            /* bins for 3000 to 3999mV in 20mV increments */
            else
            {
                ushBinNumber = ((sstBmsData_RTDC->ushCellVoltage[ushIdx] - HIST_CELL_VOLTAGE_MIN) / HIST_CELL_VOLTAGE_INTERVAL) + 1;
                st_RTDataCollection.stHistograms.uiCellSecondsAtVoltage[ushIdx][ushBinNumber]++;
            }
        }

        /* Cell Temperatures */
        for(ushIdx = 0; ushIdx < NUM_TEMP_SENSORS; ushIdx++)
        {
            /* 1 bin from -41�C and below */
            if(sstBmsData_RTDC->shTemperatureSensor[ushIdx] < HIST_CELL_TEMP_MIN)
            {
                st_RTDataCollection.stHistograms.uiCellSecondsAtTemp[ushIdx][HIST_CELL_TEMP_BOTTOM_BIN]++;
            }

            /* 1 bin from +75�C and above */
            else if (sstBmsData_RTDC->shTemperatureSensor[ushIdx] > HIST_CELL_TEMP_MAX)
            {
                st_RTDataCollection.stHistograms.uiCellSecondsAtTemp[ushIdx][HIST_CELL_TEMP_TOP_BIN]++;
            }

            /* bins for -40 to +74�C in 5�C increments */
            else
            {
                ushBinNumber = (uint16_t)(((sstBmsData_RTDC->shTemperatureSensor[ushIdx] - HIST_CELL_TEMP_MIN) / HIST_CELL_TEMP_INTERVAL) + 1);
                st_RTDataCollection.stHistograms.uiCellSecondsAtTemp[ushIdx][ushBinNumber]++;
            }
        }

        /* Battery Current */
        /* 1 bin from -81A and below*/
        if(sstBmsData_RTDC->shMeasuredCurrent < HIST_BAT_CURR_MIN)
        {
            st_RTDataCollection.stHistograms.uiCellSecondsAtCurrent[HIST_BAT_CURR_BOTTOM_BIN]++;
        }

        /* 1 bin from 81A and above */
        else if (sstBmsData_RTDC->shMeasuredCurrent > HIST_BAT_CURR_MAX)
        {
            st_RTDataCollection.stHistograms.uiCellSecondsAtCurrent[HIST_BAT_CURR_TOP_BIN]++;
        }

        /* bins for -80 to 80A in 1A increments */
        else
        {
            ushBinNumber = (uint16_t)(sstBmsData_RTDC->shMeasuredCurrent - HIST_BAT_CURR_MIN) + 1;
            st_RTDataCollection.stHistograms.uiCellSecondsAtCurrent[ushBinNumber]++;
        }

        /*******************************************************************/
        /* Operational Data */
        /*******************************************************************/
        /* Increment Data Counter */
        st_RTDataCollection.stOperationalData[ucRoundIdx].uiDataCounter++;
    
        /* Cell voltages */
        for(ushIdx = 0; ushIdx < NUM_OF_CELLS; ushIdx++)
        {
            st_RTDataCollection.stOperationalData[ucRoundIdx].ushCellVoltage[ushIdx] = sstBmsData_RTDC->ushCellVoltage[ushIdx];
        }

        /* Cell temperatures */
        st_RTDataCollection.stOperationalData[ucRoundIdx].ushCellMaxTemp = st_RTDataCollection.stContinuousData.ushMaxCellTemperature;
        st_RTDataCollection.stOperationalData[ucRoundIdx].ushCellMinTemp = st_RTDataCollection.stContinuousData.ushMinCellTemperature;

        /* Average temperature */
        uiTempAccumulator = 0;
        for(ushIdx = 0; ushIdx < NUM_TEMP_SENSORS; ushIdx++)
        {
            uiTempAccumulator += (uint32_t)sstBmsData_RTDC->shTemperatureSensor[ushIdx];
        }
        st_RTDataCollection.stOperationalData[ucRoundIdx].ushCellAverageTemp = (uint16_t)(uiTempAccumulator/NUM_TEMP_SENSORS);

        /* Battery voltage */
        st_RTDataCollection.stOperationalData[ucRoundIdx].ushBatteryVoltage = sstBmsData_RTDC->ushSumOfCellsVoltage;

        /* Network voltage */
        st_RTDataCollection.stOperationalData[ucRoundIdx].ushNetworkVoltage = sstBmsData_RTDC->ushAnalogVoltage[OP_DATA_NETWORK_VOLTAGE];

        /* Faults Status */
        st_RTDataCollection.stOperationalData[ucRoundIdx].uiFaultRegisterLo = sstBmsData_RTDC->uiFaultFlagsEvt1;
        st_RTDataCollection.stOperationalData[ucRoundIdx].uiFaultRegisterHi = sstBmsData_RTDC->uiFaultFlagsEvtVnd1;

        /* FETs Status */
        st_RTDataCollection.stOperationalData[ucRoundIdx].eFetState = sstBmsData_RTDC->eFetManagementState;

        /* Heater Status */
        st_RTDataCollection.stOperationalData[ucRoundIdx].bHeaterStatus = sstBmsData_RTDC->bHeaterEnable;

        /* Increment the round buffer (60 secs) */
        ucRoundIdx = (ucRoundIdx + 1) % OP_DATA_ONE_MINUTE_CNT;
        
        xSemaphoreGive( spSemMutex_EEPROM_Access_Lcl_Hdl );
    }   
    else
    {
        /* We could not get the semaphore, EEPROM access is still using the data buffers */
    } 

    return &st_RTDataCollection;
}


/*********************************************************************
* PUBLIC vClearRTDataCollectionBuffers()
*---------------------------------------------------------------------
* Clear real time data collection buffers .
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vClearRTDataCollectionBuffers(void)
{
    memset(&st_RTDataCollection, 0, sizeof(st_RTDataCollection_t));
}


/*********************************************************************
* PUBLIC vDetectEndOfChargeAndDischarge()
*---------------------------------------------------------------------
* Detect the end of charge and end of discharge. Log the cells that reach
* these states first.
*
* Inputs: 
*       sstBmsData_EOCD - Battery data
* Returns: None
*
**********************************************************************/
void vDetectEndOfChargeAndDischarge(stBmsData_t * sstBmsData_EOCD)
{
    static uint8_t ucEOCState = EOCD_NOT_DETECTED_STATE;
    static uint8_t ucEODState = EOCD_NOT_DETECTED_STATE;
    static uint16_t ushEOCBlankingCnt = 0;
    static uint16_t ushEODBlankingCnt = 0;    
    bool bCellEOCEvent = false;
    bool bCellEODEvent = false;

    /*******************************/
    /* End of charge */
    /*******************************/
    /* Check max cell voltage to see if any has reached EOC */
    if(sstBmsData_EOCD->ushMaxCellV >= EOC_VOLTAGE_VALUE)
    {
        bCellEOCEvent = true;
    }
    else if(sstBmsData_EOCD->ushMaxCellV < EOC_VOLTAGE_HYST_VALUE)
    {
        bCellEOCEvent = false;
    }

    /* End of charge state machine */
    switch(ucEOCState)
    {
        case EOCD_NOT_DETECTED_STATE:
        if(bCellEOCEvent == true)
        {
            /* Increment histogram count for the appropriate cell only once per event */
            st_RTDataCollection.stHistograms.uiCellEndOfChargeFirstCnt[sstBmsData_EOCD->ucMaxCellVIdx]++;
            ucEOCState = EOCD_DETECTED_STATE;
            ushEOCBlankingCnt = 0;
        }
        break;

        case EOCD_DETECTED_STATE:
        /* Wait for the time period to expire before looking for the max voltage to become lower than hysteresis value */
        if(++ushEOCBlankingCnt >= EOCD_WAITING_PERIOD)
        {
            ucEOCState = EOCD_RESOLUTION_STATE;
        }
        break;

        case EOCD_RESOLUTION_STATE:
        /* The event will be considered over after the max voltage becomes lower the hysteresis value */
        if(bCellEOCEvent == false)
        {
            ucEOCState = EOCD_NOT_DETECTED_STATE;
        }
        break;

        default:
        break;
    }


    /********************************/
    /* End of discharge */
    /********************************/
    /* Check min cell voltage to see if any has reached EOD */
    if(sstBmsData_EOCD->ushMinCellV <= EOD_VOLTAGE_VALUE)
    {
        bCellEODEvent = true;
    }
    else if(sstBmsData_EOCD->ushMinCellV > EOD_VOLTAGE_HYST_VALUE)
    {
        bCellEODEvent = false;
    }

    /* End of discharge state machine */
    switch(ucEODState)
    {
        case EOCD_NOT_DETECTED_STATE:
        if(bCellEODEvent == true)
        {
            /* Increment histogram count for the appropriate cell only once per event */
            st_RTDataCollection.stHistograms.uiCellEndOfDischargeFirstCnt[sstBmsData_EOCD->ucMinCellVIdx]++;
            ucEODState = EOCD_DETECTED_STATE;
            ushEODBlankingCnt = 0;
        }
        break;

        case EOCD_DETECTED_STATE:
        /* Wait for the time period to expire before looking for the max voltage to become lower than hysteresis value */
        if(++ushEODBlankingCnt >= EOCD_WAITING_PERIOD)
        {
            ucEODState = EOCD_RESOLUTION_STATE;
        }
        break;

        case EOCD_RESOLUTION_STATE:
        /* The event will be considered over after the max voltage becomes lower the hysteresis value */
        if(bCellEODEvent == false)
        {
            ucEODState = EOCD_NOT_DETECTED_STATE;
        }
        break;

        default:
        break;
    }
}


/*********************************************************************
* PUBLIC vDetectFaultActivity()
*---------------------------------------------------------------------
* Updates the continuous data collection counter for faults. Updated
* when they happen.
*
* Inputs:
*       sstBmsData_Faults - Runtime battery data
*       
* Returns: None
*
**********************************************************************/
void vDetectFaultActivity(stBmsData_t * sstBmsData_Faults)
{

    static uint32_t uiMesaFaultsPrev = 0;
    static uint32_t uiVendFaultsPrev = 0;

    uint32_t uiNewlyDetectedFaults;

    /* Check for newly activated Mesa faults */
    uiNewlyDetectedFaults = sstBmsData_Faults->uiFaultFlagsEvt1 & ~uiMesaFaultsPrev;

    /* If there are new faults update counter */
    if(uiNewlyDetectedFaults > 0)
    {
        vUpdateFaultCounter(uiNewlyDetectedFaults, true);
    }

    /* Save current faults into previous fault variable */
    uiMesaFaultsPrev = sstBmsData_Faults->uiFaultFlagsEvt1;


    /* Check for newly activated Vendor faults */
    uiNewlyDetectedFaults = sstBmsData_Faults->uiFaultFlagsEvtVnd1 & ~uiVendFaultsPrev;

    /* If there are new faults update counter */
    if(uiNewlyDetectedFaults > 0)
    {
        vUpdateFaultCounter(uiNewlyDetectedFaults, false);
    }

    /* Save current faults into previous fault variable */
    uiVendFaultsPrev = sstBmsData_Faults->uiFaultFlagsEvtVnd1;
}

/*********************************************************************
* PUBLIC vCalculateAmpHrThroughput()
*---------------------------------------------------------------------
* Accumulates raw current count taken at 100ms intervals.
*
* Inputs:
* stBmsData_t - structure of BMS data
* Returns: None
*
**********************************************************************/
void vAccumulateAmpHrThroughput(stBmsData_t * sstBmsData_Ctpt)
{
    if(sstBmsData_Ctpt->shMeasuredCurrent < 0)
    {
        sstBmsData_Ctpt->uiCurrentThroughputRawCount += (uint32_t)(-1 * (sstBmsData_Ctpt->shMeasuredCurrent));
    }
    else
    {
        sstBmsData_Ctpt->uiCurrentThroughputRawCount += (uint32_t)sstBmsData_Ctpt->shMeasuredCurrent;
    }
}

/******************************** End of File ********************************/
