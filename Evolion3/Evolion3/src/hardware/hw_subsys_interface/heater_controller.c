/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: heater_controller.c                                            */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  Controls GPIO to turn Heater On/Off                                      */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <conf_gpio.h>
#include <heater_controller.h>

/*********************************************************************
* PUBLIC vTurnHeaterOnOFF()
*---------------------------------------------------------------------
* Heater control
*
* Inputs:
*     ucHeaterStates - Bit encoded variable containing status of heater GPIO
* Returns: None
*
**********************************************************************/
void vTurnHeaterOnOFF(uint8_t ucHeaterStates)
{
    static uint8_t sucPrevHeaterStates = 0x55;
    
    /* Only update GPIOs states if the value has changed */
    if(ucHeaterStates != sucPrevHeaterStates)
    {
        /* Turn Heater GPIO ON/OFF based on input */
        if((HEATER_ON_ENABLE_MASK & ucHeaterStates)!=0)
        {
            /* HEATER ON */
            OUTPUT_SET_HIGH(HEATER_ENABLE_PIN);
        }
        else
        {
            /* HEATER OFF */
            OUTPUT_SET_LOW(HEATER_ENABLE_PIN);
        }
    }
    /* Save last heater State */
    sucPrevHeaterStates = ucHeaterStates;
}

/******************************** End of File ********************************/