/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: gpio_controller.c                                              */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  Reads GPIO input status for diagnostics.                                 */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <conf_gpio.h>
#include <gpio_controller.h>

/*********************************************************************
* PUBLIC vSetGPIOState()
*---------------------------------------------------------------------
* GPIO management routines
*
* Inputs:
*     ushGPIOStates - Bit encoded variable containing status of the GPIOs
* Returns: None
*
**********************************************************************/
void vSetGPIOState(uint16_t ushGPIOStates)
{
	 static uint16_t sushPrevGPIOStates = 0x00;
	 
	  /* Only update GPIOs states if the value has changed */
	  if(ushGPIOStates != sushPrevGPIOStates)
	  {
		    /* Turn GPIOs ON/OFF based on input */
				 /* MC33771 Reset */
				if((GPIO_MC33771_RESET_MASK & ushGPIOStates)!=0)
				{
				   /* MC33771 Reset ON */
				   OUTPUT_SET_HIGH(MC33771_RESET_PIN);
				}
				else
				{
				   /* MC33771 Reset OFF */
				   OUTPUT_SET_LOW(MC33771_RESET_PIN);
				}
			   
				 /* Network Measurement */
			    if((GPIO_NETWORK_MEASUREMENT_MASK & ushGPIOStates)!=0)
			    {
				    /* Network Measurement ON */
				    OUTPUT_SET_HIGH(NETWORK_MEASUREMENT_ENABLE_PIN);
			    }
			    else
			    {
				    /* Network Measurement OFF */
				    OUTPUT_SET_LOW(NETWORK_MEASUREMENT_ENABLE_PIN);
			    }
				
				 /* Sleep Power Down */
				 if((GPIO_SLEEP_PWDN_12V_MASK & ushGPIOStates)!=0)
				 {
					 /* Sleep Power Down ON */
					 OUTPUT_SET_HIGH(SLEEP_PWDN_12V_COMP_PIN);
				 }
				 else
				 {
					 /* Sleep Power Down OFF */
					 OUTPUT_SET_LOW(SLEEP_PWDN_12V_COMP_PIN);
				 }
				
				/* Over Current Reset */
				if((GPIO_OVER_CURRENT_LATCH_RESET_MASK & ushGPIOStates)!=0)
				{
					/* Over Current Reset ON */
					OUTPUT_SET_HIGH(OVER_CURRENT_LATCH_RESET_PIN);
				}
				else
				{
					/* Over Current Reset OFF */
					OUTPUT_SET_LOW(OVER_CURRENT_LATCH_RESET_PIN);
				}
			
				/* Anti Theft */
				if((GPIO_ANTI_THEFT_MASK & ushGPIOStates)!=0)
				{
					/* Anti Theft ON */
					OUTPUT_SET_HIGH(ANTI_THEFT_OPTION_OUT_PIN);
				}
				else
				{
					/* Anti Theft OFF */
					OUTPUT_SET_LOW(ANTI_THEFT_OPTION_OUT_PIN);
				}	
					  
		  		/* Fet Self Test */
		  		if((GPIO_FET_SELF_TEST_MASK & ushGPIOStates)!=0)
		  		{
			  		/*Fet Self Test ON */
				  	OUTPUT_SET_HIGH(FET_SELF_TEST_PIN);
		  		}
		  		else
		  		{
			  		/* Fet Self Test OFF */
			  		OUTPUT_SET_LOW(FET_SELF_TEST_PIN);
		  		}
				  /* Fet Self Test Voltage  */
		  		if((GPIO_FET_SELF_TEST_VOLTAGE_MASK & ushGPIOStates)!=0)
		  		{
			  		/* Fet Self Test Voltage ON */
				  	OUTPUT_SET_HIGH(FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_PIN);
		  		}
		  		else
		  		{
			  		/* Fet Self Test Voltage OFF */
			  		OUTPUT_SET_LOW(FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_PIN);
		  		}
				  
				  /*HMI Led */
		  		if((GPIO_HMI_LED_MASK & ushGPIOStates)!=0)
		  		{
			  		/* HMI Led ON */
				  	OUTPUT_SET_HIGH(HMI_LED_DRIVER_ENA_PIN);
		  		}
		  		else
		  		{
			  		/* HMI Led OFF */
			  		OUTPUT_SET_LOW(HMI_LED_DRIVER_ENA_PIN);
		  		}
	  }
	/* Save last GPIO States */
	sushPrevGPIOStates = ushGPIOStates;

}

/******************************** End of File ********************************/
