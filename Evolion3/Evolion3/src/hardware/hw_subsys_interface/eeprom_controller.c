/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: eeprom_controller.c      `                                     */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  This file implements functions to interface with the SPI EEPROM          */
/*  Read, write, Erase, initialize and control functions                     */
/*                                                                           */
/*****************************************************************************/
#include <bat_cell_controller_ic.h>
#include <iface_spi_dma.h>
#include <delay.h>
#include <eeprom_controller.h>
#include <string.h>

/* SPI Buffers */
static uint8_t ucEEPROM_RxSPIBuffer[EEPROM_MAX_BUFFER_SIZE];
static uint8_t ucEEPROM_TxSPIBuffer[EEPROM_MAX_BUFFER_SIZE];

/* Data Rx Complete Flag */
volatile bool bEEPROM_DataRxComplete;
    
    
/*****************************/
/* Private Functions         */
/*****************************/
/*********************************************************************
* PRIVATE svEEPROM_SpiRXCallback()
*---------------------------------------------------------------------
* Callback for Rx Eeprom Spi transactions.
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svEEPROM_SpiRXCallback(void)
{
    bEEPROM_DataRxComplete = true;  
}

/*********************************************************************
* PRIVATE svEEPROM_SpiTXCallback()
*---------------------------------------------------------------------
* Callback for Tx Eeprom Spi transactions.
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svEEPROM_SpiTXCallback(void)
{
    ;
}


/*****************************/
/* Public Functions         */
/*****************************/
/*********************************************************************
* PUBLIC vEEPROM_Init()
*---------------------------------------------------------------------
* Eeprom Spi comm init
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vEEPROM_Init(void)
{
    /* Configure SPI Port */
    bConfigureSpiPort(SPI_PORT_2);
    
    /* Register Callbacks for SPI RX & TX */
    vSetSpiRxCallback(SPI_PORT_2, svEEPROM_SpiRXCallback);
    vSetSpiTxCallback(SPI_PORT_2, svEEPROM_SpiTXCallback);  
    
    /* Save ID Word used to verify SPI EEPROM comm */
    vEEPROM_Write_ID(0,EEPROM_ID_0);
    delay_ms(10);
    
    vEEPROM_Write_ID(1,EEPROM_ID_1);
    delay_ms(10);
    
    vEEPROM_Write_ID(2,EEPROM_ID_2);
    delay_ms(10);
    
    vEEPROM_Write_ID(3,EEPROM_ID_3);
    delay_ms(10);    
}


/*********************************************************************
* PUBLIC vEEPROM_WriteEnable()
*---------------------------------------------------------------------
* Send Write Enable command
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vEEPROM_WriteEnable(void)
{
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_ENABLE;  /* Write Command */
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, EEPROM_ENABLE_LEN);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, EEPROM_ENABLE_LEN);
    
    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_2);
}


/*********************************************************************
* PUBLIC vEEPROM_WriteDisable()
*---------------------------------------------------------------------
* Send Write Disable command
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vEEPROM_WriteDisable(void)
{
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_DISABLE;  /* Write Command */
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, EEPROM_DISABLE_LEN);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, EEPROM_DISABLE_LEN);
    
    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_2);
}


/*********************************************************************
* PUBLIC vEEPROM_ReadStatusRegister()
*---------------------------------------------------------------------
* Read Status Register command
*
* Inputs:  None
* Returns:
*   uint8_t - Status Register
*
**********************************************************************/
uint8_t vEEPROM_ReadStatusRegister(void)
{
    /* Init Flag */
    bEEPROM_DataRxComplete = false;
    
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_RDSR; /* Read Status Register Command */
    ucEEPROM_TxSPIBuffer[1] = 0x00; /* Zeros just to generate clock to read data */
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, EEPROM_STATUS_REG_LEN);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, EEPROM_STATUS_REG_LEN);       
    
    /* Initiate transmission */
    if(bStartSpiRxTx(SPI_PORT_2) == true)
    {
		delay_us(50);
    
		/* Wait for Rx to complete */
		while(bEEPROM_DataRxComplete == false);
	}
    /* Return Status Register */
    return(ucEEPROM_RxSPIBuffer[1]);
}


/*********************************************************************
* PUBLIC vEEPROM_WriteStatusRegister()
*---------------------------------------------------------------------
* Write Status Register command
* IMPORTANT: * Must wait 10ms after this TX for EEPROM to complete writing process.
*
* Inputs:  
*   uint8_t - Status Register
* Returns: None
*   
**********************************************************************/
void vEEPROM_WriteStatusRegister(uint8_t ucStatusReg)
{
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_WRSR; /* Write Status Register Command */
    ucEEPROM_TxSPIBuffer[1] = ucStatusReg; /* New Status Register */
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, EEPROM_STATUS_REG_LEN);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, EEPROM_STATUS_REG_LEN);
        
    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_2);  
}


/*********************************************************************
* PUBLIC vEEPROM_Read_ID()
*---------------------------------------------------------------------
* Send Read ID Page command
*
* Inputs:
*   uiAddress - 8 bit source address (LSB) only
* Returns:
*   uint8_t - data retrieved from source address
*
**********************************************************************/
uint8_t vEEPROM_Read_ID(uint8_t ucAddress)
{   
    /* Write Enable must be called before any TX to the EEPROM */
    vEEPROM_WriteEnable();
    
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_RDID;  /* Read ID Page Command */
    ucEEPROM_TxSPIBuffer[1] = (uint8_t) 0;  /* Destination Address MSB */
    ucEEPROM_TxSPIBuffer[2] = (uint8_t) 0;  /* Destination Address MID */
    ucEEPROM_TxSPIBuffer[3] = (uint8_t) (ucAddress & 0x000000ff);  /* Destination Address LSB */
    ucEEPROM_TxSPIBuffer[4] = 0x00; /* Write zeros just to generate clocks for retrieving data */
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, EEPROM_SINGLE_BYTE_WRITE_LEN);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, EEPROM_SINGLE_BYTE_WRITE_LEN);
    
    /* Init Flag */
    bEEPROM_DataRxComplete = false;
    
    /* Initiate transmission */
    if(bStartSpiRxTx(SPI_PORT_2) == true)
    {
		delay_us(EEPROM_READ_DELAY);
    
		/* Wait for Rx to complete */
		while(bEEPROM_DataRxComplete == false);
	}
    /* Return data retrieved from source address */
    return(ucEEPROM_RxSPIBuffer[4]);
}


/*********************************************************************
* PUBLIC vEEPROM_Write_ID()
*---------------------------------------------------------------------
* Send Write ID Page command 
* IMPORTANT: * Must wait 10ms after this TX for EEPROM to complete writing process.
*
* Inputs:  
*   uiAddress - 8 bit source address (LSB) only
*   ucData - data to be written (byte)
* Returns: None
*
**********************************************************************/
void vEEPROM_Write_ID(uint8_t uiAddress, uint8_t ucData)
{       
    /* Write Enable must be called before any TX to the EEPROM */
    vEEPROM_WriteEnable();
    
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_WRID;  /* Write ID Page Command */
    ucEEPROM_TxSPIBuffer[1] = (uint8_t) 0; /* Destination Address MSB */   
    ucEEPROM_TxSPIBuffer[2] = (uint8_t) 0;  /* Destination Address MID */
    ucEEPROM_TxSPIBuffer[3] = (uint8_t) (uiAddress & 0x000000ff);  /* Destination Address LSB */
    ucEEPROM_TxSPIBuffer[4] = ucData; /* Data to be written */
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, EEPROM_SINGLE_BYTE_WRITE_LEN);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, EEPROM_SINGLE_BYTE_WRITE_LEN);
    
    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_2);      
}

/*********************************************************************
* PUBLIC vEEPROM_Verify_ID()
*---------------------------------------------------------------------
* Reads the ID page and Verifies the values read are correct
*
* Inputs:
*   None
* Returns:
*   boolean - True = Verified OK, False = Error
*
**********************************************************************/
bool bEEPROM_Verify_ID(void)
{
    bool bReturnValue = true;
    
    /* Verify ID 0 */
    if(vEEPROM_Read_ID(0) != EEPROM_ID_0)
    {
        bReturnValue = false;
    }
    
    /* Verify ID 1 */
    if(vEEPROM_Read_ID(1) != EEPROM_ID_1)
    {
        bReturnValue = false;
    }
    
    /* Verify ID 2 */
    if(vEEPROM_Read_ID(2) != EEPROM_ID_2)
    {
        bReturnValue = false;
    }
    
    /* Verify ID 3 */
    if(vEEPROM_Read_ID(3) != EEPROM_ID_3)
    {
        bReturnValue = false;
    }
    
    return bReturnValue;
}    

/*********************************************************************
* PUBLIC vEEPROM_Read()
*---------------------------------------------------------------------
* Send Read command
*
* Inputs:
*   uiAddress - 24 bit source address
* Returns:
*   uint8_t - data retrieved from source address
*
**********************************************************************/
uint8_t vEEPROM_Read(uint32_t uiAddress)
{
    /* Write Enable must be called before any TX to the EEPROM */
    vEEPROM_WriteEnable();
    
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_READ;  /* Read Command */
    ucEEPROM_TxSPIBuffer[1] = (uint8_t) ((uiAddress>>16) & 0x000000ff); /* Destination Address MSB */
    ucEEPROM_TxSPIBuffer[2] = (uint8_t) ((uiAddress>>8) & 0x000000ff);  /* Destination Address MID */
    ucEEPROM_TxSPIBuffer[3] = (uint8_t) (uiAddress & 0x000000ff);      /* Destination Address LSB */
    ucEEPROM_TxSPIBuffer[4] = 0x00; /* Write zeros just to generate clocks for retrieving data */
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, EEPROM_SINGLE_BYTE_WRITE_LEN);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, EEPROM_SINGLE_BYTE_WRITE_LEN);
    
    /* Init Flag */
    bEEPROM_DataRxComplete = false;
    
    /* Initiate transmission */
    if(bStartSpiRxTx(SPI_PORT_2) == true)
    {
        delay_us(EEPROM_READ_DELAY);
        
        /* Wait for Rx to complete */
        while(bEEPROM_DataRxComplete == false);
    }
    /* Return data retrieved from source address */
    return(ucEEPROM_RxSPIBuffer[4]);
}


/*********************************************************************
* PUBLIC vEEPROM_Write()
*---------------------------------------------------------------------
* Send Write command
* IMPORTANT: * Must wait 10ms after this TX for EEPROM to complete writing process.
*
* Inputs:
*   uiAddress - 24 bit destination address
*   ucData - data to be written (byte)
* Returns: None
*
**********************************************************************/
void vEEPROM_Write(uint32_t uiAddress, uint8_t ucData)
{
    /* Write Enable must be called before any TX to the EEPROM */
    vEEPROM_WriteEnable();
    
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_WRITE;  /* Write Command */
    ucEEPROM_TxSPIBuffer[1] = (uint8_t) ((uiAddress>>16) & 0x000000ff); /* Destination Address MSB */
    ucEEPROM_TxSPIBuffer[2] = (uint8_t) ((uiAddress>>8) & 0x000000ff);  /* Destination Address MID */
    ucEEPROM_TxSPIBuffer[3] = (uint8_t) (uiAddress & 0x000000ff);      /* Destination Address LSB */
    ucEEPROM_TxSPIBuffer[4] = ucData; /* Data to be written */
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, EEPROM_SINGLE_BYTE_WRITE_LEN);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, EEPROM_SINGLE_BYTE_WRITE_LEN);
    
    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_2);
}


/*********************************************************************
* PUBLIC vEEPROM_ReadBlock()
*---------------------------------------------------------------------
* Send Read a block of data command
*
* Inputs:
*   uiAddress - 24 bit source initial address
*   ucDataBuffer - destination data buffer pointer
*   ushNumBytes - number of bytes to be read
* Returns: 
*   uint16_t - Number of bytes read
*
**********************************************************************/
uint16_t vEEPROM_ReadBlock(uint32_t uiAddress, uint8_t* ucDataBuffer, uint16_t ushNumBytes)
{   
    /* Init Flag */
    bEEPROM_DataRxComplete = false;
    
    /* Limit the number of bytes requested to MAX Buffer Size */
    if(ushNumBytes > (EEPROM_MAX_BUFFER_SIZE - 4))
    {
        ushNumBytes = EEPROM_MAX_BUFFER_SIZE - 4;
    }   
        
    /* Write Enable must be called before any TX to the EEPROM */
    vEEPROM_WriteEnable();
    
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_READ;  /* Read Command */
    ucEEPROM_TxSPIBuffer[1] = (uint8_t) ((uiAddress>>16) & 0x000000ff); /* Destination Address MSB */
    ucEEPROM_TxSPIBuffer[2] = (uint8_t) ((uiAddress>>8) & 0x000000ff);  /* Destination Address MID */
    ucEEPROM_TxSPIBuffer[3] = (uint8_t) (uiAddress & 0x000000ff);      /* Destination Address LSB */
    
    /* Initialize the Tx buffer with zeros for generating clocks for the Rx line */ 
    memset(&ucEEPROM_TxSPIBuffer[4], 0, ushNumBytes);
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, ushNumBytes+4);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, ushNumBytes+4);       
    
    /* Init Flag */
    bEEPROM_DataRxComplete = false;
    
    /* Initiate transmission */
    if(bStartSpiRxTx(SPI_PORT_2) == true)
	{
		delay_us(EEPROM_READ_DELAY);
    
		/* Wait for Rx to complete */
		while(bEEPROM_DataRxComplete == false);
	}
    /* Copy data received from the Rx buffer */
    memcpy(ucDataBuffer, &ucEEPROM_RxSPIBuffer[4], ushNumBytes);
    
    /* Return number of bytes read */
    return(ushNumBytes);    
}


/*********************************************************************
* PUBLIC vEEPROM_WriteBlock()
*---------------------------------------------------------------------
* Send Write a block of data command
* IMPORTANT: * Must wait 10ms after this TX for EEPROM to complete writing process.
*
* Inputs:
*   uiAddress - 24 bit source address
*   ucDataBuffer - source data buffer pointer
*   ushNumBytes - number of bytes to be written
* Returns: 
*   uint16_t - Number of bytes written
*
**********************************************************************/
uint16_t vEEPROM_WriteBlock(uint32_t uiAddress, uint8_t* ucDataBuffer, uint16_t ushNumBytes)
{   
    /* Limit the number of bytes to be written to MAX Buffer Size */
    if(ushNumBytes > (EEPROM_MAX_BUFFER_SIZE - 4))
    {
        ushNumBytes = EEPROM_MAX_BUFFER_SIZE - 4;
    }
            
    /* Write Enable must be called before any TX to the EEPROM */
    vEEPROM_WriteEnable();
    
    /* Populate TX Data */
    ucEEPROM_TxSPIBuffer[0] = EEPROM_WRITE;  /* Write Command */
    ucEEPROM_TxSPIBuffer[1] = (uint8_t) ((uiAddress>>16) & 0x000000ff); /* Destination Address MSB */
    ucEEPROM_TxSPIBuffer[2] = (uint8_t) ((uiAddress>>8) & 0x000000ff);  /* Destination Address MID */
    ucEEPROM_TxSPIBuffer[3] = (uint8_t) (uiAddress & 0x000000ff);       /* Destination Address LSB */
    
    /* Copy data to be transmitted to the Tx buffer */
    memcpy(&ucEEPROM_TxSPIBuffer[4], ucDataBuffer, ushNumBytes);
    
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_2, ucEEPROM_RxSPIBuffer, ushNumBytes+4);
    vSetupSpiTx(SPI_PORT_2, ucEEPROM_TxSPIBuffer, ushNumBytes+4);
    
    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_2);
        
    /* Return number of bytes written */
    return(ushNumBytes);
}

    
/******************************** End of File ********************************/