/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: led_driver_ic.c                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*  This file implements functions to interface with the LED driver IC       */
/*  PCA9634. Including initialization and control interfaces.                */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <FreeRTOS.h>
#include <queue.h>
#include <iface_i2c_dma.h>
#include <iface_rtos_start.h>
#include <led_driver_ic.h>
#include <delay.h>

/* I2C TX/RX Buffers */
uint8_t uchHMI_RxI2CBuffer[3];
uint8_t uchHMI_TxI2CBuffer[3];

/*************************/
/*   Private Functions   */
/*************************/

/*********************************************************************
* PRIVATE svI2C_HMICallbackTX()
*---------------------------------------------------------------------
* HMI Led driver comm data Tx callback
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svI2C_HMICallbackTX(void)
{
    SemaphoreHandle_t pstSem_I2C_LED_Hdl;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
            
    /* Get semaphore handle */
    pstSem_I2C_LED_Hdl = sGetSemaphoreHandle(I2C_LED);
    
    /* Notify TX completion by posting semaphore */
    xSemaphoreGiveFromISR(pstSem_I2C_LED_Hdl, &xHigherPriorityTaskWoken);
}


/*************************/
/*    Public Functions   */
/*************************/
/*********************************************************************
* PUBLIC vInitializeHMILedDriver()
*---------------------------------------------------------------------
* HMI Led driver initialization
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vInitializeHMILedDriver(void)
{
    uint8_t ucIdx;
    SemaphoreHandle_t pstSem_I2C_LED_Hdl;
    SemaphoreHandle_t pstMySem_LEDCNTL_Fault_Hdl;
    bool bErrorDetectFlag = false;
    
    /* Get semaphore handle */
    pstSem_I2C_LED_Hdl = sGetSemaphoreHandle(I2C_LED);
    pstMySem_LEDCNTL_Fault_Hdl = sGetSemaphoreHandle(LEDCNTL_I2C_FAULT);
 
    /* Register Callbacks for I2C */
    vSetI2cTxCallback(svI2C_HMICallbackTX, I2C_RESOURCE_HMI);

    /* Set I2C data transfer size to 2 bytes */
    vSetupI2cTx(uchHMI_TxI2CBuffer, 3, I2C_RESOURCE_HMI);

    /* Setup for LED Driver */
    uchHMI_TxI2CBuffer[0] = 0x80; /* Register MODE1 address with auto increment */
    uchHMI_TxI2CBuffer[1] = 0x01; /* MODE1 value */
    uchHMI_TxI2CBuffer[2] = 0x01; /* MODE2 value */

    /* Start transfer */
    if(bStartI2cTx(I2C_LED_DRIVER_ADDRESS) == true)
    {
        if(xSemaphoreTake(pstSem_I2C_LED_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
        {
            bErrorDetectFlag = true;
        }
    }
    else
    {
        bErrorDetectFlag = true;
    }
    			
    /* Check result for this TX and post error flag if error detected */
    if(bErrorDetectFlag == true)
    {
        /* A communication error occurred  */
        xSemaphoreGive( pstMySem_LEDCNTL_Fault_Hdl );
    }
    delay_ms(5);

    for(ucIdx=0;ucIdx<11;ucIdx++)
    {
        switch (ucIdx)
        {
            case 0:
                /* Set all LEDs on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x55; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x55; /* LEDOUT1 value */
                break;
            case 1:
                /* Set all LEDs off */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x00; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x00; /* LEDOUT1 value */
                break;
            case 2:
                /* Set PWR LED on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x01; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x00; /* LEDOUT1 value */
                break;
            case 3:
                /* Set PWR, FLT GRN LEDs on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x05; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x00; /* LEDOUT1 value */
                break;
            case 4:
                /* Set PWR, FLT GRN, FLT RED LEDs on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x15; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x00; /* LEDOUT1 value */
                break;
            case 5:
                /* Set PWR, FLT GRN, FLT RED, SOC1 LEDs on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x55; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x00; /* LEDOUT1 value */
                break;
            case 6:
                /* Set PWR, FLT GRN, FLT RED, SOC1,2 LEDs on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x55; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x01; /* LEDOUT1 value */
                break;
            case 7:
                /* Set PWR, FLT GRN, FLT RED, SOC1,2,3 LEDs on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x55; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x05; /* LEDOUT1 value */
                break;
            case 8:
                /* Set PWR, FLT GRN, FLT RED, SOC1,2,3,4 LEDs on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x55; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x15; /* LEDOUT1 value */
                break;
            case 9:
                /* Set PWR, FLT GRN, FLT RED, SOC1,2,3,4,5 LEDs on */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x55; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x55; /* LEDOUT1 value */
                break;
            default:
                /* Set all LEDs off */
                uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
                uchHMI_TxI2CBuffer[1] = 0x00; /* LEDOUT0 value */
                uchHMI_TxI2CBuffer[2] = 0x00; /* LEDOUT1 value */
                break;
        }

        /* Start transfer */
        if(bStartI2cTx(I2C_LED_DRIVER_ADDRESS) == true)
        {
            if(xSemaphoreTake(pstSem_I2C_LED_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
            {
                bErrorDetectFlag = true;
            }
        }
        else
        {
            bErrorDetectFlag = true;
        }
            
        /* Check result for this TX and post error flag if error detected */
        if(bErrorDetectFlag == true)
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_LEDCNTL_Fault_Hdl );
        }
        delay_ms(100);
    }
}


/*********************************************************************
* PUBLIC vTurnLEDsOnOff()
*---------------------------------------------------------------------
* HMI Led driver initialization
*
* Inputs:  ucLED_Bytes : Contains LEDOUT0 and LEDOUT1 register values
* Returns: None
*
**********************************************************************/
void vTurnLEDsOnOff(uint16_t ucLED_Bytes)
{
    SemaphoreHandle_t pstSem_I2C_LED_Hdl;
    SemaphoreHandle_t pstMySem_LEDCNTL_Fault_Hdl;
    bool bErrorDetectFlag = false;
        
    /* Get semaphore handle */
    pstSem_I2C_LED_Hdl = sGetSemaphoreHandle(I2C_LED);
    pstMySem_LEDCNTL_Fault_Hdl = sGetSemaphoreHandle(LEDCNTL_I2C_FAULT);
    
    /* Register Callbacks for I2C */
    vSetI2cTxCallback(svI2C_HMICallbackTX, I2C_RESOURCE_HMI);

    /* Set I2C data transfer size to 3 bytes */
    vSetupI2cTx(uchHMI_TxI2CBuffer, 3, I2C_RESOURCE_HMI);

    /* Setup for LED Driver */
    uchHMI_TxI2CBuffer[0] = 0x8C; /* Register LEDOUT0 address with auto increment */
    uchHMI_TxI2CBuffer[1] = (uint8_t)(ucLED_Bytes & 0x00FF); /* LEDOUT0 value */
    uchHMI_TxI2CBuffer[2] = (uint8_t)((ucLED_Bytes>>8) & 0x00FF); /* LEDOUT1 value */

    /* Start transfer */
    if(bStartI2cTx(I2C_LED_DRIVER_ADDRESS) == true)
    {
        if(xSemaphoreTake(pstSem_I2C_LED_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
        {
            bErrorDetectFlag = true;
        }
    }
    else
    {
        bErrorDetectFlag = true;
    }
        
    /* Check result for this TX and post error flag if error detected */
    if(bErrorDetectFlag == true)
    {
        /* A communication error occurred  */
        xSemaphoreGive( pstMySem_LEDCNTL_Fault_Hdl );
    }
}


/*********************************************************************
* PUBLIC vLEDDriverSleep()
*---------------------------------------------------------------------
* Put Led driver in sleep mode
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vLEDDriverSleep(void)
{
    SemaphoreHandle_t pstSem_I2C_LED_Hdl;
    SemaphoreHandle_t pstMySem_LEDCNTL_Fault_Hdl;
    bool bErrorDetectFlag = false;
       
    /* Get semaphore handle */
    pstSem_I2C_LED_Hdl = sGetSemaphoreHandle(I2C_LED);
    pstMySem_LEDCNTL_Fault_Hdl = sGetSemaphoreHandle(LEDCNTL_I2C_FAULT);
    
    /* Register Callbacks for I2C */
    vSetI2cTxCallback(svI2C_HMICallbackTX, I2C_RESOURCE_HMI);

    /* Set I2C data transfer size to 2 bytes */
    vSetupI2cTx(uchHMI_TxI2CBuffer, 2, I2C_RESOURCE_HMI);

    /* Setup for LED Driver */
    uchHMI_TxI2CBuffer[0] = 0x80; /* Register MODE1 address with auto increment */
    uchHMI_TxI2CBuffer[1] = 0x11;  /* Mode1 MODE1 (Sleep Bit4 set to 1) */

    /* Start transfer */
    if(bStartI2cTx(I2C_LED_DRIVER_ADDRESS) == true)
    {
        if(xSemaphoreTake(pstSem_I2C_LED_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
        {
            bErrorDetectFlag = true;
        }
    }
    else
    {
        bErrorDetectFlag = true;
    }
    
    /* Check result for this TX and post error flag if error detected */
    if(bErrorDetectFlag == true)
    {
        /* A communication error occurred  */
        xSemaphoreGive( pstMySem_LEDCNTL_Fault_Hdl );
    }
}


/*********************************************************************
* PUBLIC vLEDDriverWakeup()
*---------------------------------------------------------------------
* Wakes up Led driver from sleep mode
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vLEDDriverWakeup(void)
{
    SemaphoreHandle_t pstSem_I2C_LED_Hdl;
    SemaphoreHandle_t pstMySem_LEDCNTL_Fault_Hdl;
    bool bErrorDetectFlag = false;
    
    /* Get semaphore handle */
    pstSem_I2C_LED_Hdl = sGetSemaphoreHandle(I2C_LED);
    pstMySem_LEDCNTL_Fault_Hdl = sGetSemaphoreHandle(LEDCNTL_I2C_FAULT);
    
    /* Register Callbacks for I2C */
    vSetI2cTxCallback(svI2C_HMICallbackTX, I2C_RESOURCE_HMI);

    /* Set I2C data transfer size to 2 bytes */
    vSetupI2cTx(uchHMI_TxI2CBuffer, 2, I2C_RESOURCE_HMI);

    /* Setup for LED Driver */
    uchHMI_TxI2CBuffer[0] = 0x80; /* Register MODE1 address with auto increment */
    uchHMI_TxI2CBuffer[1] = 0x01;  /* MODE1 value (Sleep Bit4 set to 0) */

    /* Start transfer */
    if(bStartI2cTx(I2C_LED_DRIVER_ADDRESS) == true)
    {
        if(xSemaphoreTake(pstSem_I2C_LED_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
        {
            bErrorDetectFlag = true;
        }
    }
    else
    {
        bErrorDetectFlag = true;
    }
    
    /* Check result for this TX and post error flag if error detected */
    if(bErrorDetectFlag == true)
    {
        /* A communication error occurred  */
        xSemaphoreGive( pstMySem_LEDCNTL_Fault_Hdl );
    }
}




/******************************** End of File ********************************/