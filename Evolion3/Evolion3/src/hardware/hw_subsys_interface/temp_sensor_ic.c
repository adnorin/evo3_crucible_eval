/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: temp_sensor_ic.c                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <FreeRTOS.h>
#include <queue.h>
#include <iface_i2c_dma.h>
#include <temp_sensor_ic.h>
#include <iface_rtos_start.h>
#include <conf_battery_settings.h>
#include <temp_meas.h>


/* I2C TX/RX Buffers */
uint8_t uchTemp_RxI2CBuffer[3];
uint8_t uchTemp_TxI2CBuffer[3];

/* Sensor I2C addresses */
/* NOTE: During debugging ONLY using eval board use sensor 0 as 1st sensor, otherwise replace with sensor 1 */

#ifndef I2C_TEST_ADR_SETUP
const uint8_t uchTempSensorAddressInit[NUM_TEMP_SENSORS] = {CELL_TEMP_SENSOR_0_ADDR,
                                                            CELL_TEMP_SENSOR_1_ADDR,
                                                            CELL_TEMP_SENSOR_2_ADDR,
                                                            CELL_TEMP_SENSOR_3_ADDR,
                                                            ELEC_TEMP_SENSOR_0_ADDR,
                                                            ELEC_TEMP_SENSOR_1_ADDR,
                                                            ELEC_TEMP_SENSOR_2_ADDR,
                                                            ELEC_TEMP_SENSOR_3_ADDR };
#else
#if (I2C_TEST_ADR_SETUP == 1)
const uint8_t uchTempSensorAddressInit[NUM_TEMP_SENSORS] = {ELEC_TEMP_SENSOR_0_ADDR,
                                                            ELEC_TEMP_SENSOR_1_ADDR,
                                                            ELEC_TEMP_SENSOR_2_ADDR,
                                                            ELEC_TEMP_SENSOR_3_ADDR,
                                                            ELEC_TEMP_SENSOR_0_ADDR,
                                                            ELEC_TEMP_SENSOR_1_ADDR,
                                                            ELEC_TEMP_SENSOR_2_ADDR,
                                                            ELEC_TEMP_SENSOR_3_ADDR };
#else
#if (I2C_TEST_ADR_SETUP == 2)
const uint8_t uchTempSensorAddressInit[NUM_TEMP_SENSORS] = {CELL_B_SAMPLE_TEMP_SENSOR_0_ADDR,
                                                            CELL_B_SAMPLE_TEMP_SENSOR_1_ADDR,
                                                            CELL_B_SAMPLE_TEMP_SENSOR_2_ADDR,
                                                            CELL_B_SAMPLE_TEMP_SENSOR_3_ADDR,
                                                            CELL_B_SAMPLE_TEMP_SENSOR_4_ADDR,
                                                            CELL_B_SAMPLE_TEMP_SENSOR_5_ADDR,
                                                            ELEC_B_SAMPLE_TEMP_SENSOR_0_ADDR,
                                                            ELEC_B_SAMPLE_TEMP_SENSOR_1_ADDR,
                                                            ELEC_B_SAMPLE_TEMP_SENSOR_2_ADDR,
                                                            ELEC_B_SAMPLE_TEMP_SENSOR_3_ADDR};
#else
const uint8_t uchTempSensorAddressInit[NUM_TEMP_SENSORS] = {CELL_TEMP_SENSOR_2_ADDR,
                                                            CELL_TEMP_SENSOR_3_ADDR,
                                                            CELL_TEMP_SENSOR_2_ADDR,
                                                            CELL_TEMP_SENSOR_3_ADDR,
                                                            CELL_TEMP_SENSOR_2_ADDR,
                                                            CELL_TEMP_SENSOR_3_ADDR,
                                                            CELL_TEMP_SENSOR_2_ADDR,
                                                            CELL_TEMP_SENSOR_3_ADDR };
#endif
#endif
#endif

/* I2C Temp Sensor Private Flags */
volatile bool bTempSensorTXDone;
volatile bool bTempSensorRXDone;

/*************************/
/*   Private Functions   */
/*************************/
/*********************************************************************
* PRIVATE svI2C_CallbackRX()
*---------------------------------------------------------------------
* Temperature sensors I2C comm data Rx callback
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svI2C_CallbackRX(void)
{
    uint16_t ushData;
    QueueHandle_t pstQueueHandle;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    ushData = (uchTemp_RxI2CBuffer[0] << 8) | uchTemp_RxI2CBuffer[1];

    /* Get Queue Handle */
    pstQueueHandle = sGetQueueHandle(I2C_DATA_QUEUE);

    /* Notify task by writing to queue */
    xQueueOverwriteFromISR(pstQueueHandle, &ushData, &xHigherPriorityTaskWoken);
}


/*********************************************************************
* PRIVATE svI2C_CallbackTX()
*---------------------------------------------------------------------
* Temperature sensors I2C comm data Tx callback
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svI2C_CallbackTX(void)
{
    SemaphoreHandle_t pstSemHandle;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    /* Get Semaphore Handle */
    pstSemHandle = sGetSemaphoreHandle(I2C_TEMP_SENSOR);

    /* Notify TX completion by posting semaphore */
    xSemaphoreGiveFromISR(pstSemHandle, &xHigherPriorityTaskWoken);
}


/*************************/
/*    Public Functions   */
/*************************/
/*********************************************************************
* PUBLIC usReadRawTempValue()
*---------------------------------------------------------------------
* Measure sensor temperatures
*
* Inputs:
*   ucTempSensorAddress - Temperature sensor I2C address
* Returns: 
*   uint16_t - Raw temperature value
*
**********************************************************************/
uint16_t ushReadRawTempValue(uint8_t ucTempSensorAddress)
{
    BaseType_t sStatus;
    QueueHandle_t spQueue_I2C_D_Hdl;
    SemaphoreHandle_t pstMySem_TEMPSENSE_Fault_Hdl; 
    uint16_t ushTempValue;
    uint16_t ushReturnValue;

    /* Get semaphore Handle */
    pstMySem_TEMPSENSE_Fault_Hdl = sGetSemaphoreHandle(TEMPSENSE_I2C_FAULT);

    /* Temperature Reading From Sensor */
    ushReturnValue = TEMP_SENSE_ERROR;

    /* Register Callback to RX I2C */
    vSetI2cRxCallback(svI2C_CallbackRX, I2C_RESOURCE_TEMP);

    /* Set I2C data transfer size to 2 bytes */
    vSetupI2cRx(uchTemp_RxI2CBuffer, 2, I2C_RESOURCE_TEMP);

    if(bStartI2cRx(ucTempSensorAddress) == true)
    {
        /* Wait for data to be received, this thread will pend on the queue receive until
           data is received or times out */
        spQueue_I2C_D_Hdl = sGetQueueHandle(I2C_DATA_QUEUE);
        sStatus = xQueueReceive(spQueue_I2C_D_Hdl, &ushTempValue,  pdMS_TO_TICKS(20));
        if( sStatus == pdPASS )
        {
            ushReturnValue = ushTempValue;
        }
        else
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );            
        }
    }

    return ushReturnValue;
}


/*********************************************************************
* PUBLIC vInitializeTempSensors()
*---------------------------------------------------------------------
* Temperature sensors initialization
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vInitializeTempSensors(void)
{
    uint8_t ucTempSensor;
    SemaphoreHandle_t pstSem_I2C_TempSensors_Hdl;
    SemaphoreHandle_t pstMySem_TEMPSENSE_Fault_Hdl; 
    bool bErrorDetectFlag = false;
    
    /* Get semaphore handle */
    pstSem_I2C_TempSensors_Hdl = sGetSemaphoreHandle(I2C_TEMP_SENSOR);
    pstMySem_TEMPSENSE_Fault_Hdl = sGetSemaphoreHandle(TEMPSENSE_I2C_FAULT);
   
    /* Register Callbacks for I2C */
    vSetI2cRxCallback(svI2C_CallbackRX, I2C_RESOURCE_TEMP);
    vSetI2cTxCallback(svI2C_CallbackTX, I2C_RESOURCE_TEMP);

    /* Set I2C data receive size to 2 bytes */
    vSetupI2cRx(uchTemp_RxI2CBuffer, 2, I2C_RESOURCE_TEMP);

    for(ucTempSensor=0; ucTempSensor<NUM_TEMP_SENSORS; ucTempSensor++)
    {

        /* Set I2C data transfer size to 2 bytes */
        vSetupI2cTx(uchTemp_TxI2CBuffer, 2, I2C_RESOURCE_TEMP);

        /* Write the Conf Reg value */
        /*--------------------------------*/
        bTempSensorTXDone = false;

        /* Set Fault Queue = 2 all other default */
        uchTemp_TxI2CBuffer[0] = CONFIG_REG; /* Pointer Register */
        uchTemp_TxI2CBuffer[1] = 0x08; /* LSB */

        /* Start transfer and wait until completed */
        if(bStartI2cTx(uchTempSensorAddressInit[ucTempSensor]) == true)
		{
			if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
            {
                bErrorDetectFlag = true;
            }                
		}
        else
        {
            bErrorDetectFlag = true;
        }
        
        /* Check result for this TX and post error flag if error detected */
        if(bErrorDetectFlag == true)
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
        }
        delay_us(250);
        /*--------------------------------*/


        /* Set I2C data transfer size to 3 bytes */
        vSetupI2cTx(uchTemp_TxI2CBuffer, 3, I2C_RESOURCE_TEMP);

        /* Write the Over Temp value */
        /*--------------------------------*/
        bTempSensorTXDone = false;

        /* Set the Over Temp Value to 85C */
        uchTemp_TxI2CBuffer[0] = T_OS_REG; /* Pointer Register */
        uchTemp_TxI2CBuffer[1] = 0x55; /* MSB */
        uchTemp_TxI2CBuffer[2] = 0x00; /* LSB */

        /* Start transfer and wait until completed */
        if(bStartI2cTx(uchTempSensorAddressInit[ucTempSensor]) == true)
		{
			if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
			{
    			bErrorDetectFlag = true;
			}
		}
        else
        {
            bErrorDetectFlag = true;
        }
        
        /* Check result for this TX and post error flag if error detected */
        if(bErrorDetectFlag == true)
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
        }
        delay_us(250);
        /*--------------------------------*/

        /* Write the Over Temp Hyst value */
        /*--------------------------------*/
        bTempSensorTXDone = false;

        /* Set the Over Temp Value Hyst to 75C */
        uchTemp_TxI2CBuffer[0] = T_HYST_REG; /* Pointer Register */
        uchTemp_TxI2CBuffer[1] = 0x4B; /* MSB */
        uchTemp_TxI2CBuffer[2] = 0x00; /* LSB */

        /* Start transfer and wait until completed */
        if(bStartI2cTx(uchTempSensorAddressInit[ucTempSensor]) == true)
		{
			if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
			{
    			bErrorDetectFlag = true;
			}
		}
        else
        {
            bErrorDetectFlag = true;
        }
        
        /* Check result for this TX and post error flag if error detected */
        if(bErrorDetectFlag == true)
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
        }
        delay_us(250);
        /*--------------------------------*/


        /* Set I2C data transfer size to 1 byte */
        vSetupI2cTx(uchTemp_TxI2CBuffer, 1, I2C_RESOURCE_TEMP);

        /* Set pointer register to Temp Read */
        /*--------------------------------*/
        bTempSensorTXDone = false;

        uchTemp_TxI2CBuffer[0] = TEMP_READ_REG; /* Pointer Register */

        /* Start transfer and wait until completed */
        if(bStartI2cTx(uchTempSensorAddressInit[ucTempSensor]) == true)
		{
			if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
			{
    			bErrorDetectFlag = true;
			}
		}
        else
        {
            bErrorDetectFlag = true;
        }
        
        /* Check result for this TX and post error flag if error detected */
        if(bErrorDetectFlag == true)
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
        }
        delay_us(250);
        /*--------------------------------*/
    }

}


/*********************************************************************
* PUBLIC vSetOverTempAndHystLimit()
*---------------------------------------------------------------------
* Temperature sensors initialization Over Temp Limit
*
* Inputs:
*   ushValue - Over Temp Value or Hysteresis Value * 2
*   ucPointerReg - Pointer register, Tos or Thyst
* Returns: None
*
**********************************************************************/
void vSetOverTempAndHystLimit(uint8_t ucTempSensorAdd, uint16_t ushValue, uint8_t ucPointerReg)
{
    uint16_t ushMyValue;
    SemaphoreHandle_t pstSem_I2C_TempSensors_Hdl;
    SemaphoreHandle_t pstMySem_TEMPSENSE_Fault_Hdl;
    bool bErrorDetectFlag = false;
    
    /* Get semaphore handle */
    pstSem_I2C_TempSensors_Hdl = sGetSemaphoreHandle(I2C_TEMP_SENSOR);
    pstMySem_TEMPSENSE_Fault_Hdl = sGetSemaphoreHandle(TEMPSENSE_I2C_FAULT);
    
    /* Register Callbacks for I2C */
    vSetI2cTxCallback(svI2C_CallbackTX, I2C_RESOURCE_TEMP);

    /* Set I2C data transfer size to 3 bytes */
    vSetupI2cTx(uchTemp_TxI2CBuffer, 3, I2C_RESOURCE_TEMP);

    /* Write the Conf Reg value */
    /*--------------------------------*/
    bTempSensorTXDone = false;

    /* Scale to 0.5 degC/LSB and shift to the right bit for transmission */
    ushMyValue = (ushValue & 0x03FF) << 7;

    /* Write Tos/Thyst */
    uchTemp_TxI2CBuffer[0] = ucPointerReg; /* Pointer Register */
    uchTemp_TxI2CBuffer[1] = (uint8_t)((ushMyValue >> 8) & 0x00FF); /* MSB */
    uchTemp_TxI2CBuffer[2] = (uint8_t)(ushMyValue & 0x00FF);        /* LSB */

    /* Start transfer and wait until completed */
    if(bStartI2cTx(ucTempSensorAdd) == true)
	{
		if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
        {
            bErrorDetectFlag = true;
        }                
	}
    else
    {
        bErrorDetectFlag = true;
    }
        
    /* Check result for this TX and post error flag if error detected */
    if(bErrorDetectFlag == true)
    {
        /* A communication error occurred  */
        xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
    }
    delay_us(250);
    /*--------------------------------*/

    /* Set I2C data transfer size to 1 byte */
    vSetupI2cTx(uchTemp_TxI2CBuffer, 1, I2C_RESOURCE_TEMP);

    /* Set pointer register to Temp Read */
    /*--------------------------------*/
    bTempSensorTXDone = false;

    uchTemp_TxI2CBuffer[0] = TEMP_READ_REG; /* Pointer Register */

    /* Start transfer and wait until completed */
    if(bStartI2cTx(ucTempSensorAdd) == true)
	{
		if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
		{
    		bErrorDetectFlag = true;
		}
	}
    else
    {
        bErrorDetectFlag = true;
    }
        
    /* Check result for this TX and post error flag if error detected */
    if(bErrorDetectFlag == true)
    {
        /* A communication error occurred  */
        xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
    }
    delay_us(250);
    /*--------------------------------*/
}

/*********************************************************************
* PUBLIC vTempSensorsShutdown()
*---------------------------------------------------------------------
* Shutdown temperature sensors
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vTempSensorsShutdown(void)
{
    uint8_t ucTempSensor;
    SemaphoreHandle_t pstSem_I2C_TempSensors_Hdl;
    SemaphoreHandle_t pstMySem_TEMPSENSE_Fault_Hdl;
    bool bErrorDetectFlag = false;
    
    /* Get semaphore handle */
    pstSem_I2C_TempSensors_Hdl = sGetSemaphoreHandle(I2C_TEMP_SENSOR);
    pstMySem_TEMPSENSE_Fault_Hdl = sGetSemaphoreHandle(TEMPSENSE_I2C_FAULT);
    
    /* Register Callbacks for I2C */
    vSetI2cTxCallback(svI2C_CallbackTX, I2C_RESOURCE_TEMP);

    for(ucTempSensor=0; ucTempSensor<NUM_TEMP_SENSORS; ucTempSensor++)
    {

        /* Set I2C data transfer size to 2 bytes */
        vSetupI2cTx(uchTemp_TxI2CBuffer, 2, I2C_RESOURCE_TEMP);

        /* Write the Conf Reg value */
        /*--------------------------------*/
        bTempSensorTXDone = false;

        /* Set Fault Queue = 2, Shutdown = 1,  all other default */
        uchTemp_TxI2CBuffer[0] = CONFIG_REG; /* Pointer Register */
        uchTemp_TxI2CBuffer[1] = 0x09; /* LSB */

        /* Start transfer and wait until completed */
        if(bStartI2cTx(uchTempSensorAddressInit[ucTempSensor]) == true)
		{
		    if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
		    {
    		    bErrorDetectFlag = true;
		    }
	    }
        else
        {
            bErrorDetectFlag = true;
        }
        
        /* Check result for this TX and post error flag if error detected */
        if(bErrorDetectFlag == true)
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
        }
        delay_us(250);
        /*--------------------------------*/
    }
}


/*********************************************************************
* PUBLIC vTempSensorsWakeUp()
*---------------------------------------------------------------------
* Wake up temperature sensors
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vTempSensorsWakeUp(void)
{
    uint8_t ucTempSensor;
   SemaphoreHandle_t pstSem_I2C_TempSensors_Hdl;
   SemaphoreHandle_t pstMySem_TEMPSENSE_Fault_Hdl;
   bool bErrorDetectFlag = false;
   
   /* Get semaphore handle */
   pstSem_I2C_TempSensors_Hdl = sGetSemaphoreHandle(I2C_TEMP_SENSOR);
   pstMySem_TEMPSENSE_Fault_Hdl = sGetSemaphoreHandle(TEMPSENSE_I2C_FAULT);
   
    /* Register Callbacks for I2C */
    vSetI2cTxCallback(svI2C_CallbackTX, I2C_RESOURCE_TEMP);

    for(ucTempSensor=0; ucTempSensor<NUM_TEMP_SENSORS; ucTempSensor++)
    {
        /* Set I2C data transfer size to 2 bytes */
        vSetupI2cTx(uchTemp_TxI2CBuffer, 2, I2C_RESOURCE_TEMP);

        /* Write the Conf Reg value */
        /*--------------------------------*/
        bTempSensorTXDone = false;

        /* Set Fault Queue = 2 all other default */
        uchTemp_TxI2CBuffer[0] = CONFIG_REG; /* Pointer Register */
        uchTemp_TxI2CBuffer[1] = 0x08; /* LSB */

        /* Start transfer and wait until completed */
        if(bStartI2cTx(uchTempSensorAddressInit[ucTempSensor]) == true)
		{
		    if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
		    {
    		    bErrorDetectFlag = true;
		    }
	    }
        else
        {
            bErrorDetectFlag = true;
        }
        
        /* Check result for this TX and post error flag if error detected */
        if(bErrorDetectFlag == true)
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
        }
        delay_us(250);
        /*--------------------------------*/

        /* Set I2C data transfer size to 1 byte */
        vSetupI2cTx(uchTemp_TxI2CBuffer, 1, I2C_RESOURCE_TEMP);

        /* Set pointer register to Temp Read */
        /*--------------------------------*/
        bTempSensorTXDone = false;

        uchTemp_TxI2CBuffer[0] = TEMP_READ_REG; /* Pointer Register */

        /* Start transfer and wait until completed */
        if(bStartI2cTx(uchTempSensorAddressInit[ucTempSensor]) == true)
        {
            if(xSemaphoreTake(pstSem_I2C_TempSensors_Hdl, pdMS_TO_TICKS(TIME_10MS)) != pdPASS)
            {
                bErrorDetectFlag = true;
            }
        }
        else
        {
            bErrorDetectFlag = true;
        }
        
        /* Check result for this TX and post error flag if error detected */
        if(bErrorDetectFlag == true)
        {
            /* A communication error occurred  */
            xSemaphoreGive( pstMySem_TEMPSENSE_Fault_Hdl );
        }
        delay_us(250);
        /*--------------------------------*/
    }
}

/*********************************************************************
* PUBLIC vTempSensorsSetOverTempAndHysteresis()
*---------------------------------------------------------------------
* Temperature sensors configuration
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vTempSensorsSetOverTempAndHysteresis(void)
{
    uint8_t ucTempSensor;   /* Variable used to index each temperature sensor */

    /* Iterate through each temp sensor index so that we can set its over-temperature and hysteresis thresholds */
    for(ucTempSensor = 0; ucTempSensor < NUM_TEMP_SENSORS; ucTempSensor++)
    {
        /* Set thresholds for the cell temperature sensors */
        if (ucTempSensor < NUM_CELL_TEMP_SENSORS)
        {
            vSetOverTempAndHystLimit(uchTempSensorAddressInit[ucTempSensor], const_cCfgRedundantOverTempCellHysteresis * OVER_TEMP_AND_HYSTERESIS_SCALAR, T_HYST_REG);
            vSetOverTempAndHystLimit(uchTempSensorAddressInit[ucTempSensor], const_cCfgRedundantOverTempCellThreshold * OVER_TEMP_AND_HYSTERESIS_SCALAR, T_OS_REG);
        }
        /* Set thresholds for the electronics temperature sensors */
        else
        {
            vSetOverTempAndHystLimit(uchTempSensorAddressInit[ucTempSensor], const_cCfgRedundantOverTempElectronicsHysteresis * OVER_TEMP_AND_HYSTERESIS_SCALAR, T_HYST_REG);
            vSetOverTempAndHystLimit(uchTempSensorAddressInit[ucTempSensor], const_cCfgRedundantOverTempElectronicsThreshold * OVER_TEMP_AND_HYSTERESIS_SCALAR, T_OS_REG);
        }
    }
}

/******************************** End of File ********************************/