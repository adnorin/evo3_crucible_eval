#ifndef LED_DRIVER_IC_H_
#define LED_DRIVER_IC_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: led_driver_ic.h                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for led_driver_ic.c, which provides prototypes for the        */
/* public interface functions. Also includes types and enumeration           */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define I2C_LED_DRIVER_ADDRESS 0x15

/*********************************************************************
* PUBLIC vInitializeHMILedDriver()
*---------------------------------------------------------------------
* HMI Led driver initialization
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vInitializeHMILedDriver(void);


/*********************************************************************
* PUBLIC vTurnLEDsOnOff()
*---------------------------------------------------------------------
* HMI Led driver initialization
*
* Inputs:  ucLED_Bytes : Contains LEDOUT0 and LEDOUT1 register values
* Returns: None
*
**********************************************************************/
void vTurnLEDsOnOff(uint16_t ucLED_Bytes);


/*********************************************************************
* PUBLIC vLEDDriverSleep()
*---------------------------------------------------------------------
* Put Led driver in sleep mode
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vLEDDriverSleep(void);


/*********************************************************************
* PUBLIC vLEDDriverWakeup()
*---------------------------------------------------------------------
* Wakes up Led driver from sleep mode
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vLEDDriverWakeup(void);

#endif /* LED_DRIVER_IC_H_ */