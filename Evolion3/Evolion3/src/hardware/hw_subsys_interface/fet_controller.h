#ifndef FET_STATES_H_
#define FET_STATES_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: fet_controller.h                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for fet_controller.c, which provides prototypes for the       */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define FULL_DISCHARGE_FET_ENABLE_MASK  0x0001
#define FULL_CHARGE_FET_ENABLE_MASK     0x0002
#define SAFETY_CHARGE_FET_ENABLE_MASK   0x0004
#define REG_DISCHARGE_FET_ENABLE_MASK   0x0008
#define REG_CHARGE_FET_ENABLE_MASK      0x0010
#define REGULATED_CURRENT_LVL_1_MASK    0x0020
#define REGULATED_CURRENT_LVL_2_MASK    0x0040
#define REGULATED_CURRENT_LVL_3_MASK    0x0080

/*********************************************************************
* PUBLIC vSetFetState()
*---------------------------------------------------------------------
* FET management routines
*
* Inputs:
*     ushFetStates - Bit encoded variable containing status of the Fet GPIOs
* Returns: None
*
**********************************************************************/
void vSetFetState(uint16_t ushFetStates);

#endif /* FET_STATES_H_ */