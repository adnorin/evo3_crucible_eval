/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: bat_cell_controller_ic.c                                       */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  This file implements functions to interface with the BMS IC MC33771      */
/*  Read, write, scale, CRC calculations, initialize and control functions   */
/*                                                                           */
/*****************************************************************************/
#include <bat_cell_controller_ic.h>
#include <iface_spi_dma.h>
#include <delay.h>
#include <conf_gpio.h>

/* SPI Buffers */
static uint8_t RxSPIBuffer[SPI_BUFFER_LEN];
static uint8_t TxSPIBuffer[SPI_BUFFER_LEN];
static uint8_t TxZeroBuffer[SPI_BUFFER_LEN] = {0};

/*****************************/
/* Private Functions         */
/*****************************/
/*********************************************************************
* PRIVATE svBmsSpiRXCallback()
*---------------------------------------------------------------------
* Callback for Rx BMS Spi transactions.
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svBmsSpiRXCallback(void)
{
    static uint32_t uiDummyRx = 0;

    uiDummyRx++;

}

/*********************************************************************
* PRIVATE svBmsSpiTXCallback()
*---------------------------------------------------------------------
* Callback for Tx BMS Spi transactions.
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
static void svBmsSpiTXCallback(void)
{
    static uint32_t uiDummyTx = 0;

    uiDummyTx++;
}


/*********************************************************************
* PRIVATE sucComputeCRC8()
*---------------------------------------------------------------------
* Calculates CRC value that is required for each transmission.
*
* Inputs:
*     uchSpiMsg - Pointer to message array
* Returns:
*     uint8_t - 8 bit CRC value
*
**********************************************************************/
static uint8_t sucComputeCRC8(uint8_t *uchpSPIMsgBuf)
{
    const uint8_t uchCRCPolynomial = CRC_POLYNOMIAL; /* CRC Polynomial used by the 33771A */
    uint8_t uchIdx1, uchIdx2; /* Loop indexes */
    uint8_t uchCrc = CRC_SEED_VAL; /* Seed - Initial Value */

    for(uchIdx1 = 0; uchIdx1 < MESSAGE_LEN_NOCRC; uchIdx1++)
    {
        uchCrc ^= uchpSPIMsgBuf[uchIdx1]; /* XOR-in the next input byte */

        for (uchIdx2 = 0; uchIdx2 < CRC_NUM_OF_BITS; uchIdx2++)
        {
            if ((uchCrc & 0x80) != 0)
            {
                uchCrc = (uint8_t)((uchCrc << 1) ^ uchCRCPolynomial);
            }
            else
            {
                uchCrc <<= 1;
            }
        }
    }
    return uchCrc;
}


/*********************************************************************
* PRIVATE sucConvertMessageAndCRC()
*---------------------------------------------------------------------
* Formats the BC message for SPI transmission including CRC also
* returns CRC. This function can be used for both formatting messages
* for Tx and for Calculating the CRC of Rx messages.
*
* Inputs:
*     uchSpiMsg - Pointer to message array
* Returns:
*     uint8_t - 8 bit CRC of the message
*
**********************************************************************/
static uint8_t sucConvertMessageAndCRC(BC_Message_t stCommand, uint8_t * uchpSPIMsgBuf)
{
    uchpSPIMsgBuf[0] = (uint8_t)((stCommand.ushMemoryData >> 8) & 0x00FF); /* Upper Memory Data */
    uchpSPIMsgBuf[1] = (uint8_t)((stCommand.ushMemoryData)      & 0x00FF); /* Lower Memory Data */
    uchpSPIMsgBuf[2] = stCommand.uchMMemoryAdd & 0x7f; /* Master bit + Memory Address Combined */
    uchpSPIMsgBuf[3] = ((stCommand.uchClusterID << 4) & 0xF0) | (stCommand.uchCommand & 0x0F); /* CID + Command Combined */
    uchpSPIMsgBuf[4] = sucComputeCRC8(uchpSPIMsgBuf); /* Calculates and adds CRC to message */

    return uchpSPIMsgBuf[4];
}


/*********************************************************************
* PRIVATE svBMSSendReadDataRequest()
*---------------------------------------------------------------------
* Sends request to read data from register.
*
* Inputs:
*     uchRegAddress - register address
* Returns:
*     None
*
**********************************************************************/
static void svBMSSendReadDataRequest(uint8_t uchRegAddress)
{
    static BC_Message_t stCommandRd; /* Command structure */

    /* Populate Command Data */
    stCommandRd.ushMemoryData = 0x01; /* So far multiple register read request does not work */
    stCommandRd.uchMMemoryAdd = uchRegAddress;
    stCommandRd.uchClusterID  = BC_CLUSTER_ID;
    stCommandRd.uchCommand  = BC_READ;
    stCommandRd.uchCrc      = 0x00;

    /* Prepare message for sending */
    sucConvertMessageAndCRC(stCommandRd,TxSPIBuffer);

    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_1, RxSPIBuffer, SPI_BUFFER_LEN);
    vSetupSpiTx(SPI_PORT_1, TxSPIBuffer, SPI_BUFFER_LEN);

    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_1);
}


/*********************************************************************
* PRIVATE svBMSSendWriteDataRequest()
*---------------------------------------------------------------------
* Sends request to read data from register.
*
* Inputs:
*     uchRegAddress - register address
*     ushData - data to be written
* Returns:
*     None
*
**********************************************************************/
static void svBMSSendWriteDataRequest(uint8_t uchRegAddress, uint16_t ushData)
{

    static BC_Message_t stCommandWrt; /* Command structure */

    /* Populate Command Data */
    stCommandWrt.ushMemoryData = ushData;
    stCommandWrt.uchMMemoryAdd = uchRegAddress;
    stCommandWrt.uchClusterID  = BC_CLUSTER_ID;
    stCommandWrt.uchCommand = BC_WRITE;
    stCommandWrt.uchCrc     = 0x00;

    /* Prepare message for sending */
    sucConvertMessageAndCRC(stCommandWrt,TxSPIBuffer);

    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_1, RxSPIBuffer, SPI_BUFFER_LEN);
    vSetupSpiTx(SPI_PORT_1, TxSPIBuffer, SPI_BUFFER_LEN);

    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_1);
}


/*********************************************************************
* PRIVATE svBMSSendInitRequest()
*---------------------------------------------------------------------
* Sends request to read data from register.
*
* Inputs:
*     ushData - Cluster ID to be written
* Returns:
*     None
*
**********************************************************************/
static void svBMSSendInitRequest(uint16_t ushData)
{
    static BC_Message_t stCommandInit; /* Command structure */

    /* Populate Command Data */
    stCommandInit.ushMemoryData = ushData;
    stCommandInit.uchMMemoryAdd = BC_REG_INIT;
    stCommandInit.uchClusterID  = 0x00;
    stCommandInit.uchCommand    = BC_WRITE;
    stCommandInit.uchCrc        = 0x00;

    /* Prepare message for sending */
    sucConvertMessageAndCRC(stCommandInit,TxSPIBuffer);

    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_1, RxSPIBuffer, SPI_BUFFER_LEN);
    vSetupSpiTx(SPI_PORT_1, TxSPIBuffer, SPI_BUFFER_LEN);

    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_1);
}


/*********************************************************************
* PRIVATE svBMSSendNoopCmd()
*---------------------------------------------------------------------
* This commands sends a NO Operation Command, it generates the
* clock signal needed to retrieve data on the slave transmit register.
* Also the NO OP command will be echoed back on the following data retrieve.
* This serves to verify master/slave communication.
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
static void svBMSSendNoopCmd(void)
{
    static BC_Message_t stCommandNop; /* Command structure */

    /* Populate Command Data */
    stCommandNop.ushMemoryData = 0x5555;
    stCommandNop.uchMMemoryAdd = 0x7f;
    stCommandNop.uchClusterID  = BC_CLUSTER_ID;
    stCommandNop.uchCommand = BC_NOOP;
    stCommandNop.uchCrc     = 0x00;

    /* Prepare message for sending */
    sucConvertMessageAndCRC(stCommandNop,TxSPIBuffer);

    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_1, RxSPIBuffer, SPI_BUFFER_LEN);
    vSetupSpiTx(SPI_PORT_1, TxSPIBuffer, SPI_BUFFER_LEN);

    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_1);
}


/*********************************************************************
* PRIVATE svBMSRxMessage()
*---------------------------------------------------------------------
* This commands sends no data (all zeros) but in doing so, generates the
* clock signal needed to retrieve data from the slave transmit register.
*
* Inputs:
*     None
* Returns:
*     None
*
**********************************************************************/
static void svBMSRxMessage(uint8_t * uchBuff, uint16_t ushBufLen)
{
    /* Setup Rx and Tx Buffers */
    vSetupSpiRx(SPI_PORT_1, uchBuff, ushBufLen);
    vSetupSpiTx(SPI_PORT_1, TxZeroBuffer, ushBufLen);

    /* Initiate transmission */
    bStartSpiRxTx(SPI_PORT_1);
}


/*********************************************************************
* PRIVATE VerifyRxCRC()
*---------------------------------------------------------------------
* Verifies CRC in the incoming message
*
* Inputs:
*     ushDataBuffer - reference to data buffer
*
* Returns:
*     bool - True = CRC good, False = CRC bad
*
**********************************************************************/
static bool VerifyRxCRC(void)
{
    bool bReturnValue = true; /* Defaults to true */

    /* Calculate the CRC of the received message and compare it to 
       the received CRC */
    if(sucComputeCRC8(RxSPIBuffer) != RxSPIBuffer[4])
    {
        bReturnValue = false;
    }

    return (bReturnValue);
}


/*********************************************************************
* PRIVATE sushExtractValue()
*---------------------------------------------------------------------
* Converts from counts to voltage using the given scale value. Also
* adjusts values for integer math.
*
* Inputs:
*     ushDataBuffer - reference to data buffer
*     uiScaleValue - scale value used for conversion and scale up for integer math
*     uiShiftValue - number of shifts to return value to correct range for voltage
* Returns:
*     None
*
**********************************************************************/
static uint16_t sushExtractValue(uint8_t *ushDataBuffer, uint32_t uiScaleValue, uint32_t uiShiftValue)
{
    uint32_t uiInputValue;

    uiInputValue = ((uint32_t)(((ushDataBuffer[0] & 0x7f) << 8) | ushDataBuffer[1])) * uiScaleValue;
    uiInputValue = uiInputValue >> uiShiftValue;

    return ((uint16_t)uiInputValue);
}


/*********************************************************************
* PRIVATE sushExtractRegisterData()
*---------------------------------------------------------------------
* Extracts the register data from receive buffer.
*
* Inputs:
*     None
*
* Returns:
*     Register data
*
**********************************************************************/
static uint16_t sushExtractRegisterData(uint8_t *ushDataBuffer)
{
    uint16_t ushRegisterValue;

    ushRegisterValue = ((uint16_t)((ushDataBuffer[0] << 8) | ushDataBuffer[1]));

    return (ushRegisterValue);
}

/*********************************************************************
* PRIVATE sshExtractCurrentValue()
*---------------------------------------------------------------------
* Extracts the current value from receive buffer.
*
* Inputs:
*     None
*
* Returns:
*     Current value
*
**********************************************************************/
static int16_t sshExtractCurrentValue(void)
{
    uint16_t ushRawRegisterData;            /* Used to store raw register data */
    int16_t shRawRegisterDataSignExtend;    /* Used for sign extension from raw register data */
    int32_t iTempCurrent;                   /* Used to perform current scaling */
    ushRawRegisterData = sushExtractRegisterData(RxSPIBuffer);      /* Get the raw data bytes from the SPI buffer */
    ushRawRegisterData = ushRawRegisterData & 0x7FFF;               /* Get rid of the top bit of the raw data */
    shRawRegisterDataSignExtend = (int16_t) ushRawRegisterData;     /* Convert the data to a signed integer */
    shRawRegisterDataSignExtend = shRawRegisterDataSignExtend << 1; /* Shift the data up one bit so that we can extend the sign */
    shRawRegisterDataSignExtend = shRawRegisterDataSignExtend >> 1; /* Shift the data back down to complete the sign extension */
    iTempCurrent = (int32_t)shRawRegisterDataSignExtend * ADC_CURRENT_SCALE;    /* Perform the data scaling to get an intelligible value */
    iTempCurrent = iTempCurrent >> ISENSE_SCALE;                    /* Perform the data scaling to get an intelligible value */

    return ((int16_t)iTempCurrent); /* Return the scaled current value */
}

/*********************************************************************
* PRIVATE sshExtractNetworkVoltageValue()
*---------------------------------------------------------------------
* Extracts the network voltage value from receive buffer and battery
*   voltage.
*
* Inputs:
*   pstBmsICDataRef - reference to battery data structure
*
* Returns:
*     Network voltage value
*
**********************************************************************/
static int16_t sshExtractNetworkVoltageValue(stBms_FastData_t *pstBmsICDataRef)
{
    uint16_t ushRawRegisterData;    /* Used to store raw register data */
    int32_t iTempNetworkVoltage;    /* Used to perform intermediate math */
    ushRawRegisterData = sushExtractValue(RxSPIBuffer, ADC_NET_VOLTAGE_SCALE, ANALOGVOLTAGE_SCALE); /* Get the data bytes from the SPI buffer and scale them to the correct voltage reading*/
    iTempNetworkVoltage = ( (int32_t) ushRawRegisterData ) * NETWORK_VOLTAGE_ANALOG_SCALE;
    iTempNetworkVoltage = iTempNetworkVoltage + ( ( (int32_t) pstBmsICDataRef->ushSumOfCellsVoltage ) * NETWORK_VOLTAGE_VBATT_SCALE );
    iTempNetworkVoltage = iTempNetworkVoltage + NETWORK_VOLTAGE_EXTERNAL_OFFSET;
    iTempNetworkVoltage = iTempNetworkVoltage * NETWORK_VOLTAGE_EXTERNAL_GAIN;
    iTempNetworkVoltage = iTempNetworkVoltage + NETWORK_VOLTAGE_OUTPUT_OFFSET;
    iTempNetworkVoltage = iTempNetworkVoltage / NETWORK_VOLTAGE_OUTPUT_GAIN;

    return ((int16_t)iTempNetworkVoltage); /* Return the scaled current value */
}

/*********************************************************************
* PRIVATE sshExtractMidVoltageValue()
*---------------------------------------------------------------------
* Extracts the midpoint voltage value from receive buffer and battery
*   voltage.
*
* Inputs:
*   pstBmsICDataRef - reference to battery data structure
*
* Returns:
*     Midpoint voltage value
*
**********************************************************************/
static int16_t sshExtractMidVoltageValue(stBms_FastData_t *pstBmsICDataRef)
{
    uint16_t ushRawRegisterData;    /* Used to store raw register data */
    int32_t iTempMidVoltage;        /* Used to perform intermediate math */
    ushRawRegisterData = sushExtractValue(RxSPIBuffer, ADC_MID_VOLTAGE_SCALE, ANALOGVOLTAGE_SCALE); /* Get the data bytes from the SPI buffer and scale them to the correct voltage reading*/
    iTempMidVoltage = ( (int32_t) ushRawRegisterData ) * NETWORK_VOLTAGE_ANALOG_SCALE;
    iTempMidVoltage = iTempMidVoltage + ( ( (int32_t) pstBmsICDataRef->ushSumOfCellsVoltage ) * NETWORK_VOLTAGE_VBATT_SCALE );
    iTempMidVoltage = iTempMidVoltage + NETWORK_VOLTAGE_EXTERNAL_OFFSET;
    iTempMidVoltage = iTempMidVoltage * NETWORK_VOLTAGE_EXTERNAL_GAIN;
    iTempMidVoltage = iTempMidVoltage + NETWORK_VOLTAGE_OUTPUT_OFFSET;
    iTempMidVoltage = iTempMidVoltage / NETWORK_VOLTAGE_OUTPUT_GAIN;

    return ((int16_t)iTempMidVoltage); /* Return the scaled current value */
}

/*********************************************************************
* PRIVATE sshExtractOverCurrentDetectionValue()
*---------------------------------------------------------------------
* Extracts the current value from receive buffer.
*
* Inputs:
*     None
*
* Returns:
*     Current value
*
**********************************************************************/
static int16_t sshExtractOverCurrentDetectionValue(void)
{
    uint16_t ushRawRegisterData;    /* Used to store raw register data */
    int16_t iTempCurrent;           /* Used to perform current scaling */
    ushRawRegisterData = sushExtractValue(RxSPIBuffer, ADC_OCD_VOLTAGE_SCALE, ANALOGVOLTAGE_SCALE);      /* Get the data bytes from the SPI buffer and scale them to the correct voltage reading*/
    iTempCurrent = (int16_t)ushRawRegisterData + MCM_MON_OCD_OFFSET;    /* Remove the offset added by the LT1999 */
    iTempCurrent = iTempCurrent << MCM_MON_OCD_SCALE;    /* Perform the data scaling to get the correct current value */
    return (iTempCurrent);  /* Return the scaled current value */
}

/*********************************************************************
* PRIVATE svFindMinMaxCells()
*---------------------------------------------------------------------
* Finds cells with minimum and maximum voltages. Also calculates the stack voltage
* by performing a sum of all the individual cell voltages.
*
* Inputs:
*     pstBmsICDataRef - reference to battery data structure
* Returns:
*     None
*
**********************************************************************/
static void svFindMinMaxCells(stBms_FastData_t *pstBmsICDataRef)
{
    uint8_t ucIdx;
    uint8_t ucMinIdx = 0;
    uint8_t ucMaxIdx = 0;
    uint16_t ushMin = 0;
    uint16_t ushAvg = 0;
    uint16_t ushMax = 0;
    uint16_t uiStackVoltage = 0;

    for(ucIdx=0;ucIdx<NUM_OF_CELLS;ucIdx++)
    {
        /* Find Min Cell Voltage */
        if(pstBmsICDataRef->ushCellVoltage[ucIdx] <= pstBmsICDataRef->ushCellVoltage[ucMinIdx])
        {
            ucMinIdx = ucIdx;
            ushMin = pstBmsICDataRef->ushCellVoltage[ucIdx];
        }

        /* Find Max Cell Voltage */
        if(pstBmsICDataRef->ushCellVoltage[ucIdx] >= pstBmsICDataRef->ushCellVoltage[ucMaxIdx])
        {
            ucMaxIdx = ucIdx;
            ushMax = pstBmsICDataRef->ushCellVoltage[ucIdx];
        }
        uiStackVoltage += pstBmsICDataRef->ushCellVoltage[ucIdx];
    }
    ushAvg = uiStackVoltage / NUM_OF_CELLS;

    /* Return Min and Max values */
    pstBmsICDataRef->ucMinCellVIdx = ucMinIdx;
    pstBmsICDataRef->ucMaxCellVIdx = ucMaxIdx;
    pstBmsICDataRef->ushMinCellV = ushMin;
    pstBmsICDataRef->ushMaxCellV = ushMax;
    pstBmsICDataRef->ushAvgCellV = ushAvg;

    /* Return stack voltage from sum of cell voltages */
    pstBmsICDataRef->ushSumOfCellsVoltage = uiStackVoltage/10;
}


/*********************************************************************
* PRIVATE svCheckForCellBalancingRequests()
*---------------------------------------------------------------------
* Reads and determines if changes are to be made to the cell balancing
* feature. Copies the data to the Cell balancing structure
*
* Inputs:  None
* Returns:
*       uint16_t - Cells to be balanced.
*
**********************************************************************/
static uint16_t svCheckForCellBalancingRequests(void)
{
    BaseType_t sStatus;
    static QueueHandle_t spLocalCell_Balancing_Hdl;
    static uint16_t ushMyCellBalancing = 0;
    static uint16_t ushPrevCellBalancing = 0;
    static uint16_t ushReturnValue;

    ushReturnValue = NO_CB_CHANGE;

    /* Retrieve data from queue */
    spLocalCell_Balancing_Hdl = sGetQueueHandle(CELL_BALANCING_QUEUE);
    sStatus = xQueueReceive(spLocalCell_Balancing_Hdl , &ushMyCellBalancing, NOWAIT);
    if( sStatus == pdPASS )
    {
        /* Verify data has changed and only request to write data to BMS if it has changed */
        if(ushPrevCellBalancing != ushMyCellBalancing)
        {
            ushPrevCellBalancing = ushMyCellBalancing;
            ushReturnValue  =  ushMyCellBalancing;
        }
    }

    return(ushReturnValue);
}


/*****************************/
/* Public Functions          */
/*****************************/
/*********************************************************************
* PUBLIC vInitializeBatteryCellCtl()
*---------------------------------------------------------------------
* Battery cell controller IC initialization
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vInitializeBatteryCellCtl(void)
{
    /* Configure SPI Port */
    bConfigureSpiPort(SPI_PORT_1);

    /* Register Callbacks for SPI RX & TX */
    vSetSpiRxCallback(SPI_PORT_1, svBmsSpiRXCallback);
    vSetSpiTxCallback(SPI_PORT_1, svBmsSpiTXCallback);

    /* Write new Cluster ID in BMS, this takes the BMS out of INIT state into NORMAL state */
    svBMSSendInitRequest(BC_CLUSTER_ID);
    delay_us(MESSAGE_DELAY);

    /* Mask unwanted faults from triggering Fault Pin */
    svBMSSendWriteDataRequest(BC_REG_FAULT_MASK1, 0xE7FC);
    delay_us(MESSAGE_DELAY);

    svBMSSendWriteDataRequest(BC_REG_FAULT_MASK2, 0x00EF);
    delay_us(MESSAGE_DELAY);

    svBMSSendWriteDataRequest(BC_REG_FAULT_MASK3, 0xFFFF);
    delay_us(MESSAGE_DELAY);


    svBMSSendWriteDataRequest(BC_REG_FAULT1_STATUS, 0x0);
    delay_us(MESSAGE_DELAY);

    svBMSSendWriteDataRequest(BC_REG_FAULT2_STATUS, 0x0);
    delay_us(MESSAGE_DELAY);

    svBMSSendWriteDataRequest(BC_REG_FAULT3_STATUS, 0x0);
    delay_us(MESSAGE_DELAY);



    /* Sys CFG1 Register (CYCLIC_TIMER: OFF, I_MEAS_EN Enabled, CB_AUTO_PAUSE Disabled, CB Driver Enabled, OSC_MON Enabled) */
    svBMSSendWriteDataRequest(BC_REG_SYS_CFG1, 0x1281);
    delay_us(MESSAGE_DELAY);

    /* ADC CFG Register (Gain of 4 for ADC2, Resolution: 16bit for ADC1_A, ADC1_B, ADC2) */
    svBMSSendWriteDataRequest(BC_REG_ADC_CFG, BC_ADC_CFG_VAL);
    delay_us(MESSAGE_DELAY);

    /* OV UV Enable (Use common OV and UV value for all) */
    svBMSSendWriteDataRequest(BC_REG_OV_UV_EN, 0xFFFF);
    delay_us(MESSAGE_DELAY);

    /* GPIO CFG 1 (GPIO 3, 4, 5 and 6: analog inputs for absolute measurement) */
    svBMSSendWriteDataRequest(BC_REG_GPIO_CFG1, 0x1540);
    delay_us(MESSAGE_DELAY);

    /* Common threshold for all cells OV/UV (OV:4.16, UV:2.59) */
    svBMSSendWriteDataRequest(BC_REG_TH_ALL_CT, 0xD584);
    delay_us(MESSAGE_DELAY);

}


/*********************************************************************
* PUBLIC vCellBalancingRequest()
*---------------------------------------------------------------------
* Cell Balancing request , this function queues the data to be written
* to the appropriate register.
*
* Inputs:
*       ushCellsToBeBalanced - Bit array containing the cells to be balanced
* Returns: None
*
**********************************************************************/
void vCellBalancingRequest(uint16_t ushCellsToBeBalanced)
{
    static QueueHandle_t spLocalCell_Balancing_Hdl1;
    static uint16_t ushMyCellBalancingRequest;

    ushMyCellBalancingRequest = ushCellsToBeBalanced;

    /* Write data to queue */
    spLocalCell_Balancing_Hdl1 = sGetQueueHandle(CELL_BALANCING_QUEUE);
    xQueueOverwrite(spLocalCell_Balancing_Hdl1, &ushMyCellBalancingRequest);
}


/*********************************************************************
* PUBLIC vBatteryCellCtlInterface()
*---------------------------------------------------------------------
* Battery cell controller IC interface
*
* Inputs:
*        pstBmsICData - Battery status data.
*       ucStateCtl - State control counter.
* Returns: None
*
**********************************************************************/
void vBatteryCellCtlInterface(stBms_FastData_t *pstBmsICData, uint8_t ucStateCtl)
{
    /* Get semaphore Handle */
    SemaphoreHandle_t pstMySem_SPIBMSIC_Fault_Hdl;
    
    /* Get SPI comm fault semaphore handle */
    pstMySem_SPIBMSIC_Fault_Hdl = sGetSemaphoreHandle(BMSIC_SPI_FAULT);
        
    /* 1 Request current measurement */
    svBMSSendReadDataRequest(BC_REG_MEAS_ISENSE1);
    delay_us(MESSAGE_DELAY);

    /* State control, only perform register reads at the beginning of the 100ms frame (case 0), read current every 10ms cycle */
    switch(ucStateCtl)
    {
        case 0:
        /* 2 Turn ON CB Manual Pause */
        svBMSSendWriteDataRequest(BC_REG_SYS_CFG1, BC_CB_MAN_PAUSE_ON);
        delay_us(MESSAGE_DELAY);

        /* (1) Retrieve current measurement */
        if(VerifyRxCRC() == true)
        {
            pstBmsICData->shMeasuredCurrent = sshExtractCurrentValue();
        }
        else
        {
            /* SPI Comm Error with BMS IC */
            xSemaphoreGive(pstMySem_SPIBMSIC_Fault_Hdl);
        }

        /* 3 Start a new conversion PGA_GAIN = 4, ADC1A, ADC1B = 16bit */
        svBMSSendWriteDataRequest(BC_REG_ADC_CFG, BC_ADC_CFG_SOC_VAL);
        delay_us(MESSAGE_DELAY);
        break;


        case 1:
        /* 2 Request Fault Register with thermal shutdown status */
        svBMSSendReadDataRequest(BC_REG_FAULT2_STATUS);
        delay_us(MESSAGE_DELAY);

        /* (1) Retrieve current measurement */
        if(VerifyRxCRC() == true)
        {
            pstBmsICData->shMeasuredCurrent = sshExtractCurrentValue();
        }
        else
        {
            /* SPI Comm Error with BMS IC */
            xSemaphoreGive(pstMySem_SPIBMSIC_Fault_Hdl);
        }

        /* 3 Clear Over Voltage Fault Flags */
        svBMSSendWriteDataRequest(BC_REG_CELL_OV_FLT, 0x00);
        delay_us(MESSAGE_DELAY);

        /* (2) Retrieve thermal shutdown flag register  */
        pstBmsICData->ucThermalShutdownFlag = (sushExtractRegisterData(RxSPIBuffer) & BC_REG_THRML_SHTDWN_MASK) >> 8;

        /* 4 Clear Under Voltage Fault Flags */
        svBMSSendWriteDataRequest(BC_REG_CELL_UV_FLT, 0x00);
        delay_us(MESSAGE_DELAY);

        /* 5 Start a new conversion PGA_GAIN = 4, ADC1A, ADC1B = 16bit */
        svBMSSendWriteDataRequest(BC_REG_ADC_CFG, BC_ADC_CFG_SOC_VAL);
        delay_us(MESSAGE_DELAY);
        break;


        case 2:
        /* 2 Turn OFF CB Manual Pause */
        svBMSSendWriteDataRequest(BC_REG_SYS_CFG1, BC_CB_MAN_PAUSE_OFF);
        delay_us(MESSAGE_DELAY);

        /* (1) Retrieve current measurement */
        if(VerifyRxCRC() == true)
        {
            pstBmsICData->shMeasuredCurrent = sshExtractCurrentValue();
        }
        else
        {
            /* SPI Comm Error with BMS IC */
            xSemaphoreGive(pstMySem_SPIBMSIC_Fault_Hdl);
        }

        /* 3 Request Cell OV Fault Register */
        svBMSSendReadDataRequest(BC_REG_CELL_OV_FLT);
        delay_us(MESSAGE_DELAY);

        /* 4 Request  Cell UV Fault Register */
        svBMSSendReadDataRequest(BC_REG_CELL_UV_FLT);
        delay_us(MESSAGE_DELAY);

        /* (3) Retrieve OV FLT Register  */
        pstBmsICData->ushCellOverVoltageFlags = sushExtractRegisterData(RxSPIBuffer);

        /* 5 Request stack voltage */
        svBMSSendReadDataRequest(BC_REG_MEAS_STACK);
        delay_us(MESSAGE_DELAY);

        /* (4) Retrieve UV FLT Register  */
        pstBmsICData->ushCellUnderVoltageFlags = sushExtractRegisterData(RxSPIBuffer);

        /* 6 Request cell voltage cell 14 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL14);
        delay_us(MESSAGE_DELAY);

        /* (5) Retrieve stack voltage measurement */
        pstBmsICData->ushStackVoltage = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, STACKVOLTAGE_SCALE)/10;

        /* 7 Request cell voltage Cell 13 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL13);
        delay_us(MESSAGE_DELAY);

        /* (6) Retrieve cell 14 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_14] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 8 Request cell voltage Cell 12 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL12);
        delay_us(MESSAGE_DELAY);

        /* (7) Retrieve cell 13 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_13] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 9 Request cell voltage Cell 11 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL11);
        delay_us(MESSAGE_DELAY);

        /* (8) Retrieve cell 12 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_12] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 10 Request cell voltage Cell 10 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL10);
        delay_us(MESSAGE_DELAY);

        /* (9) Retrieve cell 11 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_11] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 11 Request cell voltage Cell 9 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL9);
        delay_us(MESSAGE_DELAY);

        /* (10) Retrieve cell 10 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_10] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 12 Request cell voltage Cell 8 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL8);
        delay_us(MESSAGE_DELAY);

        /* (11) Retrieve cell 9 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_9] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 13 Request cell voltage Cell 7 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL7);
        delay_us(MESSAGE_DELAY);

        /* (12) Retrieve cell 8 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_8] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 14 Request cell voltage Cell 6 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL6);
        delay_us(MESSAGE_DELAY);

        /* (13) Retrieve cell 7 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_7] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 15 Request cell voltage Cell 5 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL5);
        delay_us(MESSAGE_DELAY);

        /* (14) Retrieve cell 6 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_6] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 16 Request cell voltage Cell 4 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL4);
        delay_us(MESSAGE_DELAY);

        /* (15) Retrieve cell 5 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_5] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 17 Request cell voltage Cell 3 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL3);
        delay_us(MESSAGE_DELAY);

        /* (16) Retrieve cell 4 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_4] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 18 Request cell voltage Cell 2 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL2);
        delay_us(MESSAGE_DELAY);

        /* (17) Retrieve cell 3 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_3] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 19 Request cell voltage Cell 1 */
        svBMSSendReadDataRequest(BC_REG_MEAS_CELL1);
        delay_us(MESSAGE_DELAY);

        /* (18) Retrieve cell 2 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_2] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* 20 Start a new conversion PGA_GAIN = 4, ADC1A, ADC1B = 16bit */
        svBMSSendWriteDataRequest(BC_REG_ADC_CFG, BC_ADC_CFG_SOC_VAL);
        delay_us(MESSAGE_DELAY);

        /* (19) Retrieve cell 1 voltage measurement */
        pstBmsICData->ushCellVoltage[CELL_1] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);

        /* Determine the Min and Max cell voltages for this frame */
        svFindMinMaxCells(pstBmsICData);
        break;


        case 3:
        /* 2 Request analog input 6 (MCM_MON_NET-) */
        svBMSSendReadDataRequest(BC_REG_MEAS_AN6);
        delay_us(MESSAGE_DELAY);

        /* (1) Retrieve current measurement */
        if(VerifyRxCRC() == true)
        {
            pstBmsICData->shMeasuredCurrent = sshExtractCurrentValue();
        }
        else
        {
            /* SPI Comm Error with BMS IC */
            xSemaphoreGive(pstMySem_SPIBMSIC_Fault_Hdl);
        }

        /* 3 Request analog input 5 (MCM_MON_3.3V) */
        svBMSSendReadDataRequest(BC_REG_MEAS_AN5);
        delay_us(MESSAGE_DELAY);

        /* (2) Retrieve analog input 6 voltage measurement (MCM_MON_NET-) */
        pstBmsICData->ushAnalogVoltage[ANALOG_IN_6] = sshExtractNetworkVoltageValue(pstBmsICData);

        /* 4 Request analog input 4 (MCM_MON_12V) */
        svBMSSendReadDataRequest(BC_REG_MEAS_AN4);
        delay_us(MESSAGE_DELAY);

        /* (3) Retrieve analog input 5 voltage measurement (MCM_MON_3.3V) */
        pstBmsICData->ushAnalogVoltage[ANALOG_IN_5] = sushExtractValue(RxSPIBuffer, ADC_3V3_VOLTAGE_SCALE, ANALOGVOLTAGE_SCALE);

        /* 5 Request analog input 3 (MCM_MON_5V) */
        svBMSSendReadDataRequest(BC_REG_MEAS_AN3);
        delay_us(MESSAGE_DELAY);

        /* (4) Retrieve analog input 4 voltage measurement (MCM_MON_12V) */
        pstBmsICData->ushAnalogVoltage[ANALOG_IN_4] = sushExtractValue(RxSPIBuffer, ADC_12V_VOLTAGE_SCALE, ANALOGVOLTAGE_SCALE);

        /* 6 Request analog input 2 (MCM_MON_MID) */
        svBMSSendReadDataRequest(BC_REG_MEAS_AN2);
        delay_us(MESSAGE_DELAY);

        /* (5) Retrieve analog input 3 voltage measurement (MCM_MON_5V) */
        pstBmsICData->ushAnalogVoltage[ANALOG_IN_3] = sushExtractValue(RxSPIBuffer, ADC_5V_VOLTAGE_SCALE, ANALOGVOLTAGE_SCALE);

        /* 7 Request analog input 1 (MCM_MON_OCD) */
        svBMSSendReadDataRequest(BC_REG_MEAS_AN1);
        delay_us(MESSAGE_DELAY);

        /* (6) Retrieve analog input 2 voltage measurement (MCM_MON_MID) */
        pstBmsICData->ushAnalogVoltage[ANALOG_IN_2] = sshExtractMidVoltageValue(pstBmsICData);

        /* 8 Request analog input 0 (Not used) */
        svBMSSendReadDataRequest(BC_REG_MEAS_AN0);
        delay_us(MESSAGE_DELAY);

        /* (7) Retrieve analog input 1 voltage measurement (MCM_MON_OCD) */
        pstBmsICData->ushAnalogVoltage[ANALOG_IN_1] = sushExtractValue(RxSPIBuffer, ADC_OCD_VOLTAGE_SCALE, ANALOGVOLTAGE_SCALE); // ADC_OCD_VOLTAGE_SCALE sshExtractOverCurrentDetectionValue();

        /* 9 Start a new conversion PGA_GAIN = 4, ADC1A, ADC1B = 16bit */
        svBMSSendWriteDataRequest(BC_REG_ADC_CFG, BC_ADC_CFG_SOC_VAL);
        delay_us(MESSAGE_DELAY);

        /* (8) Retrieve analog input 0 voltage measurement (Not used) */
        pstBmsICData->ushAnalogVoltage[ANALOG_IN_0] = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, ANALOGVOLTAGE_SCALE);
        break;


        default:
        /* 2 Start a new conversion PGA_GAIN = 4, ADC1A, ADC1B = 16bit */
        svBMSSendWriteDataRequest(BC_REG_ADC_CFG, BC_ADC_CFG_SOC_VAL);
        delay_us(MESSAGE_DELAY);

        /* (1) Retrieve current measurement */
        if(VerifyRxCRC() == true)
        {
            pstBmsICData->shMeasuredCurrent = sshExtractCurrentValue();
        }
        else
        {
            /* SPI Comm Error with BMS IC */
            xSemaphoreGive(pstMySem_SPIBMSIC_Fault_Hdl);
        }
        break;
    }

    /* Update maximum charge current */
    if(pstBmsICData->shMaxChargeCurrent < pstBmsICData->shMeasuredCurrent)
    {
        pstBmsICData->shMaxChargeCurrent = pstBmsICData->shMeasuredCurrent;
    }

    /* Update maximum discharge current */
    if(pstBmsICData->shMeasuredCurrent < pstBmsICData->shMaxDischargeCurrent)
    {
        pstBmsICData->shMaxDischargeCurrent = pstBmsICData->shMeasuredCurrent;
    }
}

/*********************************************************************
* PUBLIC vBatteryCellCtlInterfaceWrite()
*---------------------------------------------------------------------
* Battery cell controller IC interface
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vBatteryCellCtlInterfaceWrite(uint8_t ucStateCtl)
{
    uint16_t ushRegValue;
    static uint16_t ushMyCellsToBeBalanced;
    static uint16_t ushCBPreviousState = 0;
    static uint8_t ushRefreshCounter = 0; /* This counter is used to refresh the CB timer for enabled cells, when it resets to 0... approximately  */

    /* State control, only perform 2 or 3 SPI reads/writes maximum every 5ms cycle */
    switch(ucStateCtl)
    {
        case 4:
        /* Get Cells to be balanced */
        ushMyCellsToBeBalanced = svCheckForCellBalancingRequests();

        /* Increment Counter */
        ushRefreshCounter++;

        /* Trigger a refresh on the CB timer for the enabled cells */
        if(ushRefreshCounter == CB_REFRESH_TIME)
        {
            ushMyCellsToBeBalanced = ushCBPreviousState;
            ushCBPreviousState = 0;
        }

        if(ushMyCellsToBeBalanced != NO_CB_CHANGE)
        {
            /* Reset counter */
            ushRefreshCounter = 0;

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_1_MASK) != (ushMyCellsToBeBalanced & CELL_1_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_1_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB1_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_2_MASK) != (ushMyCellsToBeBalanced & CELL_2_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_2_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB2_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_3_MASK) != (ushMyCellsToBeBalanced & CELL_3_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_3_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB3_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_4_MASK) != (ushMyCellsToBeBalanced & CELL_4_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_4_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB4_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_5_MASK) != (ushMyCellsToBeBalanced & CELL_5_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_5_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB5_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_6_MASK) != (ushMyCellsToBeBalanced & CELL_6_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_6_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB6_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_7_MASK) != (ushMyCellsToBeBalanced & CELL_7_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_7_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB7_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_8_MASK) != (ushMyCellsToBeBalanced & CELL_8_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_8_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB8_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_9_MASK) != (ushMyCellsToBeBalanced & CELL_9_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_9_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB9_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_10_MASK) != (ushMyCellsToBeBalanced & CELL_10_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_10_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB10_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_11_MASK) != (ushMyCellsToBeBalanced & CELL_11_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_11_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB11_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_12_MASK) != (ushMyCellsToBeBalanced & CELL_12_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_12_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB12_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_13_MASK) != (ushMyCellsToBeBalanced & CELL_13_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_13_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB13_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }

            /* Check if the status of the CB for this particular cell changed */
            if((ushCBPreviousState & CELL_14_MASK) != (ushMyCellsToBeBalanced & CELL_14_MASK))
            {
                /* Determine if the CB Cell CFG is to be set ON or OFF (20 min timer)*/
                ushRegValue = ((ushMyCellsToBeBalanced & CELL_14_MASK) != 0) ? 0x0214 : 0x0000;

                /* Write CB Cell CFG Register */
                svBMSSendWriteDataRequest(BC_REG_CB14_CFG, ushRegValue);
                delay_us(MESSAGE_DELAY);
            }
            ushCBPreviousState = ushMyCellsToBeBalanced;
        }
        break;

        default:
        ;   /* Empty - TBD more operations */
        break;
    }
}



/*********************************************************************
* PUBLIC ushReadRegisterData()
*---------------------------------------------------------------------
* Read register data
*
* Inputs:
*     uchRegAddres - Address of the register to read
*     bDataOrVoltage - READ_REGISTER_DATA or READ_VOLTAGE_VALUE
* Returns:
*     uint16_t - register data or voltage value
*
**********************************************************************/
uint16_t ushReadRegisterData(uint8_t uchRegAddress, uint8_t bDataOrVoltage)
{
    uint16_t ushDataValue;

    /* Request read register */
    svBMSSendReadDataRequest(uchRegAddress);
    delay_us(MESSAGE_DELAY);

    /* Read requested data only, do not send any commands */
    svBMSRxMessage(RxSPIBuffer, 5);
    delay_us(MESSAGE_DELAY);

    if(bDataOrVoltage == READ_REGISTER_DATA)
    {
        /* Retrieve register data */
        ushDataValue = sushExtractRegisterData(RxSPIBuffer);
    }
    else
    {
        /* Retrieve voltage value */
        ushDataValue = sushExtractValue(RxSPIBuffer, ADC_RAW_VOLTAGE_SCALE, CELLVOLTAGE_SCALE);
    }

    return (ushDataValue);
}


/*********************************************************************
* PUBLIC ushWriteRegisterData()
*---------------------------------------------------------------------
* Write data to register
*
* Inputs:
*       uchRegAddres - Address of the register to write
*       ushRgisterData - Data to write
* Returns: None
*
**********************************************************************/
void ushWriteRegisterData(uint8_t uchRegAddress, uint16_t ushRgisterData)
{
    /* Write register data */
    svBMSSendWriteDataRequest(uchRegAddress, ushRgisterData);
    delay_us(MESSAGE_DELAY);
}


/*********************************************************************
* PUBLIC ushWriteRegisterDataNoDelay()
*---------------------------------------------------------------------
* Write data to register
*
* Inputs:
*       uchRegAddres - Address of the register to write
*       ushRgisterData - Data to write
* Returns: None
*
**********************************************************************/
void ushWriteRegisterDataNoDelay(uint8_t uchRegAddress, uint16_t ushRgisterData)
{
    /* Write register data */
    svBMSSendWriteDataRequest(uchRegAddress, ushRgisterData);
}


/*********************************************************************
* PUBLIC vStartConversion()
*---------------------------------------------------------------------
* Read register data
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vStartConversion(void)
{

    /* Start a new conversion PGA_GAIN = 4, ADC1A, ADC1B = 16bit */
    svBMSSendWriteDataRequest(BC_REG_ADC_CFG, BC_ADC_CFG_SOC_VAL);
    delay_us(800);
}



/******************************** End of File ********************************/
