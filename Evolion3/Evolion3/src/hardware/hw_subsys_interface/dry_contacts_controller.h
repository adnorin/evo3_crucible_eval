#ifndef DRY_CONTACTS_CONTROLLER_H_
#define DRY_CONTACTS_CONTROLLER_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: dry_contacts_controller.h                                      */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for dry_contacts_controller.c, which provides prototypes      */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
#define DRY_CONTACT_TB_MASK   0x01
#define DRY_CONTACT_RJ45_MASK 0x02


/*********************************************************************
* PUBLIC vTurnDryContactsOnOFF()
*---------------------------------------------------------------------
* Heater control
*
* Inputs:
*     ucDryContactsStates - Bit encoded variable containing status of
*        dry contacts GPIOs.
*
* Returns: None
*
**********************************************************************/
void vTurnDryContactsOnOFF(uint8_t ucDryContactsStates);

#endif /* DRY_CONTACTS_CONTROLLER_H_ */