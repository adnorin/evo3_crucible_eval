#ifndef TEMP_SENSOR_IC_H_
#define TEMP_SENSOR_IC_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: temp_sensor_ic.h                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for temp_sensor_ic.c, which provides prototypes for the       */
/* public interface functions. Also includes types and enumeration           */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
/* Eval Board and Rev A Board */
#define CELL_TEMP_SENSOR_0_ADDR 0x4A /* Slave address of the C2/C3 temp sensor LM75B */
#define CELL_TEMP_SENSOR_1_ADDR 0x49 /* Slave address of the C3/C4 temp sensor LM75B */
#define CELL_TEMP_SENSOR_2_ADDR 0x4B /* Slave address of the C10/C11 temp sensor LM75B */
#define CELL_TEMP_SENSOR_3_ADDR 0x4C /* Slave address of the C11/C12 temp sensor LM75B */
#define ELEC_TEMP_SENSOR_0_ADDR 0x48 /* Slave address of the Discharge MOSFETs Temperature sensor LM75A */
#define ELEC_TEMP_SENSOR_1_ADDR 0x4D /* Slave address of the Charge MOSFETs Temperature sensor LM75A */
#define ELEC_TEMP_SENSOR_2_ADDR 0x4E /* Slave address of the Regulated Current Temperature sensor LM75A */
#define ELEC_TEMP_SENSOR_3_ADDR 0x4F /* Slave address of the Controller Board Temperature sensor LM75A */

/* B-Sample Board */
#define CELL_B_SAMPLE_TEMP_SENSOR_0_ADDR 0x48   /* Slave address of the C1/C2 temp sensor PCT2075D */
#define CELL_B_SAMPLE_TEMP_SENSOR_1_ADDR 0x49   /* Slave address of the C3/C4 temp sensor PCT2075D */
#define CELL_B_SAMPLE_TEMP_SENSOR_2_ADDR 0x4A   /* Slave address of the C6/C7 temp sensor PCT2075D */
#define CELL_B_SAMPLE_TEMP_SENSOR_3_ADDR 0x4E   /* Slave address of the C9/C8 temp sensor PCT2075D */
#define CELL_B_SAMPLE_TEMP_SENSOR_4_ADDR 0x4D   /* Slave address of the C11/C10 temp sensor PCT2075D */
#define CELL_B_SAMPLE_TEMP_SENSOR_5_ADDR 0x4C   /* Slave address of the C14/C13 temp sensor PCT2075D */
#define ELEC_B_SAMPLE_TEMP_SENSOR_0_ADDR 0x75   /* Slave address of the Discharge MOSFETs Temperature sensor PCT2075D */
#define ELEC_B_SAMPLE_TEMP_SENSOR_1_ADDR 0x76   /* Slave address of the Charge MOSFETs Temperature sensor PCT2075D */
#define ELEC_B_SAMPLE_TEMP_SENSOR_2_ADDR 0x77   /* Slave address of the Regulated Current Path Temperature sensor PCT2075D */
#define ELEC_B_SAMPLE_TEMP_SENSOR_3_ADDR 0x74   /* Slave address of the Controller Board Temperature sensor PCT2075D */

#define I2C_RX_BUFFER_LEN       2   /* Buffer length */
#define I2C_TX_BUFFER_LEN       2   /* Buffer length */
#define I2C_TX_OS_BUFFER_LEN    3   /* Buffer length */
#define I2C_TX_PT_BUFFER_LEN    1   /* Buffer length */

#ifdef I2C_TEST_ADR_SETUP
    #if (I2C_TEST_ADR_SETUP == 2)
        #define NUM_CELL_TEMP_SENSORS   6   /* Number of cell temperature sensors */
        #define NUM_TEMP_SENSORS        10  /* Number of temperature sensors 10 total pre-B boards */
        #define ELEC_TEMP_SENSOR_START_INDEX    6   /* Array index of first cell temperature sensor */
        #define BMS_IC_TEMP_SENSOR_INDEX        9   /* Temperature sensor array index for sensor closest to BMS IC. Used for disabling balancing */
    #else
        #define NUM_CELL_TEMP_SENSORS   4   /* Number of cell temperature sensors */
        #define NUM_TEMP_SENSORS        8   /* Number of temperature sensors 8 total rev A boards */
        #define ELEC_TEMP_SENSOR_START_INDEX    4   /* Array index of first cell temperature sensor */
        #define BMS_IC_TEMP_SENSOR_INDEX        7   /* Temperature sensor array index for sensor closest to BMS IC. Used for disabling balancing */
    #endif
#else
    #define NUM_CELL_TEMP_SENSORS   4   /* Number of cell temperature sensors */
    #define NUM_TEMP_SENSORS        8   /* Number of temperature sensors 8 total rev A boards */
    #define ELEC_TEMP_SENSOR_START_INDEX    4   /* Array index of first cell temperature sensor */
    #define BMS_IC_TEMP_SENSOR_INDEX        7   /* Temperature sensor array index for sensor closest to BMS IC. Used for disabling balancing */
#endif

#define NUM_ELEC_TEMP_SENSORS   4   /* Number of electronics temperature sensors */
#define CELL_TEMP_SENSOR_START_INDEX    0   /* Array index of first cell temperature sensor */

#define TEMP_READ_REG   0x00
#define CONFIG_REG      0x01
#define T_HYST_REG      0x02
#define T_OS_REG        0x03

#define OVER_TEMP_AND_HYSTERESIS_SCALAR 2

/*********************************************************************
* PUBLIC usReadRawTempValue()
*---------------------------------------------------------------------
* Measure sensor temperatures
*
* Inputs:
*   ucTempSensorAddress - Temperature sensor I2C address
* Returns: None
*
**********************************************************************/
uint16_t ushReadRawTempValue(uint8_t ucTempSensorAddress);


/*********************************************************************
* PUBLIC usReadRawTempValue_Test()
*---------------------------------------------------------------------
* Measure sensor temperatures
*
* Inputs:
*   ucTempSensorAddress - Temperature sensor I2C address
* Returns: None
*
**********************************************************************/
uint16_t usReadRawTempValue_Test(uint8_t ucTempSensorAddress);

/*********************************************************************
* PUBLIC vInitializeTempSensors()
*---------------------------------------------------------------------
* Temperature sensors initialization
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vInitializeTempSensors(void);


/*********************************************************************
* PUBLIC vSetOverTempAndHystLimit()
*---------------------------------------------------------------------
* Temperature sensors initialization Over Temp Limit
*
* Inputs:
*   ucTempSensorAdd - I2C address of sensor
*   ushValue - Over Temp Value or Hysteresis Value
*   ucPointerReg - Pointer register, Tos or Thyst
* Returns: None
*
**********************************************************************/
void vSetOverTempAndHystLimit(uint8_t ucTempSensorAdd, uint16_t ushValue, uint8_t ucPointerReg);


/*********************************************************************
* PUBLIC vTempSensorsShutdown()
*---------------------------------------------------------------------
* Shutdown temperature sensors
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vTempSensorsShutdown(void);


/*********************************************************************
* PUBLIC vTempSensorsWakeUp()
*---------------------------------------------------------------------
* Wake up temperature sensors
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vTempSensorsWakeUp(void);

/*********************************************************************
* PUBLIC vTempSensorsSetOverTempAndHysteresis()
*---------------------------------------------------------------------
* Temperature sensors configuration
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vTempSensorsSetOverTempAndHysteresis(void);

#endif /* TEMP_SENSOR_IC_H_ */