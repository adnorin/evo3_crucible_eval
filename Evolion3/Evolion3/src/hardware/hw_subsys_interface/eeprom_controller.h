#ifndef EEPROM_CONTROLLER_H_
#define EEPROM_CONTROLLER_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: eeprom_controller.h                                            */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for eeprom_controller.c, which provides prototypes for        */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
#include <iface_rtos_start.h>

#define EEPROM_READ     0x03
#define EEPROM_WRITE    0x02
#define EEPROM_ENABLE   0x06
#define EEPROM_DISABLE  0x04
#define EEPROM_RDSR     0x05
#define EEPROM_WRSR     0x01
#define EEPROM_WRID     0x82
#define EEPROM_RDID     0x83

#define EEPROM_SINGLE_BYTE_READ_LEN     5
#define EEPROM_SINGLE_BYTE_WRITE_LEN    5
#define EEPROM_ENABLE_LEN               1
#define EEPROM_DISABLE_LEN              1
#define EEPROM_STATUS_REG_LEN           2
#define EEPROM_MESSAGE_DELAY            40
#define EEPROM_MAX_BUFFER_SIZE          260
#define EEPROM_READ_DELAY               50

#define EEPROM_ID_0 0x0A
#define EEPROM_ID_1 0xFA
#define EEPROM_ID_2 0xCA
#define EEPROM_ID_3 0xDE


/*********************************************************************
* PUBLIC vEEPROM_Init()
*---------------------------------------------------------------------
* Eeprom Spi comm init
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vEEPROM_Init(void);


/*********************************************************************
* PUBLIC vEEPROM_WriteEnable()
*---------------------------------------------------------------------
* Send Write Enable command
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vEEPROM_WriteEnable(void);


/*********************************************************************
* PUBLIC vEEPROM_WriteDisable()
*---------------------------------------------------------------------
* Send Write Disable command
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vEEPROM_WriteDisable(void);


/*********************************************************************
* PUBLIC vEEPROM_ReadStatusRegister()
*---------------------------------------------------------------------
* Read Status Register command
*
* Inputs:  None
* Returns:
*   uint8_t - Status Register
*
**********************************************************************/
uint8_t vEEPROM_ReadStatusRegister(void);


/*********************************************************************
* PUBLIC vEEPROM_WriteStatusRegister()
*---------------------------------------------------------------------
* Write Status Register command
* IMPORTANT: * Must wait 10ms after this TX for EEPROM to complete writing process.
*
* Inputs:
*   uint8_t - Status Register
* Returns: None
*
**********************************************************************/
void vEEPROM_WriteStatusRegister(uint8_t ucStatusReg);


/*********************************************************************
* PUBLIC vEEPROM_Read_ID()
*---------------------------------------------------------------------
* Send Read ID Page command
*
* Inputs:
*   uiAddress - 8 bit source address (LSB) only
* Returns:
*   uint8_t - data retrieved from source address
*
**********************************************************************/
uint8_t vEEPROM_Read_ID(uint8_t ucAddress);


/*********************************************************************
* PUBLIC vEEPROM_Write_ID()
*---------------------------------------------------------------------
* Send Write ID Page command
* IMPORTANT: * Must wait 10ms after this TX for EEPROM to complete writing process.
*
* Inputs:
*   uiAddress - 8 bit source address (LSB) only
*   ucData - data to be written (byte)
* Returns: None
*
**********************************************************************/
void vEEPROM_Write_ID(uint8_t uiAddress, uint8_t ucData);


/*********************************************************************
* PUBLIC vEEPROM_Verify_ID()
*---------------------------------------------------------------------
* Reads the ID page and Verifies the values read are correct
*
* Inputs:
*   None
* Returns:
*   boolean - True = Verified OK, False = Error
*
**********************************************************************/
bool bEEPROM_Verify_ID(void);


/*********************************************************************
* PUBLIC vEEPROM_Write()
*---------------------------------------------------------------------
* Send Write command
* IMPORTANT: * Must wait 10ms after this TX for EEPROM to complete writing process.
*
* Inputs:
*   uiAddress - 24 bit destination address
*   ucData - data to be written (byte)
* Returns: None
*
**********************************************************************/
void vEEPROM_Write(uint32_t uiAddress, uint8_t ucData);


/*********************************************************************
* PUBLIC vEEPROM_Read()
*---------------------------------------------------------------------
* Send Read command
*
* Inputs:
*   uiAddress - 24 bit source address
* Returns:
*   uint8_t - data retrieved from source address
*
**********************************************************************/
uint8_t vEEPROM_Read(uint32_t uiAddress);


/*********************************************************************
* PUBLIC vEEPROM_ReadBlock()
*---------------------------------------------------------------------
* Send Read a block of data command
*
* Inputs:
*   uiAddress - 24 bit source initial address
*   ucDataBuffer - destination data buffer pointer
*   ushNumBytes - number of bytes to be read
* Returns:
*   uint16_t - Number of bytes read
*
**********************************************************************/
uint16_t vEEPROM_ReadBlock(uint32_t uiAddress, uint8_t* ucDataBuffer, uint16_t ushNumBytes);


/*********************************************************************
* PUBLIC vEEPROM_WriteBlock()
*---------------------------------------------------------------------
* Send Write a block of data command
* IMPORTANT: * Must wait 10ms after this TX for EEPROM to complete writing process.
*
* Inputs:
*   uiAddress - 24 bit source address
*   ucDataBuffer - source data buffer pointer
*   ushNumBytes - number of bytes to be written
* Returns:
*   uint16_t - Number of bytes written
*
**********************************************************************/
uint16_t vEEPROM_WriteBlock(uint32_t uiAddress, uint8_t* ucDataBuffer, uint16_t ushNumBytes);

#endif /* EEPROM_CONTROLLER_H_ */