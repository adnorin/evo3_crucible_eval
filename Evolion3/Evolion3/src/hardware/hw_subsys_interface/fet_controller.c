/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: fet_controller.c                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <conf_gpio.h>
#include <fet_controller.h>

/*********************************************************************
* PUBLIC vSetFetState()
*---------------------------------------------------------------------
* FET management routines
*
* Inputs:
*     ushFetStates - Bit encoded variable containing status of the Fet GPIOs
* Returns: None
*
**********************************************************************/
void vSetFetState(uint16_t ushFetStates)
{
    static uint16_t sushPrevFetStates = 0x00;

    /* Only update GPIOs states if the value has changed */
    if(ushFetStates != sushPrevFetStates)
    {
        /* Turn FET GPIOs ON/OFF based on input */

        /* Discharge FET */
        if((FULL_DISCHARGE_FET_ENABLE_MASK & ushFetStates)!=0)
        {
            /* FET ON */
            OUTPUT_SET_HIGH(FULL_DISCHARGE_FET_ENABLE_PIN);
        }
        else
        {
            /* FET OFF */
            OUTPUT_SET_LOW(FULL_DISCHARGE_FET_ENABLE_PIN);
        }

        /* Safety charge FET enable */
        if((SAFETY_CHARGE_FET_ENABLE_MASK & ushFetStates)!=0)
        {
            /* FET ON */
            OUTPUT_SET_HIGH(SAFETY_CHARGE_FET_ENABLE_PIN);
        }
        else
        {
            /* FET OFF */
            OUTPUT_SET_LOW(SAFETY_CHARGE_FET_ENABLE_PIN);
        }

        /* Charge FET */
        if((FULL_CHARGE_FET_ENABLE_MASK & ushFetStates)!=0)
        {
            /* FET ON */
            OUTPUT_SET_HIGH(FULL_CHARGE_FET_ENABLE_PIN);
        }
        else
        {
            /* FET OFF */
            OUTPUT_SET_LOW(FULL_CHARGE_FET_ENABLE_PIN);
        }

        /* Regulated discharge FET */
        if((REG_DISCHARGE_FET_ENABLE_MASK & ushFetStates)!=0)
        {
            /* FET ON */
            OUTPUT_SET_HIGH(REG_DISCHARGE_FET_ENABLE_PIN);
        }
        else
        {
            /* FET OFF */
            OUTPUT_SET_LOW(REG_DISCHARGE_FET_ENABLE_PIN);
        }

        /* Regulated Charge FET */
        if((REG_CHARGE_FET_ENABLE_MASK & ushFetStates)!=0)
        {
            /* FET ON */
            OUTPUT_SET_HIGH(REG_CHARGE_FET_ENABLE_PIN);
        }
        else
        {
            /* FET OFF */
            OUTPUT_SET_LOW(REG_CHARGE_FET_ENABLE_PIN);
        }

        /* Regulated Current Level 1 */
        if((REGULATED_CURRENT_LVL_1_MASK & ushFetStates)!=0)
        {
            /* FET ON */
            OUTPUT_SET_HIGH(REGULATED_CURRENT_LVL_1_PIN);
        }
        else
        {
            /* FET OFF */
            OUTPUT_SET_LOW(REGULATED_CURRENT_LVL_1_PIN);
        }

        /* Regulated Current Level 2 */
        if((REGULATED_CURRENT_LVL_2_MASK & ushFetStates)!=0)
        {
            /* FET ON */
            OUTPUT_SET_HIGH(REGULATED_CURRENT_LVL_2_PIN);
        }
        else
        {
            /* FET OFF */
            OUTPUT_SET_LOW(REGULATED_CURRENT_LVL_2_PIN);
        }

        /* Regulated Current Level 3 */
        if((REGULATED_CURRENT_LVL_3_MASK & ushFetStates)!=0)
        {
            /* FET ON */
            OUTPUT_SET_HIGH(REGULATED_CURRENT_LVL_3_PIN);
        }
        else
        {
            /* FET OFF */
            OUTPUT_SET_LOW(REGULATED_CURRENT_LVL_3_PIN);
        }
    }
    /* Save last Fet States */
    sushPrevFetStates = ushFetStates;
}

/******************************** End of File ********************************/