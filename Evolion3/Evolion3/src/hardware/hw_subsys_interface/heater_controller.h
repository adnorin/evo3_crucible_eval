#ifndef HEATER_CONTROLLER_H_
#define HEATER_CONTROLLER_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: heater_controller.h                                            */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for heater_controller.c, which provides prototypes            */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
#define HEATER_ON_ENABLE_MASK 0x01


/*********************************************************************
* PUBLIC vTurnHeaterOnOFF()
*---------------------------------------------------------------------
* Heater control
*
* Inputs:
*     ucHeaterStates - Bit encoded variable containing status of heater GPIO
* Returns: None
*
**********************************************************************/
void vTurnHeaterOnOFF(uint8_t ucHeaterStates);


#endif /* HEATER_CONTROLLER_H_ */