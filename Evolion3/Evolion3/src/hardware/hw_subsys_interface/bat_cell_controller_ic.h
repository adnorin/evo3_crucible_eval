#ifndef BAT_CELL_CONTROLLER_IC_H_
#define BAT_CELL_CONTROLLER_IC_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: bat_cell_controller_ic.h                                       */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for bat_cell_controller_ic.c, which provides prototypes for   */
/* the public interface functions. Also includes types and enumeration       */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/
#include <iface_rtos_start.h>

/* Miscellaneous definitions */
#define NUM_OF_CELLS            14
#define CRC_SEED_VAL            0x42
#define CRC_POLYNOMIAL          0x2F
#define CRC_NUM_OF_BITS         8
#define MESSAGE_LEN_NOCRC       4
#define SPI_BUFFER_LEN          5
#define BC_CLUSTER_ID           0x05
#define BC_NOOP                 0x00
#define BC_READ                 0x01
#define BC_WRITE                0x02
#define MESSAGE_DELAY           40
#define EOC_TIME_DELAY          600
#define READ_REGISTER_DATA      0
#define READ_VOLTAGE_VALUE      1
#define NO_CB_CHANGE            0xC000
#define CB_REFRESH_TIME         1500 /* 2.5 minutes @ 100ms interval */
#define BC_CB_MAN_PAUSE_OFF     0x1281
#define BC_CB_MAN_PAUSE_ON      0x12A1
#define BC_ADC_CFG_SOC_VAL      0x08FF /* b0-1:ADC2_DEF: 0b11 = 16bit, b2-3:ADC1_B_DEF: 0b11 = 16bit, b4-5:ADC1_A_DEF: 0b11 = 16bit,  b6:DIS_CH_COMP: 0b1 = CT1,2 charge comp. enabled, b7:CC_RST: 0b1 = reset CC, b8-10:PGA_GAIN: 0b000 = gain of 4, b11:SOC: 0b1 = start conversion, b12-15:TAG_ID: 0b0000 = 0 */
#define BC_ADC_CFG_VAL          0x00FF /* b0-1:ADC2_DEF: 0b11 = 16bit, b2-3:ADC1_B_DEF: 0b11 = 16bit, b4-5:ADC1_A_DEF: 0b11 = 16bit,  b6:DIS_CH_COMP: 0b1 = CT1,2 charge comp. enabled, b7:CC_RST: 0b1 = reset CC, b8-10:PGA_GAIN: 0b000 = gain of 4, b11:SOC: 0b0 = no conversion, b12-15:TAG_ID: 0b0000 = 0 */
#define WRITE_CLEAR_REGISTER_DATA                       0x0000
#define WRITE_DA_DIAG_REGISTER_DATA                     0x0080
#define WRITE_NUMB_ODD_REGISTER_DATA                    0x6230
#define WRITE_CT_ODD_EVEN_REGISTER_DATA_10              0x0008
#define WRITE_CT_ODD_EVEN_REGISTER_DATA_01              0x0004
#define WRITE_CT_LEAK_POLARITY_REGISTER_DATA_10         0x0020
#define WRITE_CT_LEAK_POLARITY_REGISTER_DATA_11         0x0060
#define WRITE_CB_DRIVER_REGISTER_DATA                   0x0200
#define WRITE_CB_DRIVER_DISABLE_CYCLIC_REGISTER_DATA    0x1201
#define WRITE_CB_OL_ODD_EVEN_REGISTER_DATA_10           0x0002
#define WRITE_CB_OL_ODD_EVEN_REGISTER_DATA_01           0x0001
#define WRITE_CT_OV_UV_OL_ODD_EVEN_REGISTER_DATA_101    0x0014
#define WRITE_CT_OV_UV_OL_ODD_EVEN_REGISTER_DATA_110    0x0018
#define WRITE_I_MUX_REGISTER_DATA_11                    0x1800
#define WRITE_I_MUX_REGISTER_DATA_10                    0x1000
#define WRITE_I_DISABLE_REGISTER_DATA                   0x1001
#define WRITE_GAIN_RESET_COUNTER_REGISTER_DATA          0x083F
#define WRITE_GPIO_ANALOG_REGISTER_DATA                 0x1555
#define WRITE_GPIO_OPERATE_REGISTER_DATA                0x1540
#define WRITE_WEAK_PULL_DOWN_REGISTER_DATA              0x0200
#define WRITE_ISENSE_OL_REGISTER_DATA                   0x0400

/* Scaling Values */
#define NO_SCALE                1
#define NO_SHIFT                0
#define ADC_RAW_VOLTAGE_SCALE   10000U
#define ADC_OCD_VOLTAGE_SCALE   20315U  /* 20000 (raw scalar) * 1.015775 gain correction = 20315 */
#define ADC_MID_VOLTAGE_SCALE   20169U  /* 20000 (raw scalar) * 1.008450 gain correction = 20169 */
#define ADC_5V_VOLTAGE_SCALE    24516U  /* 20000 (raw scalar) * 1.021150 gain correction * 1.2004008016032064128256513026052 from HW resistor divider = 24516 */
#define ADC_12V_VOLTAGE_SCALE   51069U  /* 20000 (raw scalar) * 1.019850 gain correction * 2.5037593984962406015037593984962 from HW resistor divider = 51069 */
#define ADC_3V3_VOLTAGE_SCALE   20538U  /* 20000 (raw scalar) * 1.026900 gain correction = 20538 */
#define ADC_NET_VOLTAGE_SCALE   20200U  /* 20000 (raw scalar) * 1.010000 gain correction = 20200 */
#define CELLVOLTAGE_SCALE       16
#define ANALOGVOLTAGE_SCALE     17
#define STACKVOLTAGE_SCALE      12
#define ADC_CURRENT_SCALE       -3804
#define ISENSE_SCALE            11
#define MCM_MON_OCD_SCALE       1
#define MCM_MON_OCD_OFFSET      -2500
#define NETWORK_VOLTAGE_ANALOG_SCALE    10000
#define NETWORK_VOLTAGE_VBATT_SCALE     -2367
#define NETWORK_VOLTAGE_EXTERNAL_OFFSET -3934509
#define NETWORK_VOLTAGE_EXTERNAL_GAIN   -33
#define NETWORK_VOLTAGE_OUTPUT_OFFSET   468750000
#define NETWORK_VOLTAGE_OUTPUT_GAIN     78125

/* Register definitions for MC33771A */
#define BC_REG_INIT             0x01
#define BC_REG_SYS_CFG_GLOBAL   0x02
#define BC_REG_SYS_CFG1         0x03
#define BC_REG_SYS_CFG2         0x04
#define BC_REG_SYS_DIAG         0x05
#define BC_REG_ADC_CFG          0x06
#define BC_REG_ADC_OFFSET_COMP  0x07
#define BC_REG_OV_UV_EN         0x08
#define BC_REG_CELL_OV_FLT      0x09
#define BC_REG_CELL_UV_FLT      0x0A
#define BC_REG_RESERVED         0x0B
#define BC_REG_CB1_CFG          0x0C
#define BC_REG_CB2_CFG          0x0D
#define BC_REG_CB3_CFG          0x0E
#define BC_REG_CB4_CFG          0x0F
#define BC_REG_CB5_CFG          0x10
#define BC_REG_CB6_CFG          0x11
#define BC_REG_CB7_CFG          0x12
#define BC_REG_CB8_CFG          0x13
#define BC_REG_CB9_CFG          0x14
#define BC_REG_CB10_CFG         0x15
#define BC_REG_CB11_CFG         0x16
#define BC_REG_CB12_CFG         0x17
#define BC_REG_CB13_CFG         0x18
#define BC_REG_CB14_CFG         0x19
#define BC_REG_CB_OPEN_FLT      0x1A
#define BC_REG_CB_SHORT_FLT     0x1B
#define BC_REG_CB_DRM_STS       0x1C
#define BC_REG_GPIO_CFG1        0x1D
/*--------------------------------*/
#define BC_REG_GPIO_CFG2        0x1E
#define BC_REG_GPIO_STS         0x1F
#define BC_REG_AN_OT_UT_FLT     0x20
#define BC_REG_GPIO_SHORT_ANX_OPEN_STS  0x21
#define BC_REG_I_STATUS         0x22
#define BC_REG_COM_STATUS       0x23
#define BC_REG_FAULT1_STATUS    0x24
#define BC_REG_FAULT2_STATUS    0x25
#define BC_REG_FAULT3_STATUS    0x26
#define BC_REG_FAULT_MASK1      0x27
#define BC_REG_FAULT_MASK2      0x28
#define BC_REG_FAULT_MASK3      0x29
#define BC_REG_WAKEUP_MASK1     0x2A
#define BC_REG_WAKEUP_MASK2     0x2B
#define BC_REG_WAKEUP_MASK3     0x2C
#define BC_REG_CC_NB_SAMPLES    0x2D
#define BC_REG_COULOMB_CNT1     0x2E
#define BC_REG_COULOMB_CNT2     0x2F
#define BC_REG_MEAS_ISENSE1     0x30
#define BC_REG_MEAS_ISENSE2     0x31
#define BC_REG_MEAS_STACK       0x32
#define BC_REG_MEAS_CELL14      0x33
#define BC_REG_MEAS_CELL13      0x34
#define BC_REG_MEAS_CELL12      0x35
#define BC_REG_MEAS_CELL11      0x36
#define BC_REG_MEAS_CELL10      0x37
#define BC_REG_MEAS_CELL9       0x38
#define BC_REG_MEAS_CELL8       0x39
#define BC_REG_MEAS_CELL7       0x3A
#define BC_REG_MEAS_CELL6       0x3B
#define BC_REG_MEAS_CELL5       0x3C
#define BC_REG_MEAS_CELL4       0x3D
#define BC_REG_MEAS_CELL3       0x3E
#define BC_REG_MEAS_CELL2       0x3F
#define BC_REG_MEAS_CELL1       0x40
#define BC_REG_MEAS_AN6         0x41
#define BC_REG_MEAS_AN5         0x42
/*--------------------------------*/
#define BC_REG_MEAS_AN4         0x43
#define BC_REG_MEAS_AN3         0x44
#define BC_REG_MEAS_AN2         0x45
#define BC_REG_MEAS_AN1         0x46
#define BC_REG_MEAS_AN0         0x47
#define BC_REG_MEAS_IC_TEMP     0x48
#define BC_REG_MEAS_VBG_DIAG_ADC1A  0x49
#define BC_REG_MEAS_VBG_DIAG_ADC1B  0x4A
#define BC_REG_TH_ALL_CT        0x4B
#define BC_REG_TH_CT14          0x4C
#define BC_REG_TH_CT13          0x4D
#define BC_REG_TH_CT12          0x4E
#define BC_REG_TH_CT11          0x4F
#define BC_REG_TH_CT10          0x50
#define BC_REG_TH_CT9           0x51
#define BC_REG_TH_CT8           0x52
#define BC_REG_TH_CT7           0x53
#define BC_REG_TH_CT6           0x54
#define BC_REG_TH_CT5           0x55
#define BC_REG_TH_CT4           0x56
#define BC_REG_TH_CT3           0x57
#define BC_REG_TH_CT2           0x58
#define BC_REG_TH_CT1           0x59
#define BC_REG_TH_AN6_OT        0x5A
#define BC_REG_TH_AN5_OT        0x5B
#define BC_REG_TH_AN4_OT        0x5C
#define BC_REG_TH_AN3_OT        0x5D
#define BC_REG_TH_AN2_OT        0x5E
#define BC_REG_TH_AN1_OT        0x5F
#define BC_REG_TH_AN0_OT        0x60
#define BC_REG_TH_AN6_UT        0x61
#define BC_REG_TH_AN5_UT        0x62
#define BC_REG_TH_AN4_UT        0x63
#define BC_REG_TH_AN3_UT        0x64
#define BC_REG_TH_AN2_UT        0x65
#define BC_REG_TH_AN1_UT        0x66
#define BC_REG_TH_AN0_UT        0x67
#define BC_REG_ISENSE_OC        0x68
/*--------------------------------*/
#define BC_REG_TH_COULOMB_CNT_MSB   0x69
#define BC_REG_TH_COULOMB_CNT_LSB   0x6A
#define BC_REG_SILICON_REV      0x6B
#define BC_REG_EEPROM_CNTL      0x6C
#define BC_REG_DED_ENCODE1      0x6D
#define BC_REG_DED_ENCODE2      0x6E
#define BC_REG_FUSE_MIRROR_DATA 0x6F
#define BC_REG_FUSE_MIRROR_CNTL 0x70
#define BC_REG_THRML_SHTDWN_MASK    0x0100
/*--------------------------------*/

/* Indexes */
#define CELL_1      0
#define CELL_2      1
#define CELL_3      2
#define CELL_4      3
#define CELL_5      4
#define CELL_6      5
#define CELL_7      6
#define CELL_8      7
#define CELL_9      8
#define CELL_10     9
#define CELL_11     10
#define CELL_12     11
#define CELL_13     12
#define CELL_14     13

#define SENSOR_1      0
#define SENSOR_2      1
#define SENSOR_3      2
#define SENSOR_4      3
#define SENSOR_5      4
#define SENSOR_6      5
#define SENSOR_7      6
#define SENSOR_8      7
#define SENSOR_9      8
#define SENSOR_10     9
#define SENSOR_11     10
#define SENSOR_12     11
#define SENSOR_13     12
#define SENSOR_14     13

#define CELL_1_MASK     0x0001
#define CELL_2_MASK     0x0002
#define CELL_3_MASK     0x0004
#define CELL_4_MASK     0x0008
#define CELL_5_MASK     0x0010
#define CELL_6_MASK     0x0020
#define CELL_7_MASK     0x0040
#define CELL_8_MASK     0x0080
#define CELL_9_MASK     0x0100
#define CELL_10_MASK    0x0200
#define CELL_11_MASK    0x0400
#define CELL_12_MASK    0x0800
#define CELL_13_MASK    0x1000
#define CELL_14_MASK    0x2000

#define ANALOG_IN_0 0
#define ANALOG_IN_1 1
#define ANALOG_IN_2 2
#define ANALOG_IN_3 3
#define ANALOG_IN_4 4
#define ANALOG_IN_5 5
#define ANALOG_IN_6 6

/* Custom Types */
typedef struct
{
    uint16_t ushMemoryData;
    uint8_t  uchMMemoryAdd;
    uint8_t  uchClusterID;
    uint8_t  uchCommand;
    uint8_t  uchCrc;
}BC_Message_t;

/* Custom Types */
typedef struct
{
    uint16_t ushCellsToBeBalanced;
    bool bUpdateCBConfRegs;
}BC_CellBalancing_t;


/*********************************************************************
* PUBLIC vInitializeBatteryCellCtl()
*---------------------------------------------------------------------
* Battery cell controller IC initialization
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vInitializeBatteryCellCtl(void);


/*********************************************************************
* PUBLIC vBatteryCellCtlInterface()
*---------------------------------------------------------------------
* Battery cell controller IC interface
*
* Inputs:
        pstBmsICData - Battery status data.
*       ucStateCtl - State control counter.
* Returns: None
*
**********************************************************************/
void vBatteryCellCtlInterface(stBms_FastData_t *pstBmsICData, uint8_t ucStateCtl);


/*********************************************************************
* PUBLIC ushReadRegisterData()
*---------------------------------------------------------------------
* Read register data
*
* Inputs:
*     uchRegAddres - Address of the register to read
*     bDataOrVoltage - READ_REGISTER_DATA or READ_VOLTAGE_VALUE
* Returns:
*     uint16_t - register data or voltage value
*
**********************************************************************/
uint16_t ushReadRegisterData(uint8_t uchRegAddress, uint8_t bDataOrVoltage);


/*********************************************************************
* PUBLIC ushWriteRegisterData()
*---------------------------------------------------------------------
* Write data to register
*
* Inputs:
*       uchRegAddres - Address of the register to write
*       ushRgisterData - Data to write
* Returns: None
*
**********************************************************************/
void ushWriteRegisterData(uint8_t uchRegAddress, uint16_t ushRgisterData);


/*********************************************************************
* PUBLIC ushWriteRegisterDataNoDelay()
*---------------------------------------------------------------------
* Write data to register
*
* Inputs:
*       uchRegAddres - Address of the register to write
*       ushRgisterData - Data to write
* Returns: None
*
**********************************************************************/
void ushWriteRegisterDataNoDelay(uint8_t uchRegAddress, uint16_t ushRgisterData);


/*********************************************************************
* PUBLIC vStartConversion()
*---------------------------------------------------------------------
* Read register data
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vStartConversion(void);


/*********************************************************************
* PUBLIC vBatteryCellCtlInterfaceWrite()
*---------------------------------------------------------------------
* Battery cell controller IC interface
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vBatteryCellCtlInterfaceWrite(uint8_t ucStateCtl);


/*********************************************************************
* PUBLIC vCellBalancingRequest()
*---------------------------------------------------------------------
* Cell Balancing request , this function queues the data to be written
* to the appropriate register.
*
* Inputs:
*       ushCellsToBeBalanced - Bit array containing the cells to be balanced
* Returns: None
*
**********************************************************************/
void vCellBalancingRequest(uint16_t ushCellsToBeBalanced);



#endif /* BAT_CELL_CONTROLLER_IC_H_ */