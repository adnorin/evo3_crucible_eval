/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: dry_contacts_controller.c                                      */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*  Controls GPIO to turn Dry contacts On/Off                                */
/*                                                                           */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <conf_gpio.h>
#include <heater_controller.h>
#include <dry_contacts_controller.h>

/*********************************************************************
* PUBLIC vTurnDryContactsOnOFF()
*---------------------------------------------------------------------
* Heater control
*
* Inputs:
*     ucDryContactsStates - Bit encoded variable containing status of 
*        dry contacts GPIOs.
*
* Returns: None
*
**********************************************************************/
void vTurnDryContactsOnOFF(uint8_t ucDryContactsStates)
{
    static uint8_t sucPrevDryContactsStates = 0x55;
    
    /* Only update GPIOs states if the value has changed */
    if(ucDryContactsStates != sucPrevDryContactsStates)
    {
        /* Turn Terminal Block DC GPIO ON/OFF based on input */
        if((DRY_CONTACT_TB_MASK & ucDryContactsStates)!=0)
        {
            /* DC ON */
            OUTPUT_SET_HIGH(DRY_CONTACT_TB_PIN);
        }
        else
        {
            /* DC OFF */
            OUTPUT_SET_LOW(DRY_CONTACT_TB_PIN);
        }
        
        /* Turn RJ45 DC GPIO ON/OFF based on input */
        if((DRY_CONTACT_RJ45_MASK & ucDryContactsStates)!=0)
        {
            /* DC ON */
            OUTPUT_SET_HIGH(DRY_CONTACT_RJ45_PIN);
        }
        else
        {
            /* DC OFF */
            OUTPUT_SET_LOW(DRY_CONTACT_RJ45_PIN);
        }       
    }
    /* Save last DC States */
    sucPrevDryContactsStates = ucDryContactsStates;
}

/******************************** End of File ********************************/