
 #ifndef GPIO_CONTROLLER_H_
 #define GPIO_CONTROLLER_H_
 /*****************************************************************************/
 /* (C) Copyright 2017 by SAFT                                                */
 /* All rights reserved                                                       */
 /*                                                                           */
 /* This program is the property of Saft America, Inc. and can only be        */
 /* used and copied with the prior written authorization of SAFT.             */
 /*                                                                           */
 /* Any whole or partial copy of this program in either its original form or  */
 /* in a modified form must mention this copyright and its owner.             */
 /*****************************************************************************/
 /* PROJECT: Evolion 3 Software                                               */
 /*****************************************************************************/
 /* FILE NAME: gpio_controller.h                                              */
 /*****************************************************************************/
 /* AUTHOR : Adnorin L. Mendez                                                */
 /* LAST MODIFIER: None                                                       */
 /*****************************************************************************/
 /* FILE DESCRIPTION:                                                         */
 /*                                                                           */
 /* Header file for gpio_controller.c , which provides prototypes for         */
 /* the public interface functions. Also includes types and enumeration       */
 /* definitions.                                                              */
 /*                                                                           */
 /*****************************************************************************/
 
 #define GPIO_MC33771_RESET_MASK			0x0001
 #define GPIO_NETWORK_MEASUREMENT_MASK		0x0002
 #define GPIO_SLEEP_PWDN_12V_MASK			0x0004
 #define GPIO_OVER_CURRENT_LATCH_RESET_MASK	0x0008
 #define GPIO_ANTI_THEFT_MASK				0x0010
 #define GPIO_FET_SELF_TEST_MASK			0x0020
 #define GPIO_FET_SELF_TEST_VOLTAGE_MASK	0x0040
 #define GPIO_HMI_LED_MASK					0x0080


/*********************************************************************
* PUBLIC vSetGPIOState()
*---------------------------------------------------------------------
* GPIO management routines
*
* Inputs:
*     ushGPIOStates - Bit encoded variable containing status of the GPIOs
* Returns: None
*
**********************************************************************/
void vSetGPIOState(uint16_t ushGPIOStates);

 #endif /* GPIO_CONTROLLER_H_ */