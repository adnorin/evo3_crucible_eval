/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: conf_gpio.c                                                    */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: Greg Cordell                                               */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* GPIO pin definitions                                                      */
/*                                                                           */
/*****************************************************************************/

#ifndef CONF_GPIO_H_
#define CONF_GPIO_H_

/** \name MC33771 reset definitions
 * @{
 */
#define MC33771_RESET_PIN       PIN_PA01
#define MC33771_RESET_ACTIVE    true
#define MC33771_RESET_INACTIVE  !MC33771_RESET_ACTIVE
/** @} */

/** \name External RS485 enable definitions
 * @{
 */
#define RS485_EXT_TX_ENABLE_RX_DISABLE_PIN      PIN_PA02
#define RS485_EXT_TX_ENABLE_RX_DISABLE_ACTIVE   true
#define RS485_EXT_TX_ENABLE_RX_DISABLE_INACTIVE !RS485_EXT_TX_ENABLE_RX_DISABLE_ACTIVE
/** @} */

/** \name Internal RS485 enable definitions
 * @{
 */
#define RS485_INT_TX_ENABLE_RX_DISABLE_PIN      PIN_PA03
#define RS485_INT_TX_ENABLE_RX_DISABLE_ACTIVE   true
#define RS485_INT_TX_ENABLE_RX_DISABLE_INACTIVE !RS485_INT_TX_ENABLE_RX_DISABLE_ACTIVE
/** @} */

/** \name 400A over-current comparator definitions
 * @{
 */
#define OVER_CURRENT_400A_NOTIFICATION_PIN      PIN_PB04
#define OVER_CURRENT_400A_NOTIFICATION_ACTIVE   true
#define OVER_CURRENT_400A_NOTIFICATION_INACTIVE !OVER_CURRENT_400A_NOTIFICATION_ACTIVE
/** @} */

/** \name Over-current in charge notification definitions
 * @{
 */
#define OVER_CURRENT_192A_NOTIFICATION_PIN      PIN_PB05
#define OVER_CURRENT_192A_NOTIFICATION_ACTIVE   false
#define OVER_CURRENT_192A_NOTIFICATION_INACTIVE !OVER_CURRENT_192A_NOTIFICATION_ACTIVE
/** @} */

/** \name Network voltage presence wake-up definitions
 * @{
 */
#define NETWORK_WAKEUP_NOTIFICATION_PIN         PIN_PB06
#define NETWORK_WAKEUP_NOTIFICATION_ACTIVE      false
#define NETWORK_WAKEUP_NOTIFICATION_INACTIVE    !NETWORK_WAKEUP_NOTIFICATION_ACTIVE
/** @} */

/** \name BMS IC fault notification definitions
 * @{
 */
#define BMS_IC_FAULT_NOTIFICATION_PIN       PIN_PB07
#define BMS_IC_FAULT_NOTIFICATION_ACTIVE    true
#define BMS_IC_FAULT_NOTIFICATION_INACTIVE  !BMS_IC_FAULT_NOTIFICATION_ACTIVE
/** @} */

/** \name Redundant cell under-voltage notification definitions
 * @{
 */
#define REDUNDANT_UV_NOTIFICATION_PIN       PIN_PB08
#define REDUNDANT_UV_NOTIFICATION_ACTIVE    true
#define REDUNDANT_UV_NOTIFICATION_INACTIVE  !REDUNDANT_UV_NOTIFICATION_ACTIVE
/** @} */

/**
 * \name Fuse status definitions
 *  @{ */
#define NETWORK_MEASUREMENT_ENABLE_PIN      PIN_PB09
#define NETWORK_MEASUREMENT_ENABLE_ACTIVE   true
#define NETWORK_MEASUREMENT_ENABLE_INACTIVE !NETWORK_MEASUREMENT_ENABLE_ACTIVE
/** @} */

/** \name SPI1 Slave Select definitions
 * @{
 */
#define SLAVE_SELECT_PIN_P1             PIN_PA05
#define SLAVE_SELECT_PIN_P1_ACTIVE      true
#define SLAVE_SELECT_PIN_P1_INACTIVE    !SLAVE_SELECT_PIN_P1_ACTIVE
/** @} */

/** \name Dry contact in RJ45 connector definitions
 * @{
 */
#define DRY_CONTACT_RJ45_PIN    PIN_PA10
#define DRY_CONTACT_RJ45_CLOSED false
#define DRY_CONTACT_RJ45_OPEN   !DRY_CONTACT_RJ45_CLOSED
/** @} */

/** \name Dry contact on terminal blocks connector definitions
 * @{
 */
#define DRY_CONTACT_TB_PIN      PIN_PA11
#define DRY_CONTACT_TB_CLOSED   true
#define DRY_CONTACT_TB_OPEN     !DRY_CONTACT_TB_CLOSED
/** @} */

/** \name Charge FET control definitions
 * @{
 */
#define FULL_CHARGE_FET_ENABLE_PIN          PIN_PB12
#define FULL_CHARGE_FET_ENABLE_ACTIVE       true
#define FULL_CHARGE_FET_ENABLE_INACTIVE     !FULL_CHARGE_FET_ENABLE_ACTIVE
/** @} */

/** \name Discharge FET control definitions
 * @{
 */
#define FULL_DISCHARGE_FET_ENABLE_PIN       PIN_PB13
#define FULL_DISCHARGE_FET_ENABLE_ACTIVE    true
#define FULL_DISCHARGE_FET_ENABLE_INACTIVE  !FULL_DISCHARGE_FET_ENABLE_ACTIVE
/** @} */

/** \name Regulated charge FET control definitions
 * @{
 */
#define REG_CHARGE_FET_ENABLE_PIN           PIN_PB14
#define REG_CHARGE_FET_ENABLE_ACTIVE        true
#define REG_CHARGE_FET_ENABLE_INACTIVE      !REG_CHARGE_FET_ENABLE_ACTIVE
/** @} */

/** \name Regulated discharge FET control definitions
 * @{
 */
#define REG_DISCHARGE_FET_ENABLE_PIN        PIN_PB15
#define REG_DISCHARGE_FET_ENABLE_ACTIVE     true
#define REG_DISCHARGE_FET_ENABLE_INACTIVE   !REG_DISCHARGE_FET_ENABLE_ACTIVE

/** \name Over temperature on one or more sensors definitions
 * @{
 */
#define OVER_TEMPERATURE_NOTIFICATION_PIN       PIN_PA12
#define OVER_TEMPERATURE_NOTIFICATION_ACTIVE    false
#define OVER_TEMPERATURE_NOTIFICATION_INACTIVE  !OVER_TEMPERATURE_NOTIFICATION_ACTIVE
/** @} */

/** \name Sleep/Power down 12v components definitions
 * @{
 */
#define SLEEP_PWDN_12V_COMP_PIN         PIN_PA13
#define SLEEP_PWDN_12V_COMP_ACTIVE      false
#define SLEEP_PWDN_12V_COMP_INACTIVE    !SLEEP_PWDN_12V_COMP_ACTIVE
/** @} */

/** \name USB-derived 3.3V active definitions
 * @{
 */
#define USB_3V3_PRESENT_NOTIFICATION_PIN        PIN_PA14
#define USB_3V3_PRESENT_NOTIFICATION_ACTIVE     true
#define USB_3V3_PRESENT_NOTIFICATION_INACTIVE   !USB_3V3_PRESENT_NOTIFICATION_ACTIVE
/** @} */

/** \name Electronics over-temperature latch reset definitions
 * @{
 */
#define OVER_CURRENT_LATCH_RESET_PIN        PIN_PA15
#define OVER_CURRENT_LATCH_RESET_ACTIVE     false
#define OVER_CURRENT_LATCH_RESET_INACTIVE   !OVER_CURRENT_LATCH_RESET_ACTIVE
/** @} */

/** \name Regulated current selection, lvl 1 definitions
 * @{
 */
#define REGULATED_CURRENT_LVL_1_PIN         PIN_PA16 //
#define REGULATED_CURRENT_LVL_1_ACTIVE      true
#define REGULATED_CURRENT_LVL_1_INACTIVE    !REGULATED_CURRENT_LVL_1_ACTIVE
/** @} */

/** \name Regulated current selection, lvl 2 definitions
 * @{
 */
#define REGULATED_CURRENT_LVL_2_PIN         PIN_PA17 //
#define REGULATED_CURRENT_LVL_2_ACTIVE      true
#define REGULATED_CURRENT_LVL_2_INACTIVE    !REGULATED_CURRENT_LVL_2_ACTIVE
/** @} */

/** \name Regulated current selection, lvl 3 definitions
 * @{
 */
#define REGULATED_CURRENT_LVL_3_PIN         PIN_PA18 //
#define REGULATED_CURRENT_LVL_3_ACTIVE      true
#define REGULATED_CURRENT_LVL_3_INACTIVE    !REGULATED_CURRENT_LVL_3_ACTIVE
/** @} */

/** \name Notification of OT from electronics comparator definitions
 * @{
 */
#define REGULATED_CURRENT_OVER_TEMP_LATCH_CLEAR_PIN         PIN_PA19
#define REGULATED_CURRENT_OVER_TEMP_LATCH_CLEAR_ACTIVE      true
#define REGULATED_CURRENT_OVER_TEMP_LATCH_CLEAR_INACTIVE    !REGULATED_CURRENT_OVER_TEMP_LATCH_CLEAR_ACTIVE
/** @} */

/** \name SPI2 Slave Select definitions
 * @{
 */
#define SLAVE_SELECT_PIN_P2             PIN_PB17
#define SLAVE_SELECT_PIN_P2_ACTIVE      true
#define SLAVE_SELECT_PIN_P2_INACTIVE    !SLAVE_SELECT_PIN_P2_ACTIVE
/** @} */

/** \name Safety charge FET enable definitions
 * @{
 */
#define SAFETY_CHARGE_FET_ENABLE_PIN        PIN_PA20
#define SAFETY_CHARGE_FET_ENABLE_ACTIVE     true
#define SAFETY_CHARGE_FET_ENABLE_INACTIVE   !SAFETY_CHARGE_FET_ENABLE_ACTIVE
/** @} */

/** \name Heater enable definitions
 * @{
 */
#define HEATER_ENABLE_PIN       PIN_PA21
#define HEATER_ENABLE_ACTIVE    true
#define HEATER_ENABLE_INACTIVE  !HEATER_ENABLE_ACTIVE
/** @} */

/** \name Anti theft option input definitions
 * @{
 */
#define ANTI_THEFT_OPTION_IN_PIN        PIN_PA27
#define ANTI_THEFT_OPTION_IN_ACTIVE     true
#define ANTI_THEFT_OPTION_IN_INACTIVE   !ANTI_THEFT_OPTION_IN_ACTIVE
/** @} */

/** \name Anti theft option output definitions
 * @{
 */
#define ANTI_THEFT_OPTION_OUT_PIN       PIN_PA28
#define ANTI_THEFT_OPTION_OUT_ACTIVE    true
#define ANTI_THEFT_OPTION_OUT_INACTIVE  !ANTI_THEFT_OPTION_OUT_ACTIVE
/** @} */

/** \name FET self test definitions
 * @{
 */
#define FET_SELF_TEST_PIN       PIN_PB30
#define FET_SELF_TEST_ACTIVE    true
#define FET_SELF_TEST_INACTIVE  !FET_SELF_TEST_ACTIVE
/** @} */

/** \name Fet self test #2 definitions
 * @{
 */
#define FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_PIN        PIN_PB31
#define FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_ACTIVE     true
#define FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_INACTIVE   !FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_ACTIVE
/** @} */

/** \name HMI LED Driver Enable definitions
 * @{
 */
#define HMI_LED_DRIVER_ENA_PIN      PIN_PB00
#define HMI_LED_DRIVER_ENA_ACTIVE   false
#define HMI_LED_DRIVER_ENA_INACTIVE !HMI_LED_DRIVER_ENA_ACTIVE
/** @} */

/** \name Redundant cell over-voltage notification definitions
 * @{
 */
#define REDUNDANT_OV_NOTIFICATION_PIN       PIN_PB01
#define REDUNDANT_OV_NOTIFICATION_ACTIVE    true
#define REDUNDANT_OV_NOTIFICATION_INACTIVE  !REDUNDANT_OV_NOTIFICATION_ACTIVE
/** @} */

/** \name SOC Button (HMI) definitions
 * @{
 */
#define HMI_SOC_PIN             PIN_PB02
#define HMI_SOC_PIN_ACTIVE      true
#define HMI_SOC_PIN_INACTIVE    !REG_DISCHARGE_FET_ENABLE_ACTIVE
/** @} */

/** \name Power On Button (HMI) definitions
 * @{
 */
#define HMI_POWER_ON_PIN                    PIN_PB03
#define HMI_POWER_ON_PIN_ACTIVE             true
#define HMI_POWER_ON_PIN_INACTIVE           !REG_DISCHARGE_FET_ENABLE_ACTIVE
/** @} */

/** \name Macros for setting pins high, low, and to toggle
 * @{
 */
#define OUTPUT_SET_HIGH(gpio)   port_pin_set_output_level(gpio,true)
#define OUTPUT_SET_LOW(gpio)    port_pin_set_output_level(gpio,false)
#define OUTPUT_SET(gpio,state)  port_pin_set_output_level(gpio,state)
#define OUTPUT_TOGGLE(gpio)     port_pin_toggle_output_level(gpio)
/** @} */
#endif
