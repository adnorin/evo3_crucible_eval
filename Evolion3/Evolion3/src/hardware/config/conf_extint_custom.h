/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: conf_extint.c                                                  */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: Greg Cordell                                               */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* GPIO pin definitions                                                      */
/*                                                                           */
/*****************************************************************************/

#ifndef CONF_EXTINT_H_
#define CONF_EXTINT_H_

/** \name Over-current in discharge notification definitions
 * @{
 */
#define OVER_CURRENT_400A_NOTIFICATION_EIC_PIN      PIN_PB04A_EIC_EXTINT4
#define OVER_CURRENT_400A_NOTIFICATION_EIC_MUX      MUX_PB04A_EIC_EXTINT4
#define OVER_CURRENT_400A_NOTIFICATION_EIC_PINMUX   PINMUX_PB04A_EIC_EXTINT4
#define OVER_CURRENT_400A_NOTIFICATION_EIC_LINE     04
/** @} */

/** \name Over-current in charge notification definitions
 * @{
 */
#define OVER_CURRENT_192A_NOTIFICATION_EIC_PIN      PIN_PB05A_EIC_EXTINT5
#define OVER_CURRENT_192A_NOTIFICATION_EIC_MUX      MUX_PB05A_EIC_EXTINT5
#define OVER_CURRENT_192A_NOTIFICATION_EIC_PINMUX   PINMUX_PB05A_EIC_EXTINT5
#define OVER_CURRENT_192A_NOTIFICATION_EIC_LINE     05
/** @} */

/** \name Network voltage presence wake-up definitions
 * @{
 */
#define NETWORK_WAKEUP_NOTIFICATION_EIC_PIN       PIN_PB06A_EIC_EXTINT6
#define NETWORK_WAKEUP_NOTIFICATION_EIC_MUX       MUX_PB06A_EIC_EXTINT6
#define NETWORK_WAKEUP_NOTIFICATION_EIC_PINMUX    PINMUX_PB06A_EIC_EXTINT6
#define NETWORK_WAKEUP_NOTIFICATION_EIC_LINE      06
/** @} */

/** \name SOC Button (HMI) definitions
 * @{
 */
#define HMI_SOC_PIN_EIC_PIN             PIN_PB02A_EIC_EXTINT2
#define HMI_SOC_PIN_EIC_MUX             MUX_PB02A_EIC_EXTINT2
#define HMI_SOC_PIN_EIC_PINMUX          PINMUX_PB02A_EIC_EXTINT2
#define HMI_SOC_PIN_EIC_LINE            02
/** @} */

/** \name Power On Button (HMI) definitions
 * @{
 */
#define HMI_POWER_ON_PIN_EIC_PIN            PIN_PB03A_EIC_EXTINT3
#define HMI_POWER_ON_PIN_EIC_MUX            MUX_PB03A_EIC_EXTINT3
#define HMI_POWER_ON_PIN_EIC_PINMUX         PINMUX_PB03A_EIC_EXTINT3
#define HMI_POWER_ON_PIN_EIC_LINE           03
/** @} */

/** \name Over temperature on one or more sensors definitions
 * @{
 */
#define OVER_TEMPERATURE_NOTIFICATION_EIC_PIN     PIN_PA12A_EIC_EXTINT12
#define OVER_TEMPERATURE_NOTIFICATION_EIC_MUX     MUX_PA12A_EIC_EXTINT12
#define OVER_TEMPERATURE_NOTIFICATION_EIC_PINMUX  PINMUX_PA12A_EIC_EXTINT12
#define OVER_TEMPERATURE_NOTIFICATION_EIC_LINE    12
/** @} */

#endif
