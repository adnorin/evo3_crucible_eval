#ifndef IFACE_EXTINT_H_
#define IFACE_EXTINT_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: iface_extint.h                                                 */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_extint.c, which provides prototypes for the          */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/


/*********************************************************************
* PUBLIC vConfigureExternalInterruptChannels()
*---------------------------------------------------------------------
* Configures external interrupt channels
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureExternalInterruptChannels(void);


/*********************************************************************
* PUBLIC vConfigureOperationalExternalInterruptCallbacks()
*---------------------------------------------------------------------
* Registers and enables external interrupts to be run during operation
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureOperationalExternalInterruptCallbacks(void);


/*********************************************************************
* PUBLIC vConfigureSleepExternalInterruptCallbacks()
*---------------------------------------------------------------------
* Registers and enables external interrupts to be run in sleep
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureSleepExternalInterruptCallbacks(void);


/*********************************************************************
* PUBLIC vConfigureButtonExternalInterruptCallbacks()
*---------------------------------------------------------------------
* Registers and enables external interrupts to be run when button presses
* are detected.
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureButtonExternalInterruptCallbacks(void);


#endif /* IFACE_EXTINT_H_ */