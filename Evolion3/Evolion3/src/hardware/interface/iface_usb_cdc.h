#ifndef IFACE_USB_CDC_H_
#define IFACE_USB_CDC_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: iface_usb_cdc.h                                                */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_usb_cdc.c, which provides prototypes for the         */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

/*********************************************************************
* PUBLIC vStartUSBCDC()
*---------------------------------------------------------------------
* Starts USB CDC interface
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vStartUSBCDC(void);

/*********************************************************************
* PUBLIC vStopUSBCDC()
*---------------------------------------------------------------------
* Stops USB CDC interface
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vStopUSBCDC(void);

/********************************************************************
* PUBLIC uiUSBGetRxCount()
*--------------------------------------------------------------------
* Get the number of data bytes in the USB receive buffer
*
* Inputs: None
* Returns:
*   Number of bytes in USB receive buffer
*
********************************************************************/
uint32_t uiUSBGetRxCount(void);

/********************************************************************
* PUBLIC uiUSBGetTxFree()
*--------------------------------------------------------------------
* Get the number of data bytes available in the USB transmit buffer
*
* Inputs: None
* Returns:
*   Number of bytes free in USB transmit buffer
*
********************************************************************/
uint32_t uiUSBGetTxFree(void);

/********************************************************************
* PUBLIC uiUSBRxDataBuffer()
*--------------------------------------------------------------------
* Receive data bytes from the USB receive buffer
*
* Inputs:
*   vpBuffer - where the received data should be put
*   uiSize -  number of bytes to be received
* Returns:
*   0 if successful,
*   uiSize if unsuccessful
*
********************************************************************/
uint32_t uiUSBRxDataBuffer(void* vpBuffer, uint32_t uiSize);

/********************************************************************
* PUBLIC uiUSBTxDataBuffer()
*--------------------------------------------------------------------
* Push data bytes to the USB transmit buffer
*
* Inputs:
*   vpBuffer - the data to be transmitted
*   uiSize -  number of bytes to be transmitted
* Returns:
*   0 if successful,
*   uiSize if unsuccessful
*
********************************************************************/
uint32_t uiUSBTxDataBuffer(void* vpBuffer, uint32_t uiSize);

/********************************************************************
* PUBLIC uiUSBRxChar()
*--------------------------------------------------------------------
* Receive data bytes from the USB receive buffer
*
* Inputs:
*   None
* Returns:
*   character if successful,
*   0 if unsuccessful
*
********************************************************************/
uint8_t ucUSBRxChar(void);

/********************************************************************
* PUBLIC uiUSBTxChar()
*--------------------------------------------------------------------
* Push data bytes to the USB transmit buffer
*
* Inputs:
*   cData - the data to be transmitted
* Returns:
*   0 if successful,
*   1 if unsuccessful
*
********************************************************************/
bool bUSBTxChar(uint8_t cData);

/********************************************************************
* PUBLIC vCDCVBusCallback()
*--------------------------------------------------------------------
* Called when VBus line changes state.
*
* Inputs:
*   bVBusHigh - VBus state (TRUE if High)
* Returns: None
*
********************************************************************/
void vCDCVBusCallback(bool bVBusHigh);


/********************************************************************
* PUBLIC vCDCSOFCallback()
*--------------------------------------------------------------------
* Called when a start of frame is received on USB line each 1ms.
*
* Inputs: None
* Returns: None
*
*********************************************************************/
void vCDCSOFCallback(void);


/********************************************************************
* PUBLIC vCDCSuspendCallback()
*--------------------------------------------------------------------
* Called when USB host sets USB line to suspend state
*
* Inputs: None
* Returns: None
*
*********************************************************************/
void vCDCSuspendCallback(void);


/*********************************************************************
* PUBLIC vCDCResumeCallback()
*---------------------------------------------------------------------
* Called when the USB line is resumed from the suspend state
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vCDCResumeCallback(void);


/*********************************************************************
* PUBLIC vCDCSuspendLPMCallback()
*---------------------------------------------------------------------
* Called when the USB Host sets LPM suspend state
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vCDCSuspendLPMCallback(void);


/********************************************************************
* PUBLIC vCDCRemoteWakeupLPMEnableCallback()
*--------------------------------------------------------------------
* Called when the USB Host requests to enable LPM remote wakeup
*
* Inputs: None
* Returns: None
*
********************************************************************/
void vCDCRemoteWakeupLPMEnableCallback(void);


/*********************************************************************
* PUBLIC vCDCRemoteWakeupLPMDisableCallback()
*---------------------------------------------------------------------
* Called when the USB Host requests to disable LPM remote wakeup
*
* Inputs: None
* Returns: None
*
********************************************************************/
void vCDCRemoteWakeupLPMDisableCallback(void);


/*********************************************************************
* PUBLIC bCDCEnableCallback()
*---------------------------------------------------------------------
* Called when USB Host enables the CDC interface
*
* Inputs:
*   cPort - UDI CDC port number (always 1)
* Returns:
*   true  - CDC startup is successful
*   false - CDC startup failed
*
**********************************************************************/
bool bCDCEnableCallback(uint8_t cPort);


/*********************************************************************
* PUBLIC vCDCDisableCallback()
*---------------------------------------------------------------------
* Called when USB Host disables the CDC interface
*
* Inputs:
*   cPort - UDI CDC port number (always 1)
* Returns: None
*
**********************************************************************/
void vCDCDisableCallback(uint8_t cPort);


/*********************************************************************
* PUBLIC vCDCRxNotifyCallback()
*---------------------------------------------------------------------
* Called when some data has been received
*
* Inputs:
*   cPort - UDI CDC port number (always 1)
* Returns: None
*
**********************************************************************/
void vCDCRxNotifyCallback(uint8_t cPort);


/*********************************************************************
* PUBLIC vCDCTxEmptyNotifyCallback()
*---------------------------------------------------------------------
* Called when transmit buffer is empty
*
* Inputs:
*   cPort - UDI CDC port number (always 1)
* Returns: None
*
**********************************************************************/
void vCDCTxEmptyNotifyCallback(uint8_t cPort);


/*********************************************************************
* PUBLIC vCDCConfigCallback()
*---------------------------------------------------------------------
* Called when USB host changes CDC configuration (baud, parity, stop bits)
*
* Inputs:
*   cPort - UDI CDC port number (always 1)
*   *stConfig - structure of configuration:
*       le32_t dwDTERate
*       uint8_t bCharFormat
            CDC_STOP_BITS_1 = 0     ->  1 stop bit
            CDC_STOP_BITS_1_5 = 1   ->  1.5 stop bits
            CDC_STOP_BITS_2 = 2     ->  2 stop bits
*       uint8_t bParityType
            CDC_PAR_NONE = 0    ->  No parity
            CDC_PAR_ODD = 1     ->  Odd parity
            CDC_PAR_EVEN = 2    ->  Even parity
            CDC_PAR_MARK = 3    ->  Parity forced to 0 (space)
            CDC_PAR_SPACE = 4   ->  Parity forced to 1 (mark)
*       uint8_t bDataBits
* Returns: None
*
**********************************************************************/
void vCDCConfigCallback(uint8_t cPort, usb_cdc_line_coding_t *stConfig);


/*********************************************************************
* PUBLIC vCDCSetDTRCallback()
*---------------------------------------------------------------------
* Called when USB host establishes a terminal connection to the CDC driver
*
* Inputs:
*   cPort - UDI CDC port number (always 1)
*   bEnable - indication of connect/disconnect of terminal to CDC
* Returns: None
*
**********************************************************************/
void vCDCSetDTRCallback(uint8_t cPort, bool bEnable);


/*********************************************************************
* PUBLIC vCDCSetRTSCallback()
*---------------------------------------------------------------------
* Called when USB host asserts Request To Send line
*
* Inputs:
*   cPort - UDI CDC port number (always 1)
*   bEnable - indication of RTS line status to CDC
* Returns: None
*
**********************************************************************/
void vCDCSetRTSCallback(uint8_t cPort, bool bEnable);


#endif /* IFACE_USB_CDC_H_ */