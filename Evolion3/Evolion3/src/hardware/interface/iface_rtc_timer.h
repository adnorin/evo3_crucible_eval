#ifndef IFACE_RTC_TIMER_H_
#define IFACE_RTC_TIMER_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc.  and can only be       */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: iface_rtc_timer.h                                              */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_rtc.c, which provides prototypes for the             */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

/******************************/
/*      Enums and Types       */
/******************************/
/* None */

/*********************************************************************
* PUBLIC vSetupRtc()
*---------------------------------------------------------------------
* Sets up the resources and parameters for the RTC
* Registers user callback function.
*
*   *** Warning: This function is a blocking function, care must be taken
*       when called from within the RTOS  ***
*
* Inputs:
*   vpUserRtcCompareCb - Reference to the user defined callback function.
*   uiCompareCount     - Count value to count up to.
*
* Returns: None
*
**********************************************************************/
void vSetupRtc(void (*vpUserRtcCompareCb)(void), uint32_t uiCompareCount);


/*********************************************************************
* PUBLIC vRtcEnable()
*---------------------------------------------------------------------
* Enables RTC
*
*   *** Warning: This function is a blocking function, care must be taken
*       when called from within the RTOS  ***
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vRtcEnable(void);


/*********************************************************************
* PUBLIC vRtcDisable()
*---------------------------------------------------------------------
* Disables RTC
*
*   *** Warning: This function is a blocking function, care must be taken
*       when called from within the RTOS  ***
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vRtcDisable(void);


/*********************************************************************
* PUBLIC uiRtcGetCount()
*---------------------------------------------------------------------
* Gets current RTC count
*
*   *** Warning: This function is a blocking function, care must be taken
*       when called from within the RTOS  ***
*
* Inputs:  None
* Returns: RTC Count (Uint32)
*
**********************************************************************/
uint32_t uiRtcGetCount(void);


#endif /* IFACE_RTC_TIMER_H_ */