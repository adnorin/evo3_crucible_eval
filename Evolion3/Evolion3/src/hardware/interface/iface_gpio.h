#ifndef IFACE_GPIO_H_
#define IFACE_GPIO_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: iface_gpio.h                                                   */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_gpio.c, which provides prototypes for the            */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

/*********************************************************************
* PUBLIC vConfigureGPIO()
*---------------------------------------------------------------------
* Initializes GPIO input and output pins
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureGPIO(void);

/*********************************************************************
* PUBLIC vResetOverCurrentLatch()
*---------------------------------------------------------------------
* Resets over-current latch as it starts up in an undefined state
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vResetOverCurrentLatch(void);

/*********************************************************************
* PUBLIC vResetBmsIc()
*---------------------------------------------------------------------
* Resets BMS IC to make sure the configuration starts in its defaults
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vResetBmsIc(void);


#endif /* IFACE_GPIO_H_ */