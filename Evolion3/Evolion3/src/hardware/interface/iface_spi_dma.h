#ifndef IFACE_SPI_DMA_H_
#define IFACE_SPI_DMA_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc.  and can only be       */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: int_spi_dma.h                                                  */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_spi_dma.c, which provides prototypes for the public  */
/* interface functions. Also includes types and enumeration definitions.     */
/*                                                                           */
/*****************************************************************************/

#define D_CHANNEL_UNASSIGNED 99
#define SPI_BAUDRATE 2000000UL
#define LOOP_TIMEOUT 50

/******************************/
/*      Enums and Types       */
/******************************/

/* SPI Ports */
typedef enum
{
    SPI_PORT_1 = 0,
    SPI_PORT_2,
}eSpiPortsAvailable_t;


/********************************************************************
* PUBLIC bConfigureSpiPort()
*--------------------------------------------------------------------
* Configures the HW SPI port.
*
* Inputs:
*   eSpiPort - Selected SPI Port
* Returns:
*   true     - Successfully configured SPI port.
*   false    - SPI configuration unsuccessful.
*
********************************************************************/
bool bConfigureSpiPort(eSpiPortsAvailable_t eSpiPort);


/********************************************************************
* PUBLIC vSetSpiRxCallback()
*--------------------------------------------------------------------
* Receives the reference to the RX callback function for the SPI port
*
* Inputs:
*   eSpiPort        - Selected SPI Port
*   vpFunctSpiRxCb  - Reference to the RX complete callback function
* Returns: None
*
*********************************************************************/
void vSetSpiRxCallback(eSpiPortsAvailable_t eSpiPort, void (*vpFunctSpiRxCb)(void));


/********************************************************************
* PUBLIC vSetSpiTxCallback()
*--------------------------------------------------------------------
* Receives the reference to the RX callback function for the SPI port
*
* Inputs:
*   eSpiPort        - Selected SPI Port
*   vpFunctSpiTxCb  - Reference to the TX complete callback function
* Returns: None
*
*********************************************************************/
void vSetSpiTxCallback(eSpiPortsAvailable_t eSpiPort, void (*vpFunctSpiTxCb)(void));


/*********************************************************************
* PUBLIC vSetupSpiRx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the RX buffer and length and enables
* the registered callbacks to the SPI port.
*
* Inputs:
*   eSpiPort   - Selected SPI Port
*   ucBufferRx - Reference to the receive buffer
*   ucBufSize  - Length of data to be received
* Returns: None
*
**********************************************************************/
void vSetupSpiRx(eSpiPortsAvailable_t eSpiPort, uint8_t* ucBufferRx, uint16_t usiBufSize);


/*********************************************************************
* PUBLIC vSetupSpiTx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the TX buffer and length and enables
* the registered callbacks to the SPI port.
*
* Inputs:
*   eSpiPort   - Selected SPI Port
*   ucBufferTx - Reference to the transmit buffer
*   ucBufSize  - Length of data to be transmitted
* Returns: None
*
**********************************************************************/
void vSetupSpiTx(eSpiPortsAvailable_t eSpiPort, uint8_t* ucBufferTx, uint16_t usiBufSize);


/*********************************************************************
* PUBLIC bStartSpiRxTx()
*---------------------------------------------------------------------
* Starts the transmit process, when the full amount of data
* is transmitted and received the callback functions are called.
* SPI Data transfer is full duplex in order to send or receive data both
* send and receive operations are preformed. Slave pin must be selected
* (LOW) before transfer can start. Since SPI is master, the TX
* operation initiates transfer and generates the SPI clock. Once all the
* data is transferred the RX callback function de-selects the Slave
* pin (HIGH).
*
* Inputs:
*   eSpiPort - Selected SPI Port
* Returns:
*   true  - Successfully Started and new SPI transfer
*   false - SPI was busy, previous transfer has not completed
*
********************************************************************/
bool bStartSpiRxTx(eSpiPortsAvailable_t eSpiPort);


/*********************************************************************
* PUBLIC bIsSpiPortBusy()
*---------------------------------------------------------------------
* Returns the status of the SPI port, whether it is busy or not.
*
* Inputs:
*   eSpiPort - Selected SPI Port
* Returns:
*   true  - SPI port is busy, currently performing transfer.
*   false - SPI port is available.
*
**********************************************************************/
bool bIsSpiPortBusy(eSpiPortsAvailable_t eSpiPort);

/* TODO: REMOVE for debugging purposes */

void Initialize_LCD(void);
void Clear_LCD(void);
void DisplayMsg_LCD(void);

#endif /* IFACE_SPI_DMA_H_ */