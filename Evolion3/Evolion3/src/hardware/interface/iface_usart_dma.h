#ifndef INIT_USART_DMA_H_
#define INIT_USART_DMA_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc and can only be         */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: R'Evolion Software                                               */
/*****************************************************************************/
/* FILE NAME: int_usart_dma.h                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_usart_dma.c, which provides prototypes for the       */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define D_CHANNEL_UNASSIGNED 99
#define LOOP_TIMEOUT 50

/* Enums and Types */
typedef enum
{
    USART_PORT_1 = 0,
    USART_PORT_2,
}eUsartPortsAvailable_t;

typedef enum
{
    E_9600_BAUD   = 9600,
    E_19200_BAUD  = 19200,
    E_57600_BAUD  = 57600,
    E_115200_BAUD = 115200,
}eUsartPortSpeed_t;

/* Prototypes */

/********************************************************************
* PUBLIC bConfigureUsartPort()
*--------------------------------------------------------------------
* Sets the port speed, parity and stop bits.
*
* Inputs:
*   eUsartPort  - Selected USART port
*   eBaudRate   - Serial port Baud Rate (9600, 19200, 57600, 115200)
*   eParity     - Serial Port Parity (Odd, Even or None)
*   eStopBits   - Serial port stop bits (1 or 2)
* Returns:
*   true  - successfully configured USART
*   false - USART configuration unsuccessful
*
********************************************************************/

bool bConfigureUsartPort(eUsartPortsAvailable_t eUsartPort, eUsartPortSpeed_t eBaudRate,
enum usart_parity eParity, enum usart_stopbits eStopBits);


/********************************************************************
* PUBLIC vSetUsartRxCallback()
*--------------------------------------------------------------------
* Receives the reference to the RX callback function for the selected port
*
* Inputs:
*   eUsartPort       - Selected USART port
*   vpFunctUsartRxCb - Reference to the RX complete callback function
* Returns: None
*
*********************************************************************/
void vSetUsartRxCallback(eUsartPortsAvailable_t eUsartPort, void (*vpFunctUsartRxCb)(void));


/********************************************************************
* PUBLIC vSetUsartTxCallback()
*--------------------------------------------------------------------
* Receives the reference to the TX callback function for the selected port
*
* Inputs:
*   eUsartPort       - Selected USART port
*   vpFunctUsartTxCb - Reference to the TX complete callback function
* Returns: None
*
*********************************************************************/
void vSetUsartTxCallback(eUsartPortsAvailable_t eUsartPort, void (*vpFunctUsartTxCb)(void));


/*********************************************************************
* PUBLIC vSetupUsartRx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the RX buffer and length and enables
* the registered callbacks to the selected port.
*
* Inputs:
*   eUsartPort  - Selected USART port
*   ucpRxBuffer - Reference to the receive buffer
*   usiRxSize   - Length of data to be received
* Returns: None
*
**********************************************************************/
void vSetupUsartRx(eUsartPortsAvailable_t eUsartPort, char* cpRxBuffer, uint16_t usiRxSize);


/*********************************************************************
* PUBLIC vSetupUsartTx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the TX buffer and length and enables
* the registered callbacks to the selected port.
*
* Inputs:
*   eUsartPort  - Selected USART port
*   ucpTxBuffer - Reference to the transmit buffer
*   usiTxSize   - Length of data to be transmitted
* Returns: None
*
**********************************************************************/
void vSetupUsartTx(eUsartPortsAvailable_t eUsartPort, char* cpTxBuffer, uint16_t usiTxSize);


/********************************************************************
* PUBLIC vStartUsartRx()
*--------------------------------------------------------------------
* Start the receive process, when the full amount of data
* expected is received the callback function is called.
*
* Inputs:
*   eUsartPort -Selected USART port
* Returns:
*   true  - Successfully Started and new USART transfer
*   false - USART was busy, previous transfer has not completed
*
********************************************************************/
bool vStartUsartRx(eUsartPortsAvailable_t eUsartPort );


/*********************************************************************
* PUBLIC vStartUsartTx()
*---------------------------------------------------------------------
* Start the transmit process, when the full amount of data
* is transmitted the callback function is called.
*
* Inputs:
*   eUsartPort - Selected USART port
* Returns:
*   true  - Successfully Started and new USART transfer
*   false - USART was busy, previous transfer has not completed
*
********************************************************************/
bool vStartUsartTx(eUsartPortsAvailable_t eUsartPort );


/*********************************************************************
* PUBLIC bIsUsartRxPortBusy()
*---------------------------------------------------------------------
* Returns the status of the USART port, whether it is busy or not.
*
* Inputs:
*   eUsartPort - Selected USART Port
* Returns:
*   true  - USART port is busy, currently performing transfer.
*   false - USART port is available.
*
**********************************************************************/
bool bIsUsartRxPortBusy(eUsartPortsAvailable_t eUsartPort);


/*********************************************************************
* PUBLIC bIsUsartTxPortBusy()
*---------------------------------------------------------------------
* Returns the status of the USART port, whether it is busy or not.
*
* Inputs:
*   eUsartPort - Selected USART Port
* Returns:
*   true  - USART port is busy, currently performing transfer.
*   false - USART port is available.
*
**********************************************************************/
bool bIsUsartTxPortBusy(eUsartPortsAvailable_t eUsartPort);

/*********************************************************************
* PUBLIC uiUsartGetRxCount()
*---------------------------------------------------------------------
* Get the number of data bytes in the USART receive buffer
*
* Inputs:
*   eUsartPort - Selected USART Port
* Returns:
*   Number of bytes in USB receive buffer
*
**********************************************************************/
uint32_t uiUsartGetRxCount(eUsartPortsAvailable_t eUsartPort);

/*********************************************************************
* PUBLIC uiUsartGetTxFree()
*---------------------------------------------------------------------
* Get the number of data bytes in the USART transmit buffer
*
* Inputs:
*   eUsartPort - Selected USART Port
* Returns:
*   Number of bytes in USB transmit buffer
*
**********************************************************************/
uint32_t uiUsartGetTxFree(eUsartPortsAvailable_t eUsartPort);

#endif /* INIT_USART_DMA_H_ */