#ifndef INT_I2C_DMA_H_
#define INT_I2C_DMA_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: iface_i2c_dma.h                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_i2c_dma.c, which provides prototypes for the public  */
/* interface functions. Also includes types and enumeration definitions.     */
/*                                                                           */
/*****************************************************************************/

#define D_CHANNEL_UNASSIGNED 99
#define I2C_BUFFER_TIMEOUT   10000
#define LOOP_TIMEOUT 50
#define I2C_RESOURCE_FREE   0
#define I2C_RESOURCE_TEMP   1
#define I2C_RESOURCE_HMI    2
#define I2C_RESOURCE_NETMON 3

/********************************************************************
* PUBLIC bConfigureI2cPort()
*--------------------------------------------------------------------
* Configures the HW I2C port.
*
* Inputs: None
* Returns:
*   true  - successfully configured I2C
*   false - I2C configuration unsuccessfully
*
********************************************************************/
bool bConfigureI2cPort(void);


/********************************************************************
* PUBLIC vSetI2cRxCallback()
*--------------------------------------------------------------------
* Receives the reference to the RX callback function for the I2C port
*
* Inputs:
*   vpFunctI2cRxCb - Reference to the RX complete callback function
*   ucRxResource - Resource using I2C (Temp or HMI)
* Returns: None
*
*********************************************************************/
void vSetI2cRxCallback(void (*vpFunctI2cRxCb)(void), uint8_t ucRxResource);


/********************************************************************
* PUBLIC vSetI2cTxCallback()
*--------------------------------------------------------------------
* Receives the reference to the TX callback function for the I2C port
*
* Inputs:
*   vpFunctI2cTxCb   - Reference to the RX complete callback function
*   ucTxResource - Resource using I2C (Temp or HMI)
* Returns: None
*
*********************************************************************/
void vSetI2cTxCallback(void (*vpFunctI2cTxCb)(void), uint8_t ucTxResource);


/*********************************************************************
* PUBLIC vSetupI2cRx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the RX buffer and length and enables
* the registered callbacks to the I2C port.
*
* Inputs:
*   ucBufferRx  - Reference to the receive buffer
*   usiBufSize  - Length of data to be received
*   ucRxResource - Resource using I2C (Temp or HMI)
* Returns: None
*
**********************************************************************/
void vSetupI2cRx(uint8_t *ucBufferRx, uint16_t usiBufSize, uint8_t ucRxResource);


/*********************************************************************
* PUBLIC vSetupI2cTx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the TX buffer and length and enables
* the registered callbacks to the I2C port.
*
* Inputs:
*   ucBufferTx  - Reference to the transmit buffer
*   usiBufSize  - Length of data to be transmitted
*   ucTxResource - Resource using I2C (Temp or HMI)
* Returns: None
*
**********************************************************************/
void vSetupI2cTx(uint8_t *ucBufferTx, uint16_t usiBufSize, uint8_t ucTxResource);


/********************************************************************
* PUBLIC bStartI2cRx()
*--------------------------------------------------------------------
* Start the receive process, when the full amount of data
* expected is received the callback function is called.
*
* Inputs:
*    ucSlaveAddress - I2C Slave Address
* Returns:
*   true  - Successfully Started and new I2C transfer
*   false - I2C was busy, previous transfer has not completed
*
********************************************************************/
bool bStartI2cRx(uint8_t ucSlaveAddress);


/*********************************************************************
* PUBLIC bStartI2cTx()
*---------------------------------------------------------------------
* Start the transmit process, when the full amount of data
* is transmitted the callback function is called.
*
* Inputs:
*   ucSlaveAddress - I2C Slave Address
* Returns:
*   true  - Successfully Started and new I2C transfer
*   false - I2C was busy, previous transfer has not completed
*
********************************************************************/
bool bStartI2cTx(uint8_t ucSlaveAddress);


/*********************************************************************
* PUBLIC bIsI2CPortBusy()
*---------------------------------------------------------------------
* Returns the status of the I2C port, whether it is busy or not.
*
* Inputs: None
* Returns:
*   true  - I2C port is busy, currently performing transfer.
*   false - I2C port is available.
*
**********************************************************************/
bool bIsI2CPortBusy(void);

#endif /* INT_I2C_DMA_H_ */