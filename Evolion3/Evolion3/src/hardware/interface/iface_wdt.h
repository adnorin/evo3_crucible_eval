

#ifndef IFACE_WDT_H_
#define IFACE_WDT_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc and can only be         */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: R'Evolion Software                                               */
/*****************************************************************************/
/* FILE NAME: int_usart_dma.h                                                */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_wdt.c, which provides prototypes for the             */
/* for the public interface functions. Also includes types and enumeration   */
/* definitions.                                                              */
/*                                                                           */
/*****************************************************************************/

#define LOOP_TIMEOUT 50

/********************************************************************
* PUBLIC bWdtConfigure()
*--------------------------------------------------------------------
* Calls the private WDT configuration function.
*
* Inputs: None
*
* Returns:
*   true  - Successfully configured WDT
*   false - WDT configuration unsuccessful
*
*********************************************************************/
bool bWdtConfigure(void);


/********************************************************************
* PUBLIC bWdtEnable()
*--------------------------------------------------------------------
* Enables WDT.
*
* Inputs: None
*
* Returns:
*   true  - WDT enabled
*   false - WDT sync in process, not yet enabled.
*
*********************************************************************/
bool bWdtEnable(void);


/********************************************************************
* PUBLIC bWdtDisable()
*--------------------------------------------------------------------
* Disables WDT.
*
* Inputs: None
*
* Returns:
*   true  - WDT disabled
*   false - WDT sync in process, not yet disabled.
*
*********************************************************************/
bool bWdtDisable(void);


/********************************************************************
* PUBLIC bWdtReset()
*--------------------------------------------------------------------
* Resets WDT, this must be called more frequently than the WDT period
* to prevent the WDT from resetting the processor.
*
* Inputs: None
*
* Returns:
*   true  - WDT reset completed
*   false - WDT sync in process, not yet reset.
*
*********************************************************************/
bool bWdtReset(void);


/********************************************************************
* PUBLIC bWdtSyncComplete()
*--------------------------------------------------------------------
* Returns the status of the WDT sync process.
*
* Inputs: None
*
* Returns:
*   true  - WDT sync completed
*   false - WDT sync still in process
*
*********************************************************************/
bool bWdtSyncComplete(void);


/********************************************************************
* PUBLIC vCpuResetViaWdt()
*--------------------------------------------------------------------
* This function causes an immediate CPU reset, the reset cause will
* be registered as a WDT reset.
*
* Inputs: None
*
* Returns: None
*
*********************************************************************/
void vCpuResetViaWdt(void);

#endif /* IFACE_WDT_H_ */