/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: init_rtc_timer.c                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Implements all the necessary initialization functions for the watch dog   */
/* timer on the Evolion 3 Battery System. Interface functions offer          */
/* the flexibility to enable, disable and reset the WDT counter.             */
/*                                                                           */
/*****************************************************************************/

#include <asf.h>
#include <iface_rtc_timer.h>


/********************************************************************
 * PRIVATE Types and Structures                                     *
 ********************************************************************/
static struct rtc_module stRtcInstance; /* RTC Instance */


/********************************************************************
 * PRIVATE variables                                                *
 ********************************************************************/
static bool sbRtcEnabled = false;   /* RTC Enabled Flag */
static uint32_t uiCountCompareVal = 0; /* Count compare value */


/********************************************************************
* PRIVATE Callback functions                                        *
 ********************************************************************/
static void (*svpRtcCompareCb)(void) = NULL; /* Pointer to user Rx callback function Port 1 */


/********************************************************************
* PRIVATE vRtcCompareCallback()
*--------------------------------------------------------------------
* Callback (ISR) for compare value match. This function calls the user defined 
* callback and performs any necessary RTC maintenance.
*
* Inputs: None
* Returns: None
*
*********************************************************************/
static void vRtcCompareCallback(void)
{
    /* Call user Callback */
    (*svpRtcCompareCb)();   
}


/********************************************************************
* PRIVATE Functions                                                 *
*********************************************************************/
/********************************************************************
* PRIVATE vConfigureRtcCount()
*--------------------------------------------------------------------
* Configures RTC operating mode and count compare value. This is
* a blocking function. 
*
* Inputs: None
* Returns: None
*
*********************************************************************/
static void vConfigureRtcCount(uint32_t uiCompareCountValue)
{
    struct rtc_count_config stConfigRtcCount;  /* RTC configuration structure */
    
    /* Disable RTC before changing any of its values */
    if(sbRtcEnabled == true)
    {
        rtc_count_disable(&stRtcInstance);
    }
    
    /* Initialize the RTC config structure with default values */
    rtc_count_get_config_defaults(&stConfigRtcCount); 

    /* Modify needed parameters */
    stConfigRtcCount.prescaler           = RTC_COUNT_PRESCALER_DIV_1;
    stConfigRtcCount.clear_on_match      = true;
    stConfigRtcCount.compare_values[0]   = uiCompareCountValue;
    
    #ifdef FEATURE_RTC_CONTINUOUSLY_UPDATED
    stConfigRtcCount.continuously_update = true;
    #endif

    /* Initialize count and enable counter */
    rtc_count_init(&stRtcInstance, RTC, &stConfigRtcCount);
    rtc_count_enable(&stRtcInstance);
    
    /* Set enabled flag to true */
    sbRtcEnabled = true;
}


/********************************************************************
* PRIVATE vConfigureRtcCallbacks()
*--------------------------------------------------------------------
* Configures RTC callbacks.
*
* Inputs: None
* Returns: None
*
*********************************************************************/
static void vConfigureRtcCallbacks(void)
{
    /* Register callback function */
    rtc_count_register_callback(&stRtcInstance, vRtcCompareCallback, RTC_COUNT_CALLBACK_COMPARE_0);
    
    /* Enable callback function */
    rtc_count_enable_callback(&stRtcInstance, RTC_COUNT_CALLBACK_COMPARE_0);
}


/********************************************************************
 * PUBLIC Interface Functions                                       *
 ********************************************************************/
/*********************************************************************
* PUBLIC vSetupRtc()
*---------------------------------------------------------------------
* Sets up the resources and parameters for the RTC
* Registers user callback function. 
*
*   *** Warning: This function is a blocking function, care must be taken 
*       when called from within the RTOS  ***
*
* Inputs:                                         
*   vpUserRtcCompareCb - Reference to the user defined callback function.
*   uiCompareCount     - Count value to count up to.                  
*
* Returns: None
*
**********************************************************************/
void vSetupRtc(void (*vpUserRtcCompareCb)(void), uint32_t uiCompareCount)
{   
    /* Register user callback function and compare count value */
    svpRtcCompareCb = vpUserRtcCompareCb;   
    uiCountCompareVal = uiCompareCount; 
    
    /* Configure RTC */
    vConfigureRtcCount(uiCountCompareVal);          
    
    /* Configure Callbacks */
    vConfigureRtcCallbacks();
    
}


/*********************************************************************
* PUBLIC vRtcEnable()
*---------------------------------------------------------------------
* Enables RTC
*
*   *** Warning: This function is a blocking function, care must be taken
*       when called from within the RTOS  ***
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vRtcEnable(void)
{           
    /* Enable RTC */    
    vConfigureRtcCount(uiCountCompareVal);  
    
    /* Configure Callbacks */
    vConfigureRtcCallbacks();       
}


/*********************************************************************
* PUBLIC vRtcDisable()
*---------------------------------------------------------------------
* Disables RTC
*
*   *** Warning: This function is a blocking function, care must be taken
*       when called from within the RTOS  ***
*
* Inputs:  None
* Returns: None
*
**********************************************************************/
void vRtcDisable(void)
{   
    /* Disable RTC */
    rtc_count_disable(&stRtcInstance);
    
    /* Clear the RTC enabled flag */
    sbRtcEnabled = false;
}


/*********************************************************************
* PUBLIC uiRtcGetCount()
*---------------------------------------------------------------------
* Gets current RTC count
*
*   *** Warning: This function is a blocking function, care must be taken
*       when called from within the RTOS  ***
*
* Inputs:  None
* Returns: RTC Count (Uint32)
*
**********************************************************************/
uint32_t uiRtcGetCount(void)
{
    uint32_t uiReturnValue = 0; /* Return Value (default = 0) */
    
    /* If RTC is enabled return count, otherwise return 0 */
    if(sbRtcEnabled == true)
    {
        uiReturnValue = rtc_count_get_count(&stRtcInstance);
    }
    else
    {
        /* uiReturnValue is 0 */
    }
        
    return (uiReturnValue);
}


/******************************** End of File ********************************/