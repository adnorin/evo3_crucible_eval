/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: init_i2c_dma.c                                                 */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Implements all the necessary initialization functions for the I2C         */
/* port (Master) on the R'Evolion Battery System. Interface functions offer  */
/* the flexibility to choose input/output buffers, transfer data size,       */
/* and callback functions for transfer complete (ISRs)                       */
/*                                                                           */
/*****************************************************************************/

#include <asf.h>
#include <iface_i2c_dma.h>


/********************************************************************
 * PRIVATE Types and Structures                                     *
 ********************************************************************/
static struct i2c_master_module sstI2cMasterInstance; /* HW i2c module instance */

static struct dma_resource sstI2cResourceRx; /* DMA Rx resource structure */
static struct dma_resource sstI2cResourceTx; /* DMA Tx resource structure */

COMPILER_ALIGNED(16)
static DmacDescriptor sstI2cDescriptorRx SECTION_DMAC_DESCRIPTOR; /* Rx Transfer descriptor */
static DmacDescriptor sstI2cDescriptorTx SECTION_DMAC_DESCRIPTOR; /* Tx Transfer descriptor */


/********************************************************************
 * PRIVATE variables                                                *
 ********************************************************************/
static uint8_t sucI2cRxDataLen; /* I2C data length       */
static uint8_t sucI2cTxDataLen; /* I2C data length       */

static uint8_t sucActiveRxResource = I2C_RESOURCE_FREE;
static uint8_t sucActiveTxResource = I2C_RESOURCE_FREE;

volatile static  bool sbI2CRxBusy = false; /* I2C Rx Busy indicator */
volatile static  bool sbI2CTxBusy = false; /* I2C Tx Busy indicator */


/********************************************************************
* PRIVATE Callback functions                                        *
********************************************************************/
static void (*svpI2cTransferDoneRxCbTemp)(void) = NULL; /* Pointer to user Rx callback function */
static void (*svpI2cTransferDoneTxCbTemp)(void) = NULL; /* Pointer to user Rx callback function */
static void (*svpI2cTransferDoneRxCbHMI)(void) = NULL; /* Pointer to user Rx callback function */
static void (*svpI2cTransferDoneTxCbHMI)(void) = NULL; /* Pointer to user Rx callback function */
static void (*svpI2cTransferDoneRxCbNetMon)(void) = NULL; /* Pointer to user Rx callback function */
static void (*svpI2cTransferDoneTxCbNetMon)(void) = NULL; /* Pointer to user Rx callback function */


/********************************************************************
* PRIVATE svTransferRxDoneCallBack()
*--------------------------------------------------------------------
* Callback (ISR) for Rx complete. This function calls the user defined callback and performs
* any necessary I2C maintenance.
*
* Inputs:
*   pstResource - reference to the DMA resource structure
* Returns: None
*
*********************************************************************/
static void svTransferRxDoneCallBack(struct dma_resource* const pstResource )
{
     /* Flag I2C as not busy */
     sbI2CRxBusy = false;

     /* Call the appropriate callback based on the active resource */
     switch(sucActiveRxResource)
     {
         case I2C_RESOURCE_TEMP:
            (*svpI2cTransferDoneRxCbTemp)();
            break;

        case I2C_RESOURCE_HMI:
            (*svpI2cTransferDoneRxCbHMI)();
            break;

        case I2C_RESOURCE_NETMON:
            (*svpI2cTransferDoneRxCbNetMon)();
            break;

        default:
            break;
     }
}


/********************************************************************
* PRIVATE svTransferTxDoneCallBack()
*--------------------------------------------------------------------
* Callback (ISR) for Tx complete. This function calls the user defined callback and performs
* any necessary I2C maintenance.
*
* Inputs:
*   pstResource - reference to the DMA resource structure
* Returns: None
*
*********************************************************************/
static void svTransferTxDoneCallBack(struct dma_resource* const pstResource )
{
     /* Flag I2C as not busy */
     sbI2CTxBusy = false;

     /* Call the appropriate callback based on the active resource */
     switch(sucActiveTxResource)
     {
         case I2C_RESOURCE_TEMP:
            (*svpI2cTransferDoneTxCbTemp)();
            break;

         case I2C_RESOURCE_HMI:
             (*svpI2cTransferDoneTxCbHMI)();
             break;

         case I2C_RESOURCE_NETMON:
             (*svpI2cTransferDoneTxCbNetMon)();
             break;

         default:
            break;
     }
}


/********************************************************************
* PRIVATE Functions                                                 *
*********************************************************************/
/********************************************************************
* PRIVATE sbConfigureI2c()
*--------------------------------------------------------------------
* Configures I2C port selected for operation at the selected BAUD
* rate
*
* Inputs: None
* Returns:
*   true  - successfully configured I2C
*   false - I2C configuration unsuccessfully
*
*********************************************************************/
static bool sbConfigureI2c(void)
{
    struct i2c_master_config stConfigI2cMaster; /* I2C Configuration Structure */
    uint16_t usiLoopTimeout = 0;          /* Loop timeout counter */
    bool bI2cConfigurationResult = false; /* Configuration result, true = success, false (default) = unsuccessful */

    /* Initialize the config I2C structure with default values */
    i2c_master_get_config_defaults(&stConfigI2cMaster);

    /* Configuration for I2C Port */
    stConfigI2cMaster.buffer_timeout = I2C_BUFFER_TIMEOUT;
    stConfigI2cMaster.generator_source = GCLK_GENERATOR_3; /* 8MHz Clock */

    /* Wait for I2C init to complete */
    while(i2c_master_init(&sstI2cMasterInstance, CONF_I2C_MASTER_MODULE, &stConfigI2cMaster) != STATUS_OK && usiLoopTimeout < LOOP_TIMEOUT)
    {
        /* Increment loop timeout, this is the maximum number of retries to initialize the I2C port before giving up */
        usiLoopTimeout++;
    }

    /* Set return value based on the I2C initialization result */
    if(usiLoopTimeout < LOOP_TIMEOUT)
    {
        /* Successful initialization */
        bI2cConfigurationResult = true;

        /* Enable I2C */
        i2c_master_enable(&sstI2cMasterInstance);
    }
    else
    {
        /* Unsuccessful initialization */
        /* Do not enable I2C */
        /* bI2cConfigurationResult is false */
    }

    /* Init ChannelID for I2C Resources */
    sstI2cResourceRx.channel_id = D_CHANNEL_UNASSIGNED;
    sstI2cResourceTx.channel_id = D_CHANNEL_UNASSIGNED;

    return bI2cConfigurationResult;
}


/********************************************************************
* PRIVATE svConfigureDmaResrourceRx()
*--------------------------------------------------------------------
* Configures and allocates DMA resource for RX on selected port
*
* Inputs:
*   pstRxResource - reference to the DMA resource structure
* Returns: None
*
*********************************************************************/
static void svConfigureDmaResrourceRx(struct dma_resource *pstRxResource)
{
    struct dma_resource_config stRxConfig; /* DMA resource configuration structure */

    /* Initialize the config DMA structure with default values */
    dma_get_config_defaults(&stRxConfig);

    /* Modify needed parameters */
    stRxConfig.peripheral_trigger = SERCOM2_DMAC_ID_RX;
    stRxConfig.trigger_action = DMA_TRIGGER_ACTION_BEAT;

    /* Allocate the DMA resources with chosen configuration */
    dma_allocate(pstRxResource, &stRxConfig);
}


/*********************************************************************
* PRIVATE svSetupTransferDescriptorRx()
*---------------------------------------------------------------------
* Sets up the transfer descriptor structure for RX on selected port
*
* Inputs:
*   stRxDescriptor - reference to the DMAC descriptor structure
*   ucRxBuffer     - Reference to the received buffer
*   usiRxSize      - Length of data to be received
* Returns: None
*
********************************************************************/
static void svSetupTransferDescriptorRx(DmacDescriptor *stRxDescriptor,
    uint8_t* ucRxBuffer, uint16_t usiRxSize)
{
    struct dma_descriptor_config stRxDescriptorConfig; /* DMA descriptor configuration structure */

    /* Initialize the transfer descriptor configuration structure with default values */
    dma_descriptor_get_config_defaults(&stRxDescriptorConfig);

    /* Modify needed parameters */
    stRxDescriptorConfig.beat_size = DMA_BEAT_SIZE_BYTE;
    stRxDescriptorConfig.src_increment_enable = false;
    stRxDescriptorConfig.block_transfer_count = usiRxSize;
    stRxDescriptorConfig.source_address = (uint32_t)(&sstI2cMasterInstance.hw->I2CM.DATA.reg);
    stRxDescriptorConfig.destination_address = (uint32_t)ucRxBuffer + (uint32_t)usiRxSize;

    /* Create the DMA Rx descriptor from the the descriptor configuration */
    dma_descriptor_create(stRxDescriptor, &stRxDescriptorConfig);
}


/********************************************************************
* PRIVATE svConfigureDmaResourceTx()
*--------------------------------------------------------------------
* Configures and allocates DMA resource for TX on selected port
*
* Inputs:
*   par_PS_resource - reference to the DMA resource structure
* Returns: None
*
*********************************************************************/
static void svConfigureDmaResourceTx(struct dma_resource *par_PS_resource)
{
    struct dma_resource_config stTxConfig;  /* DMA resource configuration structure */

    /* Initialize the config DMA structure with default values */
    dma_get_config_defaults(&stTxConfig);

    /* Modify needed parameters */
    stTxConfig.peripheral_trigger = SERCOM2_DMAC_ID_TX;
    stTxConfig.trigger_action = DMA_TRIGGER_ACTION_BEAT;

    /* Allocate the DMA resources with chosen configuration */
    dma_allocate(par_PS_resource, &stTxConfig);
}


/*********************************************************************
* PRIVATE svSetupTransferDescriptorTx()
*---------------------------------------------------------------------
* Sets up the transfer descriptor structure for TX on selected port
*
* Inputs:
*   stTxDescriptor - reference to the DMAC descriptor structure
*   ucTxBuffer     - Reference to the transmit buffer
*   usiTxSize      - Length of data to be transmitted
* Returns: None
*
********************************************************************/
static void svSetupTransferDescriptorTx(DmacDescriptor *stTxDescriptor,
    uint8_t* ucTxBuffer, uint16_t usiTxSize)
{
    struct dma_descriptor_config stTxDescriptorConfig; /* DMA descriptor configuration structure */

    /* Initialize the transfer descriptor configuration structure with default values */
    dma_descriptor_get_config_defaults(&stTxDescriptorConfig);

    /* Modify needed parameters */
    stTxDescriptorConfig.beat_size = DMA_BEAT_SIZE_BYTE;
    stTxDescriptorConfig.dst_increment_enable = false;
    stTxDescriptorConfig.block_transfer_count = usiTxSize;
    stTxDescriptorConfig.source_address = (uint32_t)ucTxBuffer + (uint32_t)usiTxSize;
    stTxDescriptorConfig.destination_address = (uint32_t)(&sstI2cMasterInstance.hw->I2CM.DATA.reg);
    stTxDescriptorConfig.step_selection = DMA_STEPSEL_SRC;

    /* Create the DMA Tx descriptor from the the descriptor configuration */
    dma_descriptor_create(stTxDescriptor, &stTxDescriptorConfig);
}


/********************************************************************
 * PUBLIC Interface Functions                                       *
 ********************************************************************/
/********************************************************************
* PUBLIC bConfigureI2cPort()
*--------------------------------------------------------------------
* Configures the HW I2C port.
*
* Inputs: None
* Returns:
*   true  - successfully configured I2C
*   false - I2C configuration unsuccessfully
*
********************************************************************/
bool bConfigureI2cPort(void)
{
    /* Configure the HW I2C port */
    return(sbConfigureI2c());
}


/********************************************************************
* PUBLIC vSetI2cRxCallback()
*--------------------------------------------------------------------
* Receives the reference to the RX callback function for the I2C port
*
* Inputs:
*   vpFunctI2cRxCb - Reference to the RX complete callback function
*   ucRxResource - Resource using I2C (Temp or HMI)
* Returns: None
*
*********************************************************************/
void vSetI2cRxCallback(void (*vpFunctI2cRxCb)(void), uint8_t ucRxResource)
{
    /* Set callback for I2C port */
    switch(ucRxResource)
    {
         case I2C_RESOURCE_TEMP:
         svpI2cTransferDoneRxCbTemp = vpFunctI2cRxCb;
         break;

         case I2C_RESOURCE_HMI:
         svpI2cTransferDoneRxCbHMI = vpFunctI2cRxCb;
         break;

         case I2C_RESOURCE_NETMON:
         svpI2cTransferDoneRxCbNetMon = vpFunctI2cRxCb;
         break;

         default:
         /* Not used */
         break;
     }
}


/********************************************************************
* PUBLIC vSetI2cTxCallback()
*--------------------------------------------------------------------
* Receives the reference to the TX callback function for the I2C port
*
* Inputs:
*   vpFunctI2cTxCb   - Reference to the RX complete callback function
*   ucTxResource - Resource using I2C (Temp or HMI)
* Returns: None
*
*********************************************************************/
void vSetI2cTxCallback(void (*vpFunctI2cTxCb)(void), uint8_t ucTxResource)
{
     /* Set callback for I2C port */
     switch(ucTxResource)
     {
         case I2C_RESOURCE_TEMP:
         svpI2cTransferDoneTxCbTemp = vpFunctI2cTxCb;
         break;

         case I2C_RESOURCE_HMI:
         svpI2cTransferDoneTxCbHMI = vpFunctI2cTxCb;
         break;

         case I2C_RESOURCE_NETMON:
         svpI2cTransferDoneTxCbNetMon = vpFunctI2cTxCb;
         break;

         default:
         /* Not used */
         break;
     }
}


/*********************************************************************
* PUBLIC vSetupI2cRx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the RX buffer and length and enables
* the registered callbacks to the I2C port.
*
* Inputs:
*   ucBufferRx  - Reference to the receive buffer
*   usiBufSize  - Length of data to be received
*   ucRxResource - Resource using I2C (Temp or HMI)
* Returns: None
*
**********************************************************************/
void vSetupI2cRx(uint8_t *ucBufferRx, uint16_t usiBufSize, uint8_t ucRxResource)
{
    /* Save data transfer length this is needed in the start transfer function */
    sucI2cRxDataLen = usiBufSize;

    /* Configure I2C port */
    if(sucActiveRxResource != ucRxResource)
    {
        /* This is executed the first time the resource is allocated or resource changes */
        if(sstI2cResourceRx.channel_id != D_CHANNEL_UNASSIGNED)
        {
            /* If resource is allocated free it before allocating a new one */
            dma_free(&sstI2cResourceRx);
        }
        svConfigureDmaResrourceRx(&sstI2cResourceRx);
        svSetupTransferDescriptorRx(&sstI2cDescriptorRx, ucBufferRx, sucI2cRxDataLen);
        dma_add_descriptor(&sstI2cResourceRx, &sstI2cDescriptorRx);
        dma_register_callback(&sstI2cResourceRx, svTransferRxDoneCallBack, DMA_CALLBACK_TRANSFER_DONE);
        dma_enable_callback(&sstI2cResourceRx, DMA_CALLBACK_TRANSFER_DONE);

        /* Save new active resource */
        sucActiveRxResource = ucRxResource;
    }
    else
    {
        /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
        svSetupTransferDescriptorRx(&sstI2cDescriptorRx, ucBufferRx, sucI2cRxDataLen);
        sstI2cResourceRx.descriptor = NULL;
        dma_add_descriptor(&sstI2cResourceRx, &sstI2cDescriptorRx);
    }
}


/*********************************************************************
* PUBLIC vSetupI2cTx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the TX buffer and length and enables
* the registered callbacks to the I2C port.
*
* Inputs:
*   ucBufferTx  - Reference to the transmit buffer
*   usiBufSize  - Length of data to be transmitted
*   ucTxResource - Resource using I2C (Temp or HMI)
* Returns: None
*
**********************************************************************/
void vSetupI2cTx(uint8_t *ucBufferTx, uint16_t usiBufSize, uint8_t ucTxResource)
{
    /* Save data transfer length this is needed in the start transfer function */
    sucI2cTxDataLen = usiBufSize;

    /* Configure I2C port */
    if(sucActiveTxResource != ucTxResource)
    {
        /* This is executed the first time the resource is allocated or resource changes */
        if(sstI2cResourceTx.channel_id != D_CHANNEL_UNASSIGNED)
        {
            /* If resource is allocated free it before allocating a new one */
            dma_free(&sstI2cResourceTx);
        }
        /* This is executed the first time the resource is allocated */
        svConfigureDmaResourceTx(&sstI2cResourceTx);
        svSetupTransferDescriptorTx(&sstI2cDescriptorTx, ucBufferTx, sucI2cTxDataLen);
        dma_add_descriptor(&sstI2cResourceTx, &sstI2cDescriptorTx);
        dma_register_callback(&sstI2cResourceTx, svTransferTxDoneCallBack, DMA_CALLBACK_TRANSFER_DONE);
        dma_enable_callback(&sstI2cResourceTx, DMA_CALLBACK_TRANSFER_DONE);

        /* Save new active resource */
        sucActiveTxResource = ucTxResource;
    }
    else
    {
        /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
        svSetupTransferDescriptorTx(&sstI2cDescriptorTx, ucBufferTx, sucI2cTxDataLen);
        sstI2cResourceTx.descriptor = NULL;
        dma_add_descriptor(&sstI2cResourceTx, &sstI2cDescriptorTx);
    }
}


/********************************************************************
* PUBLIC bStartI2cRx()
*--------------------------------------------------------------------
* Start the receive process, when the full amount of data
* expected is received the callback function is called.
*
* Inputs:
*    ucSlaveAddress - I2C Slave Address
* Returns:
*   true  - Successfully Started and new I2C transfer
*   false - I2C was busy, previous transfer has not completed
*
********************************************************************/
bool bStartI2cRx(uint8_t ucSlaveAddress)
{
    bool bI2CStartSuccess = false; /* Return variable, true for successful, false (default) for unsuccessful */

    if(sbI2CRxBusy == false)
    {
        dma_start_transfer_job(&sstI2cResourceRx);
        i2c_master_dma_set_transfer(&sstI2cMasterInstance, ucSlaveAddress, sucI2cRxDataLen, I2C_TRANSFER_READ);
        bI2CStartSuccess = true;
    }
    else
    {
        /* bI2CStartSuccess = false, I2C was already busy */
    }

    return bI2CStartSuccess;
}


/*********************************************************************
* PUBLIC bStartI2cTx()
*---------------------------------------------------------------------
* Start the transmit process, when the full amount of data
* is transmitted the callback function is called.
*
* Inputs:
*   ucSlaveAddress - I2C Slave Address
* Returns:
*   true  - Successfully Started and new I2C transfer
*   false - I2C was busy, previous transfer has not completed
*
********************************************************************/
bool bStartI2cTx(uint8_t ucSlaveAddress)
{
    bool bI2CStartSuccess = false; /* Return variable, true for successful, false (default) for unsuccessful */

    if(sbI2CTxBusy == false)
    {
        dma_start_transfer_job(&sstI2cResourceTx);
        i2c_master_dma_set_transfer(&sstI2cMasterInstance, ucSlaveAddress, sucI2cTxDataLen, I2C_TRANSFER_WRITE);
        bI2CStartSuccess = true;
    }
    else
    {
        /* bI2CStartSuccess = false, I2C was already busy */
    }

    return bI2CStartSuccess;
}

/*********************************************************************
* PUBLIC bIsI2CPortBusy()
*---------------------------------------------------------------------
* Returns the status of the I2C port, whether it is busy or not.
*
* Inputs: None
* Returns:
*   true  - I2C port is busy, currently performing transfer.
*   false - I2C port is available.
*
**********************************************************************/
bool bIsI2CPortBusy(void)
{
    bool bReturnStatus = false; /* Return variable, false is default */

    /* Return the Status of the I2C port */
    if(sbI2CRxBusy == true || sbI2CTxBusy == true)
    {
        /* If either Rx or Tx are busy the port in general is busy */
        bReturnStatus = true;
    }
    else
    {
        /* bReturnStatus = false, I2C is available */
    }

    return bReturnStatus;
}

/******************************** End of File ********************************/
