/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: init_extint.c                                                  */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: Adnorin L. Mendez                                          */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* GPIO pin initialization                                                   */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <FreeRTOS.h>
#include <semphr.h>
#include <iface_rtos_start.h>
#include <conf_gpio.h>
#include <conf_extint_custom.h>
#include <iface_extint.h>
#include <main.h>

/* Prototypes */
void vOverCurrentDCHDetectionCallback(void);
void vOverCurrentCHADetectionCallback(void);
void vNetworkWakeupDetectionCallback(void);
void vHmiPowerOnButtonCallback(void);
void vHmiSOCButtonCallback(void);
void vOverTemperatureDetectionCallback(void);

/*********************************************************************
* PUBLIC vConfigureExternalInterruptChannels()
*---------------------------------------------------------------------
* Configures external interrupt channels
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureExternalInterruptChannels(void)
{
    struct extint_chan_conf config_extint_channel;

    /* Configure HMI button SOC external interrupt */
    extint_chan_get_config_defaults(&config_extint_channel);
    config_extint_channel.gpio_pin=             HMI_SOC_PIN_EIC_PIN;
    config_extint_channel.gpio_pin_mux=         HMI_SOC_PIN_EIC_MUX;
    config_extint_channel.gpio_pin_pull=        EXTINT_PULL_UP;           //EXTINT_PULL_NONE;
    config_extint_channel.detection_criteria=   EXTINT_DETECT_FALLING;
    config_extint_channel.filter_input_signal = true;
    extint_chan_set_config(HMI_SOC_PIN_EIC_LINE, &config_extint_channel);

    /* Configure HMI button Power ON external interrupt */
    extint_chan_get_config_defaults(&config_extint_channel);
    config_extint_channel.gpio_pin=             HMI_POWER_ON_PIN_EIC_PIN;
    config_extint_channel.gpio_pin_mux=         HMI_POWER_ON_PIN_EIC_MUX;
    config_extint_channel.gpio_pin_pull=        EXTINT_PULL_UP;          //EXTINT_PULL_NONE;
    config_extint_channel.detection_criteria=   EXTINT_DETECT_FALLING;
    config_extint_channel.filter_input_signal = true;
    extint_chan_set_config(HMI_POWER_ON_PIN_EIC_LINE, &config_extint_channel);

    /* Configure over-current in discharge external interrupt */
    extint_chan_get_config_defaults(&config_extint_channel);
    config_extint_channel.gpio_pin=             OVER_CURRENT_400A_NOTIFICATION_EIC_PIN;
    config_extint_channel.gpio_pin_mux=         OVER_CURRENT_400A_NOTIFICATION_EIC_MUX;
    config_extint_channel.gpio_pin_pull=        EXTINT_PULL_DOWN;
    config_extint_channel.detection_criteria=   EXTINT_DETECT_RISING;
    extint_chan_set_config(OVER_CURRENT_400A_NOTIFICATION_EIC_LINE, &config_extint_channel);

    /* Configure over-current in charge external interrupt */
    extint_chan_get_config_defaults(&config_extint_channel);
    config_extint_channel.gpio_pin=             OVER_CURRENT_192A_NOTIFICATION_EIC_PIN;
    config_extint_channel.gpio_pin_mux=         OVER_CURRENT_192A_NOTIFICATION_EIC_MUX;
    config_extint_channel.gpio_pin_pull=        EXTINT_PULL_UP;
    config_extint_channel.detection_criteria=   EXTINT_DETECT_FALLING;
    extint_chan_set_config(OVER_CURRENT_192A_NOTIFICATION_EIC_LINE, &config_extint_channel);

    /* Configure network wakeup external interrupt */
    extint_chan_get_config_defaults(&config_extint_channel);
    config_extint_channel.gpio_pin=             NETWORK_WAKEUP_NOTIFICATION_EIC_PIN;
    config_extint_channel.gpio_pin_mux=         NETWORK_WAKEUP_NOTIFICATION_EIC_MUX;
    config_extint_channel.gpio_pin_pull=        EXTINT_PULL_UP;
    config_extint_channel.detection_criteria=   EXTINT_DETECT_FALLING;
    extint_chan_set_config(NETWORK_WAKEUP_NOTIFICATION_EIC_LINE, &config_extint_channel);

    /* Configure over-temperature external interrupt */
    extint_chan_get_config_defaults(&config_extint_channel);
    config_extint_channel.gpio_pin=             OVER_TEMPERATURE_NOTIFICATION_EIC_PIN;
    config_extint_channel.gpio_pin_mux=         OVER_TEMPERATURE_NOTIFICATION_EIC_MUX;
    config_extint_channel.gpio_pin_pull=        EXTINT_PULL_UP;
    config_extint_channel.detection_criteria=   EXTINT_DETECT_FALLING;
    extint_chan_set_config(OVER_TEMPERATURE_NOTIFICATION_EIC_LINE, &config_extint_channel);
}

/*********************************************************************
* PUBLIC vConfigureOperationalExternalInterruptCallbacks()
*---------------------------------------------------------------------
* Registers and enables external interrupts to be run during operation
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureOperationalExternalInterruptCallbacks(void)
{
    extint_register_callback(       vOverCurrentDCHDetectionCallback,
                                    OVER_CURRENT_400A_NOTIFICATION_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);

    extint_chan_enable_callback(    OVER_CURRENT_400A_NOTIFICATION_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);

    extint_register_callback(       vOverCurrentCHADetectionCallback,
                                    OVER_CURRENT_192A_NOTIFICATION_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);

    extint_chan_enable_callback(    OVER_CURRENT_192A_NOTIFICATION_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);

    extint_register_callback(       vOverTemperatureDetectionCallback,
                                    OVER_TEMPERATURE_NOTIFICATION_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);

    extint_chan_enable_callback(    OVER_TEMPERATURE_NOTIFICATION_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);
}

/*********************************************************************
* PUBLIC vConfigureSleepExternalInterruptCallbacks()
*---------------------------------------------------------------------
* Registers and enables external interrupts to be run in sleep
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureSleepExternalInterruptCallbacks(void)
{
    extint_register_callback(       vNetworkWakeupDetectionCallback,
                                    NETWORK_WAKEUP_NOTIFICATION_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);

    extint_chan_enable_callback(    NETWORK_WAKEUP_NOTIFICATION_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);
}

/*********************************************************************
* PUBLIC vConfigureButtonExternalInterruptCallbacks()
*---------------------------------------------------------------------
* Registers and enables external interrupts to be run when button presses
* are detected.
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureButtonExternalInterruptCallbacks(void)
{
    extint_register_callback(       vHmiPowerOnButtonCallback,
                                    HMI_POWER_ON_PIN_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);

    extint_chan_enable_callback(    HMI_POWER_ON_PIN_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);


    extint_register_callback(       vHmiSOCButtonCallback,
                                    HMI_SOC_PIN_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);

    extint_chan_enable_callback(    HMI_SOC_PIN_EIC_LINE,
                                    EXTINT_CALLBACK_TYPE_DETECT);
}

/*********************************************************************
* PUBLIC vOverCurrentDCHDetectionCallback()
*---------------------------------------------------------------------
* Is executed when an over-current in discharge signal is detected
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vOverCurrentDCHDetectionCallback(void)
{
    ;
}

/*********************************************************************
* PUBLIC vOverCurrentCHADetectionCallback()
*---------------------------------------------------------------------
* Is executed when an over-current in charge signal is detected
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vOverCurrentCHADetectionCallback(void)
{
    ;
}

/*********************************************************************
* PUBLIC vNetworkWakeupDetectionCallback()
*---------------------------------------------------------------------
* Is executed when a network wakeup signal is detected
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vNetworkWakeupDetectionCallback(void)
{
    ;
}

/*********************************************************************
* PUBLIC vHmiPowerOnButtonCallback()
*---------------------------------------------------------------------
* Is executed when Power On Button press is detected
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vHmiPowerOnButtonCallback(void)
{
    ;
}

/*********************************************************************
* PUBLIC vHmiSOCButtonCallback()
*---------------------------------------------------------------------
* Is executed when SOC Button press is detected
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vHmiSOCButtonCallback(void)
{
    ;
}

/*********************************************************************
* PUBLIC vOverTemperatureDetectionCallback()
*---------------------------------------------------------------------
* Is executed when an over-temperature signal is detected from any of
* the temperature sensors.
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vOverTemperatureDetectionCallback(void)
{
    ;
}