/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: R'Evolion Software                                               */
/*****************************************************************************/
/* FILE NAME: init_spi_dma.c                                                 */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: Gerald Percherancier                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Implements all the necessary initialization functions for the SPI         */
/* port (Master) on the Evolion 3 Battery System. Interface functions offer  */
/* the flexibility to choose input/output buffers, transfer data size,       */
/* and callback functions for transfer complete (ISRs)                       */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <iface_spi_dma.h>
#include <conf_gpio.h>
#include <FreeRTOS.h>
#include <task.h>


/********************************************************************
 * PRIVATE Types and Structures                                     *
 ********************************************************************/
static struct spi_module sstSpiMasterInstanceP1; /* HW spi module instance Port 1 */
static struct spi_module sstSpiMasterInstanceP2; /* HW spi module instance Port 2 */

static struct dma_resource sstSpiResourceRxP1;  /* DMA Rx resource structure Port 1 */
static struct dma_resource sstSpiResourceTxP1;  /* DMA Tx resource structure Port 1 */
static struct dma_resource sstSpiResourceRxP2;  /* DMA Rx resource structure Port 2 */
static struct dma_resource sstSpiResourceTxP2;  /* DMA Tx resource structure Port 2 */

COMPILER_ALIGNED(16)
static DmacDescriptor sstSpiDescriptorRxP1 SECTION_DMAC_DESCRIPTOR; /* Rx Transfer descriptor Port 1 */
static DmacDescriptor sstSpiDescriptorTxP1 SECTION_DMAC_DESCRIPTOR; /* Tx Transfer descriptor Port 1 */
static DmacDescriptor sstSpiDescriptorRxP2 SECTION_DMAC_DESCRIPTOR; /* Rx Transfer descriptor Port 2 */
static DmacDescriptor sstSpiDescriptorTxP2 SECTION_DMAC_DESCRIPTOR; /* Tx Transfer descriptor Port 2 */


/********************************************************************
 * PRIVATE variables                                                *
 ********************************************************************/
volatile static  bool sbSPIBusyP1 = false;  /* SPI Busy indicator Port 1 */
volatile static  bool sbSPIBusyP2 = false;  /* SPI Busy indicator Port 2 */


/********************************************************************
* PRIVATE Callback functions                                        *
********************************************************************/
static void (*svpSpiTransferDoneRxCbP1)(void) = NULL;   /* Pointer to user Rx callback function Port 1 */
static void (*svpSpiTransferDoneTxCbP1)(void) = NULL;   /* Pointer to user Tx callback function Port 1 */
static void (*svpSpiTransferDoneRxCbP2)(void) = NULL;   /* Pointer to user Rx callback function Port 2 */
static void (*svpSpiTransferDoneTxCbP2)(void) = NULL;   /* Pointer to user Tx callback function Port 2 */


/********************************************************************
* PRIVATE svTransferRxDoneCallBackP1()
*--------------------------------------------------------------------
* Callback (ISR) for Rx complete. This function calls the user defined callback and performs
* any necessary SPI/DMA maintenance.
*
* Inputs:
*   pstResource - reference to the DMA resource structure
* Returns: None
*
*********************************************************************/
static void svTransferRxDoneCallBackP1(struct dma_resource* const pstResource )
{
    /* Set SPI Slave Select Pin High */
    OUTPUT_SET_HIGH(SLAVE_SELECT_PIN_P1);
    
    /* Flag SPI as available */
    sbSPIBusyP1 = false;
    
    (*svpSpiTransferDoneRxCbP1)();
}


/********************************************************************
* PRIVATE svTransferTxDoneCallBackP1()
*--------------------------------------------------------------------
* Callback (ISR) for Tx complete. This function calls the user defined callback and performs
* any necessary SPI/DMA maintenance.
*
* Inputs:
*   pstResource - reference to the DMA resource structure
* Returns: None
*
*********************************************************************/
static void svTransferTxDoneCallBackP1(struct dma_resource* const pstResource )
{
    /* DMA to SPI TX Complete */
    (*svpSpiTransferDoneTxCbP1)();
}


/********************************************************************
* PRIVATE svTransferRxDoneCallBackP2()
*--------------------------------------------------------------------
* Callback (ISR) for Rx complete. This function calls the user defined callback and performs
* any necessary SPI/DMA maintenance.
*
* Inputs:
*   pstResource - reference to the DMA resource structure
* Returns: None
*
*********************************************************************/
static void svTransferRxDoneCallBackP2(struct dma_resource* const pstResource )
{
    /* Set SPI Slave Select Pin High */
    OUTPUT_SET_HIGH(SLAVE_SELECT_PIN_P2);   
    
    /* Flag SPI as available */
    sbSPIBusyP2 = false;
    
    (*svpSpiTransferDoneRxCbP2)();
}


/********************************************************************
* PRIVATE svTransferTxDoneCallBackP2()
*--------------------------------------------------------------------
* Callback (ISR) for Tx complete. This function calls the user defined callback and performs
* any necessary SPI/DMA maintenance.
*
* Inputs:
*   pstResource - reference to the DMA resource structure
* Returns: None
*
*********************************************************************/
static void svTransferTxDoneCallBackP2(struct dma_resource* const pstResource )
{
    /* DMA to SPI TX Complete */
    (*svpSpiTransferDoneTxCbP2)();
}


/********************************************************************
* PRIVATE Functions                                                 *
*********************************************************************/
/********************************************************************
* PRIVATE sbConfigureSpi()
*--------------------------------------------------------------------
* Configures SPI port for operation
*
* Inputs: eSpiPort - Selected SPI Port 
* Returns: 
*   true  - successfully configured SPI
*   false - SPI configuration unsuccessfully
*
*********************************************************************/
static bool sbConfigureSpi(eSpiPortsAvailable_t eSpiPort)
{   
    struct spi_config stConfigSpiMaster;  /* SPI Configuration Structure */ 
    uint16_t usiLoopTimeout = 0;          /* Loop timeout counter */    
    bool bSpiConfigurationResult = false; /* Configuration result, true = success, false (default) = unsuccessful */
    
    /* Initialize the config SPI structure with default values */
    spi_get_config_defaults(&stConfigSpiMaster);    
    
    /* Modify needed parameters */
    stConfigSpiMaster.generator_source = GCLK_GENERATOR_0; /* 48MHz Clock */
    stConfigSpiMaster.mode_specific.master.baudrate = SPI_BAUDRATE; 

    if(eSpiPort == SPI_PORT_1)
    {
        /* Configuration for SPI Port 1 */      
        stConfigSpiMaster.mux_setting = SPI_SIGNAL_MUX_SETTING_E;
        stConfigSpiMaster.pinmux_pad0 = PINMUX_PA04D_SERCOM0_PAD0; 
        stConfigSpiMaster.pinmux_pad1 = PINMUX_UNUSED;//PINMUX_PA05D_SERCOM0_PAD1;
        stConfigSpiMaster.pinmux_pad2 = PINMUX_PA06D_SERCOM0_PAD2;
        stConfigSpiMaster.pinmux_pad3 = PINMUX_PA07D_SERCOM0_PAD3;
            
        /* NXP BMS IC Uses SPI Transfer Mode 1 */
        stConfigSpiMaster.transfer_mode = SPI_TRANSFER_MODE_1;  
        
        /* Wait for SPI init to complete */
        while(spi_init(&sstSpiMasterInstanceP1, SERCOM0, &stConfigSpiMaster) != STATUS_OK && usiLoopTimeout < LOOP_TIMEOUT)
        {
            /* Increment loop timeout, this is the maximum number of retries to initialize the SPI port before giving up */
            usiLoopTimeout++;
        }
        
        /* Set return value based on the SPI initialization result */
        if(usiLoopTimeout < LOOP_TIMEOUT)
        {
            /* Successful initialization */
            bSpiConfigurationResult = true;
                    
            /* Enable SPI */
            spi_enable(&sstSpiMasterInstanceP1);    
        }
        else
        {
            /* Unsuccessful initialization */
            /* Do not enable SPI */
            /* bSpiConfigurationResult is false */
        }
    
        /* Init ChannelID for SPI Resources */
        sstSpiResourceRxP1.channel_id = D_CHANNEL_UNASSIGNED;
        sstSpiResourceTxP1.channel_id = D_CHANNEL_UNASSIGNED;
    }
    else
    {                
        /* Configuration for SPI Port 2 */ 
        stConfigSpiMaster.mux_setting = SPI_SIGNAL_MUX_SETTING_E;  
        stConfigSpiMaster.pinmux_pad0 = PINMUX_PB16C_SERCOM5_PAD0; 
        stConfigSpiMaster.pinmux_pad1 = PINMUX_UNUSED;//PINMUX_PB17C_SERCOM5_PAD1; 
        stConfigSpiMaster.pinmux_pad2 = PINMUX_PB22D_SERCOM5_PAD2; 
        stConfigSpiMaster.pinmux_pad3 = PINMUX_PB23D_SERCOM5_PAD3; 
        
        /* Setup transfer mode for the device connected */
        stConfigSpiMaster.transfer_mode = SPI_TRANSFER_MODE_3; /* TODO: Update Based on Device Specification*/      
        
        /* Wait for SPI init to complete */
        while(spi_init(&sstSpiMasterInstanceP2, SERCOM5, &stConfigSpiMaster) != STATUS_OK && usiLoopTimeout < LOOP_TIMEOUT)     
        {
            /* Increment loop timeout, this is the maximum number of retries to initialize the SPI port before giving up */
            usiLoopTimeout++;
        }
        
        /* Set return value, if loop did not time out initialization was successful */
        if(usiLoopTimeout < LOOP_TIMEOUT)
        {
            /* Successful initialization */
            bSpiConfigurationResult = true; 
        
            /* Enable SPI */
            spi_enable(&sstSpiMasterInstanceP2);
        }
        else
        {
            /* Unsuccessful initialization */
            /* Do not enable SPI */
            /* bSpiConfigurationResult is false */
        }
        
        /* Init ChannelID for SPI Resources */
        sstSpiResourceRxP2.channel_id = D_CHANNEL_UNASSIGNED;
        sstSpiResourceTxP2.channel_id = D_CHANNEL_UNASSIGNED;
    }
    
    return bSpiConfigurationResult;
}


/********************************************************************
* PRIVATE svConfigureDmaResourceRx()
*--------------------------------------------------------------------
* Configures and allocates DMA resource for RX on selected port
*
* Inputs:
*   stRxResource - reference to the DMA resource structure
*   eSpiPort     - Selected SPI Port 
* Returns: None
*
*********************************************************************/
static void svConfigureDmaResourceRx(struct dma_resource *stRxResource, eSpiPortsAvailable_t eSpiPort)
{
    struct dma_resource_config stRxConfig; /* DMA resource configuration structure */
    
    /* Initialize the config DMA structure with default values */
    dma_get_config_defaults(&stRxConfig);

    /* Modify needed parameters */
    if (eSpiPort == SPI_PORT_1)
    {       
        /* Set trigger to port 1 SERCOM 0 RX */
        stRxConfig.peripheral_trigger = SERCOM0_DMAC_ID_RX;
    }
    else
    {
        /* Set trigger to port 2 SERCOM 5 RX */
        stRxConfig.peripheral_trigger = SERCOM5_DMAC_ID_RX;
    }
    
    stRxConfig.trigger_action = DMA_TRIGGER_ACTION_BEAT;

    /* Allocate the DMA resources with chosen configuration */
    dma_allocate(stRxResource, &stRxConfig);
}


/*********************************************************************
* PRIVATE svSetupTransferDescriptorRx()
*---------------------------------------------------------------------
* Sets up the transfer descriptor structure for RX on selected port
*
* Inputs:
*   stRxDescriptor - reference to the DMAC descriptor structure
*   eSpiPort       - Selected SPI Port 
*   ucRxBuffer     - Reference to the received buffer
*   ucRxSize       - Length of data to be received
* Returns: None
*
********************************************************************/
static void svSetupTransferDescriptorRx(DmacDescriptor *stRxDescriptor, eSpiPortsAvailable_t eSpiPort, 
    uint8_t *ucRxBuffer, uint16_t usiRxSize)
{
    struct dma_descriptor_config stRxDescriptorConfig; /* DMA descriptor configuration structure */

    /* Initialize the transfer descriptor configuration structure with default values */
    dma_descriptor_get_config_defaults(&stRxDescriptorConfig);

    /* Modify needed parameters */
    stRxDescriptorConfig.beat_size = DMA_BEAT_SIZE_BYTE;
    stRxDescriptorConfig.src_increment_enable = false;
    stRxDescriptorConfig.block_transfer_count = usiRxSize;  
    stRxDescriptorConfig.destination_address = (uint32_t)ucRxBuffer + (uint32_t)usiRxSize;
    
    /* Select Port */
    if(eSpiPort == SPI_PORT_1)
    {
        /* Set Source Address based on port selected */
        stRxDescriptorConfig.source_address = (uint32_t)(&sstSpiMasterInstanceP1.hw->SPI.DATA.reg);
        
        /* Reset the descriptor to allow the new changes to take effect */
        sstSpiResourceRxP1.descriptor = NULL;
    }
    else
    {
        /* Set Source Address based on port selected */
        stRxDescriptorConfig.source_address = (uint32_t)(&sstSpiMasterInstanceP2.hw->SPI.DATA.reg); 
        
        /* Reset the descriptor to allow the new changes to take effect */
        sstSpiResourceRxP2.descriptor = NULL;
    }   
    
    /* Create the DMA Rx descriptor from the the descriptor configuration */
    dma_descriptor_create(stRxDescriptor, &stRxDescriptorConfig);       
}


/********************************************************************
* PRIVATE svConfigureDmaResourceTx()
*--------------------------------------------------------------------
* Configures and allocates DMA resource for TX on selected port
*
* Inputs:
*   stTxResource - reference to the DMA resource structure
*   eSpiPort     - Selected SPI Port 
* Returns: None
*
*********************************************************************/
static void svConfigureDmaResourceTx(struct dma_resource *stTxResource, eSpiPortsAvailable_t eSpiPort)
{
    struct dma_resource_config stTxConfig; /* DMA resource configuration structure */
    
    /* Initialize the config DMA structure with default values */
    dma_get_config_defaults(&stTxConfig);

    /* Modify needed parameters */
    if(eSpiPort == SPI_PORT_1)
    {
        /* Set trigger to port 2 SERCOM 0 TX */
        stTxConfig.peripheral_trigger = SERCOM0_DMAC_ID_TX; 
    }
    else
    {
        /* Set trigger to port 2 SERCOM 5 TX */
        stTxConfig.peripheral_trigger = SERCOM5_DMAC_ID_TX; 
    }
        
    stTxConfig.trigger_action = DMA_TRIGGER_ACTION_BEAT;

    /* Allocate the DMA resources with chosen configuration */
    dma_allocate(stTxResource, &stTxConfig);
}


/*********************************************************************
* PRIVATE svSetupTransferDescriptorTx()
*---------------------------------------------------------------------
* Sets up the transfer descriptor structure for TX on selected port
*
* Inputs:
*   stTxDescriptor - reference to the DMAC descriptor structure
*   eSpiPort       - Selected SPI Port 
*   ucTxBuffer     - Reference to the transmit buffer
*   ucTxSize       - Length of data to be transmitted
* Returns: None
*
********************************************************************/
static void svSetupTransferDescriptorTx(DmacDescriptor *stTxDescriptor, eSpiPortsAvailable_t eSpiPort, 
    uint8_t *ucTxBuffer, uint16_t usiTxSize)
{
    struct dma_descriptor_config stTxDescriptorConfig; /* DMA descriptor configuration structure */

    /* Initialize the transfer descriptor configuration structure with default values */
    dma_descriptor_get_config_defaults(&stTxDescriptorConfig);

    /* Modify needed parameters */
    stTxDescriptorConfig.beat_size = DMA_BEAT_SIZE_BYTE;
    stTxDescriptorConfig.dst_increment_enable = false;
    stTxDescriptorConfig.block_transfer_count = usiTxSize;
    stTxDescriptorConfig.source_address =  (uint32_t)ucTxBuffer + (uint32_t)usiTxSize;
        
    /* Select port */
    if (eSpiPort == SPI_PORT_1)
    {
        /* Set destination address based on port selected */
        stTxDescriptorConfig.destination_address = (uint32_t)(&sstSpiMasterInstanceP1.hw->SPI.DATA.reg);    

        /* Reset the descriptor to allow the new changes to take effect */
        sstSpiResourceTxP1.descriptor = NULL;
    }
    else
    {
        /* Set destination address based on port selected */
        stTxDescriptorConfig.destination_address = (uint32_t)(&sstSpiMasterInstanceP2.hw->SPI.DATA.reg);
        
        /* Reset the descriptor to allow the new changes to take effect */
        sstSpiResourceTxP2.descriptor = NULL;
    }   
    
    /* Create the DMA Tx descriptor from the the descriptor configuration */
    dma_descriptor_create(stTxDescriptor, &stTxDescriptorConfig);   
}


/********************************************************************
 * PUBLIC Interface Functions                                       *
 ********************************************************************/
/********************************************************************
* PUBLIC bConfigureSpiPort()
*--------------------------------------------------------------------
* Configures the HW SPI port.
*
* Inputs: 
*   eSpiPort - Selected SPI Port 
* Returns: 
*   true     - Successfully configured SPI port.
*   false    - SPI configuration unsuccessful. 
*
********************************************************************/
bool bConfigureSpiPort(eSpiPortsAvailable_t eSpiPort)
{
    /* Configure the HW SPI port */
    return(sbConfigureSpi(eSpiPort));
}


/********************************************************************
* PUBLIC vSetSpiRxCallback()
*--------------------------------------------------------------------
* Receives the reference to the RX callback function for the SPI port
*
* Inputs:
*   eSpiPort        - Selected SPI port
*   vpFunctSpiRxCb  - Reference to the RX complete callback function
* Returns: None
*
*********************************************************************/
void vSetSpiRxCallback(eSpiPortsAvailable_t eSpiPort, void (*vpFunctSpiRxCb)(void))
{
    /* Set callback for selected SPI port RX */
    if(eSpiPort == SPI_PORT_1)
    {
        svpSpiTransferDoneRxCbP1 = vpFunctSpiRxCb;
    }
    else
    {
        svpSpiTransferDoneRxCbP2 = vpFunctSpiRxCb;
    }           
}


/********************************************************************
* PUBLIC vSetSpiTxCallback()
*--------------------------------------------------------------------
* Receives the reference to the RX callback function for the SPI port
*
* Inputs:
*   eSpiPort        - Selected SPI Port
*   vpfunctSpiTxCb  - Reference to the TX complete callback function
* Returns: None
*
*********************************************************************/
void vSetSpiTxCallback(eSpiPortsAvailable_t eSpiPort, void (*vpfunctSpiTxCb)(void))
{
    /* Set callback for selected SPI port TX */
    if(eSpiPort == SPI_PORT_1)
    {
        svpSpiTransferDoneTxCbP1 = vpfunctSpiTxCb;
    }
    else
    {
        svpSpiTransferDoneTxCbP2 = vpfunctSpiTxCb;
    }
}


/*********************************************************************
* PUBLIC vSetupSpiRx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the RX buffer and length and enables
* the registered callbacks to the SPI port.
*
* Inputs:
*   eSpiPort   - Selected SPI Port
*   ucBufferRx - Reference to the receive buffer
*   ucBufSize  - Length of data to be received
* Returns: None
*
**********************************************************************/
void vSetupSpiRx(eSpiPortsAvailable_t eSpiPort, uint8_t* ucBufferRx, uint16_t usiBufSize)
{   
    if(eSpiPort == SPI_PORT_1)
    {           
        /* Configure SPI RX on Port 1 */
        if(sstSpiResourceRxP1.channel_id == D_CHANNEL_UNASSIGNED)
        {
            /* This is executed the first time the resource is allocated */ 
            svConfigureDmaResourceRx(&sstSpiResourceRxP1, SPI_PORT_1);
            svSetupTransferDescriptorRx(&sstSpiDescriptorRxP1, SPI_PORT_1, ucBufferRx, usiBufSize);
            dma_add_descriptor(&sstSpiResourceRxP1, &sstSpiDescriptorRxP1);
            dma_register_callback(&sstSpiResourceRxP1, svTransferRxDoneCallBackP1, DMA_CALLBACK_TRANSFER_DONE);
            dma_enable_callback(&sstSpiResourceRxP1, DMA_CALLBACK_TRANSFER_DONE);
        }
        else
        {
            /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
            svSetupTransferDescriptorRx(&sstSpiDescriptorRxP1, SPI_PORT_1, ucBufferRx, usiBufSize);
            sstSpiResourceRxP1.descriptor = NULL;
            dma_add_descriptor(&sstSpiResourceRxP1, &sstSpiDescriptorRxP1);
        }
    }
    else
    {
        /* Configure SPI RX on Port 2 */
        if(sstSpiResourceRxP2.channel_id == D_CHANNEL_UNASSIGNED)
        {
            /* This is executed the first time the resource is allocated */
            svConfigureDmaResourceRx(&sstSpiResourceRxP2, SPI_PORT_2);
            svSetupTransferDescriptorRx(&sstSpiDescriptorRxP2, SPI_PORT_2, ucBufferRx, usiBufSize);
            dma_add_descriptor(&sstSpiResourceRxP2, &sstSpiDescriptorRxP2);
            dma_register_callback(&sstSpiResourceRxP2, svTransferRxDoneCallBackP2, DMA_CALLBACK_TRANSFER_DONE);
            dma_enable_callback(&sstSpiResourceRxP2, DMA_CALLBACK_TRANSFER_DONE);
        }
        else
        {
            /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
            svSetupTransferDescriptorRx(&sstSpiDescriptorRxP2, SPI_PORT_2, ucBufferRx, usiBufSize);
            sstSpiResourceRxP2.descriptor = NULL;
            dma_add_descriptor(&sstSpiResourceRxP2, &sstSpiDescriptorRxP2);
        }
    }
}


/*********************************************************************
* PUBLIC vSetupSpiTx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the TX buffer and length and enables
* the registered callbacks to the SPI port.
*
* Inputs:
*   eSpiPort   - Selected SPI Port
*   ucBufferTx - Reference to the transmit buffer
*   ucBufSize  - Length of data to be transmitted
* Returns: None
*
**********************************************************************/
void vSetupSpiTx(eSpiPortsAvailable_t eSpiPort, uint8_t* ucBufferTx, uint16_t usiBufSize)
{   
    if(eSpiPort == SPI_PORT_1)
    {
        /* Configure SPI TX on Port 1 */
        if(sstSpiResourceTxP1.channel_id == D_CHANNEL_UNASSIGNED)
        {
            /* This is executed the first time the resource is allocated */ 
            svConfigureDmaResourceTx(&sstSpiResourceTxP1, SPI_PORT_1);
            svSetupTransferDescriptorTx(&sstSpiDescriptorTxP1, SPI_PORT_1, ucBufferTx, usiBufSize);
            dma_add_descriptor(&sstSpiResourceTxP1, &sstSpiDescriptorTxP1);
            dma_register_callback(&sstSpiResourceTxP1, svTransferTxDoneCallBackP1, DMA_CALLBACK_TRANSFER_DONE);
            dma_enable_callback(&sstSpiResourceTxP1, DMA_CALLBACK_TRANSFER_DONE);
        }
        else
        {
            /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
            svSetupTransferDescriptorTx(&sstSpiDescriptorTxP1, SPI_PORT_1, ucBufferTx, usiBufSize);
            sstSpiResourceTxP1.descriptor = NULL;
            dma_add_descriptor(&sstSpiResourceTxP1, &sstSpiDescriptorTxP1);
        }
    }
    else
    {
        /* Configure SPI TX on Port 2 */
        if(sstSpiResourceTxP2.channel_id == D_CHANNEL_UNASSIGNED)
        {
            /* This is executed the first time the resource is allocated */
            svConfigureDmaResourceTx(&sstSpiResourceTxP2, SPI_PORT_2);
            svSetupTransferDescriptorTx(&sstSpiDescriptorTxP2, SPI_PORT_2, ucBufferTx, usiBufSize);
            dma_add_descriptor(&sstSpiResourceTxP2, &sstSpiDescriptorTxP2);
            dma_register_callback(&sstSpiResourceTxP2, svTransferTxDoneCallBackP2, DMA_CALLBACK_TRANSFER_DONE);
            dma_enable_callback(&sstSpiResourceTxP2, DMA_CALLBACK_TRANSFER_DONE);
        }
        else
        {
            /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
            svSetupTransferDescriptorTx(&sstSpiDescriptorTxP2, SPI_PORT_2, ucBufferTx, usiBufSize);
            sstSpiResourceTxP2.descriptor = NULL;
            dma_add_descriptor(&sstSpiResourceTxP2, &sstSpiDescriptorTxP2);
        }
    }
}

/*********************************************************************
* PUBLIC bStartSpiRxTx()
*---------------------------------------------------------------------
* Starts the transmit process, when the full amount of data
* is transmitted and received the callback functions are called. 
* SPI Data transfer is full duplex in order to send or receive data both 
* send and receive operations are preformed. Slave pin must be selected
* (LOW) before transfer can start. Since SPI is master, the TX
* operation initiates transfer and generates the SPI clock. Once all the 
* data is transferred the RX callback function de-selects the Slave 
* pin (HIGH).
*
* Inputs:  
*   eSpiPort - Selected SPI Port
* Returns: 
*   true  - Successfully Started and new SPI transfer
*   false - SPI was busy, previous transfer has not completed
*
********************************************************************/
bool bStartSpiRxTx(eSpiPortsAvailable_t eSpiPort)
{       
    bool bSpiStartSuccess = false; /* Return variable, true for successful, false (default) for unsuccessful */
    
    if(eSpiPort == SPI_PORT_1)
    {
        /* Check if SPI is busy, if SPI is busy, previous transfer has not completed. If not busy start transfer */
        if(sbSPIBusyP1 == false)
        {
            /* Flag SPI as busy, this is cleared in the Rx callback once SPI completes transfer */
            sbSPIBusyP1 = true;
                
            /* Set Slave Select Pin to Low */
            OUTPUT_SET_LOW(SLAVE_SELECT_PIN_P1);
            
            /* Start RX process */
            dma_start_transfer_job(&sstSpiResourceRxP1);
                
            /* Start TX process */
            dma_start_transfer_job(&sstSpiResourceTxP1);
                
            /* Spi was not busy, successfully started transfer */
            bSpiStartSuccess = true;
                
            /* Slave Select Pin is set to High in the Callback function of the SPI RX */
        }
        else
        {
            /* bSpiStartSuccess = false, Spi was already busy */
        }
    }
    else
    {
        /* Check if SPI is busy, if SPI is busy, previous transfer has not completed. If not busy start transfer */
        if(sbSPIBusyP2 == false)
        {
            /* Flag SPI as busy, this is cleared in the Rx callback once SPI completes transfer */
            sbSPIBusyP2 = true;
                                    
            /* Set Slave Select Pin to Low */
            OUTPUT_SET_LOW(SLAVE_SELECT_PIN_P2);                                                                
            
            /* Start RX process */
            dma_start_transfer_job(&sstSpiResourceRxP2);                                             
            
            /* Start TX process */
            dma_start_transfer_job(&sstSpiResourceTxP2);
            
            /* Spi was not busy, successfully started transfer */
            bSpiStartSuccess = true;
            
            /* Slave Select Pin is set to High in the Callback function of the SPI RX */
        }
        else
        {
            /* bSpiStartSuccess = false, Spi was already busy */
        }
    }
                
    return bSpiStartSuccess;    
}

/*********************************************************************
* PUBLIC bIsSpiPortBusy()
*---------------------------------------------------------------------
* Returns the status of the SPI port, whether it is busy or not.
*
* Inputs: 
*   eSpiPort - Selected SPI Port
* Returns: 
*   true  - SPI port is busy, currently performing transfer.
*   false - SPI port is available.
*
**********************************************************************/
bool bIsSpiPortBusy(eSpiPortsAvailable_t eSpiPort)
{
    bool bSpiStatus;
    
    /* Return the Status of the selected SPI port */
    if(eSpiPort == SPI_PORT_1)
    {
        bSpiStatus = sbSPIBusyP1;
    }
    else
    {
        bSpiStatus = sbSPIBusyP2;
    }
    
    return bSpiStatus;
}


/******************************** End of File ********************************/
