/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: R'Evolion Software                                               */
/*****************************************************************************/
/* FILE NAME: init_usart_dma.c                                               */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Implements all the necessary initialization functions for the two USARTs  */
/* available on the Evolion 3 Battery System. Interface functions offer      */
/* the flexibility to choose input/output buffers, transfer data size,       */
/* BAUD rates for both of the available ports and callback functions for     */
/* transfer complete (ISRs)                                                  */
/*                                                                           */
/*****************************************************************************/

#include <asf.h>
#include <iface_usart_dma.h>

/********************************************************************
 * PRIVATE Types and Structures                                     *
 ********************************************************************/
static struct usart_module sstUsartInstanceP1;   /* HW Usart module instance Port 1 */
static struct usart_module sstUsartInstanceP2;   /* HW Usart module instance Port 2 */

static struct dma_resource sstUsartResourceRxP1; /* DMA Rx resource structures Port 1 */
static struct dma_resource sstUsartResourceTxP1; /* DMA Tx resource structures Port 1 */
static struct dma_resource sstUsartResourceRxP2; /* DMA Rx resource structures Port 2 */
static struct dma_resource sstUsartResourceTxP2; /* DMA Tx resource structures Port 2 */

COMPILER_ALIGNED(16)
static DmacDescriptor sstUsartDescriptorRxP1 SECTION_DMAC_DESCRIPTOR; /* Rx Transfer descriptor Port 1 */ 
static DmacDescriptor sstUsartDescriptorTxP1 SECTION_DMAC_DESCRIPTOR; /* Tx Transfer descriptor Port 1 */
static DmacDescriptor sstUsartDescriptorRxP2 SECTION_DMAC_DESCRIPTOR; /* Rx Transfer descriptor Port 2 */
static DmacDescriptor sstUsartDescriptorTxP2 SECTION_DMAC_DESCRIPTOR; /* Tx Transfer descriptor Port 2 */


/********************************************************************
 * PRIVATE variables                                                *
 ********************************************************************/
volatile static  bool sbUsartRxBusyP1 = false;  /* Usart Rx Busy indicator Port 1 */
volatile static  bool sbUsartTxBusyP1 = false;  /* Usart Tx Busy indicator Port 1 */
volatile static  bool sbUsartRxBusyP2 = false;  /* Usart Rx Busy indicator Port 2 */
volatile static  bool sbUsartTxBusyP2 = false;  /* Usart Tx Busy indicator Port 2 */


/********************************************************************
* PRIVATE Callback functions                                        *
 ********************************************************************/
static void (*svpUsartTransferDoneRxCbP1)(void) = NULL; /* Pointer to user Rx callback function Port 1 */
static void (*svpUsartTransferDoneTxCbP1)(void) = NULL; /* Pointer to user Tx callback function Port 1 */
static void (*svpUsartTransferDoneRxCbP2)(void) = NULL; /* Pointer to user Rx callback function Port 2 */
static void (*svpUsartTransferDoneTxCbP2)(void) = NULL; /* Pointer to user Tx callback function Port 2 */


/********************************************************************
* PRIVATE svTransferRxDoneCallBackP1()
*--------------------------------------------------------------------
* Callback (ISR) for Rx complete. This function calls the user defined callback and performs
* any necessary Usart/DMA maintenance.
*
* Inputs: dma_resource
* Returns: None
*
*********************************************************************/
static void svTransferRxDoneCallBackP1(struct dma_resource *const pstResource )
{
        
    /* Flag Usart as available */
    sbUsartRxBusyP1 = false;
    
    (*svpUsartTransferDoneRxCbP1)();
}


/********************************************************************
* PRIVATE svTransferTxDoneCallBackP1()
*--------------------------------------------------------------------
* Callback (ISR) for Tx complete. This function calls the user defined callback and performs
* any necessary Usart/DMA maintenance.
*
* Inputs: dma_resource
* Returns: None
*
*********************************************************************/
static void svTransferTxDoneCallBackP1(struct dma_resource *const pstResource )
{
    /* Flag Usart as available */
    sbUsartTxBusyP1 = false;
    
    /* DMA to USART TX Complete */
    (*svpUsartTransferDoneTxCbP1)();
}


/********************************************************************
* PRIVATE svTransferRxDoneCallBackP2()
*--------------------------------------------------------------------
* Callback (ISR) for Rx complete. This function calls the user defined callback and performs
* any necessary Usart/DMA maintenance.
*
* Inputs: dma_resource
* Returns: None
*
*********************************************************************/
static void svTransferRxDoneCallBackP2(struct dma_resource *const pstResource )
{
    
    /* Flag Usart as available */
    sbUsartRxBusyP2 = false;
    
    (*svpUsartTransferDoneRxCbP2)();
}


/********************************************************************
* PRIVATE svTransferTxDoneCallBackP2()
*--------------------------------------------------------------------
* Callback (ISR) for Tx complete. This function calls the user defined callback and performs
* any necessary Usart/DMA maintenance.
*
* Inputs: dma_resource
* Returns: None
*
*********************************************************************/
static void svTransferTxDoneCallBackP2(struct dma_resource *const pstResource )
{
    /* Flag Usart as available */
    sbUsartTxBusyP2 = false;
    
    /* DMA to USART TX Complete */
    (*svpUsartTransferDoneTxCbP2)();
}


/********************************************************************
* PRIVATE Functions                                                 *
*********************************************************************/
/********************************************************************
* PRIVATE sbConfigureUsart()
*--------------------------------------------------------------------
* Configures USART port selected for operation at the selected BAUD
* rate
*
* Inputs:
*   eUsartPort  - Enum for the USART selection
*   eBaudRate   - USART speed
*   eParity     - USART parity
*   eStopBits   - USART stop bits
* Returns: 
*   true  - Successfully configured USART port
*   false - USART configuration unsuccessful
*
*********************************************************************/
static bool sbConfigureUsart(eUsartPortsAvailable_t eUsartPort, eUsartPortSpeed_t eBaudRate,
enum usart_parity eParity, enum usart_stopbits eStopBits)
{
    struct usart_config stConfigUsart;    /* USART Configuration Structure */   
    uint16_t usiLoopTimeout = 0;          /* Loop timeout counter */
    bool bUsartConfigurationResult = false; /* Configuration result, true = success, false (default) = unsuccessful */
    
    /* Initialize the config USART structure with default values */
    usart_get_config_defaults(&stConfigUsart);

    /* Modify needed parameters */
    stConfigUsart.generator_source = GCLK_GENERATOR_3; /* 8MHz Clock */
    stConfigUsart.baudrate = (uint32_t)eBaudRate;
    stConfigUsart.parity = eParity;
    stConfigUsart.stopbits = eStopBits;
    
    if(eUsartPort == USART_PORT_1)
    {
        /* Configuration for Port 1 */
        stConfigUsart.mux_setting = USART_RX_1_TX_0_XCK_1;
        stConfigUsart.pinmux_pad0 = PINMUX_PA22C_SERCOM3_PAD0;
        stConfigUsart.pinmux_pad1 = PINMUX_PA23C_SERCOM3_PAD1;
        stConfigUsart.pinmux_pad2 = PINMUX_UNUSED;
        stConfigUsart.pinmux_pad3 = PINMUX_UNUSED;
        
        /* Wait for USART init to complete */
        while (usart_init(&sstUsartInstanceP1, SERCOM3, &stConfigUsart) != STATUS_OK  && usiLoopTimeout < LOOP_TIMEOUT)
        {
            /* Increment loop timeout, this is the maximum number of retries to initialize the Usart port before giving up */
            usiLoopTimeout++;
        }
        
        /* Set return value based on the USART initialization result */
        if(usiLoopTimeout < LOOP_TIMEOUT)
        {
            /* Successful initialization */
            bUsartConfigurationResult = true;
            
            /* Enable USART */
            usart_enable(&sstUsartInstanceP1);
        }
        else
        {
            /* Unsuccessful initialization */
            /* Do not enable USART */
            /* bUSARTConfigurationResult is false */
        }               
        
        /* Init ChannelID for Port1 Resources */
        sstUsartResourceRxP1.channel_id = D_CHANNEL_UNASSIGNED;
        sstUsartResourceTxP1.channel_id = D_CHANNEL_UNASSIGNED;
    }
    else
    {
        /* Configuration for Port 2 */
        stConfigUsart.mux_setting = USART_RX_3_TX_2_XCK_3;
        stConfigUsart.pinmux_pad0 = PINMUX_UNUSED;
        stConfigUsart.pinmux_pad1 = PINMUX_UNUSED;
        stConfigUsart.pinmux_pad2 = PINMUX_PB10D_SERCOM4_PAD2;
        stConfigUsart.pinmux_pad3 = PINMUX_PB11D_SERCOM4_PAD3;
        
        /* Wait for USART init to complete */
        while (usart_init(&sstUsartInstanceP2, SERCOM4, &stConfigUsart) != STATUS_OK  && usiLoopTimeout < LOOP_TIMEOUT)
        {
            /* Increment loop timeout, this is the maximum number of retries to initialize the Usart port before giving up */
            usiLoopTimeout++;
        }
        
        /* Set return value based on the USART initialization result */
        if(usiLoopTimeout < LOOP_TIMEOUT)
        {
            /* Successful initialization */
            bUsartConfigurationResult = true;
            
            /* Enable USART */
            usart_enable(&sstUsartInstanceP2);
        }
        else
        {
            /* Unsuccessful initialization */
            /* Do not enable USART */
            /* bUSARTConfigurationResult is false */
        }
        
        /* Init ChannelID for Port2 Resources */
        sstUsartResourceRxP2.channel_id = D_CHANNEL_UNASSIGNED;
        sstUsartResourceTxP2.channel_id = D_CHANNEL_UNASSIGNED;
    }
    
    return bUsartConfigurationResult;
}


/********************************************************************
* PRIVATE svConfigureDmaResourceRx()
*--------------------------------------------------------------------
* Configures and allocates DMA resource for RX on selected port
*
* Inputs: 
*   par_PS_resource - reference to the DMA resource structure
*   eUsartPort - Enum for the USART selection
* Returns: None
*
*********************************************************************/
static void svConfigureDmaResourceRx(struct dma_resource *stRxResource, eUsartPortsAvailable_t eUsartPort)
{
    struct dma_resource_config stRxConfig;
    
    /* Initialize the config DMA structure with default values */
    dma_get_config_defaults(&stRxConfig);
    
    /* Modify needed parameters */
    if (eUsartPort == USART_PORT_1)
    {
        /* Set trigger to port 1 */
        stRxConfig.peripheral_trigger = SERCOM3_DMAC_ID_RX; 
    }   
    else
    {
        /* Set trigger to port 2 */
        stRxConfig.peripheral_trigger = SERCOM4_DMAC_ID_RX; 
    }
    
    stRxConfig.trigger_action = DMA_TRIGGER_ACTION_BEAT;
    
    /* Allocate the DMA resources with chosen configuration */
    dma_allocate(stRxResource, &stRxConfig);
}


/*********************************************************************
* PRIVATE svSetupTransferDescriptorRx()
*---------------------------------------------------------------------
* Sets up the transfer descriptor structure for RX on selected port
* 
* Inputs:
*   stRxDescriptor - reference to the DMAC descriptor structure
*   eUsartPort     - Enum for the USART selection
*   ucRxBuffer     - Reference to the receive buffer
*   ucRxSize       - Length of data to be received
* Returns: None
*
********************************************************************/
static void svSetupTransferDescriptorRx(DmacDescriptor *stRxDescriptor, eUsartPortsAvailable_t eUsartPort,
        char* cpRxBuffer, uint16_t usiRxSize)
{
    struct dma_descriptor_config stRxDescriptorConfig;

    /* Initialize the transfer descriptor configuration structure with default values */
    dma_descriptor_get_config_defaults(&stRxDescriptorConfig);

    /* Modify needed parameters */
    stRxDescriptorConfig.beat_size = DMA_BEAT_SIZE_BYTE;
    stRxDescriptorConfig.src_increment_enable = false;
    stRxDescriptorConfig.block_transfer_count = usiRxSize;
    stRxDescriptorConfig.destination_address = (uint32_t)cpRxBuffer + (uint32_t)(usiRxSize);
    
    /* Set Source Address based on port selected */
    if(eUsartPort == USART_PORT_1)
    {
        /* Set Source Address based on port selected */
        stRxDescriptorConfig.source_address = (uint32_t)(&sstUsartInstanceP1.hw->USART.DATA.reg);
        
        /* Reset the descriptor to allow the new changes to take effect */
        sstUsartResourceRxP1.descriptor = NULL;
    }
    else
    {
        /* Set Source Address based on port selected */
        stRxDescriptorConfig.source_address = (uint32_t)(&sstUsartInstanceP2.hw->USART.DATA.reg);
        
        /* Reset the descriptor to allow the new changes to take effect */
        sstUsartResourceRxP2.descriptor = NULL;
    }
    
    /* Create the DMA Rx descriptor from the the descriptor configuration */
    dma_descriptor_create(stRxDescriptor, &stRxDescriptorConfig);
}


/********************************************************************
* PRIVATE svConfigureDmaResourceTx()
*--------------------------------------------------------------------
* Configures and allocates DMA resource for TX on selected port
*
* Inputs:
*   stTxResource - reference to the DMA resource structure
*   eUsartPort   - Enum for the USART selection
* Returns: None
*
********************************************************************/
static void svConfigureDmaResourceTx(struct dma_resource *stTxResource, eUsartPortsAvailable_t eUsartPort)
{
    struct dma_resource_config stTxConfig;
    
    /* Initialize the config DMA structure with default values */
    dma_get_config_defaults(&stTxConfig);

    /* Modify needed parameters */
    if (eUsartPort == USART_PORT_1)
    {
        /* Set trigger to port 1 */
        stTxConfig.peripheral_trigger = SERCOM3_DMAC_ID_TX;
    }
    else
    {
        /* Set trigger to port 2 */
        stTxConfig.peripheral_trigger = SERCOM4_DMAC_ID_TX; 
    }   
    stTxConfig.trigger_action = DMA_TRIGGER_ACTION_BEAT;
    
    /* Allocate the DMA resources with chosen configuration */
    dma_allocate(stTxResource, &stTxConfig);
}


/********************************************************************
* PRIVATE svSetupTransferDescriptorTx()
*--------------------------------------------------------------------
* Sets up the transfer descriptor structure for TX on selected port
*
* Inputs:
*   stTxDescriptor - reference to the DMAC descriptor structure
*   eUsartPort   - Enum for the USART selection
*   par_PA_txBuffer   - Reference to the transmit buffer
*   par_I_txDataLen   - Length of data to be transmitted
* Returns: None
*
********************************************************************/
static void svSetupTransferDescriptorTx(DmacDescriptor *stTxDescriptor, eUsartPortsAvailable_t eUsartPort, 
        char* cpTxBuffer, uint16_t ucTxSize)
{
    struct dma_descriptor_config stTxDescriptorConfig;
    
    /* Initialize the transfer descriptor configuration structure with default values */
    dma_descriptor_get_config_defaults(&stTxDescriptorConfig);

    /* Modify needed parameters */
    stTxDescriptorConfig.beat_size = DMA_BEAT_SIZE_BYTE;
    stTxDescriptorConfig.dst_increment_enable = false;
    stTxDescriptorConfig.block_transfer_count = ucTxSize;
    stTxDescriptorConfig.source_address = (uint32_t)cpTxBuffer + (uint32_t)(ucTxSize);
    
    /* Set Source Address based on port selected */
    if(eUsartPort == USART_PORT_1)
    {
        stTxDescriptorConfig.destination_address =  (uint32_t)(&sstUsartInstanceP1.hw->USART.DATA.reg);
        
        /* Reset the descriptor to allow the new changes to take effect */
        sstUsartResourceTxP1.descriptor = NULL;
    }
    else
    {
        stTxDescriptorConfig.destination_address =  (uint32_t)(&sstUsartInstanceP2.hw->USART.DATA.reg);
        
        /* Reset the descriptor to allow the new changes to take effect */
        sstUsartResourceTxP2.descriptor = NULL;
    }
    
    /* Create the DMA Tx descriptor from the the descriptor configuration */
    dma_descriptor_create(stTxDescriptor, &stTxDescriptorConfig);
}


/********************************************************************
 * PUBLIC Interface Functions                                       *
 ********************************************************************/
/********************************************************************
* PUBLIC bConfigureUsartPort()
*--------------------------------------------------------------------
* Sets the port speed, parity and stop bits.
*
* Inputs:
*   eUsartPort  - Selected USART port
*   eBaudRate   - Serial port Baud Rate (9600, 19200, 57600, 115200)
*   eParity     - Serial Port Parity (Odd, Even or None)
*   eStopBits   - Serial port stop bits (1 or 2)
* Returns: 
*   true  - successfully configured USART
*   false - USART configuration unsuccessful
*
********************************************************************/

bool bConfigureUsartPort(eUsartPortsAvailable_t eUsartPort, eUsartPortSpeed_t eBaudRate,
    enum usart_parity eParity, enum usart_stopbits eStopBits)
{
    /* Configure the HW USART port */
    return(sbConfigureUsart(eUsartPort, eBaudRate, eParity, eStopBits));
}


/********************************************************************
* PUBLIC vSetUsartRxCallback()
*--------------------------------------------------------------------
* Receives the reference to the RX callback function for the selected port
*
* Inputs:
*   eUsartPort       - Selected USART port 
*   vpFunctUsartRxCb - Reference to the RX complete callback function
* Returns: None
*
*********************************************************************/
void vSetUsartRxCallback(eUsartPortsAvailable_t eUsartPort, void (*vpFunctUsartRxCb)(void))
{   
    /* Set callback for selected USART port RX */
    if(eUsartPort == USART_PORT_1)
    {   
        svpUsartTransferDoneRxCbP1 = vpFunctUsartRxCb;                  
    }
    else
    {   
        svpUsartTransferDoneRxCbP2 = vpFunctUsartRxCb;      
    }
}


/********************************************************************
* PUBLIC vSetUsartTxCallback()
*--------------------------------------------------------------------
* Receives the reference to the TX callback function for the selected port
*
* Inputs:
*   eUsartPort       - Selected USART port 
*   vpFunctUsartTxCb - Reference to the TX complete callback function
* Returns: None
*
*********************************************************************/
void vSetUsartTxCallback(eUsartPortsAvailable_t eUsartPort, void (*vpFunctUsartTxCb)(void))
{
    if(eUsartPort == USART_PORT_1)
    {
        /* Set callback for port 1 */       
        svpUsartTransferDoneTxCbP1 = vpFunctUsartTxCb;
    }
    else
    {
        /* Set callback for port 2 */       
        svpUsartTransferDoneTxCbP2 = vpFunctUsartTxCb;
    }
}


/*********************************************************************
* PUBLIC vSetupUsartRx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the RX buffer and length and enables
* the registered callbacks to the selected port. 
*
* Inputs:
*   eUsartPort  - Selected USART port 
*   ucpRxBuffer - Reference to the receive buffer
*   usiRxSize   - Length of data to be received 
* Returns: None
*
**********************************************************************/
void vSetupUsartRx(eUsartPortsAvailable_t eUsartPort, char *cpRxBuffer, uint16_t usiRxSize)  
{           
    if(eUsartPort == USART_PORT_1)
    {
        /* Configure USART RX on Port 1 */
        if(sstUsartResourceRxP1.channel_id == D_CHANNEL_UNASSIGNED)
        {   
            /* This is executed the first time the resource is allocated */ 
            svConfigureDmaResourceRx(&sstUsartResourceRxP1, USART_PORT_1);      
            svSetupTransferDescriptorRx(&sstUsartDescriptorRxP1, USART_PORT_1, cpRxBuffer, usiRxSize);      
            dma_add_descriptor(&sstUsartResourceRxP1, &sstUsartDescriptorRxP1);     
            dma_register_callback(&sstUsartResourceRxP1, svTransferRxDoneCallBackP1, DMA_CALLBACK_TRANSFER_DONE);               
            dma_enable_callback(&sstUsartResourceRxP1,  DMA_CALLBACK_TRANSFER_DONE);
        }
        else
        {
            /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
            svSetupTransferDescriptorRx(&sstUsartDescriptorRxP1, USART_PORT_1, cpRxBuffer, usiRxSize);
            sstUsartResourceRxP1.descriptor = NULL;
            dma_add_descriptor(&sstUsartResourceRxP1, &sstUsartDescriptorRxP1);
        }
    }   
    else 
    {
        /* Configure USART RX on Port 2 */
        if(sstUsartResourceRxP2.channel_id == D_CHANNEL_UNASSIGNED)
        {
            /* This is executed the first time the resource is allocated */
            svConfigureDmaResourceRx(&sstUsartResourceRxP2, USART_PORT_2);      
            svSetupTransferDescriptorRx(&sstUsartDescriptorRxP2, USART_PORT_2, cpRxBuffer, usiRxSize);      
            dma_add_descriptor(&sstUsartResourceRxP2, &sstUsartDescriptorRxP2);     
            dma_register_callback(&sstUsartResourceRxP2, svTransferRxDoneCallBackP2, DMA_CALLBACK_TRANSFER_DONE);               
            dma_enable_callback(&sstUsartResourceRxP2,  DMA_CALLBACK_TRANSFER_DONE);        
        }
        else
        {
            /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
            svSetupTransferDescriptorRx(&sstUsartDescriptorRxP2, USART_PORT_2, cpRxBuffer, usiRxSize);
            sstUsartResourceRxP2.descriptor = NULL;
            dma_add_descriptor(&sstUsartResourceRxP2, &sstUsartDescriptorRxP2);
        }
    }   
}


/*********************************************************************
* PUBLIC vSetupUsartTx()
*---------------------------------------------------------------------
* Allocates the DMA resources sets the TX buffer and length and enables
* the registered callbacks to the selected port. 
*
* Inputs:
*   eUsartPort  - Selected USART port 
*   ucpTxBuffer - Reference to the transmit buffer
*   usiTxSize   - Length of data to be transmitted
* Returns: None
*
**********************************************************************/
void vSetupUsartTx(eUsartPortsAvailable_t eUsartPort, char *cpTxBuffer, uint16_t usiTxSize)
{       
    if(eUsartPort == USART_PORT_1)
    {
        /* Configure USART TX on Port 1 */
        if(sstUsartResourceTxP1.channel_id == D_CHANNEL_UNASSIGNED)
        {   
            /* This is executed the first time the resource is allocated */             
            svConfigureDmaResourceTx(&sstUsartResourceTxP1, USART_PORT_1);
            svSetupTransferDescriptorTx(&sstUsartDescriptorTxP1, USART_PORT_1, cpTxBuffer, usiTxSize);
            dma_add_descriptor(&sstUsartResourceTxP1, &sstUsartDescriptorTxP1);
            dma_register_callback(&sstUsartResourceTxP1, svTransferTxDoneCallBackP1, DMA_CALLBACK_TRANSFER_DONE);       
            dma_enable_callback(&sstUsartResourceTxP1,  DMA_CALLBACK_TRANSFER_DONE);            
        }
        else
        {
            /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
            svSetupTransferDescriptorTx(&sstUsartDescriptorTxP1, USART_PORT_1, cpTxBuffer, usiTxSize);
            sstUsartResourceTxP1.descriptor = NULL;
            dma_add_descriptor(&sstUsartResourceTxP1, &sstUsartDescriptorTxP1);         
        }
    }
    else
    {
        /* Configure USART TX on Port 2 */  
        if(sstUsartResourceTxP2.channel_id == D_CHANNEL_UNASSIGNED)
        {   
            /* This is executed the first time the resource is allocated */
            svConfigureDmaResourceTx(&sstUsartResourceTxP2, USART_PORT_2);      
            svSetupTransferDescriptorTx(&sstUsartDescriptorTxP2, USART_PORT_2, cpTxBuffer, usiTxSize);      
            dma_add_descriptor(&sstUsartResourceTxP2, &sstUsartDescriptorTxP2);     
            dma_register_callback(&sstUsartResourceTxP2, svTransferTxDoneCallBackP2, DMA_CALLBACK_TRANSFER_DONE);               
            dma_enable_callback(&sstUsartResourceTxP2,  DMA_CALLBACK_TRANSFER_DONE);            
        }
        else
        {
            /* Resource is already allocated and callbacks registered only change the descriptor to change buffer and data length */
            svSetupTransferDescriptorTx(&sstUsartDescriptorTxP2, USART_PORT_2, cpTxBuffer, usiTxSize);
            sstUsartResourceTxP2.descriptor = NULL;
            dma_add_descriptor(&sstUsartResourceTxP2, &sstUsartDescriptorTxP2);
        }
    }
}


/********************************************************************
* PUBLIC vStartUsartRx()
*--------------------------------------------------------------------
* Start the receive process, when the full amount of data 
* expected is received the callback function is called.
*
* Inputs:
*   eUsartPort -Selected USART port 
* Returns: 
*   true  - Successfully Started and new USART transfer
*   false - USART was busy, previous transfer has not completed
*
********************************************************************/
bool vStartUsartRx(eUsartPortsAvailable_t eUsartPort )
{
    bool bUsartStartSuccess = false; /* Return variable, true for successful, false (default) for unsuccessful */
    
    if(eUsartPort  == USART_PORT_1)
    {
        
        if(sbUsartRxBusyP1 == false)
        {
            /* Flag Usart RX P1 as busy */
            sbUsartRxBusyP1 = true;
            
            /* Start receiving USART data port 1 */
            dma_start_transfer_job(&sstUsartResourceRxP1);
            
            /* Usart was not busy, successfully started transfer */
            bUsartStartSuccess = true;
        }
        else
        {
            /* bUsartStartSuccess = false, USART Rx P1 was already busy */
        }
    }
    else
    {       
        if(sbUsartRxBusyP2 == false)
        {
            /* Flag Usart RX P21 as busy */
            sbUsartRxBusyP2 = true;
            
            /* Start receiving USART data port 2 */
            dma_start_transfer_job(&sstUsartResourceRxP2);
        
            /* Usart was not busy, successfully started transfer */
            bUsartStartSuccess = true;
        }
        else
        {
            /* bUsartStartSuccess = false, USART Rx P1 was already busy */
        }
    }   
    
    return bUsartStartSuccess;
}


/*********************************************************************
* PUBLIC vStartUsartTx()
*---------------------------------------------------------------------
* Start the transmit process, when the full amount of data 
* is transmitted the callback function is called.
*
* Inputs:
*   eUsartPort - Selected USART port 
* Returns: 
*   true  - Successfully Started and new USART transfer
*   false - USART was busy, previous transfer has not completed
*
********************************************************************/
bool vStartUsartTx(eUsartPortsAvailable_t eUsartPort )
{
    bool bUsartStartSuccess = false; /* Return variable, true for successful, false (default) for unsuccessful */
    
    if(eUsartPort  == USART_PORT_1)
    {
        if(sbUsartTxBusyP1 == false)
        {
            /* Flag Usart TX P1 as busy */
            sbUsartTxBusyP1 = true;
            
            /* Start transmitting USART data port 1 */
            dma_start_transfer_job(&sstUsartResourceTxP1);
            
            /* Usart was not busy, successfully started transfer */
            bUsartStartSuccess = true;
        }
        else
        {
            /* bUsartStartSuccess = false, USART Rx P1 was already busy */
        }
    }
    else
    {
        
        if(sbUsartTxBusyP2 == false)
        {
            /* Flag Usart TX P2 as busy */
            sbUsartTxBusyP2 = true;
            
            /* Start transmitting USART data port 2 */
            dma_start_transfer_job(&sstUsartResourceTxP2);
            
            /* Usart was not busy, successfully started transfer */
            bUsartStartSuccess = true;
        }
        else
        {
            /* bUsartStartSuccess = false, USART Rx P2 was already busy */
        }       
    }
    
    return bUsartStartSuccess;
}


/*********************************************************************
* PUBLIC bIsUsartRxPortBusy()
*---------------------------------------------------------------------
* Returns the status of the USART port, whether it is busy or not.
*
* Inputs:
*   eUsartPort - Selected USART Port
* Returns:
*   true  - USART port is busy, currently performing transfer.
*   false - USART port is available.
*
**********************************************************************/
bool bIsUsartRxPortBusy(eUsartPortsAvailable_t eUsartPort)
{
    bool bUsartStatus; /* Return variable, true busy, false available */
    
    /* Return the Status of the selected USART port */
    if(eUsartPort == USART_PORT_1)
    {
        bUsartStatus = sbUsartRxBusyP1;
    }
    else
    {
        bUsartStatus = sbUsartRxBusyP2;
    }
    
    return bUsartStatus;
}


/*********************************************************************
* PUBLIC bIsUsartTxPortBusy()
*---------------------------------------------------------------------
* Returns the status of the USART port, whether it is busy or not.
*
* Inputs:
*   eUsartPort - Selected USART Port
* Returns:
*   true  - USART port is busy, currently performing transfer.
*   false - USART port is available.
*
**********************************************************************/
bool bIsUsartTxPortBusy(eUsartPortsAvailable_t eUsartPort)
{
    bool bUsartStatus; /* Return variable, true busy, false available */
    
    /* Return the Status of the selected USART port */
    if(eUsartPort == USART_PORT_1)
    {
        bUsartStatus = sbUsartTxBusyP1;
    }
    else
    {
        bUsartStatus = sbUsartTxBusyP2;
    }
    
    return bUsartStatus;
}

/*********************************************************************
* PUBLIC uiUsartGetRxCount()
*---------------------------------------------------------------------
* Get the number of data bytes in the USART receive buffer
*
* Inputs:
*   eUsartPort - Selected USART Port   
* Returns:
*   Number of bytes in USB receive buffer   
*
**********************************************************************/
uint32_t uiUsartGetRxCount(eUsartPortsAvailable_t eUsartPort)
{
    uint32_t uiSize = 0;
    /*  */
    if(eUsartPort == USART_PORT_1)
    {
        uiSize = sstUsartResourceRxP1.transfered_size; 
    }
    else
    {
        uiSize = sstUsartResourceRxP2.transfered_size;  
    }
    
    return uiSize;
}

/*********************************************************************
* PUBLIC uiUsartGetTxFree()
*---------------------------------------------------------------------
* Get the number of data bytes in the USART transmit buffer
*
* Inputs:
*   eUsartPort - Selected USART Port  
* Returns:
*   Number of bytes in USB transmit buffer 
*
**********************************************************************/
uint32_t uiUsartGetTxFree(eUsartPortsAvailable_t eUsartPort)
{
    uint32_t uiSize = 0;
    /*  */
    if(eUsartPort == USART_PORT_1)
    {
        uiSize = sstUsartResourceTxP1.transfered_size;
    }
    else
    {
        uiSize = sstUsartResourceTxP2.transfered_size;
    }
    
    return uiSize;
}
/******************************** End of File ********************************/
