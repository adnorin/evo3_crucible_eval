/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: init_wdt.c                                                     */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Implements all the necessary initialization functions for the watch dog   */
/* timer on the Evolion 3 Battery System. Interface functions offer          */
/* the flexibility to enable, disable and reset the WDT counter.             */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include "iface_wdt.h"


/********************************************************************
 * PRIVATE variables                                                *
 ********************************************************************/
/* None */

/********************************************************************
* PRIVATE Functions                                                 *
*********************************************************************/
/********************************************************************
* PRIVATE bWdtConfigure()
*--------------------------------------------------------------------
* Configures WDT.
*
* Inputs: None
*   
* Returns:
*   true  - Successfully configured WDT
*   false - WDT configuration unsuccessful
*
*********************************************************************/
static bool sbConfigureWdt(void)
{
    struct wdt_conf stWdtConfig;          /* WDT Configuration Structure */ 
    uint16_t usiLoopTimeout = 0;          /* Loop timeout counter */
    bool bWdtConfigurationResult = false; /* Configuration result, true for successful, false (default) for unsuccessful */
    
    /* Get configuration default values then change needed parameters */
    wdt_get_config_defaults(&stWdtConfig);
    
    stWdtConfig.always_on      = false;
    stWdtConfig.clock_source   = GCLK_GENERATOR_2;
    stWdtConfig.timeout_period = WDT_PERIOD_2048CLK; /* 2 Seconds */

    /* Wait for WDT init to complete */
    if(wdt_set_config(&stWdtConfig) == STATUS_OK && usiLoopTimeout < LOOP_TIMEOUT)
    {
        /* Increment loop timeout, this is the maximum number of retries to initialize the WDT port before giving up */
        usiLoopTimeout++;
    }
    
    /* Set return value based on the SPI initialization result */
    if(usiLoopTimeout < LOOP_TIMEOUT)
    {
        bWdtConfigurationResult = true;         
    }   
        
    return bWdtConfigurationResult;
}


/********************************************************************
* PUBLIC Functions                                                  *
*********************************************************************/
/********************************************************************
* PUBLIC bWdtConfigure()
*--------------------------------------------------------------------
* Calls the private WDT configuration function.
*
* Inputs: None
*
* Returns:
*   true  - Successfully configured WDT
*   false - WDT configuration unsuccessful
*
*********************************************************************/
bool bWdtConfigure(void)
{
     /* Configure the WDT  */
     return(sbConfigureWdt());
}
 
/********************************************************************
* PUBLIC bWdtEnable()
*--------------------------------------------------------------------
* Enables WDT.
*
* Inputs: None
*
* Returns:
*   true  - WDT enabled 
*   false - WDT sync in process, not yet enabled.
*
*********************************************************************/
bool bWdtEnable(void)
{
    Wdt *const stWdtModule = WDT;   

    /* Enable the Watchdog module */
    stWdtModule->CTRL.reg |= WDT_CTRL_ENABLE;            
            
    return (!wdt_is_syncing());
}


/********************************************************************
* PUBLIC bWdtDisable()
*--------------------------------------------------------------------
* Disables WDT.
*
* Inputs: None
*
* Returns:
*   true  - WDT disabled 
*   false - WDT sync in process, not yet disabled.
*
*********************************************************************/
bool bWdtDisable(void)
{
    Wdt *const stWdtModule = WDT;

    /* Disable the Watchdog module */
    stWdtModule->CTRL.reg &= ~WDT_CTRL_ENABLE;
        
    return (!wdt_is_syncing());
}


/********************************************************************
* PUBLIC bWdtReset()
*--------------------------------------------------------------------
* Resets WDT, this must be called more frequently than the WDT period
* to prevent the WDT from reseting the processor.
*
* Inputs: None
*
* Returns:
*   true  - WDT reset completed
*   false - WDT sync in process, not yet reset.
*
*********************************************************************/
bool bWdtReset(void)
{
    Wdt *const stWdtModule = WDT;

    /* Clear Watchdog count */
    stWdtModule->CLEAR.reg = WDT_CLEAR_CLEAR_KEY;

    return (!wdt_is_syncing());
}


/********************************************************************
* PUBLIC bWdtSyncComplete()
*--------------------------------------------------------------------
* Returns the status of the WDT sync process. 
*
* Inputs: None
*
* Returns:
*   true  - WDT sync completed
*   false - WDT sync still in process
*
*********************************************************************/
bool bWdtSyncComplete(void)
{
    return (!wdt_is_syncing());
}


/********************************************************************
* PUBLIC vCpuResetViaWdt()
*--------------------------------------------------------------------
* This function causes an immediate CPU reset, the reset cause will
* be registered as a WDT reset.
* 
* Inputs: None
*
* Returns: None
*
*********************************************************************/
void vCpuResetViaWdt(void)
{
    Wdt *const WDT_module = WDT;

    /* Writing a value other than the clear key will cause immediate reset */
    WDT_module->CLEAR.reg = 0xFF;   
}

/******************************** End of File ********************************/