/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of Saft America, Inc. and can only be        */
/* used and copied with the prior written authorization of Saft.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: Evolion 3 Software                                               */
/*****************************************************************************/
/* FILE NAME: init_gpio.c                                                    */
/*****************************************************************************/
/* AUTHOR : Greg Cordell                                                     */
/* LAST MODIFIER: Adnorin L. Mendez                                          */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* GPIO pin initialization                                                   */
/*                                                                           */
/*****************************************************************************/
#include <asf.h>
#include <conf_gpio.h>
#include <iface_gpio.h>

/*********************************************************************
* PUBLIC vConfigureGPIO()
*---------------------------------------------------------------------
* Initializes GPIO input and output pins
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vConfigureGPIO(void)
{
    struct port_config config_port_pin;

    port_get_config_defaults(&config_port_pin);

    /* Configure inputs with pull-ups */
    config_port_pin.direction=  PORT_PIN_DIR_INPUT;
    config_port_pin.input_pull= PORT_PIN_PULL_UP;

    port_pin_set_config(OVER_CURRENT_192A_NOTIFICATION_PIN, &config_port_pin);
    port_pin_set_config(NETWORK_WAKEUP_NOTIFICATION_PIN, &config_port_pin);
    port_pin_set_config(OVER_TEMPERATURE_NOTIFICATION_PIN, &config_port_pin);
    port_pin_set_config(HMI_SOC_PIN, &config_port_pin);
    port_pin_set_config(HMI_POWER_ON_PIN, &config_port_pin);


    /* Configure inputs with pull-downs */
    config_port_pin.input_pull= PORT_PIN_PULL_DOWN;

    port_pin_set_config(OVER_CURRENT_400A_NOTIFICATION_PIN, &config_port_pin);
    port_pin_set_config(BMS_IC_FAULT_NOTIFICATION_PIN, &config_port_pin);
    port_pin_set_config(ANTI_THEFT_OPTION_IN_PIN, &config_port_pin);
    port_pin_set_config(REDUNDANT_UV_NOTIFICATION_PIN, &config_port_pin);
    port_pin_set_config(REDUNDANT_OV_NOTIFICATION_PIN, &config_port_pin);


    /* Configure inputs with external pull-ups or pull-downs */
    config_port_pin.input_pull= PORT_PIN_PULL_NONE;

    port_pin_set_config(USB_3V3_PRESENT_NOTIFICATION_PIN, &config_port_pin);

    /* Configure outputs */
    config_port_pin.direction=  PORT_PIN_DIR_OUTPUT;

    port_pin_set_config(MC33771_RESET_PIN, &config_port_pin);
    port_pin_set_config(RS485_EXT_TX_ENABLE_RX_DISABLE_PIN, &config_port_pin);
    port_pin_set_config(RS485_INT_TX_ENABLE_RX_DISABLE_PIN, &config_port_pin);

    port_pin_set_config(NETWORK_MEASUREMENT_ENABLE_PIN, &config_port_pin);

    port_pin_set_config(SLAVE_SELECT_PIN_P1, &config_port_pin);

    port_pin_set_config(DRY_CONTACT_RJ45_PIN, &config_port_pin);
    port_pin_set_config(DRY_CONTACT_TB_PIN, &config_port_pin);

    port_pin_set_config(FULL_CHARGE_FET_ENABLE_PIN, &config_port_pin);
    port_pin_set_config(FULL_DISCHARGE_FET_ENABLE_PIN, &config_port_pin);
    port_pin_set_config(REG_CHARGE_FET_ENABLE_PIN, &config_port_pin);
    port_pin_set_config(REG_DISCHARGE_FET_ENABLE_PIN, &config_port_pin);

    port_pin_set_config(SLEEP_PWDN_12V_COMP_PIN, &config_port_pin);

    port_pin_set_config(OVER_CURRENT_LATCH_RESET_PIN, &config_port_pin);

    port_pin_set_config(REGULATED_CURRENT_LVL_1_PIN, &config_port_pin);
    port_pin_set_config(REGULATED_CURRENT_LVL_2_PIN, &config_port_pin);
    port_pin_set_config(REGULATED_CURRENT_LVL_3_PIN, &config_port_pin);
    port_pin_set_config(REGULATED_CURRENT_OVER_TEMP_LATCH_CLEAR_PIN, &config_port_pin);

    port_pin_set_config(SLAVE_SELECT_PIN_P2, &config_port_pin);
    port_pin_set_config(SAFETY_CHARGE_FET_ENABLE_PIN, &config_port_pin);
    port_pin_set_config(HEATER_ENABLE_PIN, &config_port_pin);

    port_pin_set_config(ANTI_THEFT_OPTION_OUT_PIN, &config_port_pin);

    port_pin_set_config(FET_SELF_TEST_PIN, &config_port_pin);
    port_pin_set_config(FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_PIN, &config_port_pin);
    port_pin_set_config(HMI_LED_DRIVER_ENA_PIN, &config_port_pin);

    /* Default State for Output Pins */
    OUTPUT_SET(MC33771_RESET_PIN,MC33771_RESET_INACTIVE);
    OUTPUT_SET(RS485_EXT_TX_ENABLE_RX_DISABLE_PIN,RS485_EXT_TX_ENABLE_RX_DISABLE_INACTIVE);
    OUTPUT_SET(RS485_INT_TX_ENABLE_RX_DISABLE_PIN,RS485_INT_TX_ENABLE_RX_DISABLE_INACTIVE);

    OUTPUT_SET(NETWORK_MEASUREMENT_ENABLE_PIN,NETWORK_MEASUREMENT_ENABLE_ACTIVE);

    OUTPUT_SET(SLAVE_SELECT_PIN_P1,SLAVE_SELECT_PIN_P1_ACTIVE);

    OUTPUT_SET(DRY_CONTACT_RJ45_PIN,DRY_CONTACT_RJ45_CLOSED);
    OUTPUT_SET(DRY_CONTACT_TB_PIN,DRY_CONTACT_TB_OPEN);

    OUTPUT_SET(FULL_CHARGE_FET_ENABLE_PIN,FULL_CHARGE_FET_ENABLE_INACTIVE);
    OUTPUT_SET(FULL_DISCHARGE_FET_ENABLE_PIN,FULL_DISCHARGE_FET_ENABLE_INACTIVE);
    OUTPUT_SET(REG_CHARGE_FET_ENABLE_PIN,REG_CHARGE_FET_ENABLE_INACTIVE);
    OUTPUT_SET(REG_DISCHARGE_FET_ENABLE_PIN,REG_DISCHARGE_FET_ENABLE_INACTIVE);

    OUTPUT_SET(SLEEP_PWDN_12V_COMP_PIN,SLEEP_PWDN_12V_COMP_INACTIVE);

    OUTPUT_SET(OVER_CURRENT_LATCH_RESET_PIN,OVER_CURRENT_LATCH_RESET_INACTIVE);

    OUTPUT_SET(REGULATED_CURRENT_LVL_1_PIN,REGULATED_CURRENT_LVL_1_INACTIVE);
    OUTPUT_SET(REGULATED_CURRENT_LVL_2_PIN,REGULATED_CURRENT_LVL_2_INACTIVE);
    OUTPUT_SET(REGULATED_CURRENT_LVL_3_PIN,REGULATED_CURRENT_LVL_3_INACTIVE);
    OUTPUT_SET(REGULATED_CURRENT_OVER_TEMP_LATCH_CLEAR_PIN,REGULATED_CURRENT_OVER_TEMP_LATCH_CLEAR_INACTIVE);

    OUTPUT_SET(SLAVE_SELECT_PIN_P2,SLAVE_SELECT_PIN_P2_ACTIVE);
    OUTPUT_SET(SAFETY_CHARGE_FET_ENABLE_PIN,SAFETY_CHARGE_FET_ENABLE_INACTIVE);
    OUTPUT_SET(HEATER_ENABLE_PIN,HEATER_ENABLE_INACTIVE);

    OUTPUT_SET(ANTI_THEFT_OPTION_OUT_PIN,ANTI_THEFT_OPTION_OUT_INACTIVE);

    OUTPUT_SET(FET_SELF_TEST_PIN,FET_SELF_TEST_INACTIVE);
    OUTPUT_SET(FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_PIN,FET_SELF_TEST_VOLTAGE_MEASUREMENT_ENABLE_ACTIVE);
    OUTPUT_SET(HMI_LED_DRIVER_ENA_PIN,HMI_LED_DRIVER_ENA_ACTIVE);
}

/*********************************************************************
* PUBLIC vResetOverCurrentLatch()
*---------------------------------------------------------------------
* Resets over-current latch as it starts up in an undefined state
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vResetOverCurrentLatch(void)
{
    /* Activate over-current latch reset */
    OUTPUT_SET(OVER_CURRENT_LATCH_RESET_PIN,OVER_CURRENT_LATCH_RESET_ACTIVE);
    /* Wait a little bit for the GPIO to change state (15ns) and the signal to propagate through the latch (7.5ns) with some allowance for interrupts to take place */
    delay_us(1);
    /* Deactivate over-current latch reset */
    OUTPUT_SET(OVER_CURRENT_LATCH_RESET_PIN,OVER_CURRENT_LATCH_RESET_INACTIVE);
}


/*********************************************************************
* PUBLIC vResetBmsIc()
*---------------------------------------------------------------------
* Resets BMS IC to make sure the configuration starts in its defaults
*
* Inputs: None
* Returns: None
*
**********************************************************************/
void vResetBmsIc(void)
{
    /* Activate BMS IC reset line */
    OUTPUT_SET(MC33771_RESET_PIN,MC33771_RESET_ACTIVE);
    /* Must wait at least 100us, 100us is de-glitch filter in NXP part */
    delay_us(200);
    /* Deactivate BMS IC reset line */
    OUTPUT_SET(MC33771_RESET_PIN,MC33771_RESET_INACTIVE);
    /* Must wait at least 650ms for NXP part to initialize */
    delay_s(1);
}
