#ifndef INT_SPI_DMA_H_
#define INT_SPI_DMA_H_
/*****************************************************************************/
/* (C) Copyright 2017 by SAFT                                                */
/* All rights reserved                                                       */
/*                                                                           */
/* This program is the property of SAFT BORDEAUX FRANCE and can only be      */
/* used and copied with the prior written authorization of SAFT.             */
/*                                                                           */
/* Any whole or partial copy of this program in either its original form or  */
/* in a modified form must mention this copyright and its owner.             */
/*****************************************************************************/
/* PROJECT: R'Evolion Software                                               */
/*****************************************************************************/
/* FILE NAME: int_spi_dma.h                                                  */
/*****************************************************************************/
/* AUTHOR : Adnorin L. Mendez                                                */
/* LAST MODIFIER: None                                                       */
/*****************************************************************************/
/* FILE DESCRIPTION:                                                         */
/*                                                                           */
/* Header file for init_spi_dma.c, which provides prototypes for the         */
/* public interface functions. Also includes types and enumeration           */
/* definitions.                                                              */
/*                                                                           */ 
/*****************************************************************************/

#endif /* INT_SPI_DMA_H_ */